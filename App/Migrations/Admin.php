<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Admin{

	public $connection = 'default';
 
	function __construct() {
		$this->create_Admin_Table(); 
	}

	function create_Admin_Table() {

		$schema = Capsule::schema(); 

		if ($schema->hasTable(ADMIN_TABLE)){
			$schema->table(ADMIN_TABLE, function(Blueprint $table) use ($schema) {				
			    if($schema->hasColumn(ADMIN_TABLE, LEVEL)) return;
				$table->integer(LEVEL, false, true)->default(1);
				echo 'Admin Table updated <br />';
			});
			echo 'Admin Table already exists';
			return;
		}

		$schema->create(ADMIN_TABLE, function(Blueprint $table) {
				
			$table->increments(TABLE_ID);
			$table->string(USERNAME, 40)->unique();
			$table->string(PASSWORD, 72);
			$table->string(EMAIL, 50);
			$table->integer(LEVEL, false, true)->default(1);
			$table->string(REMEMBER_LOGIN)->nullable(true);
			$table->integer(FAILED_LOGIN_COUNT, false, true)->default(0);
			$table->integer(FIRST_FAILED_LOGIN, false, true)->default(0);
			
// 			$table->timestamps();
			$table->timestamp(CREATED_AT)->nullable(true);
			$table->timestamp(UPDATED_AT)->nullable(true);
			$table->engine = 'InnoDB';
 
			echo 'Admin table created';
			
			
		});
		
		\App\Migrations\Admin::seedAdminTable();
		
	}
	
	static function seedAdminTable() {
	    $obj = new \App\Models\Admin();
		$obj[USERNAME] = 'admin';
		$obj[PASSWORD] = cryptPassword('admin');
		$obj[EMAIL] = 'admin@email.com';
		$obj->save();
		
		echo '<br />Admin table seeded';
	}

}