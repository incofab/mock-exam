<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class QuestionAttempts{

	public $connection = 'default';

	function __construct() {
		$this->create_Table();
	}

	function create_Table() 
	{
		$schema = Capsule::schema();
 
		if ($schema->hasTable(QUESTION_ATTEMPTS_TABLE))
		{
			echo 'Question Attempts already exists';
			
			return;
		}

		$schema->create(QUESTION_ATTEMPTS_TABLE, function(Blueprint $table) 
		{
		    $table->increments(TABLE_ID);
		    $table->integer(EXAM_SUBJECT_ID, false, true);
		    $table->integer(QUESTION_ID, false, true);
		    $table->string(ATTEMPT, 2);
		    $table->string(ANSWER, 2);
		    
		    // 		    $table->timestamps();
		    $table->timestamp(CREATED_AT)->nullable(true);
		    $table->timestamp(UPDATED_AT)->nullable(true);
		    $table->engine = 'InnoDB';
		    
		    $table->foreign(EXAM_SUBJECT_ID)->references(TABLE_ID)->on(EXAM_SUBJECTS_TABLE)
                    ->onDelete('cascade')->onUpdate('cascade');
		    
	        $table->foreign(QUESTION_ID)->references(TABLE_ID)->on(QUESTIONS_TABLE)
                    ->onDelete('cascade')->onUpdate('cascade');
	        		    
			echo 'Question Attempts table created';
		});


	}

}