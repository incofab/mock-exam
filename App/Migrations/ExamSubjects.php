<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class ExamSubjects{

	public $connection = 'default';

	function __construct() {
		$this->create_Table();
	}

	function create_Table() 
	{
		$schema = Capsule::schema();
 
		if ($schema->hasTable(EXAM_SUBJECTS_TABLE))
		{
		    $schema->table(EXAM_SUBJECTS_TABLE, function(Blueprint $table) use ($schema) {
		        
		        if($schema->hasColumn(EXAM_SUBJECTS_TABLE, SCORE)) return;
		        
		        $table->integer(SCORE, false, true)->nullable(true);
		        
		        $table->integer(NUM_OF_QUESTIONS, false, true)->nullable(true);
		        
		        echo 'Exam Subjects Table updated <br />';
		    });
		    
			echo 'Exam Subjects already exists';
			
			return;
		}

		$schema->create(EXAM_SUBJECTS_TABLE, function(Blueprint $table) 
		{
		    $table->increments(TABLE_ID);
		    $table->string(EXAM_NO, 15);
		    $table->integer(COURSE_ID, false, true); 
		    $table->integer(COURSE_SESSION_ID, false, true);
		    $table->integer(SCORE, false, true)->nullable(true);
		    $table->integer(NUM_OF_QUESTIONS, false, true)->nullable(true);
		    $table->string(STATUS)->default(STATUS_ACTIVE);
		    
		    // 		    $table->timestamps();
		    $table->timestamp(CREATED_AT)->nullable(true);
		    $table->timestamp(UPDATED_AT)->nullable(true);
		    $table->engine = 'InnoDB';
		    
		    $table->foreign(EXAM_NO)->references(EXAM_NO)->on(EXAMS_TABLE)
		          ->onDelete('cascade')->onUpdate('cascade');
		    
	        $table->foreign(COURSE_ID)->references(TABLE_ID)->on(COURSES_TABLE)
	               ->onDelete('cascade')->onUpdate('cascade');
		    
           $table->foreign(COURSE_SESSION_ID)->references(TABLE_ID)->on(SESSIONS_TABLE)
	               ->onDelete('cascade')->onUpdate('cascade');
		    
			echo 'Exam Subjects table created';
		});


	}

}