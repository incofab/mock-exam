<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class ExamCenters{

	public $connection = 'default';

	function __construct() {
		$this->create_Table();
	}

	function create_Table() 
	{
		$schema = Capsule::schema();
 
		if ($schema->hasTable(EXAM_CENTERS_TABLE))
		{
			echo 'Exam Centers already exists';
			
			return;
		}

		$schema->create(EXAM_CENTERS_TABLE, function(Blueprint $table) 
		{
		    $table->increments(TABLE_ID);
		    $table->string(USERNAME, 40)->unique();
		    $table->string(PASSWORD, 72);
		    $table->string(ADDED_BY, 40);
		    $table->string(CENTER_CODE, 4)->unique();
		    $table->string(CENTER_NAME, 255);
		    $table->string(ADDRESS)->nullable(true);
		    $table->string(PHONE_NO, 15)->nullable(true);
		    $table->string(EMAIL, 150)->nullable(true);
		    $table->string(STATUS)->default(STATUS_ACTIVE);
		    
		    $table->string(REMEMBER_LOGIN)->nullable(true);
		    $table->integer(FAILED_LOGIN_COUNT, false, true)->default(0);
		    $table->integer(FIRST_FAILED_LOGIN, false, true)->default(0);
		    
		    // 		    $table->timestamps();
		    $table->timestamp(CREATED_AT)->nullable(true);
		    $table->timestamp(UPDATED_AT)->nullable(true);
		    $table->engine = 'InnoDB';
		    
		    $table->foreign(ADDED_BY)->references(USERNAME)->on(ADMIN_TABLE)
		    ->onDelete('cascade')->onUpdate('cascade');
		    
			echo 'Exam Centers table created';
		});


	}

}