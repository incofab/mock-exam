<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class CenterSettings
{
	public $connection = 'default';

	function __construct() 
	{
		$this->create_Table();
	}

	function create_Table() 
	{
		$schema = Capsule::schema();
 
		if ($schema->hasTable(CENTER_SETTINGS_TABLE))
		{
// 		    $schema->table(CENTER_SETTINGS_TABLE, function(Blueprint $table) use ($schema) {
// 		        if(!$schema->hasColumn(CENTER_SETTINGS_TABLE, NUM_OF_ACTIVATIONS))
// 		        {
//     		        $table->integer(NUM_OF_ACTIVATIONS, false, true)->default(0);
//     		        echo 'Events Table updated <br />';
// 		        }
// 		    });
			echo 'Center Settings already exists';
			
			return;
		}

		$schema->create(CENTER_SETTINGS_TABLE, function(Blueprint $table) 
		{
		    $table->increments(TABLE_ID);
		    $table->string(CENTER_CODE, 4); 
		    $table->string(KEY); 
		    $table->text(VALUE)->nullable(true); 
		    $table->text(DESCRIPTION)->nullable(true);
		    
		    $table->timestamp(CREATED_AT)->nullable(true);
		    $table->timestamp(UPDATED_AT)->nullable(true);
		    $table->engine = 'InnoDB';
		    
		    $table->foreign(CENTER_CODE)->references(CENTER_CODE)->on(EXAM_CENTERS_TABLE)
		      ->onUpdate('cascade')->onDelete('cascade');
		    
			echo 'Center Settings table created';
		});

	}

}