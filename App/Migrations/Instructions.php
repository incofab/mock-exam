<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Instructions{

	public $connection = 'default';

	function __construct() {
		$this->create__Table(); 
	}

	function create__Table() {

		$schema = Capsule::schema($this->connection);

		if ($schema->hasTable(INSTRUCTIONS_TABLE)){
			echo 'Instructions Table already exists';
			return;
		}

		$schema->create(INSTRUCTIONS_TABLE, function(Blueprint $table) {

			$table->increments(TABLE_ID);
			$table->integer(COURSE_ID, false, true);
			$table->integer(COURSE_SESSION_ID, false, true);
			
			$table->text(INSTRUCTION);
			$table->integer(FROM_, false, true);
			$table->integer(TO_, false, true);				

			// 			$table->timestamps();
			$table->timestamp(CREATED_AT)->nullable(true);
			$table->timestamp(UPDATED_AT)->nullable(true);
			$table->engine = 'InnoDB';

			$table->foreign(COURSE_ID)->references(TABLE_ID)->on(COURSES_TABLE)
				->onDelete('cascade')->onUpdate('cascade');
			$table->foreign(COURSE_SESSION_ID)->references(TABLE_ID)->on(SESSIONS_TABLE)
				->onDelete('cascade')->onUpdate('cascade');

			echo 'Instructions table created';

		});


	}

}