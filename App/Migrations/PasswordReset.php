<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class PasswordReset{

	public $connection = 'default';

	function __construct() {
		$this->create_Table();
	}

	function create_Table() { 

		$schema = Capsule::schema();

		if ($schema->hasTable(PASSWORD_RESET_TABLE)){
			echo 'Password Reset Table already exists';
			return;
		}

		$schema->create(PASSWORD_RESET_TABLE, function(Blueprint $table) {

			$table->increments(TABLE_ID);
			$table->string(PHONE_NO, 15)->unique();
			$table->string(PASSWORD_RESET_CODE, 10)->unique();
			$table->integer(NUM_OF_ATTEMPTS, false, true)->default(1);
			$table->integer(EXPIRY_TIME, false, true)->default(1);
			$table->boolean(IS_STILL_VALID)->default(true);

			// 			$table->timestamps();
			$table->timestamp(CREATED_AT)->nullable(true);
			$table->timestamp(UPDATED_AT)->nullable(true);
			$table->engine = 'InnoDB';

			$table->foreign(PHONE_NO)->references(PHONE_NO)->on(USERS_TABLE)
					->onDelete('cascade')->onUpdate('cascade');
				
			echo 'Password Reset table created';

		});


	}

}