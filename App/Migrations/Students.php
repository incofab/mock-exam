<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Students{

	public $connection = 'default';
  
	function __construct() {
		$this->create_Users_Table();
	}

	function create_Users_Table() {

		$schema = Capsule::schema();
 
		if ($schema->hasTable(STUDENTS_TABLE))
		{
			echo 'Students Table already exists';
			return;
		}

		$schema->create(STUDENTS_TABLE, function(Blueprint $table) {
				
			$table->increments(TABLE_ID);
			$table->string(STUDENT_ID, 15)->unique();
			$table->string(FULLNAME, 150)->nullable(true);
			$table->string(FIRSTNAME, 70)->nullable(true);
			$table->string(LASTNAME, 70)->nullable(true);
			$table->string(OTHERNAMES, 100)->nullable(true);
			$table->string(CENTER_CODE, 4);
			$table->float(BALANCE)->default(0);
			$table->float(BONUS_BALANCE)->default(0);
			$table->integer(POINTS, false, true)->default(0);
			$table->string(EMAIL, 50)->nullable(true);
			$table->text(PROFILE_PIC)->nullable(true);
			$table->string(PHONE_NO, 15)->nullable(true);
			$table->string(ADDRESS)->nullable(true);
			$table->string(STATUS, 15)->default(STATUS_ACTIVE);
			$table->string(GENDER, 15)->nullable(true);
			$table->date(DOB)->nullable(true);
			$table->string(REMEMBER_LOGIN)->nullable(true);
			$table->integer(FAILED_LOGIN_COUNT, false, true)->default(0);
			$table->integer(FIRST_FAILED_LOGIN, false, true)->default(0);
			
				
			// 			$table->timestamps();
			$table->timestamp(CREATED_AT)->nullable(true);
			$table->timestamp(UPDATED_AT)->nullable(true);
			$table->engine = 'InnoDB';
			
			$table->foreign(CENTER_CODE)->references(CENTER_CODE)->on(EXAM_CENTERS_TABLE)
			     ->onDelete('cascade')->onUpdate('cascade');
			
			echo 'Students table created';
			
		});


	}

}