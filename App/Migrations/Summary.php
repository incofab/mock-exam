<?php
namespace App\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Summary{
    
    public $connection = 'default';
    
    function __construct() {
        $this->create_summary_Table();
    }
    
    function create_summary_Table() {
        
        $schema = Capsule::schema($this->connection);
        
        if ($schema->hasTable(SUMMARY_TABLE)){
            echo 'Summary Table already exists';
            return;
        }
        
        $schema->create(SUMMARY_TABLE, function(Blueprint $table) {
            
            $table->increments(TABLE_ID);
            $table->integer(COURSE_ID, false, true);
            
            $table->string(CHAPTER_NO);
            $table->text(TITLE)->nullable(true);
            $table->text(DESCRIPTION)->nullable(true);
            
            $table->text(SUMMARY)->nullable(true);
            
            // 			$table->timestamps();
            $table->timestamp(CREATED_AT)->nullable(true);
            $table->timestamp(UPDATED_AT)->nullable(true);
            $table->engine = 'InnoDB';
            
            $table->foreign(COURSE_ID)->references(TABLE_ID)->on(COURSES_TABLE)
            ->onDelete('cascade')->onUpdate('cascade');
            
            
            echo 'Summary table created';
            
        });
            
            
    }
    
}