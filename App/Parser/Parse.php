<?php
namespace App\Parser;

class Parse
{
    const startFrom2000 = TRUE;
 
    const INPUT_BASE_DIR = 'C:/Users/USER PC/Desktop/Project Files/Jamb - HTML/';
//     'D:/Users/user/Desktop/players/Exam Driller/Subjects/';// biology/';
    
    const IMG_OUTPUT_BASE_DIR = 'C:/wamp64/www/mock/public/img/jamb/'; 
    
    private $jsonFilesDir = APP_DIR . '../public/output/jamb/';
    
    function __construct() 
    {
        
    }
    
    private $imgID = 0;
    
    private $subject = '';
    
    private $year = '';
    private $htmExt = '.min.htm';
    
    /**
     * Parse the HTML format of JAMB questions and save them in DB.
     * Copy their images to the image folder self::IMG_OUTPUT_BASE_DIR
     */
    function parse() 
    {
//         dDie((new \App\Parser\FileCript(new \App\Parser\MCrypt(), $this->jsonFilesDir)));
//         die('Dont run yet');
//         dDie((new \App\Parser\OutputJson())->generateJSON());
//         dDie($this->parseOptions(self::INPUT_BASE_DIR.'Geography/Geography_2001_Option.htm'));
        ini_set('max_execution_time', 1200);  
        
        foreach (self::COURSE_TITLES as $subject => $courseTitle)
        {
            $subjectDir = self::INPUT_BASE_DIR . $subject;
            
            $this->subject = $subject;
            
            $subjectModel = new \App\Models\Courses();
            
            $subjectModel->insert([
                
                COURSE_CODE => $this->subject,
                
                COURSE_TITLE => array_get(self::COURSE_TITLES, $this->subject, $this->subject),
                
                DESCRIPTION => '',
            ]);
            
            if(empty($subjectModel[COURSE_CODE]))
            {
                echo 'subjectModel empty, Probable cause: '.$subject.' already exists <br /><br />';
                
                continue;
            }
            
            $files = scandir($subjectDir);
            
            foreach ($files as $file)
            {
                if(substr($file, -10) != 'Answer.txt') continue;
                
                $prefix = substr($file, 0, -10);
                
                $answersfileName   = $subjectDir.'/'.$prefix.'Answer.txt';
                $optionsfileName   = $subjectDir.'/'.$prefix.'Option'.$this->htmExt;
                $questionsfileName = $subjectDir.'/'.$prefix.'Question'.$this->htmExt;
                $explanationsFilename = $subjectDir.'/'.$prefix.'Explanations'.$this->htmExt;
                // Retrieve explanations filename here
                
                $this->processSubjectYear($questionsfileName, $optionsfileName, $answersfileName, $explanationsFilename);
                
                $summaryChaptersFilename = $subjectDir.'/'.$prefix.'Summary_Chapters'.$this->htmExt;
                $summaryContentFilename = $subjectDir.'/'.$prefix.'Summary_Content'.$this->htmExt;

                if(file_exists($summaryChaptersFilename) && file_exists($summaryChaptersFilename))
                {
                    $this->processSummary($summaryChaptersFilename, $summaryContentFilename);
                }
            }
            
            echo $subject.' Done <br /><br />';
        }
        
        die('Operation Complete');
    }
    
    private function processSubjectYear($questionsfileName, $optionsfileName, $answersfileName, $explanationsFilename) 
    {
        $sujectDetail = explode('_', pathinfo($questionsfileName, PATHINFO_FILENAME));
        
        $this->year = $sujectDetail[1];
        
        $this->imgID = 0;
        
        $answers = $this->parseAnswers($answersfileName);
        
        $options = $this->parseOptions($optionsfileName);
        
        $questions = $this->parseQuestion($questionsfileName);
        
        $explanations = $this->parseQuestion($explanationsFilename);
        
        $session = new \App\Models\Sessions();
        
        $session = $session->insert([
            COURSE_CODE => $this->subject,
            SESSION => $this->year,
            GENERAL_INSTRUCTIONS => 'Answer all questions',
        ]);
        
        if(empty($session[SESSION])) 
        {
            dDie("Failure on Acad Session - {$this->subject} of {$this->year}");
        }
                        
        foreach ($questions as $questionNo => $question) 
        {
            $questionsModel = new \App\Models\Questions();
            
            if(empty($answers[$questionNo])) continue;

            if(empty($options[$questionNo])) 
            {
                dlog("questionNo = $questionNo");
                
                dDie("Failure on options[questionNo] - {$this->subject} of {$this->year}");
            }
            
            $thisOption = $options[$questionNo];
            
            $questionsArr = [
                COURSE_CODE => $this->subject,
                SESSION_ID  => $session[TABLE_ID],
                QUESTION_NO => $questionNo,
                QUESTION => empty($question['content']) ? "Null" : $question['content'],
                OPTION_A => $thisOption['A']['option-content'],
                OPTION_B => $thisOption['B']['option-content'],
                OPTION_C => isset($thisOption['C']) ? $thisOption['C']['option-content'] : null,
                OPTION_D => isset($thisOption['D']) ? $thisOption['D']['option-content'] : null,
                OPTION_E => isset($thisOption['E']) ? $thisOption['E']['option-content'] : null,
                ANSWER   => $answers[$questionNo]['answer'],
                ANSWER_META => isset($explanations[$questionNo]) ? $explanations[$questionNo]['content'] : null,
            ];
            
            $questionRet = $questionsModel->insert($questionsArr);
            
            if(empty($questionRet[SUCCESSFUL]))
            {
                dlog(array_merge($questionsArr, $questionRet));
                
                dDie($questionRet);
            }
        }
    }
    
    private function processSummary($summaryChaptersFilename, $summaryContentFilename)
    {        
        $summaryChapters = $this->parseQuestion($summaryChaptersFilename);
        
        $summaryContent = $this->parseQuestion($summaryContentFilename);
        
        if(count($summaryChapters) !== count($summaryContent))
        {
            dDie("No of chapter in the summary must match the content");
        }
        
        foreach ($summaryChapters as $chapterNo => $chapterTitle)
        {
            $summaryModel = new \App\Models\Summary();
            
            if(empty($summaryContent[$chapterNo]) || empty($summaryContent[$chapterNo]['content'])
                || empty($chapterTitle['content']))
            {
                dlog("Chapter $chapterNo");
                
                dDie("Failure on summaryContent[questionNo] - {$this->subject} of {$this->year}");
            }
            
            $summaryArr = [
                COURSE_CODE => $this->subject,
                CHAPTER_NO  => $chapterNo,
                TITLE => $chapterTitle['content'],
                DESCRIPTION => ' ',
                SUMMARY => $summaryContent[$chapterNo]['content']
            ];
            
            $summaryRet = $summaryModel->insert($summaryArr);
            
            if(!$summaryRet)
            {
                dlog($summaryArr);
                
                dDie("Summary failed to Insert");
            }
        }
    }
    
    private function parseAnswers($fileName)
    {
        $answers = file_get_contents($fileName);
        
        $answersArr = explode(PHP_EOL, $answers);
        
        $arr = [];
        
        foreach ($answersArr as $answer) 
        {
            if (empty($answer)) continue;
            
            $ansArr = explode('.', $answer);
            
            $questionNo = $ansArr[0];
            
            $ans = $ansArr[1];
            
            $arr[$questionNo] = [
                'answer' => $ans,
            ];
        }
        
        return $arr;
    }
    
    function parseOptions($fileName)
    {
        $dom = new \DOMDocument();
        
//         $dom->loadHTMLFile($fileName);
        $htmlContent = file_get_contents($fileName);
        $htmlContent = str_ireplace(['<p ', '<p>', '</p>'], ['<div ', '<div>', '</div>'], $htmlContent);
        
        $dom->loadHTML($htmlContent);
        
        $this->handleImgs($dom, $fileName);
        
        /** @var \DOMNodeList $paragraphs */
        $paragraphs = $dom->getElementsByTagName('div');
        
        $arr = [];
        
        $content = '';
        
        $currentQuestionNoAndOption = null;
        
        $nextQuestionNoAndOption = null;
        
        /** @var \DOMNode $p */
        foreach ($paragraphs as $p)
        {
            $text = $p->nodeValue;
            
            $questionNoAndOption = $this->getQuestionNo($text); 
            
            if(!$questionNoAndOption)
            {
                $content .= $dom->saveHtml($p);
                
                continue;
            }
            
            $nextQuestionNoAndOption = $questionNoAndOption['question_no'];
            
            if(!$currentQuestionNoAndOption) 
            {
                $currentQuestionNoAndOption = $nextQuestionNoAndOption;
                
                $content = '';
                
                continue;
            }
            
            $questionNo = substr($currentQuestionNoAndOption, 0, -1);
            
            $optionLetter = substr($currentQuestionNoAndOption, -1);
            
            $arr[$questionNo][$optionLetter] = [
                'option-content' => $content,
            ];
            
            $currentQuestionNoAndOption = $nextQuestionNoAndOption;
            
            $content = $questionNoAndOption['text'];
        }
        
        $questionNo = substr($currentQuestionNoAndOption, 0, -1);
        
        $optionLetter = substr($currentQuestionNoAndOption, -1);
        
        $arr[$questionNo][$optionLetter] = [
            'option-content' => $content,
        ];
        
        return $arr;
    }
    
    function parseQuestion($fileName)
    {
        $arr = [];
        
        if(!file_exists($fileName)) return $arr;
        
        $dom = new \DOMDocument();
        
//         $dom->loadHTMLFile($fileName);
        $htmlContent = file_get_contents($fileName);
        $htmlContent = str_ireplace(['<p ', '<p>', '</p>'], ['<div ', '<div>', '</div>'], $htmlContent);

        $dom->loadHTML($htmlContent);
        
        $this->handleImgs($dom, $fileName);
        
        /** @var \DOMNodeList $paragraphs */
        $paragraphs = $dom->getElementsByTagName('div');
        
        $content = '';
        
        $currentQuestionNo = null;
        
        $nextQuestionNo = null;
        
        /** @var \DOMNode $p */
        foreach ($paragraphs as $p)
        {
            $text = $p->nodeValue;
            
            $questionNo = $this->getQuestionNo($text); 
            
            if(!$questionNo) 
            {
                $content .= $dom->saveHtml($p);

                continue;
            }
            
            $nextQuestionNo = $questionNo['question_no'];
            
            if(!$currentQuestionNo)
            {
                $currentQuestionNo = $nextQuestionNo;
                
                $content = '';
                
                continue;
            }
            
            $arr[$currentQuestionNo] = [
                'content' => $content
            ];
            
            $currentQuestionNo = $nextQuestionNo;
            
            $content = $questionNo['text'];
        }
        
        $arr[$nextQuestionNo] = [
            'content' => $content
        ];
        
        return $arr; // die(json_encode($arr, JSON_PRETTY_PRINT));
    }
    
    private function getQuestionNo($text) 
    {   
        $text = trim($text);

//         if(substr($text, 0, 3) != '/@Q') return null;
        $startPos = strpos($text, '/@Q');
        
        if($startPos === false) return null;
        
        $text = substr($text, $startPos + 3);
        
        $pos = strpos($text, '/');
        
        $questionNo = substr($text, 0, $pos);
        
        $otherText = substr($text, $pos+1);
        
        return  ['question_no' => $questionNo, 'text' => $otherText];
    }
    
    private function handleImgs($dom, $fileName) 
    {
        $imgs = $dom->getElementsByTagName('img');
        
        /** @var \DOMNode $img */
        foreach ($imgs as $img)
        {
            $src = $img->getAttribute('src');
            
            $imgExt = pathinfo($src, PATHINFO_EXTENSION);
            
            $imgName = pathinfo($fileName, PATHINFO_FILENAME);
            
            $this->imgID++;
            
            $imgSrc = "$imgName-00{$this->imgID}.$imgExt";
            
            $imgDir = self::IMG_OUTPUT_BASE_DIR . $this->subject.'/'.$this->year.'/';
            
            if(!file_exists($imgDir))mkdir($imgDir, 0777, true);
            
            $imgFile = self::INPUT_BASE_DIR.$this->subject.'/'.$src;
            
            copy($imgFile, $imgDir . $imgSrc);
            
            $img->setAttribute('src', $imgSrc);
        }
    }
    
    function zCount()
    {
//         die('zCount(): Halt');
        $courses = \App\Models\Courses:://where(COURSE_CODE, 'LIKE', '%_waec%')
            with(['sessions'])
            ->get();
        $arr = [];
        foreach($courses as $course)
        {
            //             dDie($course->toArray());
            $sessions = $course['sessions'];
            //             $session = $course->sessions()->first();
            $zCountSessions = [];
            foreach ($sessions as $session)
            {
                if(self::startFrom2000)
                {
                    if(2000 > $session[SESSION]) continue;
                }
                
                $questions = $session->questions()->get();
                $count = $questions->count();
                //                 echo("Course Code = {$course[COURSE_CODE]}, session = {$session[SESSION]}, count = $count,  <br />");
                $zCountSessions[$session[SESSION]] = $count;
            }
            $arr[$course[COURSE_CODE]] = $zCountSessions;
        }
        $filename = 'C:/wamp64/www/mock/public/output/jamb/zcount.json';
        file_put_contents($filename, json_encode($arr));
        echo '<br /> Operation ended <br />';
        dDie($arr);
        //         dlog($msg)
    }
    
    function renameExplanationFiles()
    {
        $subjetsArr = self::COURSE_TITLES;//['Biology', 'Commerce', 'Economics', 'Government', 'Mathematics'];
        
        foreach ($subjetsArr as $subject => $subjectFullname)
        {
//             $subjectDir = "C:/Users/USER PC/Desktop/Project Files/Jamb Explanations/Ready - Batch 3_2/" . $subject;
            $subjectDir = "C:/Users/USER PC/Desktop/Project Files/Jamb Explanations/Updated Jamb Explanations/" . $subject;
            
            if(!file_exists($subjectDir))
            {
                echo "$subjectFullname directory does not exist <br />";
                
                continue;
            }
            
            $files = scandir($subjectDir);
            
            foreach ($files as $file)
            {
                if(is_dir($file)) continue;
                
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $year = pathinfo($file, PATHINFO_FILENAME);
                if (strlen($year) > 4 || !is_numeric($year))
                {
                    echo "$subject - $year not a number or has length more than 4 <br />";
                
                    continue;
                }
                
                $oldname = "$subjectDir/$file";
//                 die('--- <br />'.$oldname);
                $newFilename = "$subjectDir/$subject".'_'.$year.'_Explanations.'.$ext;
                rename($oldname, $newFilename);
            }
        }
    }
    
    const COURSE_TITLES = [
    /*
        'Sweet16' => 'Sweet Sixteen Novel',
        'English' => 'English Language',
        'Account' => 'Accounting',
        'Biology' => 'Biology',
        'Chemistry' => 'Chemistry',
        'Commerce' => 'Commerce',
        'CRK' => 'Christian Religious Knowledge',
        'Economics' => 'Economics',
        'Geography' => 'Geography',
     */
        'Government' => 'Government',
        'Literature' => 'Literature in English',
        'Mathematics' => 'Mathematics',
        'Physics' => 'Physics',
    /*
     * */
    ];
    
}







