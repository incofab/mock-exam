<?php
namespace App\Parser;

class GenericParse
{
    const IMG_OUTPUT_PUBLIC_PATH = 'public/img/content/'; 
    const IMG_OUTPUT_BASE_DIR = APP_DIR . '../'.self::IMG_OUTPUT_PUBLIC_PATH; 
//     const IMG_OUTPUT_BASE_DIR = APP_DIR . '../public/img/content/'; 
    
    private $jsonFilesDir = APP_DIR . '../public/output/content/';
    
    function __construct($year, $subjectDir, $fromMinified = false) 
    {
        $this->year = $year;
        
        $this->subjectDir = $subjectDir;
        
        $this->htmExt = $fromMinified ? '.min.htm' : '.htm';
    }
    
    private $imgID = 0;    
    private $subject = '';
    private $subjectDir = '';
    private $subjectFullname = '';
    
    private $year = '';
    private $htmExt = '';
    
    /**
     * Parse the HTML format of JAMB questions and save them in DB.
     * Copy their images to the image folder self::IMG_OUTPUT_BASE_DIR
     */
    function parse($courseData) 
    {
        $this->subject = $courseData[COURSE_CODE];
        $this->subjectFullname = $courseData[COURSE_TITLE];
        
        $files = scandir($this->subjectDir);
        
        foreach ($files as $file)
        {
            if($file == '.' || $file == '..') continue;
                
            if(substr($file, -10) != 'Answer.txt') continue;
            
            $prefix = substr($file, 0, -10);
            
            $answersfileName   = $this->subjectDir.'/'.$prefix.'Answer.txt';
            $optionsfileName   = $this->subjectDir.'/'.$prefix.'Option'.$this->htmExt;
            $questionsfileName = $this->subjectDir.'/'.$prefix.'Question'.$this->htmExt;
            // Retrieve explanations filename here
            
            if(!file_exists($answersfileName)) return [SUCCESSFUL => false, MESSAGE => 'Answers file not found'];
            
            if(!file_exists($optionsfileName)) return [SUCCESSFUL => false, MESSAGE => 'Options file not found'];
            
            if(!file_exists($questionsfileName)) return [SUCCESSFUL => false, MESSAGE => 'Questions file not found'];
            
            $ret = $this->processSubjectYear($questionsfileName, $optionsfileName, $answersfileName);
            
            return $ret;
            
//             dDie($ret); 
            
//             $insertData = $this->insertSessionAndQuestionsRecord($ret['session_data'], $ret['questions']);

//             if(!$insertData[SUCCESSFUL]) return $insertData;
        }
        
        return [SUCCESSFUL => true, MESSAGE => $this->subject.' Done <br /><br />'];
    }
    
    private function processSubjectYear($questionsfileName, $optionsfileName, $answersfileName) 
    {
        $this->imgID = 0;
        
        $answers = $this->parseAnswers($answersfileName);
        
        $options = $this->parseOptions($optionsfileName);
        
        $questions = $this->parseQuestion($questionsfileName);
        
        $sessionData = [
            COURSE_CODE => $this->subject,
            SESSION => $this->year,
            GENERAL_INSTRUCTIONS => 'Answer all questions',
        ];
        
        $formattedQuestions = [];
        
        foreach ($questions as $questionNo => $question) 
        {
            if(empty($answers[$questionNo])) continue;

            if(empty($options[$questionNo])) 
            {
                dlog("questionNo = $questionNo");
                
                return [SUCCESSFUL => false, 
                    MESSAGE => "Failure on options[questionNo] - {$this->subject} of {$this->year}"];
            }
            
            $thisOption = $options[$questionNo];
            
            $questionsArr = [
                COURSE_CODE => $this->subject,
//                 SESSION_ID  => $session[TABLE_ID],
                QUESTION_NO => $questionNo,
                QUESTION => empty($question['content']) ? "Null" : $question['content'],
                OPTION_A => $thisOption['A']['option-content'],
                OPTION_B => $thisOption['B']['option-content'],
                OPTION_C => isset($thisOption['C']) ? $thisOption['C']['option-content'] : null,
                OPTION_D => isset($thisOption['D']) ? $thisOption['D']['option-content'] : null,
                OPTION_E => isset($thisOption['E']) ? $thisOption['E']['option-content'] : null,
                ANSWER   => $answers[$questionNo]['answer'],
                ANSWER_META => null,
            ];
            
            $formattedQuestions[] = $questionsArr;
            
        }
        return [SUCCESSFUL => true, MESSAGE => "", 
            "session_data" => $sessionData, 'questions' => $formattedQuestions];
    }
    
    function insertSessionAndQuestionsRecord($sessionData, $formattedQuestions) 
    {
        $session = new \App\Models\Sessions();
        
        $session = $session->insert($sessionData);
        
        if(empty($session[SESSION]))
        {
            return [SUCCESSFUL => FALSE, MESSAGE => "Failed to record Acad Session"];
        }
        
        foreach ($formattedQuestions as $questionsArr) 
        {
            $questionsArr[SESSION_ID] = $session[TABLE_ID];
            
            $questionRet = (new \App\Models\Questions())->insert($questionsArr);
            
            if(empty($questionRet[SUCCESSFUL]))
            {
                dlog(array_merge($questionsArr, $questionRet));
                
                dDie($questionRet);
            }
        }
        
        return [SUCCESSFUL => TRUE, MESSAGE => "Data recorded"];
    }
    
    private function parseAnswers($fileName)
    {
        $answers = file_get_contents($fileName);
        
        $answersArr = explode(PHP_EOL, $answers);
        
        $arr = [];
        
        foreach ($answersArr as $answer) 
        {
            if (empty($answer)) continue;
            
            $ansArr = explode('.', $answer);
            
            $questionNo = $ansArr[0];
            
            $ans = $ansArr[1];
            
            $arr[$questionNo] = [
                'answer' => $ans,
            ];
        }
        
        return $arr;
    }
    
    private function parseOptions($fileName)
    {
        $dom = new \DOMDocument();
        
        $dom->loadHTMLFile($fileName);
        
        $this->handleImgs($dom, $fileName);
        
        /** @var \DOMNodeList $paragraphs */
        $paragraphs = $dom->getElementsByTagName('p');
        
        $arr = [];
        
        $content = '';
        
        $currentQuestionNoAndOption = null;
        
        $nextQuestionNoAndOption = null;
        
        /** @var \DOMNode $p */
        foreach ($paragraphs as $p)
        {
            $text = $p->nodeValue;
            
            $questionNoAndOption = $this->getQuestionNo($text); 
            
            if(!$questionNoAndOption)
            {
                $content .= $dom->saveHtml($p);
                
                continue;
            }
            
            $nextQuestionNoAndOption = $questionNoAndOption['question_no'];
            
            if(!$currentQuestionNoAndOption) 
            {
                $currentQuestionNoAndOption = $nextQuestionNoAndOption;
                
                $content = '';
                
                continue;
            }
            
            $questionNo = substr($currentQuestionNoAndOption, 0, -1);
            
            $optionLetter = substr($currentQuestionNoAndOption, -1);
            
            $arr[$questionNo][$optionLetter] = [
                'option-content' => $content,
            ];
            
            $currentQuestionNoAndOption = $nextQuestionNoAndOption;
            
            $content = $questionNoAndOption['text'];
        }
        
        $questionNo = substr($currentQuestionNoAndOption, 0, -1);
        
        $optionLetter = substr($currentQuestionNoAndOption, -1);
        
        $arr[$questionNo][$optionLetter] = [
            'option-content' => $content,
        ];
        
        return $arr;
    }
    
    private function parseQuestion($fileName)
    {
        $arr = [];
        
        $dom = new \DOMDocument();
        
        $dom->loadHTMLFile($fileName);
        
        $this->handleImgs($dom, $fileName);
        
        /** @var \DOMNodeList $paragraphs */
        $paragraphs = $dom->getElementsByTagName('p');
        
        $content = '';
        
        $currentQuestionNo = null;
        
        $nextQuestionNo = null;
        
        /** @var \DOMNode $p */
        foreach ($paragraphs as $p)
        {
            $text = $p->nodeValue;
            
            $questionNo = $this->getQuestionNo($text); 
            
            if(!$questionNo) 
            {
                $content .= $dom->saveHtml($p);
                
                continue;
            }
            
            $nextQuestionNo = $questionNo['question_no'];
            
            if(!$currentQuestionNo)
            {
                $currentQuestionNo = $nextQuestionNo;
                
                $content = '';
                
                continue;
            }
            
            $arr[$currentQuestionNo] = [
                'content' => $content
            ];
            
            $currentQuestionNo = $nextQuestionNo;
            
            $content = $questionNo['text'];
        }
        
        $arr[$nextQuestionNo] = [
            'content' => $content
        ];
        
        return $arr; // die(json_encode($arr, JSON_PRETTY_PRINT));
    }
    
    private function getQuestionNo($text) 
    {   
        $text = trim($text);

//         if(substr($text, 0, 3) != '/@Q') return null;
        $startPos = strpos($text, '/@Q');
        
        if($startPos === false) return null;
        
        $text = substr($text, $startPos + 3);
        
        $pos = strpos($text, '/');
        
        $questionNo = substr($text, 0, $pos);
        
        $otherText = substr($text, $pos+1);
        
        return  ['question_no' => $questionNo, 'text' => $otherText];
    }
    
    private function handleImgs($dom, $fileName) 
    {
        $imgs = $dom->getElementsByTagName('img');
        
        /** @var \DOMNode $img */
        foreach ($imgs as $img)
        {
            $src = $img->getAttribute('src');
            
            $imgExt = pathinfo($src, PATHINFO_EXTENSION);
            
            $imgName = pathinfo($fileName, PATHINFO_FILENAME);
            
            $this->imgID++;
            
            $imgSrc = "$imgName-00{$this->imgID}.$imgExt";
            
            $imgDir = self::IMG_OUTPUT_BASE_DIR . $this->subject.'/'.$this->year.'/';
            
            if(!file_exists($imgDir))mkdir($imgDir, 0777, true);
            
//             $imgFile = self::INPUT_BASE_DIR.$this->subject.'/'.$src;
            $imgFile = $this->subjectDir.'/'.$src;
            
            copy($imgFile, $imgDir . $imgSrc);
            
            $img->setAttribute('src', $imgSrc);
        }
    }
    
    function zCount()
    {
        die('djdjdjdjdjdj');
        $courses = \App\Models\Courses::where(COURSE_CODE, 'LIKE', '%_waec%')
        ->with(['sessions'])
        ->get();
        $arr = [];
        foreach($courses as $course)
        {
            //             dDie($course->toArray());
            $sessions = $course['sessions'];
            //             $session = $course->sessions()->first();
            $zCountSessions = [];
            foreach ($sessions as $session)
            {
                $questions = $session->questions()->get();
                $count = $questions->count();
                //                 echo("Course Code = {$course[COURSE_CODE]}, session = {$session[SESSION]}, count = $count,  <br />");
                $zCountSessions[$session[SESSION]] = $count;
            }
            $arr[$course[COURSE_CODE]] = $zCountSessions;
        }
        $filename = 'C:/wamp64/www/mock/public/output/jamb/zcount.json';
        file_put_contents($filename, json_encode($arr));
        echo '<br /> Operation ended <br />';
        dDie($arr);
        //         dlog($msg)
    }
    
    
}







