<?php
namespace App\Parser;

use Illuminate\Routing\Controller;

class FileCript extends Controller{
	
    private $parentDir;
    private $mcrypt;
    
    static function encriptJamb()
    {
//         die('Dont run yet');
        $jambFilesDir = APP_DIR . '../public/output/jamb/';
        ini_set('max_execution_time', 480);
        dDie((new \App\Parser\FileCript(new \App\Parser\MCrypt(), $jambFilesDir)));
    }
    
    function __construct(\App\Parser\MCrypt $mcrypt, $parentDir) 
    {
	   $this->parentDir = rtrim($parentDir, '/');
	   
	   $this->mcrypt = $mcrypt;
	   
// 	   die('Prevent unnecessary calls to this method');

	   $this->criptFile($this->parentDir);
	}	

	private function criptFile($fileDir, $decrypt = false) 
	{	    
	    $allFiles = scandir($fileDir);
	    
	    foreach ($allFiles as $file) 
	    {
	        if(in_array($file, ['.', '..'])) continue;
	        
	        $file = "$fileDir/$file";
	        
	        if(is_dir($file))
	        { 
	            $this->criptFile("$file", $decrypt);
	            
	            continue;
	        }
	        
	        $content = file_get_contents($file);
	        
	        $ext = pathinfo($file, PATHINFO_EXTENSION);
	        
	        if($ext != 'json' && $ext != 'crypt') continue;
	        
	        $dirName = pathinfo($file, PATHINFO_DIRNAME);
	        
	        $fileName = pathinfo($file, PATHINFO_FILENAME);
	        
	        $fileName = str_replace('.json', '', $fileName);
	        $fileName = str_replace('.crypt', '', $fileName);
	        
	        $cryptDir = "{$this->parentDir}/crypt" . str_replace($this->parentDir, '', $dirName);
	        
	        if(!file_exists($cryptDir)) mkdir($cryptDir, 0777, true);
	        
	        $fullFileName = "$cryptDir/$fileName.crypt.json";
	        
	        if($decrypt)
	        {
	            file_put_contents($fullFileName, $this->mcrypt->decrypt($content));
	        }
	        else 
	        {
	            file_put_contents($fullFileName, $this->mcrypt->encrypt($content));
	        }
	        
	        echo "$fullFileName <br />";
	    }
	}
	
	
}