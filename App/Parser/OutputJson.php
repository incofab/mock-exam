<?php
namespace App\Parser;

use App\Core\Helper;

class OutputJson
{
    private $toEncrpt;
    private $mCrypt;
    
    const OUTPUT_BASE_DIR = APP_DIR . '../public/output/';
    private $baseDir = self::OUTPUT_BASE_DIR;
    
    function __construct(\App\Parser\MCrypt2 $mCrypt) 
    {
        $this->mCrypt = $mCrypt;
    }
    
    function isOutputJsonAvailable($examContentName)
    {
        $dir = $this->getBaseFolder($examContentName);
        
        return file_exists($dir);
    }
    
    function deleteOutputJson($examContentId)
    {
        $examContent = \App\Models\ExamContent::where(TABLE_ID, '=', $examContentId)->first();
        
        if(!$examContent) return [SUCCESSFUL => false, MESSAGE => "Exam content not found"];
        
        $this->baseDir = $this->getBaseFolder($examContent[EXAM_NAME]);
        $imagesOutputFolder = $this->getBaseFolder($examContent[EXAM_NAME]."_images");
        
        if(file_exists($this->baseDir))
        {
            \App\Core\Helper::deleteDir($this->baseDir, true);
        }
        
        if(file_exists($imagesOutputFolder))
        {
            \App\Core\Helper::deleteDir($imagesOutputFolder, true);
        }
        
        return [SUCCESSFUL => true, MESSAGE => 'Folder and its content deleted'];
    }
    
    function outputJson($examContentId, $toEncrypt = false)
    {
//         die('Dont run yet');
        ini_set('max_execution_time', 960); 
     
        $this->toEncrpt = $toEncrypt;
        
        $examContent = \App\Models\ExamContent::where(TABLE_ID, '=', $examContentId)
            ->with(['courses'])->first();
        
        if(!$examContent) return [SUCCESSFUL => false, MESSAGE => "Exam content not found"];
        
        $courses = $examContent['courses'];
        
        $this->baseDir = $this->getBaseFolder($examContent[EXAM_NAME]);
        
        if(file_exists($this->baseDir))
        {
            return [SUCCESSFUL => false, MESSAGE => "Content already processed"];
        }
        
        $this->zCount($examContent);
        
        $ret = $this->generateJSON($courses);
        
        if(!$ret[SUCCESSFUL]) return $ret;
        
        $this->compileImages($examContent[EXAM_NAME], $courses);
        
        return $ret;
    }
    
    private function generateJSON(\Illuminate\Database\Eloquent\Collection $allCourseDetails) 
    {
    	if (!$allCourseDetails->first()) die('No record found');
    	
    	$arr = [];
    	
    	foreach ($allCourseDetails as $CourseDetails) 
    	{    	    
//     	    if($CourseDetails[COURSE_CODE] != 'Sweet16') continue;
    		$arr[] = [
    			
    		    TABLE_ID => $CourseDetails[TABLE_ID],
    		    
    			'course_name' => $CourseDetails[COURSE_CODE],
    		    
    			'course_fullname' => $CourseDetails[COURSE_TITLE],
    		    
    			DESCRIPTION => $CourseDetails[DESCRIPTION],
    		];
    		
    		$sessionRet = $this->generateYearsJSON($CourseDetails[TABLE_ID]);
    		
    		if(!$sessionRet[SUCCESSFUL]) return $sessionRet;
    		
    		$summaryRet = $this->generateSummaryJSON($CourseDetails[TABLE_ID]);    		

    		if(!$summaryRet[SUCCESSFUL]) return $summaryRet;
    	}
    	
    	$filename = "{$this->baseDir}all_courses.json" ;
    	
    	$this->saveContentInFile($filename, json_encode($arr), $this->toEncrpt);
    	
    	return [SUCCESSFUL => true, MESSAGE => "Operation Complete"];
    }
    
    private function generateYearsJSON($courseId) 
    {
    	$obj = (new \App\Models\Sessions());
    	
    	$allSessions =  $obj->where(COURSE_ID, '=', $courseId)->orderBy(TABLE_ID, 'DESC')->get();
    	
    	if (!$allSessions->first())
    	{
    		return [SUCCESSFUL => true, MESSAGE => "No record found for $courseId, create a new one"]; 
    	}
    	
    	$arr = [];
    	
    	$yearIDs = [];
    	
    	/** @var \App\Models\Sessions $session */
    	foreach ($allSessions as $session)
    	{
    	    if(\App\Parser\Parse::startFrom2000)
    	    {
    	        if(2000 > $session[SESSION]) continue;
    	    }
    	    
    	    $per_question_instr = $session->instructions()->get();
    		
    	    $passages = $session->passages()->get();
    		
    		$arr[] = [
				'year_id' => $session[TABLE_ID],
		    
				'year' => $session[SESSION],
		    
				CATEGORY => $session[CATEGORY],
		    
				GENERAL_INSTRUCTIONS => $session[GENERAL_INSTRUCTIONS],
		    
		        'per_question_instructions' => $this->formatPerQuestionInstructions($per_question_instr),
		    
		        'passages' => $this->formatPassages($passages),
    		];
    		
    		$yearIDs[] = $session[TABLE_ID];
    	}
    	
    	$folder = "{$this->baseDir}$courseId/";
    	
    	// Create the directory if it doesnt exists
    	if(!file_exists($folder)) mkdir($folder, 0777, true);
    	
    	$filename = $folder . strtolower(str_replace(' ', '_', $courseId)) . '.json';
    	
    	$this->saveContentInFile($filename, json_encode($arr), $this->toEncrpt);

    	//For topics
    	$topicsContent = $this->getTopics($courseId);
    	if(!empty($topicsContent))
    	{
        	$topicsFilename = $folder.'t.json';
        	$this->saveContentInFile($topicsFilename, json_encode($topicsContent), $this->toEncrpt);
    	}
    	
    	return $this->generateCryptYearsQuestionsJSON($courseId, $yearIDs);
    } 
     
    private function generateCryptYearsQuestionsJSON($courseId, $yearIDs) 
    {
    	$obj = (new \App\Models\Questions());
    	
    	foreach ($yearIDs as $year_id) 
    	{
    	    $allCourseYearQuestions =  $obj->where(COURSE_SESSION_ID, '=', $year_id)
    	    ->orderBy(QUESTION_NO, 'ASC')->get();
    		
    		if (!$allCourseYearQuestions->first())
    		{
    			continue; 
    		}
    	    
    		$yearquestions = [];
    		
    		foreach ($allCourseYearQuestions as $courseYear) 
    		{
    			$yearquestions[] = [
    			        TOPIC_ID => $courseYear[TOPIC_ID],
    					QUESTION_NO => $courseYear[QUESTION_NO],
    					QUESTION => $courseYear[QUESTION],
    					OPTION_A => $courseYear[OPTION_A],
    					OPTION_B => $courseYear[OPTION_B],
    					OPTION_C => $courseYear[OPTION_C],
    					OPTION_D => $courseYear[OPTION_D],
    					OPTION_E => $courseYear[OPTION_E],
    					ANSWER => $courseYear[ANSWER],
    					ANSWER_META => $courseYear[ANSWER_META],
    			];
    		}
    		
    		$folder = "{$this->baseDir}$courseId/";
    		
    		// Create the directory if it doesnt exists
    		if(!file_exists($folder)) mkdir($folder, 0777, true);
    	
    		$filename = $folder 
    				. strtolower(str_replace(' ', '_', $courseId) . "_$year_id").'.json';
    	
			$this->saveContentInFile($filename, json_encode($yearquestions), $this->toEncrpt);
//     		file_put_contents($filename, json_encode($yearquestions));
    	}
    	
    	return [SUCCESSFUL => true, MESSAGE => "generateYearsQuestionsJSON method Done"];
    } 
    
    private function formatPerQuestionInstructions($allInstructions) 
    {
        $arr = [];
        foreach ($allInstructions as $instruction) 
        {
            $ins = [];
            $ins['per_question_instruction'] = $instruction[INSTRUCTION];
            $ins[FROM_] = $instruction[FROM_];
            $ins[TO_] = $instruction[TO_];
            
            $arr[] = $ins;
        }
        /*    
         * To be deleted only after editting the android app    
        $instr = [];
        $from = [];
        $to = [];
        foreach ($allInstructions as $instruction) 
        {
            $instr[] =  $instruction[INSTRUCTION];
            $from[] =  $instruction[FROM_];
            $to[] =  $instruction[TO_];
        }
        
        return [
            'per_question_instruction' => $instr,
            FROM_ => $from,
            TO_ => $to,
        ];
        */
        return $arr;
    }
    
    private function formatPassages($allPassages)  
    {
        $arr = [];
        foreach ($allPassages as $passage)
        {
            $p = [];
            $p['per_question_instruction'] = $passage[PASSAGE];
            $p[FROM_] = $passage[FROM_];
            $p[TO_] = $passage[TO_];
            
            $arr[] = $p;
        }
        /*
         * To be deleted only after editting the android app   
        $passagesArr = []; 
        $from = [];
        $to = [];
        foreach ($allPassages as $passage)  
        {
            $passagesArr[] =  $passage[PASSAGE];
            $from[] =  $passage[FROM_];
            $to[] =  $passage[TO_];
        }
        return [
            'per_question_passage' => $passagesArr,
            FROM_ => $from,
            TO_ => $to,
        ];
        */
        return $arr;
    }
    
    private function generateSummaryJSON($courseId)
    {
        $obj = (new \App\Models\Summary());
        
        $allSummary =  $obj->where(COURSE_ID, '=', $courseId)->get([TABLE_ID, COURSE_ID, CHAPTER_NO, TITLE, DESCRIPTION, SUMMARY]);
        
        if (!$allSummary->first())
        {
            return [SUCCESSFUL => true, MESSAGE => "Done, No summary content"];;
        }
        
        $arr = $allSummary->toArray();
        
        $folder = "{$this->baseDir}/summary/$courseId/";
        
        // Create the directory if it doesnt exists
        if(!file_exists($folder)) mkdir($folder, 0777, true);
        
        $filename = $folder . strtolower(str_replace(' ', '_', $courseId)) . '.json';
        
        $this->saveContentInFile($filename, json_encode($arr), $this->toEncrpt);
        
        return [SUCCESSFUL => true, MESSAGE => "generateSummaryJSON method Done"];
    }
    
    private function getTopics($courseId)
    {
        $obj = (new \App\Models\Topics());
        
        $allSummary =  $obj->where(COURSE_ID, '=', $courseId)->get([TABLE_ID, COURSE_ID, TITLE, DESCRIPTION]);
        
        $arr = $allSummary->toArray();
        
        return $arr;
    }
    
    function zCount(\App\Models\ExamContent $examContent)
    {
        $courses = \App\Models\Courses::where(EXAM_CONTENT_ID, '=', $examContent[TABLE_ID])
            ->with(['sessions'])->get();
        
        $arr = [];
        foreach($courses as $course)
        {
            $sessions = $course['sessions'];
            
            $zCountSessions = [];
            foreach ($sessions as $session)
            {
                if(\App\Parser\Parse::startFrom2000 && is_numeric($session[SESSION]))
                {
                    if(2000 > (int)$session[SESSION]) continue;
                }
                
                $questions = $session->questions()->get();
                $count = $questions->count();
                
                $zCountSessions[$session[SESSION]] = $count;
            }
            
            $arr[$course[COURSE_CODE]] = $zCountSessions;
        }
        
        $baseDir = $this->getBaseFolder($examContent[EXAM_NAME]);
        $filename = $baseDir."zcount.json";
        
        if(!file_exists($baseDir)) mkdir($baseDir, 0777, true);

        $this->saveContentInFile($filename, json_encode($arr), false);
        
        return [SUCCESSFUL => true, MESSAGE => 'Zcount completed'];
    }
    
    private function getBaseFolder($examContentName)
    {
        $folder = $this->filterFolderName($examContentName);
        
        return self::OUTPUT_BASE_DIR . "$folder/";
    }
    
    private function filterFolderName($folderName)
    {
        return str_replace(['/', '.', ':', ','], ['_-_'], $folderName);
    }
    
    private function saveContentInFile($filename, string $strContent, $toEncrypt)
    {
        if($toEncrypt)
        {
            $strContent = $this->mCrypt->encrypt($strContent);
        }
        
        file_put_contents($filename, $strContent);
    }
    
    private function compileImages($examContentName, $courses) 
    {
        $imagesBaseFolder = \App\Core\CourseInstaller::$imagesBaseFolder;
        $imagesOutputFolder = $this->getBaseFolder($examContentName."_images");
        foreach ($courses as $course) 
        {
            $courseImgFolder = $imagesBaseFolder.$course[TABLE_ID];
            $destinationImgFolder = $imagesOutputFolder.$course[TABLE_ID];
            
            if(!file_exists($courseImgFolder)) continue;
            
            Helper::copy($courseImgFolder, $destinationImgFolder);
        }
    }
    
}





