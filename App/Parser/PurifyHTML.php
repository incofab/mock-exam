<?php
namespace App\Parser;
class PurifyHTML
{
    const INPUT_BASE_DIR = 'C:/Users/Admin/Desktop/dummy/';// biology/';
//     const INPUT_BASE_DIR = 'C:/Users/USER PC/Desktop/Project Files/Jamb Explanations/Updated Jamb Explanations/';// biology/';
    
    function __construct() 
    {
        require_once APP_DIR.'lib/htmlpurifier-4.13.0/library/HTMLPurifier.auto.php';
    }
    
    private $imgID = 0;
    
    private $subject = '';
    
    private $year = '';
    
    function purify() 
    {
//         die('Dont run yet');
        ini_set('max_execution_time', 720);  
        $config = \HTMLPurifier_Config::createDefault();
//                 $config->set('CSS.AllowedProperties', array());
//                 $config->set('Attr.EnableID', false);
//                 $config->set('AutoFormat.RemoveEmpty', true);
//                 $config->set('Attr.AllowedClasses', array());
                // only src and alt attributes allowed... No style, class or id attributes allowed
//         $config->set('HTML.AllowedElements', array('table'));
        $config->set('HTML.AllowedAttributes', array('src', 'alt', 'table.style', 'th.style', 'td.style', 'tr.style'));
        $config->set('AutoFormat.RemoveSpansWithoutAttributes', true);
        $config->set('AutoFormat.RemoveEmpty', true);
        $purifier = new \HTMLPurifier($config);
        
        $operation = '';
        foreach (self::COURSE_TITLES as $subject => $courseTitle)
        {
            $subjectDir = self::INPUT_BASE_DIR . $this->subjectFolder($subject); 
            
            $files = scandir($subjectDir);
            
            foreach ($files as $file)
            {
                $prefix = substr($file, -4);
                
                if($prefix != '.htm' || is_dir($file) || substr($file, -8) == '.min.htm'){
//                     echo "$file, prefix = $prefix <br />";
                    continue;
                }

                $fileName   = $subjectDir.'/'.$file;
//                 dDie($fileName);
                $dirty_html = file_get_contents($fileName);
                $dirty_html = str_ireplace(['&nbsp;'], [' '], $dirty_html);
                $clean_html = $purifier->purify($dirty_html);
                $clean_html = str_replace(array("\r", "\n"), ' ', $clean_html);
                $clean_html = str_replace(array("  ", "   "), ' ', $clean_html);
                $minFilename = substr($fileName, 0, -4.).'.min.htm';
                file_put_contents($minFilename, $clean_html);
//                 dlog($clean_html);
//                 dDie($minFilename);
//                 dDie($clean_html);  
                $operation .= "$file <br />";
            }
        }
        
        die('Operation Complete <br />'.$operation);
    }
    
    function subjectFolder($subject)
    {
        $str = str_replace(\App\Parser\ParseWAEC::WAEC_TAG, '', $subject);
        
        return $str;
    }
    
    const COURSE_TITLES = [
        'Math' => 'math',
    /*
        'Account'.\App\Parser\ParseWAEC::WAEC_TAG => 'Accounting',
        'Agriculture'.\App\Parser\ParseWAEC::WAEC_TAG => 'Agriculture',
        'Biology'.\App\Parser\ParseWAEC::WAEC_TAG => 'Biology',
        'Chemistry'.\App\Parser\ParseWAEC::WAEC_TAG => 'Chemistry',
        'Civic'.\App\Parser\ParseWAEC::WAEC_TAG => 'Civic',
        'Commerce'.\App\Parser\ParseWAEC::WAEC_TAG => 'Commerce',
        'Computer'.\App\Parser\ParseWAEC::WAEC_TAG => 'Computer',
        'CRK'.\App\Parser\ParseWAEC::WAEC_TAG => 'Christian Religious Knowledge',
        'Economics'.\App\Parser\ParseWAEC::WAEC_TAG => 'Economics',
        'Geography'.\App\Parser\ParseWAEC::WAEC_TAG => 'Geography',
        'Government'.\App\Parser\ParseWAEC::WAEC_TAG => 'Government',
        'Literature'.\App\Parser\ParseWAEC::WAEC_TAG => 'Literature in English',
        'Mathematics'.\App\Parser\ParseWAEC::WAEC_TAG => 'Mathematics',
        'Physics'.\App\Parser\ParseWAEC::WAEC_TAG => 'Physics',
        'English'.\App\Parser\ParseWAEC::WAEC_TAG => 'English Language',
    */
     
    /*
        'Sweet16' => 'Sweet Sixteen Novel',
        'English' => 'English Language',
        'Account' => 'Accounting',
        'Biology' => 'Biology',
        'Chemistry' => 'Chemistry',
        'Commerce' => 'Commerce',
        'CRK' => 'Christian Religious Knowledge',
        'Economics' => 'Economics',
        'Geography' => 'Geography',
        'Literature' => 'Literature in English',
        'Government' => 'Government',
        'Mathematics' => 'Mathematics',
        'Physics' => 'Physics',
     */
    /*
    */
    /*
      */
    ];
    
}







