<?php
namespace App\Interfaces\League;

interface PlayPK
{
    /**
     * Randomly generates a penaltyKick score for this rating
     * @param integer $rating
     * @return boolean true if user scored, false otherwise 
     */
    function getPKResult($rating);
}

