<?php


/**
 * @var \FastRoute\RouteCollector $r
 */
$r;

$r->get($addr.'/rough/{courseCode}/{session_id}', function(
    $courseCode, $session_id
){
    $obj = new \App\Models\Sessions();
    
    $sessionDetails = $obj->where(TABLE_ID, '=', $session_id)->first();
    
    if (!$sessionDetails){
        return view('layout', [ 'error' => 'No Record found' ]);
    }
    
    $allSessionQuestions = $sessionDetails->questions()->get();
    // 		$perQuestionInstructions = $sessionDetails->instructions()->get();
    // 		$passages = $sessionDetails->passages()->get();
    
    $allPassages = $sessionDetails->passages()->get();
    $allInstructions = $sessionDetails->instructions()->get();
    
    if (!$allSessionQuestions->first()){
        return view('layout', [ 'error' => 'No question recorded for this course. '
            . '<a href="' . getAddr('ccd_new_session_question', [$courseCode, $session_id])
            . '">Click here to start recording question</a>'
        ]);
    }
    
    return view('dummy/index', [
        'year' => $sessionDetails[SESSION],
        'courseName' => $courseCode,
        'allCourseYearQuestions' =>  $allSessionQuestions,
        'year_id' => $session_id,
        'session' => $sessionDetails[SESSION],
        'allPassages' => $allPassages,
        'allInstructions' => $allInstructions,
    ]);
    
});
    
$r->get($addr.'/dummy', function(){
//     $obj = new \App\Parser\Parse();
//     $ret = $obj->parseOptions(\App\Parser\Parse::INPUT_BASE_DIR.'Commerce/Commerce_2016_Option.min.htm');
//     dDie($ret);
//     dDie('--'.trim(last($ret)['content'], ' \n\r\t\0\x0B').'--');
//     return (new \App\Parser\ParseWAEC())->parse();
    return (new \App\Parser\Parse())->renameExplanationFiles();
//     return view('exams/exam_page', []);
    return view('dummy/index', []);
});
   
$r->get($addr.'/purify', function(){
    return (new \App\Parser\PurifyHTML())->purify();
});
$r->get($addr.'/zcount', function(){
    return (new \App\Parser\Parse())->zCount();
});
$r->get($addr.'/parse'.'/{for}', function($for){
    if($for == 'jamb') return (new \App\Parser\Parse())->parse();
});
$r->get($addr.'/output-json'.'/{for}', function($for){
    if($for == 'jamb') return \App\Parser\OutputJson::outputJamb();
});
$r->get($addr.'/encrypt-output-json'.'/{for}', function($for){
    if($for == 'jamb') return \App\Parser\FileCript::encriptJamb();
});

$r->get($addr.'/init', function(){
	
	ini_set('max_execution_time', 1200); 
	
    require APP_DIR . 'migrate.php';
//     createAllTables();
});

$r->get($addr.'/seed', function(){
    require APP_DIR . 'faker/faker.php';
});
    
$r->post($addr.'/upload'.'/{for}', function($for){
    
    $allPost = json_decode($_POST['data'], true);
    
    $ret = 'Done';
    
    switch ($for) {
        case SESSIONS_TABLE:
            foreach ($allPost as $post){
                $ret = (new \App\Models\Sessions)->insert($post, true);
            }
        break;
        case COURSES_TABLE:
            foreach ($allPost as $post){
                $ret = (new \App\Models\Courses())->insert($post);
            }
        break;
        case SUMMARY_TABLE:
            foreach ($allPost as $post){
                $ret = (new \App\Models\Summary())->insert($post);
            }
        break;
        case QUESTIONS_TABLE:
            foreach ($allPost as $post){
                $ret = (new \App\Models\Questions())->insert($post);
            }
        break;
        case INSTRUCTIONS_TABLE:
            foreach ($allPost as $post) 
            {
                $ret = \App\Models\Instructions::create([
                    COURSE_CODE => $post[COURSE_CODE],
                    SESSION_ID  => $post[SESSION_ID],
                    INSTRUCTION => $post[INSTRUCTION],
                    FROM_ => $post[FROM_],
                    TO_   => $post[TO_],
                ]);
            }
        break;
        case PASSAGES_TABLE:
            foreach ($allPost as $post) 
            {
                $ret = \App\Models\Passages::create([
                    COURSE_CODE => $post[COURSE_CODE],
                    SESSION_ID  => $post[SESSION_ID],
                    PASSAGE => $post[PASSAGE],
                    FROM_ => $post[FROM_],
                    TO_   => $post[TO_],
                ]);
            }
        break;
        
        default:
            
        break;
    }
    dDie($ret);
});


?>