<?php

session_start();


class FB {
	
	private static $fb = null;
	
	static function getFB(){

		if(!self::$fb){
			self::$fb = new \Facebook\Facebook([
					'app_id' => '850621878409943',
					'app_secret' => 'a56ffec416aee0eab9b97ac3da118ecc',
					'default_graph_version' => 'v2.9',
					//'default_access_token' => '{access-token}', // optional
			]);
		}
		return self::$fb;		
	}
	
	static function getLoginUrl()
	{
		$helper = self::getFB()->getRedirectLoginHelper();
		$permissions = ['email', 'public_profile', 'user_friends', 'publish_actions'];
		$loginUrl = $helper->getLoginUrl( 'http://' . $_SERVER['HTTP_HOST'] . getAddr('facebook_login'), $permissions);
		return $loginUrl;
	}
	
	/**
	 * This essentially logs in the user, retrieves and save his access token token and FB ID
	 * @param unknown $userData
	 * @return boolean[]|string[]|boolean[]|mixed[]
	 */
	static function isLoggedIn($userData){

		$helper = self::getFB()->getRedirectLoginHelper();
		
		try {
			
			if(isset($userData[FB_ACCESS_TOKEN])){
				$accessToken = new \Facebook\Authentication\AccessToken($userData[FB_ACCESS_TOKEN]);
			}else{
				// Redirect user to FB login page 
				// and/or grabs the user's access token if he's logged in before
				$accessToken = $helper->getAccessToken();
			}
			
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
// 			echo 'Graph returned an error 1: ' . $e->getMessage();
			return [SUCCESSFUL => false, 'is_logged_in' => false, 'login_url' => self::getLoginUrl(),
					MESSAGE => 'Graph returned an error 1: ' . $e->getMessage()
			];
			exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			return [SUCCESSFUL => false, 'is_logged_in' => false, 'login_url' => self::getLoginUrl(),
					MESSAGE => 'Facebook SDK returned an error 1: ' . $e->getMessage()
			];
// 			echo 'Facebook SDK returned an error 1: ' . $e->getMessage();
			exit;
		}
		
		if(!isset($accessToken))
		{		
			return [SUCCESSFUL => false, 'is_logged_in' => false, 'login_url' => self::getLoginUrl()];
		}

		if(isset($_GET['state'])) 
		{
			$_SESSION['FBRLH_' . 'state'] = $_GET['state'];
		}
		
		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = self::getFB()->getOAuth2Client();
			
		if (!$accessToken->isLongLived()) 
		{
			// Exchanges a short-lived access token for a long-lived one
			try {
				$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
// 				$userData[FB_ACCESS_TOKEN] = $accessToken->getValue();
// 				$userData->save();
			} catch (Facebook\Exceptions\FacebookSDKException $e) {
				return [SUCCESSFUL => false, 'is_logged_in' => false, 'login_url' => self::getLoginUrl(),
						MESSAGE => "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n"
				];
			}
		}

		$msg = 'Facebook Login successful';
		
		if(!isset($userData[FB_ACCESS_TOKEN]))
		{
			$userData[FB_ACCESS_TOKEN] = $accessToken->getValue();
			$userData->save();
		}
		
		if(!$userData[FB_ID])
		{
			static::getUserFBID($userData, $accessToken);

			if(!$userData[FB_ID]) $msg = "This Facebook account already exists";
		}
		
		return [SUCCESSFUL => true, 'is_logged_in' => true, MESSAGE => $msg];
		
	}
	/**
	 * Retrieves and registers user's facebook ID
	 * @param unknown $userData
	 * @param unknown $accessToken
	 */
	private static function getUserFBID($userData, $accessToken)
	{
		$userFBData = static::getUserData($accessToken);
		if($userFBData[SUCCESSFUL])
		{
			$FB_id = $userFBData['data']['id'];
			// Ensure that this ID does not already exist
			if(!$userData->where(FB_ID, '=', $FB_id)->first())
			{
				$userData[FB_ID] = $FB_id;
				$userData[BONUS_BALANCE] += 100;
				$messageData = [
					TITLE => 'Verification Bonus',
					MESSAGE => "You have been rewarded " . NAIRA_SIGN . '100 for verifying your facebook',
				];
				
				$userData->save();
// 				\App\Models\Notifications::send($userData, 
// 						\App\Game\Commands::NOTIFICATION_MESSAGE, $messageData, false);
			}else{
				dlog("{$userData[USERNAME]} tried to supply a Facebook ID that already exists. ID = $FB_id");
				$userData[FB_ID] = null;
				$userData[FB_ACCESS_TOKEN] = null;
				$userData->save();
			}
		}
	}
	
	static function getUserData($accessToken) {

		try {
			// Get the \Facebook\GraphNodes\GraphUser object for the current user.
			// If you provided a 'default_access_token', the '{access-token}' is optional.
			$response = self::getFB()->get('/me', $accessToken);
		
		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			return [SUCCESSFUL => false, 'login_url' => self::getLoginUrl(), 
					MESSAGE => 'Graph returned an error: ' . $e->getMessage()];
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			return [SUCCESSFUL => false, 'login_url' => self::getLoginUrl(), 
					MESSAGE => 'Facebook SDK returned an error: ' . $e->getMessage()];
		}
		
		$body = json_decode($response->getBody(), true); // Contains array (name => ?, id => ? )
		
		if(!getValue($body, 'id'))
		{
			return [SUCCESSFUL => false, 'login_url' => self::getLoginUrl(), MESSAGE => 'User ID cannot be retrieved'];
		}
		
		return [SUCCESSFUL => true, 'data' => $body];
	}
	
	static function postOnTimeLine($msg, $accessToken) {

		try {
			$data = ['message' => $msg];
			$request = self::getFB()->post('/me/feed', $data, $accessToken);
			$response = $request->getGraphUser();
		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			return [SUCCESSFUL => false, MESSAGE => 'Graph returned an error: ' . $e->getMessage(), 'login_url' => self::getLoginUrl()];
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			return [SUCCESSFUL => false, MESSAGE => 'Facebook SDK returned an error: ' . $e->getMessage(), 'login_url' => self::getLoginUrl()];
		}
		
		return [SUCCESSFUL => true, MESSAGE => 'Data sent', 'response' => $response->all(), 'login_url' => self::getLoginUrl()];
		
	}
	
	
	
	
	
	
	
	
	
// 	static function getUser($userData) {

// 		  $helper = self::getFB()->getRedirectLoginHelper();

// 		  try {
// 		  	if(isset($userData[FB_ACCESS_TOKEN])){
// 		  		$accessToken = $userData[FB_ACCESS_TOKEN]; 
// 		  	}else {
// 		  		$accessToken = $helper->getAccessToken();
// 		  	}
// 		  } catch(Facebook\Exceptions\FacebookResponseException $e) {
// 		  	// When Graph returns an error
// 		  	echo 'Graph returned an error: ' . $e->getMessage();
// 		  	exit;
// 		  } catch(Facebook\Exceptions\FacebookSDKException $e) {
// 		  	// When validation fails or other local issues
// 		  	echo 'Facebook SDK returned an error: ' . $e->getMessage();
// 		  	exit;
// 		  }
		  
// 		  if(!isset($accessToken)){

// 		  	$permissions = ['email', 'public_profile', 'user_friends', 'publish_actions']; // Optional permissions
// 		  	$loginUrl = $helper->getLoginUrl( 'http://' . $_SERVER['HTTP_HOST'] . getAddr(null), $permissions);
		  	 
// 		  	echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
// 		  	exit;
// 		  }
		  
// 	  	// The OAuth 2.0 client handler helps us manage access tokens
// 	  	$oAuth2Client = self::getFB()->getOAuth2Client();
	  			  	
// 	  	if (!$accessToken->isLongLived()) {
// 	  		// Exchanges a short-lived access token for a long-lived one
// 	  		try {
// 	  			$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
// 	  		} catch (Facebook\Exceptions\FacebookSDKException $e) {
// 	  			echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
// 	  			exit;
// 	  		}
	  	
// 	  		$userData[FB_ACCESS_TOKEN] = $accessToken->getValue();
// 	  		$userData->save();
	  		
// 	  		self::getFB()->setDefaultAccessToken($userData[FB_ACCESS_TOKEN]);
// 	  	}
		  	
		  

// 	  	try {
// 	  		// Get the \Facebook\GraphNodes\GraphUser object for the current user.
// 	  		// If you provided a 'default_access_token', the '{access-token}' is optional.
// 	  		$response = self::getFB()->get('/me', $accessToken);
	  			
// 	  	} catch(\Facebook\Exceptions\FacebookResponseException $e) {
// 	  		// When Graph returns an error
// 	  		echo 'Graph returned an error: ' . $e->getMessage();
// 	  		exit;
// 	  	} catch(\Facebook\Exceptions\FacebookSDKException $e) {
// 	  		// When validation fails or other local issues
// 	  		echo 'Facebook SDK returned an error: ' . $e->getMessage();
// 	  		exit;
// 	  	}
	  	
// 	  	$me = $response->getGraphUser();
// 	  	dDie($me);
// 	  	echo 'Logged in as ' . $me->getName();
		
// 	}
	
}