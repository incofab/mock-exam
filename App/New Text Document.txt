<?php

use App\Parser\Parse;

/**
 * @var \FastRoute\RouteCollector $r
 */
$r;

$r->get($addr.'/rough', function(
){
//     $str = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzeXN0ZW1faWQiOiJhNzM0NmZlY2JmODY3N2RkNjhjODU5YWMwZWM3YWI0NCIsInBsYW4iOiIxIE1vbnRoIiwicGluIjoiMTIzNDU2Nzg5MCIsInN1YnNjcmlwdGlvbl9kYXRlIjoiMjAxOS0wMS0xMCAwOTo1MTowMyIsImV4cGlyeSI6IjIwMTktMDItMDkgMDk6NTE6MDMifQ.KAOJjCyVUm--pNRnsxStI0-zl09xvxg1ExQx3ThWHzE';
//     $jwt = new \App\Core\JWT();
//     dDie($jwt->decode($str, SECRET_KEY_JWT));
    
    function UniqueMachineID($salt = "") {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $temp = sys_get_temp_dir().DIRECTORY_SEPARATOR."diskpartscript.txt";
            if(!file_exists($temp) && !is_file($temp)) file_put_contents($temp, "select disk 0\ndetail disk");
            $output = shell_exec("diskpart /s ".$temp);
            $lines = explode("\n",$output);
            $result = array_filter($lines,function($line) {
                return stripos($line,"ID:")!==false;
            });
            if(count($result)>0) {
                $result = array_shift(array_values($result));
                $result = explode(":",$result);
                $result = trim(end($result));
            } else $result = $output;
        } else {
            $result = shell_exec("blkid -o value -s UUID");
            if(stripos($result,"blkid")!==false) {
                $result = $_SERVER['HTTP_HOST'];
            }
        }
        return md5($salt.md5($result));
    }
    
    
    die(UniqueMachineID());
    
    $time = (35*60)+(43);
    
    $hours = floor($time/(60*60));
    $remMins = $time%(60*60);
    $mins = floor($remMins/60);
    $sec = floor($remMins%60);
    
    dDie("hours = $hours, mins = $mins, sec = $sec");
});
    
$r->get($addr.'/dummy', function(){
//     $obj = new \App\Parser\Parse();
//     $ret = $obj->parseOptions(\App\Parser\Parse::INPUT_BASE_DIR.'Commerce/Commerce_2016_Option.min.htm');
//     dDie($ret);
//     dDie('--'.trim(last($ret)['content'], ' \n\r\t\0\x0B').'--');
//     return (new \App\Parser\ParseWAEC())->parse();
    return (new \App\Parser\Parse())->renameExplanationFiles();
//     return view('exams/exam_page', []);
    return view('dummy/index', []);
});
   
$r->get($addr.'/purify', function(){
    return (new \App\Parser\PurifyHTML())->purify();
});
$r->get($addr.'/zcount', function(){
    return (new \App\Parser\Parse())->zCount();
});
$r->get($addr.'/parse'.'/{for}', function($for){
    if($for == 'jamb') return (new \App\Parser\Parse())->parse();
});
$r->get($addr.'/output-json'.'/{for}', function($for){
    if($for == 'jamb') return \App\Parser\OutputJson::outputJamb();
});
$r->get($addr.'/encrypt-output-json'.'/{for}', function($for){
    if($for == 'jamb') return \App\Parser\FileCript::encriptJamb();
});

$r->get($addr.'/init', function(){
    require APP_DIR . 'migrate.php';
//     createAllTables();
});

$r->get($addr.'/seed', function(){
    require APP_DIR . 'faker/faker.php';
});
    
$r->post($addr.'/upload'.'/{for}', function($for){
    
    $allPost = json_decode($_POST['data'], true);
    
    $ret = 'Done';
    
    switch ($for) {
        case SESSIONS_TABLE:
            foreach ($allPost as $post){
                $ret = (new \App\Models\Sessions)->insert($post, true);
            }
        break;
        case COURSES_TABLE:
            foreach ($allPost as $post){
                $ret = (new \App\Models\Courses())->insert($post);
            }
        break;
        case SUMMARY_TABLE:
            foreach ($allPost as $post){
                $ret = (new \App\Models\Summary())->insert($post);
            }
        break;
        case QUESTIONS_TABLE:
            foreach ($allPost as $post){
                $ret = (new \App\Models\Questions())->insert($post);
            }
        break;
        case INSTRUCTIONS_TABLE:
            foreach ($allPost as $post) 
            {
                $ret = \App\Models\Instructions::create([
                    COURSE_CODE => $post[COURSE_CODE],
                    SESSION_ID  => $post[SESSION_ID],
                    INSTRUCTION => $post[INSTRUCTION],
                    FROM_ => $post[FROM_],
                    TO_   => $post[TO_],
                ]);
            }
        break;
        case PASSAGES_TABLE:
            foreach ($allPost as $post) 
            {
                $ret = \App\Models\Passages::create([
                    COURSE_CODE => $post[COURSE_CODE],
                    SESSION_ID  => $post[SESSION_ID],
                    PASSAGE => $post[PASSAGE],
                    FROM_ => $post[FROM_],
                    TO_   => $post[TO_],
                ]);
            }
        break;
        
        default:
            
        break;
    }
    dDie($ret);
});


?>