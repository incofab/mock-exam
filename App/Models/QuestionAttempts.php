<?php
namespace App\Models;


class QuestionAttempts extends BaseModel
{
    public $table = QUESTION_ATTEMPTS_TABLE;
	
    public $fillable = [
        TABLE_ID, EXAM_SUBJECT_ID, QUESTION_ID, ATTEMPT, ANSWER
	];
	
	public $rules_insert = [
	    'required'   => [ [EXAM_SUBJECT_ID], [QUESTION_ID], [ATTEMPT] ],
	    
	    'in'     => [ [ATTEMPT, ['A', 'B', 'C', 'D', 'E', 'CH'] ] ],
	];
	
	const STATUSES = [STATUS_ACTIVE, STATUS_ENDED, STATUS_EXPIRED, STATUS_PAUSED];
	
	const EXAM_TIME_ALLOWANCE = 100; // 100 seconds
	
	public $rules_update = [
		
	];
	
	function __construct($param = null)
	{
	    parent::__construct($param);
	}
	
	function insert(
	    $post,  
	    $studentData, 
	    \App\Models\ExamSubjects $examSubjectsModel,
	    \App\Models\Questions $questionModel,
	    \Carbon\Carbon $carbon
    ){	
	    $val = $this->validatorWrapper->initValidator($post);
	    
	    $val->rules($this->rules_insert);
	    $val->labels($this->labels);
	    
	    if (!$val->validate())
	    {
	        return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
	    }
// 	    dlog($post);
	    /** @var \App\Models\ExamSubjects $examSubject */
	    $examSubject = $examSubjectsModel->where(TABLE_ID, '=', $post[EXAM_SUBJECT_ID])->with(['exam'])->first();
	    
	    $exam = array_get($examSubject, 'exam');
// 	    dlog($examSubject->toArray());
// 	    dlog($exam->toArray());
	    if (!$examSubject)
	    {
	        return [SUCCESS => false, MESSAGE => 'Exam Subject record not found'];
	    }
	    
// 	    if($examSubject[STATUS] != STATUS_ACTIVE)
// 	    {
// 	        return [SUCCESS => false, MESSAGE => 'Exam subject inactive'];
// 	    }
	    
	    if (!$exam)
	    {
	        return [SUCCESS => false, MESSAGE => 'Exam record not found'];
	    }
	    
	    if($exam[STATUS] != STATUS_ACTIVE)
	    {
	        return [SUCCESS => false, MESSAGE => 'Exam inactive'];
	    }
	    
	    if($exam[STUDENT_ID] != $studentData[STUDENT_ID])
	    {
	        return [SUCCESS => false, MESSAGE => 'Student ID mismatch'];
	    }
	    
	    if(!$exam[START_TIME])
	    {
	        return [SUCCESS => false, MESSAGE => 'Exam has not started'];
	    }
	    elseif (!$exam[END_TIME] && $exam[PAUSED_TIME])
	    {
	        $elapsedTime = $carbon->parse($exam[START_TIME])->diffInSeconds($exam[PAUSED_TIME]);
	        
	        $timeRemaining = $exam[DURATION] - $elapsedTime;
	        
	        if($timeRemaining < self::EXAM_TIME_ALLOWANCE) // allowance, 100secons
	        {
	            $exam[STATUS] = STATUS_EXPIRED;
	            
	            $exam->save();
	            
    	        return [SUCCESS => false, MESSAGE => 'Time elapsed'];
	        }
	        
	        $exam[END_TIME] = $carbon->parse($exam[PAUSED_TIME])->addSeconds($timeRemaining)->toDateTimeString();

	        $exam[STATUS] = STATUS_ACTIVE;
	        
	        $exam->save();
	    }
	    elseif (($carbon->parse($exam[END_TIME])->getTimestamp()+self::EXAM_TIME_ALLOWANCE) < $carbon->now()->getTimestamp())
	    {
            $exam[STATUS] = STATUS_EXPIRED;
            
            $exam->save();
            
	        return [SUCCESS => false, MESSAGE => 'Time elapsed'];
	    }
	    
	    $questionAttempt = $this->where(EXAM_SUBJECT_ID, '=', $post[EXAM_SUBJECT_ID])
	                       ->where(QUESTION_ID, '=', $post[QUESTION_ID])->first();
	    
        if($questionAttempt)
        {
            $questionAttempt[ATTEMPT] = $post[ATTEMPT];
            
            $questionAttempt->save();
            
	        return [SUCCESS => true, MESSAGE => 'Question Attempt updated'];
        }

        $question = $questionModel->where(TABLE_ID, '=', $post[QUESTION_ID])->first();
        
        if(!$question)
        {
            return [SUCCESS => false, MESSAGE => 'Question not found'];
        }
        
		$arr = [];
		$arr[EXAM_SUBJECT_ID] = $post[EXAM_SUBJECT_ID];
		$arr[QUESTION_ID] = $post[QUESTION_ID];
		$arr[ATTEMPT] = $post[ATTEMPT];
		$arr[ANSWER] = $question[ANSWER];

        $data = $this->create($arr);
        
        if ($data)
		{
			return [SUCCESS => true, MESSAGE => 'Question Attempt recorded', 'data' => $data->toArray()];
		}
		
		return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	function getScore($examSubjectId)
	{
	    $sql = 'SELECT COUNT('.TABLE_ID.") AS count_query FROM "
	        .QUESTION_ATTEMPTS_TABLE.' WHERE '.EXAM_SUBJECT_ID.'=:exam_subject_id '
            .' AND '.ATTEMPT.' = '.ANSWER;
        
        $arr = [':exam_subject_id' => $examSubjectId];
        
        $superArray = $this->pdoQuery($sql, $arr);
        
        return array_get($superArray, 'count_query', 0);
	}

	
// 	/** @var \App\Models\Merchants */
// 	function merchant()
// 	{
// 	    return $this->hasOne(\App\Models\Merchants::class, USER_ID, TABLE_ID)->first();
// 	}
	
// 	function transactions()
// 	{
// 	    return $this->hasMany(\App\Models\Transactions::class, USER_ID, TABLE_ID)->get();
// 	}
}


?>