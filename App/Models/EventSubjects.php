<?php
namespace App\Models;


class EventSubjects extends BaseModel
{
    public $table = EVENT_SUBJECTS_TABLE;
	
    public $fillable = [
        TABLE_ID, EVENT_ID, COURSE_ID, COURSE_SESSION_ID, STATUS
	];
	
	public $rules_insert = [ 
	    'required'   => [ [EVENT_ID], [COURSE_SESSION_ID] ],
	    'integer'     => [ [EVENT_ID] ],
	];
	
	public $rules_update = [
		
	];
	
	function __construct($param = null)
	{
	    parent::__construct($param);
	}
	
	function validateParams($post, 
	    \App\Models\Events $eventsModel, 
	    \App\Models\Sessions $sessionsModel,
	    \App\Models\Courses $coursesModel
    ){
	    $val = $this->validatorWrapper->initValidator($post);
	    
	    $val->rules($this->rules_insert);
	    $val->labels($this->labels);
	    
	    if (!$val->validate())
	    {
	        return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
	    }
	    
	    if (!$eventsModel->where(TABLE_ID, '=', $post[EVENT_ID])->first())
	    {
	        return [SUCCESS => false, MESSAGE => 'Event not found'];
	    }
	    
	    if (!$coursesModel->where(TABLE_ID, '=', $post[COURSE_ID])->first())
	    {
	        return [SUCCESS => false, MESSAGE => 'Subject not found'];
	    }
	    
	    if (!$sessionsModel->where(TABLE_ID, '=', $post[COURSE_SESSION_ID])->first())
	    {
	        return [SUCCESS => false, MESSAGE => 'Session not found'];
	    }
	    
        return [SUCCESS => true, MESSAGE => ''];
	}
	
	function insert($postvalidatedPostData, \App\Core\CodeGenerator $codeGenerator)
	{	
		$arr = [];
		$arr[EVENT_ID] = htmlentities($postvalidatedPostData[EVENT_ID]);
		$arr[COURSE_ID] = htmlentities($postvalidatedPostData[COURSE_ID]);
		$arr[COURSE_SESSION_ID] = htmlentities($postvalidatedPostData[COURSE_SESSION_ID]);
		$arr[STATUS] = STATUS_ACTIVE;

        $data = $this->create($arr);
        
        if ($data)
		{
			return [SUCCESS => true, MESSAGE => 'Data recorded', 'data' => $data->toArray()];
		}
		
		return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	function multiSubjectInsert(
	    $post,
	    \App\Models\Events $eventsModel, 
	    \App\Models\Sessions $sessionsModel,
	    \App\Models\Courses $coursesModel,
	    \App\Core\CodeGenerator $codeGenerator
    ){	
        $val = $this->validatorWrapper->initValidator($post);
        
        $val->rules($this->rules_insert);
        $val->labels($this->labels);
        
        if (!$val->validate())
        {
            return [SUCCESS => false, MESSAGE => 'Validation failed:<br>'.formatValidationErrors($val->errors(), true) , 'val_errors' => $val->errors() ];
        }
        
        if (!$eventsModel->where(TABLE_ID, '=', $post[EVENT_ID])->first())
        {
            return [SUCCESS => false, MESSAGE => 'Event not found'];
        }
	    
        $sessionIDs = $post[COURSE_SESSION_ID]; 
	    
	    
	    foreach ($sessionIDs as $sessionId)
	    {
	        $acadSession = $sessionsModel->where(TABLE_ID, '=', $sessionId)->first();
                
            if (!$acadSession) return [SUCCESSFUL => false, MESSAGE => 'An invalid Academic Session was supplied'];

	        // No 2 courses (even if with different years shoub be in the save event)
	        //Check if this course code already exist in this event
            if($this->where(EVENT_ID, '=', $post[EVENT_ID])
                ->where(COURSE_ID, '=', $acadSession[COURSE_ID])->first()){
                return [SUCCESSFUL => false, MESSAGE => 'A subject cannot appear multiple times'];
            }
	            
    		$arr = [];
    		$arr[EVENT_ID] = $post[EVENT_ID];
    		$arr[COURSE_ID] = $acadSession[COURSE_ID];
    		$arr[COURSE_SESSION_ID] = $sessionId;
    		$arr[STATUS] = STATUS_ACTIVE;
    
            $data = $this->create($arr);
	    }
        
		return [SUCCESS => true, MESSAGE => 'Data recorded'];
	}

	function event()
	{
	    return $this->belongsTo(\App\Models\Events::class, EVENT_ID, TABLE_ID);
	}
	
	function course()
	{
	    return $this->belongsTo(\App\Models\Courses::class, COURSE_ID, TABLE_ID);
	}
	
	function session()
	{
	    return $this->belongsTo(\App\Models\Sessions::class, COURSE_SESSION_ID, TABLE_ID);
	}
	
}


?>