<?php
namespace App\Models;

class Session extends BaseModel{

	public $table = 'sessions';
	public $fillable = ['id', 'payload', 'last_activity'];
	
	public $timestamps = false;
	
}
?>