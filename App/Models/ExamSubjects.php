<?php
namespace App\Models;


class ExamSubjects extends BaseModel
{
    public $table = EXAM_SUBJECTS_TABLE;
	
    public $fillable = [
        TABLE_ID, EXAM_NO, COURSE_ID, COURSE_SESSION_ID, STATUS, SCORE, NUM_OF_QUESTIONS
	];
    
    const STATUSES = [STATUS_ACTIVE, STATUS_ENDED];
	
	public $rules_insert = [
	    'required'   => [ [EXAM_NO], [COURSE_SESSION_ID] ],
	    'digits'     => [ [EXAM_NO] ],
	];
	
	public $rules_update = [
		
	];
	
	function __construct($param = null)
	{
	    parent::__construct($param);
	}
	
	function validateParams($post, 
	    \App\Models\Exams $examModel,
	    \App\Models\Sessions $sessionsModel,
	    \App\Models\Courses $coursesModel
    ){
	    $val = $this->validatorWrapper->initValidator($post);
	    
	    $val->rules($this->rules_insert);
	    $val->labels($this->labels);
	    
	    if (!$val->validate())
	    {
	        return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
	    }
	    
	    if (!$examModel->where(EXAM_NO, '=', $post[EXAM_NO])->first())
	    {
	        return [SUCCESS => false, MESSAGE => 'Exam No not found'];
	    }
	    
	    if (!$coursesModel->where(TABLE_ID, '=', $post[COURSE_ID])->first())
	    {
	        return [SUCCESS => false, MESSAGE => 'Subject not found'];
	    }
	    
	    if (!$sessionsModel->where(TABLE_ID, '=', $post[COURSE_SESSION_ID])->first())
	    {
	        return [SUCCESS => false, MESSAGE => 'Session not found'];
	    }
	    
        return [SUCCESS => true, MESSAGE => ''];
	}
	
	function insert($postvalidatedPostData, \App\Core\CodeGenerator $codeGenerator)
	{	
		$arr = [];
		$arr[EXAM_NO] = htmlentities($postvalidatedPostData[EXAM_NO]);
		$arr[COURSE_ID] = htmlentities($postvalidatedPostData[COURSE_ID]);
		$arr[COURSE_SESSION_ID] = htmlentities($postvalidatedPostData[COURSE_SESSION_ID]);
		$arr[STATUS] = STATUS_ACTIVE;

        $data = $this->create($arr);
        
        if ($data)
		{
			return [SUCCESS => true, MESSAGE => 'Data recorded', 'data' => $data->toArray()];
		}
		
		return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	function multiSubjectInsert(
	    $selectedSessionIDs,
	    \App\Models\Exams $exam,
	    \App\Models\EventSubjects $eventSubjectsModel,
	    \App\Core\CodeGenerator $codeGenerator
    ){	
	    
	    foreach ($selectedSessionIDs as $sessionIdOrCourseCode)
	    {
	        $sessionIdOrCourseCode = trim($sessionIdOrCourseCode);
	        
	        $eventSubject = $eventSubjectsModel
    	        ->where(function ($query) use ($exam, $sessionIdOrCourseCode) {    	            
    	           $query->where(EVENT_ID, '=', $exam[EVENT_ID])
    	           ->where(COURSE_SESSION_ID, '=', $sessionIdOrCourseCode);
    	        })
//     	        ->orWhere(function ($query) use ($exam, $sessionIdOrCourseCode) {
//     	           $query->where(EVENT_ID, '=', $exam[EVENT_ID])
//     	           ->where(COURSE_ID, '=', $sessionIdOrCourseCode);
//     	        })
                ->first();
                
	        if (!$eventSubject) continue;
	        
    		$arr = [];
    		$arr[EXAM_NO] = $exam[EXAM_NO];
    		$arr[COURSE_ID] = $eventSubject[COURSE_ID];
    		$arr[COURSE_SESSION_ID] = $eventSubject[COURSE_SESSION_ID];
    		$arr[STATUS] = STATUS_ACTIVE;
    
            $data = $this->create($arr);
	    }
        
		return [SUCCESS => true, MESSAGE => 'Data recorded'];
	}
	
	function exam()
	{
	    return $this->belongsTo(\App\Models\Exams::class, EXAM_NO, EXAM_NO);
	}
	
	function course()
	{
	    return $this->belongsTo(\App\Models\Courses::class, COURSE_ID, TABLE_ID);
	}
	
	function session()
	{
	    return $this->belongsTo(\App\Models\Sessions::class, COURSE_SESSION_ID, TABLE_ID);
	}
	
	function questionAttempts()
	{
	    return $this->hasMany(\App\Models\QuestionAttempts::class, EXAM_SUBJECT_ID, TABLE_ID);
	}
	
}


?>