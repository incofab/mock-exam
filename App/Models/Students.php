<?php
namespace App\Models;


class Students extends BaseModel{

	public $table = STUDENTS_TABLE;
	
	public $fillable = [TABLE_ID, STUDENT_ID, EMAIL, FIRSTNAME, CENTER_CODE,
	    LASTNAME, DOB, PHONE_NO, BALANCE, BONUS_BALANCE, ZORANGA_BALANCE, POINTS,
	    ACTIVATED, ADDRESS, GENDER, STATUS
	];
	
	public $rules_insert = [
			'required'   => [ [FIRSTNAME], [LASTNAME] ],
// 			'length'     => [ [PHONE_NO, 11] ],
			'digits'     => [ [PHONE_NO] ],
			'email'      => [ [EMAIL] ],	
	];
	
	public $rules_update = [
	        'required' =>  [ [TABLE_ID] ],
			'email'    =>  [ [EMAIL] ],	
	];
		
	function __construct($param = null)
	{
	    parent::__construct($param);
	}
	
	function clearPreviousExamLoginSession($examNo)
	{
	    $allSessions = \App\Models\Session::all();
	    
	    foreach ($allSessions as $session)
	    {
	        $oldPayload  = unserialize(base64_decode($session['payload']));
	        
	        $sessionData = getValue($oldPayload, STUDENT_SESSION_DATA);
	        
	        if(getValue($sessionData, EXAM_NO) == $examNo)
	        {
	            $session->delete();
	        }
	    }
	}
	
	function validateStudent($post, $examCenter) 
	{
	    $val = $this->validatorWrapper->initValidator($post);
	    
	    $val->rules($this->rules_insert);
	    $val->labels($this->labels);
	    
	    if (!$val->validate())
	    {
	        return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
	    }
	    
// 	    if ($this->where(PHONE_NO, '=', $post[PHONE_NO])
// 	        ->where(CENTER_CODE, '=', $examCenter[CENTER_CODE])->first())
// 	    {
// 	        return [SUCCESS => false, MESSAGE => 'Phone no '.htmlentities($post[PHONE_NO]).' already exists'];
// 	    }
	    
	    return [SUCCESSFUL => true, MESSAGE => 'Valid'];
	}
	
	function multiInsert($validatedPost, $examCenter, \App\Core\CodeGenerator $codeGenerator) 
	{
		foreach ($validatedPost as $arr) 
		{
		    $arr[STUDENT_ID] = $this->generateStudentID($codeGenerator);
		    
            $this->create($arr);
		}
		
		return [SUCCESS => true, MESSAGE => 'All records inserted'];
	}
	
	function insert($post, $examCenter, \App\Core\CodeGenerator $codeGenerator) 
	{
        $val = $this->validateStudent($post, $examCenter);
        
		if($val[SUCCESSFUL] !== true) return $val;
		
		$arr = [];
		$arr[STUDENT_ID] = $this->generateStudentID($codeGenerator);
	    
		$upload = null;
		if(isset($_FILES[PROFILE_PIC]) && !empty($_FILES[PROFILE_PIC]['name']))
		{
		    $upload = $this->uploadProfilePhoto($_FILES, $arr[STUDENT_ID]);
		    
		    if(!$upload[SUCCESSFUL]) return $upload;
		}
		
		$arr[EMAIL] = htmlentities(getValue($post, EMAIL));
		$arr[FIRSTNAME] = htmlentities(getValue($post, FIRSTNAME));
		$arr[LASTNAME] = htmlentities(getValue($post, LASTNAME));
		$arr[PHONE_NO] = htmlentities(getValue($post, PHONE_NO, '-'));
		$arr[ADDRESS] = htmlentities(getValue($post, ADDRESS));
		$arr[CENTER_CODE] = $examCenter[CENTER_CODE];
		$arr[BALANCE] = 0;
		$arr[PROFILE_PIC] = getValue($upload, 'filename');

        $data = $this->create($arr);
        
        if ($data)
		{
			$msg = 'Registration successful, You can login now';

			return [SUCCESS => true, MESSAGE => $msg, 'data' => $data->toArray()];
		}
		
		return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	function edit($post, $examCenter) 
	{
	    $val = $this->initValidator($post);
		
		$val->rules($this->rules_update);
		$val->labels($this->labels);
	
		if (!$val->validate()) return [SUCCESS => false, MESSAGE => 'Input validation failed', 'val_errors' => $val->errors()];
		
		$oldData = $this->where(TABLE_ID, '=', $post[TABLE_ID])->first();
		
		if(!$oldData) return [SUCCESS => false, MESSAGE => 'Student\'s record not found'];
		
// 		if (($oldData[PHONE_NO] != $post[PHONE_NO]) && $this->where(PHONE_NO, '=', $post[PHONE_NO])->first())
// 		{
// 		    return [SUCCESS => false, MESSAGE => 'Phone no already exists'];
// 		}
		
		if ($oldData[CENTER_CODE] != $examCenter[CENTER_CODE])
		{
		    return [SUCCESS => false, MESSAGE => 'Student is not in your center'];
		}
		
		$upload = null;
		if(isset($_FILES[PROFILE_PIC]) && !empty($_FILES[PROFILE_PIC]['name']))
		{
		    $upload = $this->uploadProfilePhoto($_FILES, $oldData[STUDENT_ID]);
		    
		    if(!$upload[SUCCESSFUL]) return $upload;
		}
		
		$oldData[FIRSTNAME] = htmlentities(getValue($post, FIRSTNAME, $oldData[FIRSTNAME]));
		$oldData[LASTNAME] = htmlentities(getValue($post, LASTNAME, $oldData[LASTNAME]));
		$oldData[EMAIL] = htmlentities(getValue($post, EMAIL, $oldData[EMAIL]));
		$oldData[PHONE_NO] = htmlentities(getValue($post, PHONE_NO, $oldData[PHONE_NO]));
		
		$oldData[ADDRESS] = htmlentities(getValue($post, ADDRESS, $oldData[ADDRESS]));
		$oldData[PROFILE_PIC] = getValue($upload, 'filename', $oldData[ADDRESS]);
		
		$success = $oldData->save();
		
		if ($success)
		{
			return [SUCCESS => true, MESSAGE => 'Record updated successfully', 'data' => $oldData];
		}

		return [SUCCESS => false, MESSAGE => 'Error: Update failed'];
	}
	
	function uploadProfilePhoto($files, $studentId) 
	{
		if(!isset($files[PROFILE_PIC])) return [SUCCESSFUL => false, MESSAGE => 'Invalid File'];
		
		// First check if file type is image
		$fileType = $files[PROFILE_PIC]["type"];
		$validExtensions = array("jpeg", "jpg", "png");
		$maxFilesize = 100000; // 100kb 
		
		$valideFileTypes = ['image/png', 'image/jpg', 'image/jpeg'];
		
		$name = $files[PROFILE_PIC]["name"];
		
		$ext = pathinfo($name, PATHINFO_EXTENSION);
		
		if(!in_array($fileType, $valideFileTypes)) return [SUCCESSFUL => false, MESSAGE => 'Invalid file type'];
		
		if($files[PROFILE_PIC]["size"] > $maxFilesize) return [SUCCESSFUL => false, MESSAGE => 'File greater than 100kb'];
		
		if(!in_array($ext, $validExtensions)) return [SUCCESSFUL => false, MESSAGE => 'Invalid file Extension'];
		
		// Check if the file contains errors
		if($files[PROFILE_PIC]["error"] > 0) return [SUCCESSFUL => false, MESSAGE => "Return Code: " . $files[PROFILE_PIC]["error"]];
		
		// Now upload the file
		$filename = "$studentId.$ext";
		$destinationPath = APP_DIR . "../public/img/profile/$filename";
		 
		$tempPath = $files[PROFILE_PIC]['tmp_name'];  
		
		@move_uploaded_file($tempPath, $destinationPath); // Moving Uploaded file
		
		$this[PROFILE_PIC] = $filename;
		$this->save();
		
		return [SUCCESSFUL => true, MESSAGE => "File uploaded successfully", 'filename' => $filename];
	}

	function centerStudentsCount($centerCode)
	{
	    $sql = "SELECT COUNT(".TABLE_ID.") AS query_result FROM ".STUDENTS_TABLE
	    . " WHERE ".CENTER_CODE."=:center_code";
	    
	    $superArray = $this->pdoQuery($sql, [':center_code' => $centerCode]);
	    
	    return array_get($superArray, 'query_result', 0);
	}
	
	function generateStudentID(\App\Core\CodeGenerator $codeGenerator)
	{
	    $prefix = 'S-';
	    
	    $key = $prefix.$codeGenerator->generateRandomDigits(6);
	    
	    while($this->where(STUDENT_ID, '=', $key)->first())
	    {
	        $key = $prefix.$codeGenerator->generateRandomDigits(6);
	    }
	    return $key;
	}
		
	/** @var \App\Models\ExamCenters */
	function examCenter()
	{
	    return $this->hasOne(\App\Models\ExamCenters::class, CENTER_CODE, CENTER_CODE)->first();
	}
	
}

