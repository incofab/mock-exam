<?php

namespace App\Models;


class Sessions extends BaseModel {
    public $table = SESSIONS_TABLE;
    public $fillable = [TABLE_ID, COURSE_ID, CATEGORY, SESSION, GENERAL_INSTRUCTIONS, FILE_PATH, FILE_VERSION];
    
    public $rules_insert = [
        'required' => [ [COURSE_ID] ]
    ];
    
    public $rules_update = [];
    
    function insert($post)
    {
        $val = new \Valitron\Validator($post);
        
        $val->rules($this->rules_insert);
        $val->labels($this->labels);
        if (!$val->validate())
        {
            return [SUCCESSFUL => false, MESSAGE => 'Session: validation failed - '.formatValidationErrors($val->errors(), true), 'val_errors' => $val->errors()];
        }
        
        $course = \App\Models\Courses::where(TABLE_ID, '=', $post[COURSE_ID])
        ->with(['examContent'])->first();
        
        if (!$course)
        {
            return [SUCCESSFUL => false, MESSAGE => 'Course detail not found'];
        }
        
        if ($this->where(COURSE_ID, '=', $post[COURSE_ID])
            ->where(SESSION, '=', $post[SESSION])->first())
        {
            return [SUCCESSFUL => false, MESSAGE => "{$post[SESSION]} session already exist for this course ID = {$post[COURSE_ID]}"];
        }
        
        $arr = [];
        $arr[COURSE_ID] = $post[COURSE_ID];
        $arr[SESSION] = $post[SESSION];
        $arr[CATEGORY] = getValue($post, CATEGORY);
        $arr[GENERAL_INSTRUCTIONS] = $post[GENERAL_INSTRUCTIONS];
        
        $ret = $this->create($arr);
        
        if ($ret)
        {
            // Create Questions table
            $this->savePerQuestionsInstructions($post[COURSE_ID], $ret[TABLE_ID], static::joinUpInstruction($post));
            
            $this->savePassages($post[COURSE_ID], $ret[TABLE_ID], static::joinUpPassage($post));
            
            if(empty($course[IS_FILE_CONTENT_UPLOADED]) && $arr[FILE_PATH])
            {
                $course[IS_FILE_CONTENT_UPLOADED] = true;
                $course->save();
            }
            
            return [SUCCESSFUL => true, MESSAGE => 'Data recorded successfully'];
        }
        
        return [SUCCESSFUL => false, MESSAGE => 'Error: operation failed'];
    }
    
    function updateRecord($post)
    {
        $val = new \Valitron\Validator($post);
        
        $val->rules($this->rules_update);
        //Assign labels to the validator... Use to replace form name
        $val->labels($this->labels);
        if (!$val->validate())
        {
            return [SUCCESSFUL => false, MESSAGE => 'Input validation failed', 'val_errors' => $val->errors()];
        }
        
        $old = $this->where(TABLE_ID, '=', $post[TABLE_ID])
        ->with(['course', 'course.examContent'])->first();
        
        //Check if row exists
        if (!$old)
        {
            return [SUCCESSFUL => false, MESSAGE => 'There is no existing record, create new one', ];
        }
        
        $old[COURSE_ID] = $post[COURSE_ID];
        $old[SESSION] = $post[SESSION];
        $old[CATEGORY] = $post[CATEGORY];
        $old[GENERAL_INSTRUCTIONS] = $post[GENERAL_INSTRUCTIONS];
        
        if ($old->save())
        {
            $this->savePerQuestionsInstructions($post[COURSE_ID], $old[TABLE_ID], static::joinUpInstruction($post));
            $this->savePassages($post[COURSE_ID], $old[TABLE_ID], static::joinUpPassage($post));
            
            return [SUCCESSFUL => true, MESSAGE => 'Record updated successfully'];
        }
        
        return [SUCCESSFUL => false, MESSAGE => 'Error: Update failed'];
    }
    
    function savePerQuestionsInstructions($courseId, $sessionID, $post)
    {
        // First delete all the existing ones
        \App\Models\Instructions::where(COURSE_SESSION_ID, '=', $sessionID)->delete();
        
        foreach ($post as $data) {
            
            if(empty($data[INSTRUCTION]) || empty($data[FROM_]) || empty($data[TO_])) continue;
            if(!is_numeric($data[FROM_]) || !is_numeric($data[TO_])) continue;
            
            \App\Models\Instructions::create([
                COURSE_ID => $courseId,
                COURSE_SESSION_ID  => $sessionID,
                INSTRUCTION => $data[INSTRUCTION],
                FROM_ => $data[FROM_],
                TO_   => $data[TO_],
            ]);
            
        }
        
    }
    
    function savePassages($courseId, $sessionID, $post)
    {
        // First delete all the existing ones
        \App\Models\Passages::where(COURSE_SESSION_ID, '=', $sessionID)->delete();
        
        foreach ($post as $data) {
            
            if(empty($data[PASSAGE]) || empty($data[FROM_]) || empty($data[TO_])) continue;
            if(!is_numeric($data[FROM_]) || !is_numeric($data[TO_])) continue;
            
            \App\Models\Passages::create([
                COURSE_ID => $courseId,
                COURSE_SESSION_ID  => $sessionID,
                PASSAGE => $data[PASSAGE],
                FROM_ => $data[FROM_],
                TO_   => $data[TO_],
            ]);
            
        }
        
    }
    
    static function joinUpInstruction($post) {
        
        $arr = [];
        if(!isset($post[ALL_INSTRUCTION])) return $arr;
        
        $len = count(getValue($post[ALL_INSTRUCTION], INSTRUCTION, []));
        
        for ($i = 0; $i < $len; $i++) {
            $arr[] = [
                INSTRUCTION => $post[ALL_INSTRUCTION][INSTRUCTION][$i],
                FROM_ => $post[ALL_INSTRUCTION][FROM_][$i],
                TO_ => $post[ALL_INSTRUCTION][TO_][$i],
                TABLE_ID => getValue(getValue($post[ALL_INSTRUCTION], TABLE_ID), $i),
            ];
        }
        return $arr;
    }
    
    static function joinUpPassage($post) {
        
        $arr = [];
        if(!isset($post[ALL_PASSAGES])) return $arr;
        
        $len = count(getValue($post[ALL_PASSAGES], PASSAGE, []));
        
        for ($i = 0; $i < $len; $i++) {
            
            $arr[] = [
                PASSAGE => $post[ALL_PASSAGES][PASSAGE][$i],
                FROM_ => $post[ALL_PASSAGES][FROM_][$i],
                TO_ => $post[ALL_PASSAGES][TO_][$i],
                TABLE_ID => getValue(getValue($post[ALL_PASSAGES], TABLE_ID), $i),
            ];
            
        }
        
        return $arr;
    }
    
    
    
    
    
    
    
    function course() {
        return $this->belongsTo(\App\Models\Courses::class, COURSE_ID, TABLE_ID);
    }
    
    function questions() {
        return $this->hasMany(\App\Models\Questions::class, COURSE_SESSION_ID, TABLE_ID);
    }
    
    function instructions() {
        return $this->hasMany(\App\Models\Instructions::class, COURSE_SESSION_ID, TABLE_ID);
    }
    
    function passages() {
        return $this->hasMany(\App\Models\Passages::class, COURSE_SESSION_ID, TABLE_ID);
    }
    
    
    
}