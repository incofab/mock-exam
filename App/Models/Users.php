<?php
namespace App\Models;


class Users extends BaseModel{

	public $table = USERS_TABLE;
	
	public $fillable = [TABLE_ID, USERNAME, DISPLAY_USERNAME, PASSWORD, EMAIL, FIRSTNAME,
	    LASTNAME, DOB, PHONE_NO, BALANCE, BONUS_BALANCE, ZORANGA_BALANCE, POINTS,
	    ACTIVATED, BANK_NAME, ACCOUNT_NUMBER, ADDRESS, COUNTRY, IS_VIRTUAL_USER, GENDER,
	    AGE
	];
	
	const AVAILABLE_PAYMENT_METHODS = [
	    DEPOSIT_METHOD_BANK,
	    DEPOSIT_METHOD_BITCOIN,
	    DEPOSIT_METHOD_ONLINE,
	    DEPOSIT_METHOD_ZORANGA,
	];
	
	public $rules_insert = [
			'required'   => [ [USERNAME], [PASSWORD],  [PHONE_NO], [EMAIL], [FIRSTNAME], [LASTNAME] ],
			'length'     => [ [PHONE_NO, 11] ],
			'digits'     => [ [PHONE_NO] ],
			'username'   => [ [USERNAME] ],
			'lengthMin'  => [ [USERNAME, 4], [PASSWORD, 6] ],
			'lengthMax'  => [ [USERNAME, 40] ],
			'email'      => [ [EMAIL] ],	
// 	        'in'         => [ [COUNTRY, self::COUNTRIES] ],
	];
	
	public $rules_update = [
			'required' =>  [ [USERNAME] ],
			'email'    =>  [ [EMAIL] ],	
	];
	
	/**
	 * @var \Session 
	 */
	private $session;
	/**
	 * @var \App\Core\Settings 
	 */
	private $settings;
	/**
	 * @var \App\Models\Session 
	 */
	private $sessionModel;
	
	function __construct($param = null)
	{
	    parent::__construct($param);

	    $this->session = $this->container->get(\Session::class); 
	    $this->settings = $this->container->get(\App\Core\Settings::class);
	    $this->sessionModel = $this->container->get(\App\Models\Session::class);
	}

	function getRequiredUserData($data) {
		return [
			TABLE_ID => getValue($data, TABLE_ID),
			USERNAME => getValue($data, USERNAME),
			EMAIL => getValue($data, EMAIL),
			FIRSTNAME => getValue($data, FIRSTNAME),
			LASTNAME => getValue($data, LASTNAME),
// 			DOB => getValue($data, DOB),
			PHONE_NO => getValue($data, PHONE_NO),
// 			ADDRESS => getValue($data, ADDRESS),
// 			COUNTRY => getValue($data, COUNTRY),
// 			STATUS => getValue($data, STATUS),
// 			FAVOURITES => getValue($data, FAVOURITES),
// 			BONUS_BALANCE => number_format(getValue($data, BONUS_BALANCE), 2),
	        BALANCE => $data[BALANCE],
// 	        ZORANGA_BALANCE => number_format($this->calcZeeBalance($data[BALANCE], $data[ZORANGA_BALANCE]), 2),
			BANK_NAME => getValue($data, BANK_NAME),
			ACCOUNT_NUMBER => getValue($data, ACCOUNT_NUMBER),
			GENDER => getValue($data, GENDER),
// 			AGE => getValue($data, AGE),
// 			POINTS => getValue($data, POINTS),
			PROFILE_PIC => getValue($data, PROFILE_PIC),
		];
	}
	
	function save(array $options = array())
	{
		$isSaved = parent::save($options);
		
		if(!$isSaved) return $isSaved;
		
		// Update session
		$sessionData = $this->session->get(USER_SESSION_DATA, false);
		
		// Update session if it is an action taken by the user
		if ($sessionData && $sessionData[TABLE_ID] == $this[TABLE_ID])
		{
			$this->session->put(USER_SESSION_DATA, $this->getRequiredUserData($this));
		}
		else 
		{
		    $this->updateUserSession();
		}
		
		return $isSaved;
	}
	
	function updateUserSession()
	{
	    // Reaching here means action was not taken by the user
	    // So, get all the sessions and check the one that matches the user
	    $allUserSessions = $this->sessionModel->all();
	    
	    foreach ($allUserSessions as $userSession)
	    {
	        $oldPayload  = unserialize(base64_decode($userSession['payload']));
	        
	        $sessionData = getValue($oldPayload, USER_SESSION_DATA);
	        
	        if(    getValue($sessionData, USERNAME) == $this[USERNAME]
	            && getValue($sessionData, TABLE_ID) == $this[TABLE_ID])
	        {
	            $oldPayload[USER_SESSION_DATA] = $this->getRequiredUserData($this);
	            
	            $this->sessionModel->where('id', '=', $userSession['id'])
	                   ->update(['payload' => base64_encode(serialize($oldPayload))]);
	            
	            break;
	        }
	    }
	}
	
	function insert($post) 
	{
		if(isset($post[PASSWORD_CONFIRMATION])){
			if($post[PASSWORD_CONFIRMATION] !== $post[PASSWORD]){
				return [SUCCESS => false, MESSAGE => 'Password mismatch'];
			}
		}
		
		$val = $this->validatorWrapper->initValidator($post);
		
		$val->rules($this->rules_insert);
		$val->labels($this->labels);
	
		if (!$val->validate())
		{ 
			return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
		}
		// Check if username, email, or phone number already exists
		if ($this->where(USERNAME, '=', $post[USERNAME])->first())
		{
			return [SUCCESS => false, MESSAGE => 'Username already exists'];
		}

		if ($this->where(PHONE_NO, '=', $post[PHONE_NO])->first())
		{
			return [SUCCESS => false, MESSAGE => 'Phone no already exists'];
		}
	    
		$arr = [];
		$arr[USERNAME] = htmlentities($post[USERNAME]);
		$arr[PASSWORD] = cryptPassword($post[PASSWORD]);
		$arr[EMAIL] = htmlentities(getValue($post, EMAIL));
		$arr[FIRSTNAME] = htmlentities(getValue($post, FIRSTNAME));
		$arr[LASTNAME] = htmlentities(getValue($post, LASTNAME));
		$arr[PHONE_NO] = htmlentities($post[PHONE_NO]);
		$arr[BALANCE] = 0;
		$arr[ACTIVATED] = true;
		
		$arr[BANK_NAME] = htmlentities(getValue($post, BANK_NAME));
		$arr[ACCOUNT_NUMBER] = htmlentities(getValue($post, ACCOUNT_NUMBER));
		$arr[ADDRESS] = htmlentities(getValue($post, ADDRESS));

        $data = $this->create($arr);
        
        if ($data)
		{
			$msg = 'Registration successful, You can login now';

			return [SUCCESS => true, MESSAGE => $msg, 'data' => $data->toArray()];
		}
		
		return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	function edit($post) 
	{
	    $val = $this->initValidator($post);
		
		$val->rules($this->rules_update);
		$val->labels($this->labels);
	
		if (!$val->validate()) return [SUCCESS => false, MESSAGE => 'Input validation failed', 'val_errors' => $val->errors()];
		
		$oldData = $this->where(USERNAME, '=', $post[USERNAME])->first();
		
		if(!$oldData) return [SUCCESS => false, MESSAGE => 'User not found'];
			
		$oldData[FIRSTNAME] = htmlentities(getValue($post, FIRSTNAME, $oldData[FIRSTNAME]));
		$oldData[LASTNAME] = htmlentities(getValue($post, LASTNAME, $oldData[LASTNAME]));
		$oldData[EMAIL] = htmlentities(getValue($post, EMAIL, $oldData[EMAIL]));
		
		$oldData[BANK_NAME] = htmlentities(getValue($post, BANK_NAME, $oldData[BANK_NAME]));
		$oldData[ACCOUNT_NUMBER] = htmlentities(getValue($post, ACCOUNT_NUMBER, $oldData[ACCOUNT_NUMBER]));
		$oldData[ADDRESS] = htmlentities(getValue($post, ADDRESS, $oldData[ADDRESS]));
		
		$success = $oldData->save();
		
		if ($success)
		{
			return [SUCCESS => true, MESSAGE => 'Record updated successfully', 'data' => $oldData];
		}

		return [SUCCESS => false, MESSAGE => 'Error: Update failed'];
	}
	
	function uploadProfilePhoto($files) 
	{
		if(!isset($files[PROFILE_PIC])) return [SUCCESSFUL => false, MESSAGE => 'Invalid File'];
		
		// First check if file type is image
		$fileType = $files[PROFILE_PIC]["type"];
		$validExtensions = array("jpeg", "jpg", "png");
		$maxFilesize = 100000; // 100kb 
		
		$valideFileTypes = ['image/png', 'image/jpg', 'image/jpeg'];
		
		$name = $files[PROFILE_PIC]["name"];
		
		$ext = pathinfo($name, PATHINFO_EXTENSION);
		
		if(!in_array($fileType, $valideFileTypes)) return [SUCCESSFUL => false, MESSAGE => 'Invalid file type'];
		
		if($files[PROFILE_PIC]["size"] > $maxFilesize) return [SUCCESSFUL => false, MESSAGE => 'File greater than 100kb'];
		
		if(!in_array($ext, $validExtensions)) return [SUCCESSFUL => false, MESSAGE => 'Invalid file Extension'];
		
		// Check if the file contains errors
		if($files[PROFILE_PIC]["error"] > 0) return [SUCCESSFUL => false, MESSAGE => "Return Code: " . $files[PROFILE_PIC]["error"]];
		
		// Now upload the file
		$filename = "{$this[USERNAME]}.$ext";
		$destinationPath = APP_DIR . "../public/img/profile/$filename";
		 
		$tempPath = $files[PROFILE_PIC]['tmp_name'];  
		
		@move_uploaded_file($tempPath, $destinationPath); // Moving Uploaded file
		
		$this[PROFILE_PIC] = $filename;
		$this->save();
		
		return [SUCCESSFUL => true, MESSAGE => "File uploaded successfully", 'filename' => $filename];
	}
		
	function getTotalNumOfRealUsers()
	{
	    $sql = 'SELECT COUNT('.TABLE_ID.") AS count_users FROM {$this->getTable()}";
	    
	    $arr = [];
	    
	    $superArray = $this->pdoQuery($sql, $arr);
	    
	    return array_get($superArray, 'count_users', 0);
	}
		
	function getTotalUsersBalance()
	{
	    $sql = 'SELECT SUM('.BALANCE.") AS balance_sum FROM {$this->getTable()}";
	    
	    $arr = [];
	    
	    $superArray = $this->pdoQuery($sql, $arr);
	    
	    return array_get($superArray, 'balance_sum', 0);
	}
	
	function getBalance()
	{
	    return $this[BALANCE];
	}
	
	function isBalanceEnough($amount)
	{
	    return $this[BALANCE] >= $amount;
	}
	
	function debitUser($amount)
	{
	    $this[BALANCE] = $this[BALANCE] - $amount;
	    
	    $this->save();
	}
	
	function creditUser($amount)
	{
	    $this[BALANCE] = $this[BALANCE] + $amount;
	    
	    $this->save();
	}
	
	/** @var \App\Models\Merchants */
	function merchant()
	{
	    return $this->hasOne(\App\Models\Merchants::class, USER_ID, TABLE_ID)->first();
	}
	
	function transactions()
	{
	    return $this->hasMany(\App\Models\Transactions::class, USER_ID, TABLE_ID)->get();
	}
}


?>