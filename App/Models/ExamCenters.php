<?php
namespace App\Models;


class ExamCenters extends BaseModel
{
    public $table = EXAM_CENTERS_TABLE;
	
    public $fillable = [
        TABLE_ID, USERNAME, ADDED_BY, PASSWORD, EMAIL, PHONE_NO, 
        CENTER_CODE, CENTER_NAME, ADDRESS, STATUS,
	];
	
	public $rules_insert = [
			'required'   => [ [USERNAME], [PASSWORD] ],
// 			'length'     => [ [PHONE_NO, 11] ],
			'digits'     => [ [PHONE_NO] ],
			'username'   => [ [USERNAME] ],
			'lengthMin'  => [ [USERNAME, 4], [PASSWORD, 6] ],
			'lengthMax'  => [ [USERNAME, 40] ],
			'email'      => [ [EMAIL] ],	
	];
	
	public $rules_update = [
			'required' =>  [ [TABLE_ID] ],
			'email'    =>  [ [EMAIL] ],	
	];
	
	/**
	 * @var \Session 
	 */
	private $session;
	/**
	 * @var \App\Core\Settings 
	 */
	private $settings;
	/**
	 * @var \App\Models\Session 
	 */
	private $sessionModel;
	
	function __construct($param = null)
	{
	    parent::__construct($param);

	    $this->session = $this->container->get(\Session::class); 
	    $this->settings = $this->container->get(\App\Core\Settings::class);
	    $this->sessionModel = $this->container->get(\App\Models\Session::class);
	}

	function getRequiredUserData($data) 
	{
		return [
			TABLE_ID => getValue($data, TABLE_ID),
			USERNAME => getValue($data, USERNAME),
			EMAIL => getValue($data, EMAIL),
			FIRSTNAME => getValue($data, FIRSTNAME),
			LASTNAME => getValue($data, LASTNAME),
			PHONE_NO => getValue($data, PHONE_NO),
	        BALANCE => $data[BALANCE],
			BANK_NAME => getValue($data, BANK_NAME),
			ACCOUNT_NUMBER => getValue($data, ACCOUNT_NUMBER),
			GENDER => getValue($data, GENDER),
		];
	}
	
	function save(array $options = array())
	{
		$isSaved = parent::save($options);
		
		if(!$isSaved) return $isSaved;
		
		// Update session
		$sessionData = $this->session->get(USER_SESSION_DATA, false);
		
		// Update session if it is an action taken by the user
		if ($sessionData && $sessionData[TABLE_ID] == $this[TABLE_ID])
		{
			$this->session->put(USER_SESSION_DATA, $this->getRequiredUserData($this));
		}
		else 
		{
		    $this->updateUserSession();
		}
		
		return $isSaved;
	}
	
	function updateUserSession()
	{
	    // Reaching here means action was not taken by the user
	    // So, get all the sessions and check the one that matches the user
	    $allUserSessions = $this->sessionModel->all();
	    
	    foreach ($allUserSessions as $userSession)
	    {
	        $oldPayload  = unserialize(base64_decode($userSession['payload']));
	        
	        $sessionData = getValue($oldPayload, USER_SESSION_DATA);
	        
	        if(    getValue($sessionData, USERNAME) == $this[USERNAME]
	            && getValue($sessionData, TABLE_ID) == $this[TABLE_ID])
	        {
	            $oldPayload[USER_SESSION_DATA] = $this->getRequiredUserData($this);
	            
	            $this->sessionModel->where('id', '=', $userSession['id'])
	                   ->update(['payload' => base64_encode(serialize($oldPayload))]);
	            
	            break;
	        }
	    }
	}
	
	function insert(
	    $post, $admin,
	   \App\Core\CodeGenerator $codeGenerator   
    ){
		if(isset($post[PASSWORD_CONFIRMATION])){
			if($post[PASSWORD_CONFIRMATION] !== $post[PASSWORD]){
				return [SUCCESS => false, MESSAGE => 'Password mismatch'];
			}
		}
		
		$val = $this->validatorWrapper->initValidator($post);
		
		$val->rules($this->rules_insert);
		$val->labels($this->labels);
	
		if (!$val->validate())
		{ 
			return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
		}
		
		// Check if username, email, or phone number already exists
		if ($this->where(USERNAME, '=', $post[USERNAME])->first())
		{
			return [SUCCESS => false, MESSAGE => 'Username already exists'];
		}
		
		$arr = [];
		$arr[USERNAME] = htmlentities($post[USERNAME]);
		$arr[PASSWORD] = cryptPassword($post[PASSWORD]);
		$arr[EMAIL] = htmlentities(getValue($post, EMAIL));
		$arr[ADDED_BY] = $admin[USERNAME];
		$arr[PHONE_NO] = htmlentities(getValue($post, PHONE_NO, '-'));
		$arr[CENTER_CODE] = $this->generateCenterCode($codeGenerator);
		$arr[CENTER_NAME] = htmlentities($post[CENTER_NAME]);
		
		$arr[ADDRESS] = htmlentities(getValue($post, ADDRESS));
		$arr[STATUS] = STATUS_ACTIVE;

        $data = $this->create($arr);
        
        if ($data)
		{
			$msg = 'Registration successful, You can login now';

			return [SUCCESS => true, MESSAGE => $msg, 'data' => $data->toArray()];
		}
		
		return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	function edit($post) 
	{
	    $val = $this->initValidator($post);
		
		$val->rules($this->rules_update);
		$val->labels($this->labels);
	
		if (!$val->validate()) return [SUCCESS => false, MESSAGE => 'Input validation failed', 'val_errors' => $val->errors()];
		
		$oldData = $this->where(TABLE_ID, '=', $post[TABLE_ID])->first();
		
		if(!$oldData) return [SUCCESS => false, MESSAGE => 'User not found'];
		
		$oldData[PHONE_NO] = htmlentities(getValue($post, PHONE_NO, $oldData[PHONE_NO]));
		$oldData[CENTER_NAME] = htmlentities(getValue($post, CENTER_NAME, $oldData[CENTER_NAME]));
		$oldData[EMAIL] = htmlentities(getValue($post, EMAIL, $oldData[EMAIL]));
		$oldData[ADDRESS] = htmlentities(getValue($post, ADDRESS, $oldData[ADDRESS]));
		
		$success = $oldData->save();
		
		if ($success)
		{
			return [SUCCESS => true, MESSAGE => 'Record updated successfully', 'data' => $oldData];
		}

		return [SUCCESS => false, MESSAGE => 'Error: Update failed'];
	}
	
	function generateCenterCode(\App\Core\CodeGenerator $codeGenerator)
	{
	    $key = $codeGenerator->generateRandomDigits(4);
	    
	    while($this->where(CENTER_CODE, '=', $key)->first())
	    {
	        $key = $codeGenerator->generateRandomDigits(4);
	    }
	    return $key;
	}
	
// 	/** @var \App\Models\Merchants */
// 	function merchant()
// 	{
// 	    return $this->hasOne(\App\Models\Merchants::class, USER_ID, TABLE_ID)->first();
// 	}
	
	function students()
	{
	    return $this->hasMany(\App\Models\Students::class, CENTER_CODE, CENTER_CODE)
	    ->select(FIRSTNAME, LASTNAME, PHONE_NO, GENDER);
	}
}


?>