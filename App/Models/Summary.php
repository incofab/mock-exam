<?php
namespace App\Models;


class Summary extends BaseModel {
    public $table = SUMMARY_TABLE;
    
    public $rules_insert = [
        'required' =>  [ [COURSE_ID], [CHAPTER_NO], [TITLE], [SUMMARY] ],
    ];
    
    function insert($post) {
        
        $val = new \Valitron\Validator($post);
        
        $val->rules($this->rules_insert);
        $val->labels($this->labels);
        
        if (!$val->validate())
        {
            return [SUCCESSFUL => false, MESSAGE => 'Input validation failed', 'val_errors' => $val->errors()];
        }
        
        //Check if chapter_no already exists under the same courseName (chapter numbers must be unique
        if ($this->where(CHAPTER_NO, '=', $post[CHAPTER_NO])
            ->where(COURSE_ID, '=', $post[COURSE_ID])->first())
        {
            return [SUCCESSFUL => false, MESSAGE => 'This Chapter No already
					exists, Chapter Numbers must be unique'];
        }
        
        $this[COURSE_ID] = $post[COURSE_ID];
        $this[CHAPTER_NO] = $post[CHAPTER_NO];
        $this[TITLE] = $post[TITLE];
        $this[DESCRIPTION] = array_get($post, DESCRIPTION);
        $this[SUMMARY] = $post[SUMMARY];
        
        if ($this->save())
        {
            return [SUCCESSFUL => true, MESSAGE => 'Data recorded successfully'];
        }
        
        return [SUCCESSFUL => false, MESSAGE => 'Error: Data entry failed'];
    }
    
    function updateRecord($post)
    {
        $val = new \Valitron\Validator($post);
        
        $val->rules($this->rules_update);
        //Assign labels to the validator... Use to replace form name
        $val->labels($this->labels);
        
        if (!$val->validate())
        {
            return [SUCCESSFUL => false, MESSAGE => 'Input validation failed', 'val_errors' => $val->errors()];
        }
        
        $old = $this->where(TABLE_ID, '=', $post[TABLE_ID])->first();
        
        //Check if row exists
        if (!$old)
        {
            return [SUCCESSFUL => false, MESSAGE => 'There is no existing record, create new one'];
        }
        
        $old[COURSE_ID] = $post[COURSE_ID];
        $old[CHAPTER_NO] = $post[CHAPTER_NO];
        $old[TITLE] = $post[TITLE];
        $old[DESCRIPTION] = array_get($post, DESCRIPTION);
        $old[SUMMARY] = $post[SUMMARY];
        
        if ($old->save())
        {
            return [SUCCESSFUL => true, MESSAGE => 'Record updated successfully'];
        }
        
        return [SUCCESSFUL => false, MESSAGE => 'Error: Update failed'];
    }
    
    function course()
    {
        return $this->belongsTo(\App\Models\Courses::class, COURSE_ID, TABLE_ID);
    }
    
    
    
}