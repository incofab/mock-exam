<?php
namespace App\Models;


class Exams extends BaseModel
{
    public $table = EXAMS_TABLE;
	
    public $fillable = [
        TABLE_ID, EVENT_ID, STUDENT_ID, EXAM_NO, START_TIME, 
        END_TIME, PAUSED_TIME, STATUS, NUM_OF_QUESTIONS, SCORE
	];
	
	public $rules_insert = [
	    'required'   => [ [STUDENT_ID] ],
	    'digits'     => [ [EXAM_NO] ],
	    'integer'    => [ [EVENT_ID] ],
	];
	
	public $rules_update = [
		
	];
	
	function __construct($param = null)
	{
	    parent::__construct($param);
	}
	
	function validateParams($post, 
	    \App\Models\Students $studentModel
    ){
	    $val = $this->validatorWrapper->initValidator($post);
	    
	    $val->rules($this->rules_insert);
	    $val->labels($this->labels);
	    
	    if (!$val->validate())
	    {
	        return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
	    }
	    
	    if ($this->where(EVENT_ID, '=', $post[EVENT_ID])
	        ->where(STUDENT_ID, '=', $post[STUDENT_ID])->first())
	    {
	        return [SUCCESS => false, MESSAGE => 'This student has already been registered in this Event'];
	    }
	    
	    if (!$studentModel->where(STUDENT_ID, '=', $post[STUDENT_ID])->first())
	    {
	        return [SUCCESS => false, MESSAGE => 'Student record not found'];
	    }
	    
// 	    if (!$eventsModel->where(TABLE_ID, '=', $post[EVENT_ID])->first())
// 	    {
// 	        return [SUCCESS => false, MESSAGE => 'Event not found'];
// 	    }
	    
        return [SUCCESS => true, MESSAGE => ''];
	}
	
	function insert($post, 
	    \App\Core\CodeGenerator $codeGenerator,
	    \App\Models\Students $studentModel, 
	    \App\Models\Events $event
    ){	
        $post[EVENT_ID] = $event[TABLE_ID];
        
	    $val = $this->validateParams($post, $studentModel);
	    
	    if(!$val[SUCCESSFUL]) return $val;
	    
// 	    $event = $eventsModel->where(TABLE_ID, '=', $post[EVENT_ID])->first();
	    
		$arr = [];
		$arr[EVENT_ID] = $event[TABLE_ID];
		$arr[STUDENT_ID] = htmlentities($post[STUDENT_ID]);
		$arr[EXAM_NO] = $this->generateExamNo($event[CENTER_CODE], $codeGenerator); 
		$arr[STATUS] = STATUS_ACTIVE;

        $data = $this->create($arr);
        
        if ($data)
		{
			return [SUCCESS => true, MESSAGE => 'Data recorded', 'data' => $data];
		}
		
		return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	function pauseExam($examNo,
	    $studentData, \Carbon\Carbon $carbon,
	    \App\Models\QuestionAttempts $questionAttemptsModel,
	    \App\Models\Questions $questionsModel
    ){
        $exam = $this->where(EXAM_NO, '=', $examNo)->first();
        
        if (!$exam)
        {
            return [SUCCESS => false, MESSAGE => 'Exam record not found'];
        }
        
        if($exam[STATUS] != STATUS_ACTIVE)
        {
            return [SUCCESS => false, MESSAGE => 'Exam inactive'];
        }
        
        if($exam[STUDENT_ID] != $studentData[STUDENT_ID])
        {
            return [SUCCESS => false, MESSAGE => 'Student ID mismatch'];
        }
        
        if(!$exam[END_TIME])
        {
            return [SUCCESS => false, MESSAGE => 'Exam has no end time'];
        }
        elseif ($carbon->parse($exam[END_TIME])->getTimestamp() < $carbon->now()->getTimestamp())
        {
//             $exam[STATUS] = STATUS_EXPIRED;
//             $exam->save();
            
            $this->endExam($examNo, $studentData, $questionsModel, $questionAttemptsModel);
            
            return [SUCCESS => false, MESSAGE => 'Time elapsed'];
        }
        
        $exam[STATUS] = STATUS_PAUSED;
        
        $exam[PAUSED_TIME] = $carbon->now()->toDateTimeString();
        
        $exam[END_TIME] = null;
        
        $exam->save();
        (new \App\Controllers\Helpers\ExamHandler())->syncExamFile($exam);
        
        return [SUCCESS => true, MESSAGE => 'Exam paused'];
	}
	
	function pauseSelectedExam(
	    \App\Models\Exams $exam
    ){
        if (!$exam)
        {
            return [SUCCESS => false, MESSAGE => 'Exam record not found'];
        }
        
        if($exam[STATUS] != STATUS_ACTIVE)
        {
            return [SUCCESS => false, MESSAGE => 'Exam inactive'];
        }
        
        if(!$exam[END_TIME])
        {
            return [SUCCESS => false, MESSAGE => 'Exam has no end time'];
        }
        
        $exam[STATUS] = STATUS_PAUSED;
        
        $exam[PAUSED_TIME] = \Carbon\Carbon::now()->toDateTimeString();
        
        $exam[END_TIME] = null;
        
        $exam->save();
        
        (new \App\Controllers\Helpers\ExamHandler())->syncExamFile($exam);
        
        return [SUCCESS => true, MESSAGE => 'Exam paused'];
	}
	
	
	function endExam(
	    $examNo, $studentData, 
	    \App\Models\Questions $questionsModel,
	    \App\Models\QuestionAttempts $questionAttemptsModel
    ){
        /** @var \App\Models\Exams $exam */
	    $exam = $this->where(EXAM_NO, '=', $examNo)->where(STUDENT_ID, '=', $studentData[STUDENT_ID])
	           ->with(['examSubjects', 'event'])->first();
        
        if (!$exam)
        {
            return [SUCCESS => false, MESSAGE => 'Exam record not found'];
        }
        
        $examSubjects = $exam['examSubjects'];
        
        if($exam[STATUS] == STATUS_ENDED)
        {
            return [SUCCESS => false, MESSAGE => 'Exam already submitted'];
        }
        
        $totalScore = 0;
        $totalNumOfQuestions = 0;
        $examHandler = new \App\Controllers\Helpers\ExamHandler();
        /** @var \App\Models\ExamSubjects $examSubject */
        foreach ($examSubjects as $examSubject) 
        {
            $scoreDetail = $examHandler->calculateScoreFromFile($examSubject, $exam);
            
            $score = $scoreDetail['score'];//$questionAttemptsModel->getScore($examSubject[TABLE_ID]);
            
            $numOfQuestions = $scoreDetail['num_of_questions'];//$questionsModel->getNumOfQuestions($examSubject[COURSE_CODE], $examSubject[COURSE_SESSION_ID]);
            
            $examSubject[SCORE] = $score;
            
            $examSubject[NUM_OF_QUESTIONS] = $numOfQuestions;
            
            $examSubject[STATUS] = STATUS_ENDED;
            
            $examSubject->save();
            
            $totalScore += $score;
            
            $totalNumOfQuestions += $numOfQuestions;
            
//             dlog("totalScore = $totalScore");
        }
        
        $event = $exam['event'];
        
        $exam[STATUS] = STATUS_ENDED;
        $exam[SCORE] = $totalScore;
        $exam[NUM_OF_QUESTIONS] = $totalNumOfQuestions;
        
        $exam->save();
        $examHandler->syncExamFile($exam);
//         $event[STATUS] = STATUS_ENDED;
//         $event->save();
        
        return [SUCCESS => true, MESSAGE => 'Exam ended'];
	}
	
	function startExam(
	    $examNo, 
	    $student,
	    \App\Models\ExamSubjects $examSubjectModel,
	    \App\Models\Passages $passagesModel,
	    \App\Models\Instructions $instructionsModel,
	    \Carbon\Carbon $carbon,
	    \App\Models\Questions $questionsModel,
	    \App\Models\QuestionAttempts $questionAttemptsModel
    ){
        $exam = $this->where(EXAM_NO, '=', $examNo)->where(STUDENT_ID, '=', $student[STUDENT_ID])->first();
        
        if (!$exam)
        {
            return [SUCCESS => false, MESSAGE => 'Exam record not found'];
        }
        
        if($exam[STATUS] != STATUS_ACTIVE && $exam[STATUS] != STATUS_PAUSED)
        {
            return [SUCCESS => false, MESSAGE => 'Exam inactive'];
        }
        
        $event = $exam->event()->first();
        
        if($event[STATUS] != STATUS_ACTIVE)
        {
            return [SUCCESS => false, MESSAGE => "{$event[TITLE]} is not started yet"];
        }
        
        if ($exam[END_TIME] && ($carbon->parse($exam[END_TIME])->getTimestamp() < $carbon->now()->getTimestamp()))
        {
//             $exam[STATUS] = STATUS_EXPIRED;
//             $exam->save();

            $this->endExam($examNo, $student, $questionsModel, $questionAttemptsModel);
            
            return [SUCCESS => false, MESSAGE => 'Time elapsed, Exam ended'];
        }
        
        $examHandler = new \App\Controllers\Helpers\ExamHandler();
        
        $examSubjects = $examSubjectModel->where(EXAM_NO, '=', $examNo)
                        ->with(['course', 'session'])->get();
                        
        $arr = [];
        
        $contentRet = $examHandler->getContent($exam['event_id'], $exam['exam_no'], $exam['student_id'], false);
        
        $allQuestionAttempts = $contentRet[SUCCESSFUL] ? $contentRet['content']['attempts'] : null;
        
        /** @var \App\Models\ExamSubjects $examSubject */
        foreach ($examSubjects as $examSubject) 
        {
            $subject = $examSubject['course'];
            $session = $examSubject['session'];
//             $questionAttempts = $examSubject['questionAttempts'];
            $questions = $questionsModel->where(COURSE_SESSION_ID, '=', $session[TABLE_ID])->get();
//                             ->where(COURSE_CODE, '=', $subject[COURSE_CODE])
            
            $questionsFormated = [];
            
            $attemptedQuestionsFormated = [];
            
            foreach ($questions as $question) 
            {
                $questionsFormated[] = [
                    QUESTION_ID => $question[TABLE_ID],
                    QUESTION_NO => $question[QUESTION_NO],
                    QUESTION => $question[QUESTION],
                    OPTION_A => $question[OPTION_A],
                    OPTION_B => $question[OPTION_B],
                    OPTION_C => $question[OPTION_C],
                    OPTION_D => $question[OPTION_D],
                    OPTION_E => $question[OPTION_E],
                ];
            }
            
            $questionAttempts = array_get($allQuestionAttempts, $examSubject[TABLE_ID], []);
            
            foreach ($questionAttempts as $questionAttempt) 
            {
                $attemptedQuestionsFormated[] = [
                    QUESTION_ID => $questionAttempt[QUESTION_ID],
                    ATTEMPT => $questionAttempt[ATTEMPT],
                ];
            }
            
            $passages = $passagesModel->where(COURSE_SESSION_ID, '=', $session[TABLE_ID])->get();
            
            $instructions = $instructionsModel->where(COURSE_SESSION_ID, '=', $session[TABLE_ID])->get();
            
            $subjectDataFormatted = [
                EXAM_SUBJECT_ID => $examSubject[TABLE_ID],
                'session_id' => $session[TABLE_ID],
                'course_id' => $subject[TABLE_ID],
                'course_code' => $subject[COURSE_CODE],
                'course_title' => $subject[COURSE_TITLE],
                'year' => $session[SESSION],
                GENERAL_INSTRUCTIONS => $session[GENERAL_INSTRUCTIONS],
                'instructions' => $instructions->toArray(),
                'passages' => $passages->toArray(),
                'attempted_questions' => $attemptedQuestionsFormated,
                'questions' => $questionsFormated,
            ];
            
            $arr['all_exam_subject_data'][] = $subjectDataFormatted;
//             dlog($attemptedQuestionsFormated);
        }
        
        if($exam[STATUS] == STATUS_PAUSED)
        {
            $timeElapsed = $carbon->parse($exam[START_TIME])->diffInSeconds($carbon->parse($exam[PAUSED_TIME]), false); 
            
            $timeRemaining = $event[DURATION] - $timeElapsed;
            
            if ($timeRemaining < 2)
            {
                $this->endExam($examNo, $student, $questionsModel, $questionAttemptsModel);
                
                return [SUCCESS => false, MESSAGE => 'Exam was paused when time has already elapsed'];
            }
            
            $exam[START_TIME] = $carbon->now()->toDateTimeString();
            
            $exam[END_TIME] = $carbon->parse($exam[START_TIME])->addSeconds($timeRemaining)->toDateTimeString();
            
            $exam[STATUS] = STATUS_ACTIVE;
        }
        elseif(empty($exam[START_TIME])) 
        {
            $exam[START_TIME] = $carbon->now()->toDateTimeString();
            
            $exam[END_TIME] = $carbon->parse($exam[START_TIME])->addSeconds($event[DURATION])->toDateTimeString();
        }
        
        $exam->save();
        $examHandler->syncExamFile($exam);
        
        $arr['meta']['time_remaining'] = $carbon->now()->diffInSeconds($carbon->parse($exam[END_TIME]), false);
        $arr['exam'] = $exam->toArray();
        
        return [SUCCESS => true, MESSAGE => 'Exam started', 'data' => $arr];
	}
	
	function generateExamNo($centerCode, \App\Core\CodeGenerator $codeGenerator)
	{
	    $key = date('Y') . $centerCode . $codeGenerator->generateRandomDigits(4);
	    
	    while($this->where(EXAM_NO, '=', $key)->first())
	    {
    	    $key = date('Y') . $centerCode . $codeGenerator->generateRandomDigits(4);
	    }
	    
	    return $key;
	}
	
	function examSubjects()
	{
	    return $this->hasMany(\App\Models\ExamSubjects::class, EXAM_NO, EXAM_NO);
	}
	
	function student()
	{
	    return $this->belongsTo(\App\Models\Students::class, STUDENT_ID, STUDENT_ID);
	}
	
	function event()
	{
	    return $this->belongsTo(\App\Models\Events::class, EVENT_ID, TABLE_ID);
	}
	
}


?>