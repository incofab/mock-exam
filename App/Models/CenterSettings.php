<?php
namespace App\Models;


class CenterSettings extends BaseModel
{
    public $table = CENTER_SETTINGS_TABLE;
	
    public $fillable = [
        TABLE_ID, KEY, VALUE, CENTER_CODE, DESCRIPTION
	];
	
	public $rules_insert = [
	    'required'   => [ [KEY] ],
	    'lengthMax'  => [ [KEY, 200] ],
	];
	
	public $rules_update = [
	    'required'   => [ [TABLE_ID], [KEY]  ],
	    'lengthMax'  => [ [KEY, 200] ],
	];
	
	function __construct($param = null)
	{
	    parent::__construct($param);
	}
	
	function insert($post, $centerCode)
	{	
        $val = $this->validatorWrapper->initValidator($post);
        
        $val->rules($this->rules_insert);
        $val->labels($this->labels);
        
        if (!$val->validate())
        {
            return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
        }
	    
		$arr = [];
		$arr[KEY] = htmlentities($post[KEY]);
		$arr[VALUE] = array_get($post, VALUE);
		$arr[CENTER_CODE] = $centerCode;
		$arr[DESCRIPTION] = htmlentities(array_get($post, DESCRIPTION));

        $data = $this->create($arr);
        
        if ($data)
		{
			return [SUCCESS => true, MESSAGE => 'Data recorded', 'data' => $data];
		}
		
		return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	function edit($post, $centerCode)
	{
        $val = $this->validatorWrapper->initValidator($post);
        
        $val->rules($this->rules_update);
        $val->labels($this->labels);
        
        if (!$val->validate())
        {
            return [SUCCESS => false, MESSAGE => 'Validation failed', 'val_errors' => $val->errors() ];
        }
        
        $oldData = $this->where(TABLE_ID, '=', $post[TABLE_ID])
            ->where(CENTER_CODE, '=', $centerCode)->first();
        
        if (!$oldData)
        {
            return [SUCCESS => false, MESSAGE => 'Parent record not found'];
        }
        
        $arr = [];
        $oldData[KEY] = htmlentities(array_get($post, KEY, $oldData[KEY]));
        $oldData[VALUE] = array_get($post, VALUE, $oldData[VALUE]);
        $oldData[DESCRIPTION] = htmlentities(array_get($post, DESCRIPTION, $oldData[DESCRIPTION]));
        
        $success = $oldData->save();
        
        if ($success)
        {
            return [SUCCESS => true, MESSAGE => 'Data recorded', 'data' => $oldData];
        }
        
        return [SUCCESS => false, MESSAGE => 'Error: Data entry failed'];
	}
	
	
}


?>