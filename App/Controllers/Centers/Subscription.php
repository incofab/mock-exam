<?php
namespace App\Controllers\Centers;

class Subscription extends BaseCenter
{
    private $subscriptionPlan;

    private $centerSettingsModel;

    private $eventsModel;
	
	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session,
	    \App\Models\ExamCenters $examCenterModel, 
	    \App\Models\Events $eventsModel, 
	    \App\Models\CenterSettings $centerSettingsModel, 
	    \App\Core\SubscriptionPlan $subscriptionPlan
    ){
        parent::__construct($c, $session, $examCenterModel);
        
        $this->subscriptionPlan = $subscriptionPlan;
        
        $this->centerSettingsModel = $centerSettingsModel;
        
        $this->eventsModel = $eventsModel;
	    
	    $this->subscriptionPlan->init($this->getCenterData()[CENTER_CODE], getAddr('center_dashboard'));
	}

	function subscribe() 
	{
	    if(!$_POST)
	    {
	        $events = $this->eventsModel
	        ->where(CENTER_CODE, '=', $this->getCenterData()[CENTER_CODE])
	        ->orderBy(TABLE_ID, 'DESC')->take(50)->get();
	        
	        return $this->view('centers/subscribe', [
	            'subscriptionPlan' => $this->subscriptionPlan,
	            'events' => $events
	        ]);
	    }
	    
	    $pin = array_get($_POST, 'pin');
	    $eventId = array_get($_POST, 'event');
	    
	    $ret = $this->subscriptionPlan->generateSubscriptionData($pin, $eventId);
	    $ret = is_array($ret) ? $ret : json_decode($ret, true);
	    
	    if(!array_get($ret, SUCCESSFUL))
	    {
    	    $this->session->flash('error', getValue($ret, MESSAGE));
    	    
    	    $this->session->flash('post', $_POST);
    	    
    	    redirect_(getAddr(null));
	    }
	    
	    $subscriptionData = array_get($ret, SUBSCRIPTION_DATA);
	    
	    $plan = array_get($ret, 'plan');
	    
	    $eventId = array_get($ret, 'event');
	    
	    $numOfActivations = (int)array_get($ret, NUM_OF_ACTIVATIONS);
	    
	    $post = [
	        KEY => SUBSCRIPTION_DATA,
	        
	        VALUE => $subscriptionData,
	    ];
	    
	    $insertRet = $this->centerSettingsModel->insert($post, $this->getCenterData()[CENTER_CODE]);
	    
	    if(!$insertRet[SUCCESSFUL])
	    {
	        $this->session->flash('error', 'Pin accepted but Activation data failed to save.');
	        
	        $this->session->flash('post', $_POST);
	        
	        redirect_(getAddr(null));
	    }
	    
	    if($plan == \App\Core\SubscriptionPlan::SUBSCRIPTION_PLAN_PER_EVENT)
	    {
	        $event = $this->eventsModel->where(TABLE_ID, '=', $eventId)->first();
	        
	        if($event)
	        {
	            $event[NUM_OF_ACTIVATIONS] += $numOfActivations;
	            
	            $event->save();
	        }
	    }
	    
	    $msg = "You have successfully subscribed to our <b>"
	        .htmlentities(array_get($ret, 'plan'))
	        .'</b> Subscription Plan. <b>Thank You!!!</b>';
	        
        $this->session->flash('success', $msg);
        
        redirect_(getAddr('center_dashboard'));
	}


}





