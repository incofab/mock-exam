<?php
namespace App\Controllers\Centers;

class Center extends BaseCenter{
	
    private $passwordChange;
    private $browserDetector;
    private $cookieManager;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c,
        \Session $session,
        \App\Models\ExamCenters $centersModel, 
        \App\Core\Password\PasswordChange $passwordChange,
        \App\Core\BrowserDetector $browserDetector,
        \App\Core\CookieManager $cookieManager
    ){ 
        parent::__construct($c, $session, $centersModel);
        
        $this->cookieManager   = $cookieManager;
        $this->passwordChange  = $passwordChange;
        $this->browserDetector = $browserDetector;
    }
	
	/**
	 * Users are directed to this page after successful login
	 */
	function index(
	    \App\Models\Students $studentModel,
	    \App\Models\Events $eventsModel
    ){
		return $this->view('centers/index', [
		    'students_count' => $studentModel->centerStudentsCount($this->getCenterData()[CENTER_CODE]),
		    'events_count' => $eventsModel->centerEventsCount($this->getCenterData()[CENTER_CODE]),
		]);
	}
		
	function profile() 
	{
	    return $this->view('dashboard/profile', []);
	}
	
	function editProfile() 
	{
	    if(!$_POST)
	    {
	        return $this->view('dashboard/edit_profile', [
	            
	            'alertMsg' => $this->session->getFlash('alertMsg'),
	            
	            'post' => $this->session->getFlash('post', $this->getUserData()),
	            
	            'valErrors' => $this->session->getFlash('val_errors'),
	        ]);
	    }
	    
	    $userData = $this->getUserDataObj();
		
	    $_POST[USERNAME] = $userData[USERNAME];
	    
		$ret = $userData->edit($_POST);
		
		$this->session->flash($ret[SUCCESSFUL] ? SUCCESSFUL : 'error', $ret[MESSAGE]);
		
		if($ret[SUCCESSFUL]) redirect_(getAddr('user_dashboard'));
		
		if(!empty($ret['val_errors'])) $this->session->flash('val_errors', $ret['val_errors']);
		
		$this->session->flash('post', $_POST);
		
		redirect_(null);
	}

	function logout() 
	{
		// Delete remeber login cookie
// 		$this->cookieManager->deleteLoginCookie('user'); 
		
		return $this->logout_('center_login');
	}
	
	function changePassword() 
	{
		if(!$_POST)
		{
		    return $this->view('dashboard/change_password');
		}
		
		$ret = $this->passwordChange->changeUserPassword($_POST, $this->getUserDataObj());
		
		$this->session->flash($ret[SUCCESSFUL] ? SUCCESSFUL : 'error', $ret[MESSAGE]);
		
		if($ret[SUCCESSFUL]) redirect_(getAddr('user_dashboard'));
		
		redirect_(null);
	}	

	function getProfileUploadForm() 
	{ 
	    return $this->view('api/profile_pic_upload_form', ['data' => $this->getUserData()]); 
	}
	
}