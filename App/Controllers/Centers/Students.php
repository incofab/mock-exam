<?php
namespace App\Controllers\Centers;

class Students extends BaseCenter
{
	private $studentsModel;  
	private $eventsModel;  
	private $codeGenerator; 
	private $smsHandle;
	
	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session,
	    \App\Models\ExamCenters $centersModel, 
	    \App\Models\Students $studentsModel, 
	    \App\Models\Events $eventsModel, 
	    \App\Core\CodeGenerator $codeGenerator,
	    \App\Core\SMSHandler $smsHandle
    ){
        parent::__construct($c, $session, $centersModel);
        
        $this->studentsModel = $studentsModel;
        
        $this->eventsModel = $eventsModel;
        
        $this->codeGenerator = $codeGenerator; 
        
        $this->smsHandle = $smsHandle;
	}

	function all() 
	{
	    $allRecords = $this->studentsModel
    	    ->where(CENTER_CODE, '=', $this->getCenterData()[CENTER_CODE])
    	    ->skip($this->num * ($this->page - 1))
	        ->orderBy(TABLE_ID, 'DESC')->take($this->num)->get();
		
		return $this->view('centers/students/all', [
			'allRecords' => $allRecords,
		    'count' => $this->studentsModel->getCount(),
		]);
	}

	function add() 
	{
	    if(!$_POST)
	    {
    		return $this->view('centers/students/add', [
    		    'post' =>  $this->session->getFlash('post', []),
    		]);
	    }
	    
	    $ret = $this->studentsModel->insert($_POST, $this->data, $this->codeGenerator);
	    
	    if($ret[SUCCESS])
	    {
	        $this->session->flash('success', 'Student registered, Record exam subjects');
	        
	        redirect_(getAddr('center_register_exam', $ret['data'][STUDENT_ID]));
	    }
	    
	    $this->session->flash('val_errors', getValue($ret, 'val_errors'));
	    $this->session->flash('error', getValue($ret, MESSAGE));
	    $this->session->flash('post', $_POST);
	    
	    redirect_(getAddr(null));
	}

	function edit($tableId) 
	{
	    if(!$_POST)
	    {
    		return $this->view('centers/students/add', [
    		    'post' =>  $this->session->getFlash('post', $this->studentsModel->where(TABLE_ID, '=', $tableId)->first()),
    		    'edit' => true,
    		    'tableId' => $tableId
    		]);
	    }
	    
	    $_POST[TABLE_ID] = $tableId;
	    
	    $ret = $this->studentsModel->edit($_POST, $this->data);
	    
	    if($ret[SUCCESS])
	    {
	        $this->session->flash('success', $ret[MESSAGE]);
	        
	        redirect_(getAddr('center_view_all_students'));
	    }
	    
	    $this->session->flash('val_errors', getValue($ret, 'val_errors'));
	    $this->session->flash('post', $_POST);
	    
	    redirect_(getAddr(null));
	}
	
	function suspend($table_id) 
	{
	    $success = $this->studentsModel->where(TABLE_ID, '=', $table_id)
	        ->where(CENTER_CODE, '=', $this->data[CENTER_CODE])
			->update([STATUS => STATUS_SUSPENDED]);
		
		if ($success)
		{
			$this->session->flash('success', 'Student has been suspended');
		}
		else
		{
			$this->session->flash('success', 'Error suspending Student');
		}
		
		redirect_(getAddr('center_view_all_students'));
	}

	function unSuspend($table_id) 
	{
	    $success = $this->studentsModel->where(TABLE_ID, '=', $table_id)
	           ->where(CENTER_CODE, '=', $this->data[CENTER_CODE])
	           ->update([STATUS => STATUS_ACTIVE]);
		
		if ($success){
			$this->session->flash('success', 'Student has been unsuspended');
		}else{
			$this->session->flash('success', 'Error unsuspending Student');
		}
		
		redirect_(getAddr('center_view_all_students'));
	}
	
	function viewStudentDetail($table_id_or_studentId) 
	{
	    $studentData = $this->studentsModel->where(TABLE_ID, '=', $table_id_or_studentId)
	        ->where(CENTER_CODE, '=', $this->data[CENTER_CODE])
			->orWhere(STUDENT_ID, '=', $table_id_or_studentId) 
			->where(CENTER_CODE, '=', $this->data[CENTER_CODE])->first();
		
		return view('centers/students/profile', [
		    'studentData' => $studentData
		]);
	}

	function delete($table_id) 
	{
	    if($this->studentsModel->where(TABLE_ID, '=', $table_id)
	        ->where(CENTER_CODE, '=', $this->data[CENTER_CODE])->delete())
		{
			$this->session->flash('success', 'Student deleted successfully');
		}
		else
		{
			$this->session->flash('error', 'Error deleting Student');
		}
		redirect_(getAddr('center_view_all_students'));
	}

	function multiDeleteStudents() 
	{
	    $studentIDs = explode(',', array_get($_POST, STUDENT_ID));

	    if(empty($studentIDs))
		{
			$this->session->flash('success', 'No student selected');

			redirect_(getAddr('center_view_all_students'));
		}
	    
		$builder = $this->studentsModel->where(CENTER_CODE, '=', $this->getCenterData()[CENTER_CODE])
		  ->whereIn(STUDENT_ID, $studentIDs);
	    	    
	    $builder->delete();
	    
		$this->session->flash(SUCCESSFUL, 'Students deleted');
		
		redirect_(getAddr('center_view_all_students'));
	}

	function uploadStudents(
	    \App\Controllers\Helpers\StudentsUploadHelper $studentsUploadHelper
    ){
	    
        if(!$_POST)
        {
            $events = $this->eventsModel->getActiveEvents($this->getCenterData()[CENTER_CODE]);
            
            return  $this->view('centers/students/upload_students', [
                
                'error' => \Session::getFlash('error') ,
                
                'success' => \Session::getFlash('success'),
                
                'report' => \Session::getFlash('report'),
                
                'events' => $events,
            ]);
        }
        
        $event = null;
        
        if(!empty($_POST[EVENT_ID]))
        {
            $event = $this->eventsModel->where(TABLE_ID, '=', $_POST[EVENT_ID])->first();
            
            if(!$event)
            {
                $this->session->flash('error', 'Event not found');
                
                redirect_(null);
            }
            
            if ($event[CENTER_CODE] != $this->getCenterData()[CENTER_CODE])
            {
                $this->session->flash('error', 'This event does not belong to this center');
                
                redirect_(null);
            }
        }
        
        $ret = $studentsUploadHelper->uploadStudent($_FILES, $this->getCenterDataObj(), $event);
		
        if(!$ret[SUCCESSFUL])
        {
    		$this->session->flash('error', $ret[MESSAGE]);
            
    		if(isset($ret['val_errors'])) $this->session->flash('val_errors', $ret['val_errors']);
    		
    		redirect_(null);
        }
        
		$this->session->flash(SUCCESSFUL, $ret[MESSAGE]);
		
		redirect_(getAddr('center_view_all_students'));
    }
	
}





