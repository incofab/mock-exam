<?php
namespace App\Controllers\Centers;

class Exams extends BaseCenter
{
	private $studentsModel;  
	private $eventsModel;  
	private $examsModel;  
	private $examCenterModel;  
	private $examSubjectsModel;  
	private $questionsModel;  
	private $questionAttemptsModel;  
	private $sessionsModel;  
	private $coursesModel;  
	private $examHelper;  
	private $codeGenerator; 
	private $subscriptionPlan; 
	private $smsHandle;
	
	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session,
	    \App\Models\Events $eventsModel, 
	    \App\Models\ExamCenters $examCenterModel, 
	    \App\Models\Students $studentsModel, 
	    \App\Models\Exams $examsModel, 
	    \App\Models\ExamSubjects $examSubjectsModel, 
	    \App\Models\Questions $questionsModel, 
	    \App\Models\QuestionAttempts $questionAttemptsModel, 
	    \App\Models\Sessions $sessionsModel, 
	    \App\Models\Courses $coursesModel, 
	    \App\Controllers\Helpers\ExamHelper $examHelper, 
	    \App\Core\CodeGenerator $codeGenerator,
	    \App\Core\SubscriptionPlan $subscriptionPlan,
	    \App\Core\SMSHandler $smsHandle
    ){
        parent::__construct($c, $session, $examCenterModel);
        
        $this->studentsModel = $studentsModel;
        
        $this->examsModel = $examsModel;
        
        $this->eventsModel = $eventsModel;
        
        $this->examCenterModel = $examCenterModel;
        
        $this->examSubjectsModel = $examSubjectsModel;
        
        $this->questionsModel = $questionsModel;
        
        $this->questionAttemptsModel = $questionAttemptsModel;
        
        $this->sessionsModel = $sessionsModel;
        
        $this->coursesModel = $coursesModel;
        
        $this->examHelper = $examHelper;
        
        $this->codeGenerator = $codeGenerator; 
        
        $this->smsHandle = $smsHandle;
        
        $this->subscriptionPlan = $subscriptionPlan;
	}

	function all($eventId = null) 
	{
	    if(empty($eventId))
	    {
    	    $eventIds = $this->eventsModel
    	           ->where(CENTER_CODE, '=', $this->getCenterData()[CENTER_CODE])
    	           ->lists(TABLE_ID);
	    }
	    else 
	    {
    	    $eventIds = $this->eventsModel
    	           ->where(CENTER_CODE, '=', $this->getCenterData()[CENTER_CODE])
    	           ->where(TABLE_ID, '=', $eventId)
    	           ->lists(TABLE_ID);	        
	    }
	    
        $builder = $this->examsModel
        ->with(['examSubjects', 'examSubjects.session', 'examSubjects.session.course', 'student', 'event'])
	           ->whereIn(EVENT_ID, $eventIds);
        
        $allRecords = $builder->skip($this->num * ($this->page - 1))
	           ->orderBy(TABLE_ID, 'DESC')->take($this->num)->get();
	           
		return $this->view('centers/exams/all', [
			'allRecords' => $allRecords,
		    'count' => $builder->get()->count(),
		    'eventId' => $eventId
		]);
	}

	function registerExam($studentId = null) 
	{
       
	   if(!$_POST)
	   {
            $studentId = array_get($_REQUEST, STUDENT_ID, $studentId);
            
            $student = $this->studentsModel->where(STUDENT_ID, '=', $studentId)
               ->where(CENTER_CODE, '=', $this->data[CENTER_CODE])->first();
//                ->orWhere(TABLE_ID, '=', $studentId)
//                ->where(CENTER_CODE, '=', $this->data[CENTER_CODE])->first();
            
            if(!$student)
            {
            	return $this->view('centers/exams/enter_student_id');
            }
            
            /* Getting here means, student is available */
            
            $events = $this->eventsModel->getActiveEvents($this->getCenterData()[CENTER_CODE]);
            
            if(!$events->first())
            {
                $this->session->flash('error', 'First create an event');
                
                redirect_(getAddr('center_add_event'));
            }
            
            return $this->view('centers/exams/add', [
                'post' =>  $this->session->getFlash('post', []),
                'student' => $student,
                'events' => $events,
            ]);
	    }
	    
	    $eventId = array_get($_POST, EVENT_ID);
	    
	    $studentId = array_get($_POST, STUDENT_ID);
	    
	    $selectedSubjectSessionIDs = array_get($_POST, COURSE_SESSION_ID);
	    
	    $ret = $this->examHelper->registerExam($eventId, $studentId, 
	        $selectedSubjectSessionIDs, $this->getCenterDataObj());
	    
	    if(!$ret[SUCCESS])
	    {
    	    $this->session->flash('val_errors', getValue($ret, 'val_errors'));

    	    $this->session->flash('error', getValue($ret, MESSAGE));
    	    
    	    $this->session->flash('post', $_POST);
    	    
    	    redirect_(getAddr(null));
	    }
	    
        $this->session->flash('success', $ret[MESSAGE]);
        
        redirect_(getAddr('center_view_all_exams'));
	}

	function multiRegisterExam($eventId) 
	{
       $event = $this->eventsModel->where(TABLE_ID, '=', $eventId)
       ->with(['eventSubjects', 'eventSubjects.course'])->first();
	    
       if(!$event)
       {
           $this->session->flash('error', 'Event not found');
           
           redirect_(getAddr('center_view_all_events'));
       }
       
       if ($event[CENTER_CODE] != $this->getCenterData()[CENTER_CODE])
       {
           $this->session->flash('error', 'This event does not belong to this center');
           
           redirect_(getAddr('center_view_all_events'));
       }
       
	   if(!$_POST)
	   {
	       $alreadyRegisteredStudentsID = $this->examsModel
	           ->where(EVENT_ID, '=', $eventId)->lists(STUDENT_ID);
	       
           $eligibleStudents = $this->studentsModel
               ->where(CENTER_CODE, '=', $this->data[CENTER_CODE])
               ->whereNotIn(STUDENT_ID, $alreadyRegisteredStudentsID)
               ->take(40)->get();
	        
           if(!$eligibleStudents->first())
	       {
	           $this->session->flash('error', 'No more students to register');
	            
	           redirect_(getAddr('center_view_all_events'));
	       }
	       
        	return $this->view('centers/exams/multi_register_exam', [
        		    'post' =>  $this->session->getFlash('post', []),
        		    'students' => $eligibleStudents,
        		    'event' => $event,
        		    'eventSubjects' => $event['eventSubjects'],
        		]);
	    }
	    
	    $students = array_get($_POST, STUDENT_ID);
	    $sessions = array_get($_POST, COURSE_SESSION_ID);
	    
	    $len = count($students);
	    
	    ini_set('max_execution_time', 1440);  
	    
	    for ($i = 0; $i < $len; $i++) 
	    {
	        $studentId = $students[$i];
	        
	        if(empty($sessions[$i])) continue;
	        
	        $session = $sessions[$i];
	        
	        $this->examHelper->registerExam($event, $studentId, $session, $this->getCenterDataObj());
	    }
	    
	    $this->session->flash(SUCCESSFUL, 'Exams recorded');
	    
	    redirect_(getAddr('center_view_all_exams'));
	}

	function edit($tableId) 
	{
	    die('Not implemented, delete instead');
	    
	    if(!$_POST)
	    {
    		return $this->view('centers/students/add', [
    		    'post' =>  $this->session->getFlash('post', $this->examsModel->where(TABLE_ID, '=', $tableId)->first()),
    		    'edit' => true,
    		    'tableId' => $tableId
    		]);
	    }
	    
	    $ret = $this->examsModel->edit($_POST, $this->data);
	    
	    if($ret[SUCCESS])
	    {
	        $this->session->flash('success', $ret[MESSAGE]);
	        
	        redirect_(getAddr('center_view_all_exams'));
	    }
	    
	    $this->session->flash('val_errors', getValue($ret, 'val_errors'));
	    $this->session->flash('post', $_POST);
	    
	    redirect_(getAddr(null));
	}
	
	function suspend($table_id) 
	{
	    $ret = $this->verifyExamID($table_id);
	    
	    if(!$ret[SUCCESSFUL])
	    {
	        $this->session->flash('error', $ret[MESSAGE]);
	        
	        redirect_(getAddr('center_view_all_exams'));
	    }
	    
	    $exam = $ret['exam'];
	    
	    $success = $exam->update([STATUS => STATUS_SUSPENDED]);
		
		if ($success)
		{
			$this->session->flash('success', 'Exam has been suspended');
		}
		else
		{
			$this->session->flash('success', 'Error suspending Exams');
		}
		
		redirect_(getAddr('center_view_all_exams'));
	}

	function unSuspend($table_id) 
	{
	    $ret = $this->verifyExamID($table_id);
	    
	    if(!$ret[SUCCESSFUL])
	    {
	        $this->session->flash('error', $ret[MESSAGE]);
	        
	        redirect_(getAddr('center_view_all_exams'));
	    }
	    
	    $exam = $ret['exam'];
	    
	    $success = $exam->update([STATUS => STATUS_ACTIVE]);
		
		if ($success){
			$this->session->flash('success', 'Exam has been unsuspended');
		}else{
			$this->session->flash('success', 'Error unsuspending Exam');
		}
		
		redirect_(getAddr('center_view_all_exams'));
	}

	function forceEndExam($examNo) 
	{
	    $exam = $this->examsModel->where(EXAM_NO, '=', $examNo)->with(['student'])->first();
		
	    if (!$exam)
	    {
			$this->session->flash('error', 'Exam not found');

			redirect_(getAddr('center_view_all_exams'));
		}
		
		$student = $exam['student'];
		
		$ret = $this->examsModel->endExam($examNo, $student, 
		    $this->questionsModel, $this->questionAttemptsModel);
		
		$this->session->flash($ret[SUCCESS]?SUCCESSFUL:'error', $ret[MESSAGE]);
		
		redirect_(getAddr('center_view_all_exams'));
	}
	

    function delete($table_id) 
    {
        $ret = $this->verifyExamID($table_id);
        
        if(!$ret[SUCCESSFUL])
        {
            $this->session->flash('error', $ret[MESSAGE]);
            
            redirect_(getAddr('center_view_all_exams'));
        }
        
        $exam = $ret['exam'];
        
        if($exam->delete())
        {
        	$this->session->flash('success', 'Exam deleted successfully');
        }
        else
        {
        	$this->session->flash('error', 'Error deleting Exam');
        }
        
        redirect_(getAddr('center_view_all_exams'));
    }
    
    function extendExamTime($examNo)
    {
        $exam = $this->examsModel->where(EXAM_NO, '=', $examNo)->with(['student', 'event'])->first();
        
        if(!$exam)
        {
            $this->session->flash('error', 'Exam record not found');
            
            redirect_(getAddr('center_view_all_exams'));
        }
        
        if($exam[STATUS] !== STATUS_PAUSED && empty($exam[END_TIME]))
        {
            $this->session->flash('error', 'Exam has not started yet');
            
            redirect_(getAddr('center_view_all_exams'));            
        }
        
        $event = $exam['event'];
        
        if(!$_POST)
        {
            $startTime = \Carbon\Carbon::parse($exam[START_TIME]);
            $pausedTime = \Carbon\Carbon::parse($exam[PAUSED_TIME]);
            $endTime = \Carbon\Carbon::parse($exam[END_TIME]);
            
            if($exam[STATUS] === STATUS_PAUSED){
                $timeElapsed = $startTime->diffInSeconds($pausedTime);
                $timeRemaining = $event[DURATION] - $timeElapsed;
            }
            else
            {
                $timeRemaining = \Carbon\Carbon::now()->diffInSeconds($endTime, false);
            }
            
            return $this->view('centers/exams/extend_time', [
                'timeRemaining' => \App\Core\Settings::splitTime($timeRemaining, true),
                'exam' => $exam,
                'student' => $exam['student'],
            ]);

        }

        $time = (int)array_get($_POST, 'extend_time');
        
        $ret = $this->examHelper->extendExam($exam, $time);
        
        $this->session->flash($ret[SUCCESSFUL]?'success':'error', $ret[MESSAGE]);
        
        redirect_(getAddr('center_view_all_exams'));
    }
    
	function viewExamResult($examNo, $studentID)
	{
	    $redirectAddr = array_get($_REQUEST, 'next', getAddr('center_view_all_exams'));
	    
	    return $this->displayExamResult($examNo, $studentID);
	}
	
	private function verifyExamID($examID)
	{
	    $exam = $this->examsModel->where(TABLE_ID, '=', $examID)
	       ->with(['event'])->first();
	    
	    if(!$exam) return [SUCCESSFUL => false, MESSAGE => 'Exam record not found'];
	    
	    $event = $exam['event'];
	    
	    if($event[CENTER_CODE] != $this->data[CENTER_CODE])
	    {
	        return [SUCCESSFUL => false, MESSAGE => 'This exam does not exist in this center'];
	    }
	    
        return [SUCCESSFUL => true, MESSAGE => '', 'exam' => $exam, 'event' => $event];
	}

}





