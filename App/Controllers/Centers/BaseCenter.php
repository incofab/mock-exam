<?php
namespace App\Controllers\Centers;


use App\Controllers\BaseController;
use App\Core\ErrorCodes;

class BaseCenter extends BaseController
{

    protected $centerData = null;
	
	protected $data = null;    
	
	protected $centerModel;     
	 
	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session, 
	    \App\Models\ExamCenters $centerModel, 
	    $fromAPI = false
    ){	
	    parent::__construct($c, $session);
	    
	    $this->centerModel = $centerModel;
	    
		$this->data = $this->session->get(CENTER_SESSION_DATA, null);
		
		if (!$this->data)
		{
			$this->session->flash('error', 'You are not logged in');
			
		    if($fromAPI) die(json_encode([SUCCESSFUL => false, MESSAGE => 'You are not logged in',
		        ERROR_CODE => ErrorCodes::UNATHORIZED_ACCESS]));
			
		    redirect_(getAddr('center_login', '?next=' . getAddr('')));
		}
	}
	
	protected function view($view, array $data = [])
	{
	    $data['data'] = isset($data['data']) ? $data['data'] : $this->data;
	    
	    $data['isCenter'] = isset($data['isCenter']) ? $data['isCenter'] : true;
	    
	    return parent::view($view, $data);
	}
	
	function getCenterData()  
	{
	    return $this->data;
	}
	
	function setCenterData($data) 
	{
	    $this->data = $data;
	}
	
	/**
	 * @return \App\Models\ExamCenters
	 */
	function getCenterDataObj() 
	{
	    if($this->centerData) return $this->centerData;
	    
	    $this->centerData = $this->centerModel->where(USERNAME, '=', getValue($this->data, USERNAME))
	               ->first();
	    
       if(!$this->centerData)
       {
           $this->session->put(CENTER_SESSION_DATA, null);
           die(json_encode([SUCCESSFUL => false, 
               MESSAGE => 'Unknow error, cannot retrieve user data',
               ERROR_CODE => ErrorCodes::UNATHORIZED_ACCESS]));
       }
       
	    return $this->centerData;
	}
	
		
	
}