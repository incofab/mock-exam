<?php
namespace App\Controllers\Centers;

use App\Models\BaseModel;
use App\Models\Exams;

class Events extends BaseCenter
{
    private $examsModel;   
	private $eventsModel;   
	private $eventsHelper;   
	private $examCenterModel;  
	private $eventSubjectsModel;  
	private $sessionsModel;  
	private $coursesModel;  
	private $codeGenerator; 
	private $smsHandle;
	private $resultsDir = APP_DIR.'../public/files/';
	
	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session,
	    \App\Models\ExamCenters $examCenterModel, 
	    \App\Models\Exams $examsModel, 
	    \App\Models\Events $eventsModel, 
	    \App\Models\EventSubjects $eventSubjectsModel,  
	    \App\Models\Sessions $sessionsModel, 
	    \App\Models\Courses $coursesModel, 
	    \App\Controllers\Helpers\EventHelper $eventsHelper, 
	    \App\Core\CodeGenerator $codeGenerator,
	    \App\Core\SMSHandler $smsHandle
    ){
        parent::__construct($c, $session, $examCenterModel);
        
        $this->eventsModel = $eventsModel; 
        
        $this->examCenterModel = $examCenterModel;
        
        $this->eventSubjectsModel = $eventSubjectsModel; 
        
        $this->sessionsModel = $sessionsModel;
        
        $this->coursesModel = $coursesModel;
        
        $this->eventsHelper = $eventsHelper;
        
        $this->codeGenerator = $codeGenerator; 
        
        $this->examsModel = $examsModel; 
        
        $this->smsHandle = $smsHandle;
	}

	function all() 
	{
	    $allRecords = $this->eventsModel
	           ->where(CENTER_CODE, '=', $this->getCenterData()[CENTER_CODE])
	           ->with(['eventSubjects'])
	           ->skip($this->num * ($this->page - 1))
	           ->orderBy(TABLE_ID, 'DESC')->take($this->num)->get();
		
		return $this->view('centers/events/all', [
			'allRecords' => $allRecords,
		    'count' => $this->eventsModel->where(CENTER_CODE, '=', $this->getCenterData()[CENTER_CODE])->get()->count(),
		]);
	}

	function createEvent() 
	{
	    if(!$_POST)
	    {
    		return $this->view('centers/events/add', [
    		    'post' =>  $this->session->getFlash('post', []),
    		    'subjects' => $this->coursesModel->all(),
    		]);
	    }
	    
	    BaseModel::beginTransaction();
	    
	    $duration = ((int)array_get($_POST, 'hours', 0)) * 60 * 60
            	    + ((int)array_get($_POST, 'minutes', 0)) * 60
            	    + (int)array_get($_POST, 'seconds', 0);
	    
	    $_POST[DURATION] = $duration;
	    $_POST[CENTER_CODE] = $this->getCenterData()[CENTER_CODE];
	    
	    $ret = $this->eventsModel->insert($_POST, $this->codeGenerator, $this->examCenterModel);
	    
	    if(!$ret[SUCCESS])
	    {
	        BaseModel::rollBack();
    	    $this->session->flash('val_errors', getValue($ret, 'val_errors'));

    	    $this->session->flash('error', getValue($ret, MESSAGE));
    	    
    	    $this->session->flash('post', $_POST);
    	    
    	    redirect_(getAddr(null));
	    }
	    
	    /** @var \App\Models\Events $event */
	    $event = $ret['data']; 
	    
	    $post = [];
	    $post[EVENT_ID] = $event[TABLE_ID];
	    $post[COURSE_SESSION_ID] = array_get($_POST, COURSE_SESSION_ID);
	    
	    $ret = $this->eventSubjectsModel->multiSubjectInsert($post, $this->eventsModel, 
	        $this->sessionsModel, $this->coursesModel, $this->codeGenerator); 
	    
	    if(!$ret[SUCCESS])
	    {
	        BaseModel::rollBack();
	        $this->session->flash('error', getValue($ret, MESSAGE));
	        $this->session->flash('post', $_POST);
	        redirect_(getAddr(null));
	    }
	    
	    BaseModel::commit();
	    
        $this->session->flash('success', $ret[MESSAGE]);
        
        redirect_(getAddr('center_view_all_events'));
	}

	function edit($tableId) 
	{
// 	    die('Not implemented yet');
// 	    $this->examsModel->where(EVENT_ID, '=', $tableId)->first()
	    
	    /** @var \App\Models\Events $oldEvent */
	    $oldEvent = $this->eventsModel->where(TABLE_ID, '=', $tableId)
	               ->with(['eventSubjects'])->first();
//        dDie($oldEvent->toArray());
	    if(!$oldEvent)
	    {
	        $this->session->flash('error', 'Event record not found');
	        
	        redirect_(getAddr('center_view_all_events'));
	    }
	    $activeExam = $this->examsModel->where(EVENT_ID, '=', $tableId)
	       ->whereNotNull(START_TIME)->first();
       if($activeExam)
	    {	        
	        $this->session->flash('error', 'Cannot edit event because it contains some active exam(s)');
	        
	        redirect_(getAddr('center_view_all_events'));
	    }
	    
	    if(!$_POST)
	    {
    	    $splitTime = \App\Core\Settings::splitTime($oldEvent[DURATION]);
    	    $oldEvent['hours'] = $splitTime['hours'];
    	    $oldEvent['minutes'] = $splitTime['minutes'];
    	    $oldEvent['seconds'] = $splitTime['seconds'];
    	    $eventSubjects = $oldEvent['event_subjects'];
    	    $selectedSessionIDs = [];
    	    foreach ($eventSubjects as $eventSubjects) 
    	    {
    	        $selectedSessionIDs[] = $eventSubjects[COURSE_SESSION_ID];
    	    }
//     	    dDie($oldEvent->toArray());
//     	    $dum = $this->eventSubjectsModel->where(EVENT_ID, '=', $oldEvent[TABLE_ID]);
//     	    dDie(array_keys($dum->get([COURSE_SESSION_ID])->toArray()));
//     	    dDie($exams->first());
	        return $this->view('centers/events/add', [
	            'post' =>  $this->session->getFlash('post', $oldEvent),
	            'subjects' => $this->coursesModel->all(),
	            'selectedSessionIDs' => $selectedSessionIDs,
	            'edit' => true,
	            'oldEvent' => $oldEvent
	        ]);
	    }
	    
	    $duration = array_get($_POST, 'hours', 0) * 60 * 60
        	    + array_get($_POST, 'minutes', 0) * 60
        	    + array_get($_POST, 'seconds', 0);
	    
	    $_POST[DURATION] = $duration;
	    $_POST[TABLE_ID] = $tableId;
	    
	    BaseModel::beginTransaction();
	    
	    $ret = $this->eventsModel->edit($_POST);
	    
	    if(!$ret[SUCCESSFUL])
	    {
	        BaseModel::rollBack();
	        $this->session->flash('val_errors', getValue($ret, 'val_errors'));
	        $this->session->flash('error', getValue($ret, MESSAGE));
	        $this->session->flash('post', $_POST);
	        redirect_(getAddr(null));
	    }
	    
	    /** @var \App\Models\Events $event */
	    $event = $ret['data'];
	    
	    // Delete previous event subjects
	    $this->eventSubjectsModel->where(EVENT_ID, '=', $event[TABLE_ID])->delete();
	    
	    $post = [];
	    $post[EVENT_ID] = $event[TABLE_ID];
	    $post[COURSE_SESSION_ID] = array_get($_POST, COURSE_SESSION_ID);
	    
	    $ret = $this->eventSubjectsModel->multiSubjectInsert($post, $this->eventsModel,
	        $this->sessionsModel, $this->coursesModel, $this->codeGenerator);
	    
	    if(!$ret[SUCCESSFUL])
	    {
	        BaseModel::rollBack();
	        $this->session->flash('error', getValue($ret, MESSAGE));
	        $this->session->flash('post', $_POST);
	        redirect_(getAddr(null));
	    }
	    
	    BaseModel::commit();
	    
	    $this->session->flash('success', $ret[MESSAGE]);
	    
	    redirect_(getAddr('center_view_all_events'));
	}
	
	function suspend($table_id) 
	{
	    $success = $this->eventsModel->where(TABLE_ID, '=', $table_id) 
	        ->where(CENTER_CODE, '=', $this->getCenterData()[CENTER_CODE])
			->update([STATUS => STATUS_SUSPENDED]);
		
		if ($success)
		{
			$this->session->flash('success', 'Event has been suspended');
		}
		else
		{
			$this->session->flash('success', 'Error suspending Event');
		}
		
		redirect_(getAddr('center_view_all_events'));
	}

	function unSuspend($table_id) 
	{
	    $success = $this->eventsModel->where(TABLE_ID, '=', $table_id)
	           ->where(CENTER_CODE, '=', $this->getCenterData()[CENTER_CODE])
	           ->update([STATUS => STATUS_ACTIVE]);
		
		if ($success){
			$this->session->flash('success', 'Event has been unsuspended');
		}else{
			$this->session->flash('success', 'Error unsuspending Event');
		}
		
		redirect_(getAddr('center_view_all_events'));
	}

	function delete($table_id) 
	{
	    $activeExam = $this->examsModel->where(EVENT_ID, '=', $table_id)->first();
	    
	    if($activeExam)
	    {
	        $this->session->flash('error', 'Cannot delete event because it already contains some exam(s)');
	        
	        redirect_(getAddr('center_view_all_events'));
	    }
	    
	    if($this->eventsModel->where(TABLE_ID, '=', $table_id)
	        ->where(CENTER_CODE, '=', $this->getCenterData()[CENTER_CODE])->delete())
		{
			$this->session->flash('success', 'Event deleted successfully');
		}
		else
		{
			$this->session->flash('error', 'Error deleting Event');
		}
		
		redirect_(getAddr('center_view_all_events'));
	}
	
	function pauseAllExams($eventId) 
	{
	    $ret = $this->eventsHelper->pauseAllExam($eventId);
	    
        $this->session->flash($ret[SUCCESSFUL]?'success':'error', $ret[MESSAGE]);
	    
	    redirect_(getAddr('center_view_all_exams', [$eventId]));
	}
	
	function eventResult($event_id)
	{
	    $ret = $this->eventsHelper->getEventResult($event_id, 
	        $this->getCenterData()[CENTER_CODE], getAddr('center_view_all_events'));
	    
	    if(!$ret[SUCCESSFUL])
	    {
	        $this->session->flash('error', $ret[MESSAGE]);
	        
	        redirect_(getAddr('center_view_all_events'));
	    }
	    
	    return $this->view('centers/events/event_result', [
	        'allRecords' => $ret['result_list'],
	        'event' => $ret['event'],
	    ]);
	}
	
	function previewEvent($event_id)
	{
	    $event = $this->eventsModel->where(TABLE_ID, '=', $event_id)
	       ->with(['eventSubjects', 'eventSubjects.course', 'eventSubjects.session'])->first();
	    
       if(!$event)
	    {
	        $this->session->flash('error', 'Event not found');
	        
	        redirect_(getAddr('center_view_all_events'));
	    }
	    
	    return $this->view('centers/events/preview_event', [
	        'event' => $event,
	    ]);
	}
	
	function smsEventResult($event_id)
	{
	    if(!$_POST)
	    {
	        /** @var \App\Models\Events $event */
	        $event = $this->eventsModel->where(TABLE_ID, '=', $event_id)
	           ->where(CENTER_CODE, '=', $this->getCenterData()[CENTER_CODE])->first();
    	        
    	    if(!$event)
    	    {
    	        $this->session->flash('error', 'Event not found');
    	        
    	        redirect_(getAddr('center_view_all_events'));
    	    }
    	    
    	    return $this->view('centers/events/sms_event_result', [
    	        'event' => $event,
    	        'post' =>  $this->session->getFlash('post', []),
    	    ]);	        
	    }
	    
	    ini_set('max_execution_time', 960); 
	    
	    $ret = $this->eventsHelper->smsResult($event_id, 
	        array_get($_POST, USERNAME), array_get($_POST, PASSWORD),
	        $this->getCenterData()[CENTER_CODE], getAddr('center_view_all_events'));
	    
	    if(!$ret[SUCCESSFUL])
	    {
	        $this->session->flash('error', $ret[MESSAGE]);
	        
	        $this->session->flash('post', $_POST);
	        
	        redirect_(null);
	    }
	    
        $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);
        
        redirect_(getAddr('center_view_all_events'));
	}
	
	function smsInvite($event_id)
	{
        /** @var \App\Models\Events $event */
        $event = $this->eventsModel->where(TABLE_ID, '=', $event_id)
           ->where(CENTER_CODE, '=', $this->getCenterData()[CENTER_CODE])->first();
	        
	    if(!$event)
	    {
	        $this->session->flash('error', 'Event not found');
	        
	        redirect_(getAddr('center_view_all_events'));
	    }
	    
	    if(!$_POST)
	    {
    	    return $this->view('centers/events/sms_invite', [
    	        'event' => $event,
    	        'post' =>  $this->session->getFlash('post', []),
    	    ]);	        
	    }
	    	    
	    ini_set('max_execution_time', 960); 
	    
	    $ret = $this->eventsHelper->smsInvite($event, 
	        array_get($_POST, USERNAME), array_get($_POST, PASSWORD),
	        array_get($_POST, 'time'), getAddr('center_view_all_events'));
	    
	    if(!$ret[SUCCESSFUL])
	    {
	        $this->session->flash('error', $ret[MESSAGE]);
	        
	        $this->session->flash('post', $_POST);
	        
	        redirect_(null);
	    }
	    
        $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);
        
        redirect_(getAddr('center_view_all_events'));
	}
	
	function downloadEventResult($event_id)
	{
	    $ret = $this->eventsHelper->getEventResult($event_id, 
	        $this->getCenterData()[CENTER_CODE], getAddr('center_view_all_events'));
	    
	    if(!$ret[SUCCESSFUL])
	    {
	        $this->session->flash('error', $ret[MESSAGE]);
	        
	        redirect_(getAddr('center_view_all_events'));
	    }
	    
	    $resultList = $ret['result_list'];
	    
	    $event = $ret['event'];
	    
	    $headers = ['S/No', 'Name', 'Student ID', 'Exam No', 'Subjects', 'Correct Answers', 'Score'];
	    
	    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->resultsDir . 'resultsheet-template.xlsx');
	    
	    $sheetData = $spreadsheet->getActiveSheet();
	    
	    $i = 1;
	    foreach ($headers as $value) 
	    {
	        $sheetData->setCellValueByColumnAndRow($i, 1, $value);
	        $i++;
	    }
	    
	    $eventSubjects = $event->eventSubjects;
	    foreach ($eventSubjects as $eventSubject) {
	        $sheetData->setCellValueByColumnAndRow($i, 1, $eventSubject->course->course_code);
	        $i++;
	    }
		
	    $j = 2;
	    $serialNo = 1;
	    foreach ($resultList as $result) 
	    {
	        $sheetData->setCellValueByColumnAndRow(1, $j, $serialNo);
	        $sheetData->setCellValueByColumnAndRow(2, $j, $result['name']);
	        $sheetData->setCellValueByColumnAndRow(3, $j, $result[STUDENT_ID]);
	        $sheetData->setCellValueByColumnAndRow(4, $j, $result[EXAM_NO]);
	        $sheetData->setCellValueByColumnAndRow(5, $j, $result['subjects']);
	        $sheetData->setCellValueByColumnAndRow(6, $j, "{$result['total_score']}/{$result['total_num_of_questions']}");
	        $sheetData->setCellValueByColumnAndRow(7, $j, "{$result['total_score_percent']}/{$result['total_num_of_questions_percent']}");
	        
	        $i = 8;
	        $subjectsAndScores = $result['subjects_and_scores'];
	        foreach ($eventSubjects as $eventSubject) 
	        {
	            $courseCode = $eventSubject->course->course_code;
	            
	            if(!empty($subjectsAndScores[$courseCode]))
	            {
	               $sheetData->setCellValueByColumnAndRow($i, $j, $subjectsAndScores[$courseCode]['score']);
	            }
	            
	            $i++;
	        }
			
			$j++;
	        $serialNo++;
	    }
	    
	    $title = str_replace(' ', '_', $event[TITLE]);
	    
	    $fileName = "$title--{$event[TABLE_ID]}--results.xlsx";
	    
	    // Output the file so that user can download
	    header("Content-Type: application/vnd.ms-excel");
	    header("Content-Disposition: attachment; filename=$fileName");
	    header("Cache-Control:max-age=0");
	    
	    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
	    $writer->save('php://output');
	    
	    exit();
	}
	
	
}





