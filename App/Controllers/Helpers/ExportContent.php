<?php
namespace App\Controllers\Helpers;

class ExportContent
{
    private $baseDir = APP_DIR.'../public/export/';
    
    private $coursesModel;
    private $sessionModel;
    private $questionsModel;
    private $summaryModel;
    
    function __construct(
        \App\Models\Courses $coursesModel,
        \App\Models\Sessions $sessionModel,
        \App\Models\Questions $questionsModel,
        \App\Models\Summary $summaryModel
    ){
        $this->coursesModel = $coursesModel;
        
        $this->sessionModel = $sessionModel;
        
        $this->questionsModel = $questionsModel;
        
        $this->summaryModel = $summaryModel;
    }
    
    function exportCourse(\App\Models\Courses $course) 
    {
        ini_set('max_execution_time', 480);  
        
        $courseName = $course[COURSE_CODE];
        
    	$allSessions =  $this->sessionModel->where(COURSE_CODE, '=', $courseName)
	           ->with(['passages', 'instructions'])->get();
        
    	$summary =  $this->summaryModel->where(COURSE_CODE, '=', $courseName)->get();
    	
    	if (!$allSessions->first())
    	{
    		die("No record found for $courseName, create a new one");
    	}
    	
    	$baseFolder = $this->baseDir.$courseName;
    	
    	if(is_dir($baseFolder))
    	{
    	    \App\Core\Helper::deleteDir($baseFolder, false);
    	}
    	else 
    	{
    	    mkdir($baseFolder, 0777, true);
    	}
    	
    	file_put_contents("$baseFolder/course.json", json_encode($course->toArray(), JSON_PRETTY_PRINT));

    	file_put_contents("$baseFolder/sessions.json", json_encode($allSessions->toArray(), JSON_PRETTY_PRINT));
    	
    	if($summary->first())
    	{
        	file_put_contents("$baseFolder/summary.json", json_encode($summary->toArray(), JSON_PRETTY_PRINT));	    
    	}
    	
    	/** @var \App\Models\Sessions $session */
    	foreach ($allSessions as $session)
    	{
    	    $question = $session->questions()->get();
//     	    $question = $this->questionsModel->where(COURSE_CODE, '=', $courseName)
//     	           ->where(SESSION_ID, '=', $session[TABLE_ID])->get();
    	    
        	file_put_contents("$baseFolder/questions_{$session[SESSION]}_{$session[TABLE_ID]}.json", 
        	       json_encode($question->toArray(), JSON_PRETTY_PRINT));
    	}
    	
    	// The images to copy
//     	$imageContentFolder = APP_DIR . '../public/img/content/'."$courseName";
    	$imageContentFolder = APP_DIR . '../public/img/questions/'."$courseName";
    	
    	// Where to copy the images to
    	$imgBaseFolder = "$baseFolder/img";
    	
    	if(!\App\Core\Helper::is_dir_empty($imageContentFolder))
    	{
    	    mkdir($imgBaseFolder, 0777, true);
    	    
        	\App\Core\Helper::copy($imageContentFolder, $imgBaseFolder);
    	}
    	
    	$currentTime = str_replace([' ', ':'], ['_', '-'], \Carbon\Carbon::now()->toDateTimeString());
    	
    	$zipFilename = "{$this->baseDir}$courseName.$currentTime.zip";
    	
    	\App\Core\Helper::zipContent($baseFolder, $zipFilename);
    	
    	$this->downloadContent($zipFilename);
    	
    	// Delete the file afterwards
    	unlink($zipFilename);
    	
    	if(is_dir($baseFolder)) \App\Core\Helper::deleteDir($baseFolder);
    	
    	exit;
    } 
    
    private function downloadContent($fileToDownload) 
    {   
        $file_name = basename($fileToDownload);
        
        header("Content-Type: application/zip");
        
        header("Content-Disposition: attachment; filename=$file_name");
        
        header("Content-Length: " . filesize($fileToDownload));
        
        readfile($fileToDownload);
    }
    
}





