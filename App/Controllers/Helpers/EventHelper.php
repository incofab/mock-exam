<?php
namespace App\Controllers\Helpers;

use App\Models\Events;

class EventHelper
{
    private $eventsModel;
    private $examsModel;
    private $subscriptionPlan;
    private $sysID;
    
    function __construct(
        \App\Models\Events $eventModel,
        \App\Models\Exams $examsModel,
        \App\Core\SubscriptionPlan $subscriptionPlan,
        \App\Core\SystemIdentificator $sysID
    ){
        $this->eventsModel = $eventModel;
        
        $this->examsModel = $examsModel;
        
        $this->subscriptionPlan = $subscriptionPlan;
        
        $this->sysID = $sysID;
    }
    
    function getEventResult($event_id, $centerCode = null, $redirectAddr=null)
    {
        /** @var \App\Models\Events $event */
        $event = $this->eventsModel->where(TABLE_ID, '=', $event_id);
        
        if($centerCode) $event = $event->where(CENTER_CODE, '=', $centerCode);
        
        $event = $event->with(['eventSubjects', 'eventSubjects.course'])->first();
        
        if(!$event)
        {
            return [SUCCESSFUL => false, MESSAGE => 'Event not found'];
        }
        
        $this->subscriptionPlan->init($centerCode, $redirectAddr);
        
        $isPerEventSub = false;//$this->subscriptionPlan->isPerEventSubscription();
        
        $isExpired = false;//$this->subscriptionPlan->isExpired();
        
        if(!$isPerEventSub && $isExpired)
        {
            return [SUCCESSFUL => false, MESSAGE => 'You do not have an subscription'];
        }
        
        /** @var \App\Models\Exams $examData */
        if($isPerEventSub)
        {
            if(empty($event[NUM_OF_ACTIVATIONS]))
            {
                return [SUCCESSFUL => false, MESSAGE => 'This event has not been activated'];            
            }
            $exams = $this->examsModel->where(EVENT_ID, '=', $event[TABLE_ID])
                ->take($event[NUM_OF_ACTIVATIONS]);
        }
        else 
        {
            $exams = $this->examsModel->where(EVENT_ID, '=', $event[TABLE_ID]);
        }
        
        $exams = $exams->with(['student', 'examSubjects', 'examSubjects.course'])->get();
        
        $resultList = [];
        foreach ($exams as $examData)
        {
//             $student = $examData->student()->first();
            $student = $examData['student'];
            /** @var \App\Models\ExamSubjects $examSubject */
            $examSubjects = $examData['examSubjects'];
            
            $subjectsAndScoresArr = [];
            $subjectsCourseCode = [];
            $scorePercent = 0;
            foreach ($examSubjects as $examSubject)
            {
                $course = $examSubject['course'];
                
                $subjectPercentScore = \App\Core\Settings::getPercentage($examSubject[SCORE], $examSubject[NUM_OF_QUESTIONS], 0);
                
                $subjectsCourseCode[] = $course[COURSE_CODE]."=$subjectPercentScore";
                
                $scorePercent += $subjectPercentScore;
				
                $subjectsAndScoresArr[$course['course_code']] = [
                    'score' => $examSubject['score'],
                    'total' => $examSubject[NUM_OF_QUESTIONS],
                ];
            }
            
            $resultList[] = [
                'firstname' => $student[FIRSTNAME],
                
                'lastname' => $student[LASTNAME],
                
                'name' => $student[FIRSTNAME] . ' ' . $student[LASTNAME],
                
                STUDENT_ID => $student[STUDENT_ID],
                
                PHONE_NO => $student[PHONE_NO],
                
                EXAM_NO => $examData[EXAM_NO],
                
                'subjects' => implode(', ', $subjectsCourseCode),
                
                'total_score' => $examData[SCORE],
                
                'total_score_percent' => $scorePercent,
                
                'total_num_of_questions' => $examData[NUM_OF_QUESTIONS],
                
                'total_num_of_questions_percent' => $examSubjects->count() * 100,

                'subjects_and_scores' => $subjectsAndScoresArr,
            ];
        }
        
        return [SUCCESSFUL => true, MESSAGE => '', 'result_list' => $resultList, 'event' => $event];
    }
    
    function smsResult($eventId, $username, $password, $centerCode, $redirect=null) 
    {
        $ret = $this->getEventResult($eventId, $centerCode, $redirect);
        
        if(!$ret[SUCCESSFUL]) return $ret;
        
        $resultList = $ret['result_list'];
        
        $event = $ret['event']->toArray();
        
//         MESSAGE => "Your {$event[TITLE]} score is {$result['subjects']}"
//             ." - Total={$result['total_score_percent']}/100",
        
        $url = DEV ? 'http://localhost/formula1/mock/send-result-sms'
            : 'http://formula1autozone.com.ng/mock/send-result-sms';
        
        $data = [
            'result_list' => json_encode($resultList),
            
            'event' => json_encode($event),
            
            'password' => $password,
            
            'username' => $username,
            
            'sys_id' => $this->sysID->getId(),
        ];
            
        return $this->executeCurl($url, $data);
    }
    
    function smsInvite(\App\Models\Events $event, $username, $password, $time, $redirect=null) 
    {
        $exams = $event->exams()->with(['student'])->get([TABLE_ID, EXAM_NO, STUDENT_ID]);
        
        $event = $event->toArray();
        
        $url = DEV ? 'http://localhost/formula1/mock/send-sms-invite'
            : 'http://formula1autozone.com.ng/mock/send-sms-invite';
        
        $data = [
            'event' => json_encode($event),
            
            'exams' => json_encode($exams),
            
            'password' => $password,
            
            'username' => $username,
            
            'time' => $time,
            
            'sys_id' => $this->sysID->getId(),
        ];
            
        return $this->executeCurl($url, $data);
    }
    
    private function executeCurl($url, $data)
    {
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        
        // converting
        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        
        if($error) return [SUCCESSFUL => false, MESSAGE => "Error connecting to server"];
        
        return json_decode($response, true);
    }
    
    function pauseAllExam($eventId) 
    {
        $event = $this->eventsModel->where(TABLE_ID, '=', $eventId)->first();
        
        if(!$event) return [SUCCESSFUL => false, MESSAGE => "Event Not found"];
        
        $exams = $event->exams()->get();
        
        $num = 0;
        foreach ($exams as $exam) 
        {
            $ret = $exam->pauseSelectedExam($exam);
            
            if($ret[SUCCESSFUL]) $num++; 
        }
        
        return [SUCCESSFUL => false, MESSAGE => "$num student(s) exam paused"];
    }
    
    function list($status = 'all', $centerCode = null, $num = 100, $page = 1, $lastIndex = 0)
    {
        $allRecords = Events::with(['eventSubjects']);
        
        if($centerCode) $allRecords = $allRecords->where(CENTER_CODE, '=', $centerCode);
        
        if($status && $status !== 'all') $allRecords = $allRecords->where(STATUS, '=', $status);
        
        if($lastIndex != 0) $allRecords = $allRecords->where(TABLE_ID, '<', $lastIndex);
        
        else $allRecords = $allRecords->skip($num * ($page-1));
        
        $allRecords = $allRecords->orderBy(TABLE_ID, 'DESC')
        ->skip($num * ($page - 1))->take($num)->get();
        
        $count = 1000;
        
        return [SUCCESSFUL => true, MESSAGE => "", 'result' => $allRecords, 'count' => $count];
    }
}



