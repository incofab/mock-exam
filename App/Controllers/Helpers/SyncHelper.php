<?php
namespace App\Controllers\Helpers;

use App\Models\Events;
use App\Core\Helper;

class SyncHelper
{
    private $baseFolder = APP_DIR.'../public/files/sync-content/';
    function __construct(){
        
    }
    
    function syncEvent($eventId, $centerCode) 
    {
        ini_set('max_execution_time', 480);
        
        $event = Events::where('id', '=', $eventId)
        ->where(CENTER_CODE, '=', $centerCode)
        ->with(['eventSubjects', 'eventSubjects.session', 'eventSubjects.session.course', 'exams', 'exams.student', 'exams.examSubjects'])
        ->first();
        
        if(!$event) return [SUCCESSFUL => false, MESSAGE => 'Event record not found'];
        
        $eventSubjects = $event['eventSubjects'];
        $exams = $event['exams'];
        
        $sessions = [];
        $courses = [];
        foreach ($eventSubjects as $eventSubject) {
            $session = $eventSubject['session'];
            $courses[] = $session['course'];
            unset($session['course']);
            $sessions[] = $session;
        }
        
        unset($eventSubjects['session']);
//         $students = [];
//         foreach ($exams as $exam) {
//             $students[] = $exam['student'];
//             unset($exam['student']);
//         }
        $formattedEventData = [
            TABLE_ID => $event[TABLE_ID], 
            TITLE => $event[TITLE], 
            DESCRIPTION => $event[DESCRIPTION], 
            CENTER_CODE => $event[CENTER_CODE], 
            DURATION => $event[DURATION], 
            STATUS => $event[STATUS], 
            NUM_OF_ACTIVATIONS => $event[NUM_OF_ACTIVATIONS],
            
            'event_subjects' => $eventSubjects,
            'sessions' => $sessions,
            'courses' => $courses,
            'exams' => $exams, // Exams contains student and exam subjects
        ];
        
        return [SUCCESSFUL => true, MESSAGE => '', 'result' => $formattedEventData];
    } 
    
    function getQuestions($eventId, $centerCode)
    {
        ini_set('max_execution_time', 480);
        
        $event = Events::where('id', '=', $eventId)
        ->where(CENTER_CODE, '=', $centerCode)
        ->with(['eventSubjects', 'eventSubjects.session', 'eventSubjects.session.course'])
        ->first();
        
        if(!$event) return [SUCCESSFUL => false, MESSAGE => 'Event record not found'];
        
        $eventFolder = "{$this->baseFolder}event_{$event['id']}/";
        $imagesFolder = $eventFolder.'img/';
        
        $eventSubjects = $event['eventSubjects'];
        
        if(file_exists($eventFolder)) Helper::deleteDir($eventFolder);
        else mkdir($eventFolder, 0777, true);
        
        if(file_exists($imagesFolder)) Helper::deleteDir($imagesFolder);
        else mkdir($imagesFolder, 0777, true);
            
        foreach ($eventSubjects as $eventSubject) 
        {
            $session = $eventSubject['session'];
            $course = $session['course'];
            
            $subjectFolder = "{$eventFolder}course_{$course['id']}/";
            
            if(!file_exists($subjectFolder)) mkdir($subjectFolder, 0777, true);
            
            $questions = \App\Models\Questions::where(COURSE_SESSION_ID, '=', $session['id'])->get();
        
            file_put_contents("{$subjectFolder}session_{$session['id']}.json", $questions);
            
            $imageSourceFolder = APP_DIR . "../public/img/content/{$course[TABLE_ID]}/{$session[TABLE_ID]}";
            
            if(!file_exists($imageSourceFolder))
            {
                $imagesTargetFolder = $imagesFolder."{$course[TABLE_ID]}/{$session[TABLE_ID]}";
                
                if(!file_exists($imagesTargetFolder)) mkdir($imagesTargetFolder, 0777, true);
                
                Helper::copy($imagesFolder, $imagesTargetFolder);
            }
            
        }
        
        
        $zipFilename = "{$this->baseFolder}event_{$event['id']}.zip";
        
        \App\Core\Helper::zipContent($eventFolder, $zipFilename);
        
        $this->downloadContent($zipFilename);
        
        // Delete the file afterwards
        unlink($zipFilename);
        
        if(is_dir($eventFolder)) \App\Core\Helper::deleteDir($eventFolder);
        
        exit;
    }
    
    private function downloadContent($fileToDownload) 
    {   
        $file_name = basename($fileToDownload);
        
        header("Content-Type: application/zip");
        
        header("Content-Disposition: attachment; filename=$file_name");
        
        header("Content-Length: " . filesize($fileToDownload));
        
        readfile($fileToDownload);
    }
    
}





