<?php
namespace App\Controllers\Helpers;

class StudentsUploadHelper
{
    private $studentsModel;
    private $examHelper;
    private $codeGenerator;
    
    function __construct(
        \App\Models\Students $studentsModel,
        \App\Controllers\Helpers\ExamHelper $examHelper,
        \App\Core\CodeGenerator $codeGenerator
    ){ 
        $this->studentsModel = $studentsModel;
        
        $this->examHelper = $examHelper;
        
        $this->codeGenerator = $codeGenerator;
    }
    
    function uploadStudent($files, $examCenter, $event = null)
    {
        $ret = $this->uploadFile($_FILES);
        
        if(!$ret[SUCCESSFUL]) return $ret;
        
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($ret['full_path']);
        
        $sheetData = $spreadsheet->getActiveSheet();

        $allStudents = $sheetData->toArray(null, true, true, true);
        
        $formattedStudents = [];
        $selectedSubjectsArr = [];
        
        $colFirstname = 'A';
        $colLastname = 'B';
        $colPhone = 'C';
        $colEmail = 'D';
        $colAddress = 'E';
        $colSubjects = 'F';
        
        foreach ($allStudents as $row => $student) 
        {
            if($row == '1' || $student == null) continue;
            
            $firstName = array_get($student, $colFirstname);
            $lastName = array_get($student, $colLastname);

            if(empty($firstName) || empty($lastName)) continue;
            
            $arr = [
                FIRSTNAME => $firstName,
                LASTNAME => $lastName,
                PHONE_NO => $this->formatPhoneNo(array_get($student, $colPhone)),
                ADDRESS => array_get($student, $colAddress),
                EMAIL => trim(array_get($student, $colEmail)),
                CENTER_CODE => $examCenter[CENTER_CODE],
            ];
            
            $ret = $this->studentsModel->validateStudent($arr, $examCenter);
            
            if(!$ret[SUCCESSFUL]) {
//                 echo json_encode($ret).'<br />';
//                 dDie($arr);
                return $ret;
            }
            
            $formattedStudents[] = $arr;
            
            $selectedSubjectsArr[] = array_get($student, $colSubjects);
        }
        
        // Now, Insert the students to DB
        $len = count($formattedStudents);
        
        for ($i = 0; $i < $len; $i++) 
        {
            $studentData = $formattedStudents[$i];
            
            $studentSubjects = array_get($selectedSubjectsArr, $i);
            
            $studentData[STUDENT_ID] = $this->studentsModel->generateStudentID($this->codeGenerator);
            
            $createdStudent = $this->studentsModel->create($studentData);
            
            if(!$createdStudent || empty($studentSubjects) || !$event) continue;
                    
            $studentId = $createdStudent[STUDENT_ID];
            
            $selectedSubjectSessionIDsOrEventCourseCodes = explode(',', $studentSubjects);
            
            $this->examHelper->registerExam($event, $studentId, $selectedSubjectSessionIDsOrEventCourseCodes, $examCenter);
        }
        
        return [SUCCESS => true, MESSAGE => 'All records saved'];
    }
    
    private function uploadFile($files)
    {
        if(!isset($files['content'])) return [SUCCESSFUL => false, MESSAGE => 'Invalid File'];
        
        // First check if file type is image
        $validExtensions = array("xls", 'xlsx');
        
        $maxFilesize = 1000000; // 1mb
        
        $name = $files['content']["name"];
        
        $ext = pathinfo($name, PATHINFO_EXTENSION);
        
        $originalFilename = pathinfo($name, PATHINFO_FILENAME);
        
        if($files['content']["size"] > $maxFilesize) return [SUCCESSFUL => false, MESSAGE => 'File greater than 1mb'];
        
        if(!in_array($ext, $validExtensions)) return [SUCCESSFUL => false, MESSAGE => 'Invalid file Extension'];
        
        // Check if the file contains errors
        if($files['content']["error"] > 0) return [SUCCESSFUL => false, MESSAGE => "Return Code: " . $files['content']["error"]];
        
        // Now upload the file
        $filename = "$originalFilename".'_'.uniqid().".$ext";
        $destinationFolder = APP_DIR . "../public/files/upload/";
        if(!file_exists($destinationFolder))mkdir($destinationFolder, 0777, true);
        $destinationPath = $destinationFolder.$filename;
        
        $tempPath = $files['content']['tmp_name'];
        
        @move_uploaded_file($tempPath, $destinationPath); // Moving Uploaded file
        
        return [SUCCESSFUL => true, MESSAGE => "File uploaded successfully",
            'filename' => $filename, 'full_path' => $destinationPath];
    }
    
    private function formatPhoneNo($phone) 
    {
        if(substr($phone, 0, 3) == '234')
        {
            return '0'.substr($phone, 3);
        }
        if(substr($phone, 0, 4) == '+234')
        {
            return '0'.substr($phone, 4);            
        }
        if(substr($phone, 0, 1) != '0')
        {
            return '0'.$phone;            
        }
        return $phone;
    }
    
}



