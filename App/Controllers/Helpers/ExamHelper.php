<?php
namespace App\Controllers\Helpers;

class ExamHelper
{
    private $eventsModel;
    private $examsModel;
    private $studentsModel;
    private $examSubjectsModel;
    private $eventSubjectsModel;
    private $questionsModel;
    private $examHandler;
    private $codeGenerator;
    private $subscriptionPlan;
    
    function __construct(
        \App\Models\Events $eventsModel,
        \App\Models\Exams $examsModel,
        \App\Models\Students $studentsModel,
        \App\Models\ExamSubjects $examSubjectsModel,
        \App\Models\EventSubjects $eventSubjectsModel,
        \App\Models\Questions $questionsModel,
        \App\Controllers\Helpers\ExamHandler $examHandler,
        \App\Core\CodeGenerator $codeGenerator,
        \App\Core\SubscriptionPlan $subscriptionPlan
    ){
        $this->eventsModel = $eventsModel;
        
        $this->examsModel = $examsModel;
        
        $this->studentsModel = $studentsModel;
        
        $this->examSubjectsModel = $examSubjectsModel;
        
        $this->eventSubjectsModel = $eventSubjectsModel;
        
        $this->questionsModel = $questionsModel;
        
        $this->examHandler = $examHandler;
        
        $this->codeGenerator = $codeGenerator;
        
        $this->subscriptionPlan = $subscriptionPlan;
    }
    
    function registerExam($eventDataOrID, $studentId, 
        $selectedSubjectSessionIDs,
        \App\Models\ExamCenters $examCenter
    ){
        if($eventDataOrID instanceof \App\Models\Events)
        {
            $event = $eventDataOrID;
        }
        else 
        {
            $event = $this->eventsModel->where(TABLE_ID, '=', $eventDataOrID)->first();
            
            if (!$event)
            {
                return [SUCCESS => false, MESSAGE => 'Event not found'];
            }
            
            if ($event[CENTER_CODE] != $examCenter[CENTER_CODE])
            {
                return [SUCCESS => false, MESSAGE => 'This event does not belong to this center'];
            }
        }
        
        $post = [STUDENT_ID => $studentId];
        
        $ret = $this->examsModel->insert($post, $this->codeGenerator, $this->studentsModel, $event);
        
        if (!$ret[SUCCESSFUL]) return $ret;
        
        /** @var \App\Models\Exams $exam */
        $exam = $ret['data'];
        
        $this->examSubjectsModel->multiSubjectInsert($selectedSubjectSessionIDs,
            $exam, $this->eventSubjectsModel, $this->codeGenerator);
        
        return [SUCCESS => true, MESSAGE => 'Exam registered'];
    }
    
    function extendExam(\App\Models\Exams $exam, $mins) 
    {
        if($mins < 1) {
            return [SUCCESS => false, MESSAGE => "Set a valid extension time"];
        }
        
        if($exam[STATUS] !== STATUS_PAUSED && empty($exam[END_TIME])) {
            return [SUCCESS => false, MESSAGE => "Exam has not started yet"];
        }

        $currentEndTime = \Carbon\Carbon::parse($exam[END_TIME]);
        
        if($exam[STATUS] === STATUS_ENDED || \Carbon\Carbon::now()->diffInSeconds($currentEndTime, false) < 10)
        {
            $currentEndTime = \Carbon\Carbon::now();
            $exam[STATUS] = STATUS_ACTIVE;
        }
        
        if($exam[STATUS] === STATUS_ENDED && !empty($exam[PAUSED_TIME]))
        {
            $pauseTime = \Carbon\Carbon::parse($exam[PAUSED_TIME]);
            
            $exam[PAUSED_TIME] = $pauseTime->subMinute($mins);
        }
        else
        {
            $exam[END_TIME] = $currentEndTime->addMinute($mins);        
        }
        
        $exam->save();
        
        return [SUCCESS => true, MESSAGE => "Exam time has been extende by {$mins}mins. Student should refresh his/her page."];
    }
    
}



