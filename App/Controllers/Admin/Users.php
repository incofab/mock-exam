<?php
namespace App\Controllers\Admin;


class Users extends BaseAdmin
{
	private $usersModel;
	private $smsHandle;
	
	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session,
	    \App\Models\Users $usersModel,
	    \App\Core\SMSHandler $smsHandle
    ){
        parent::__construct($c, $session);
        
        $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
        
        $this->usersModel = $usersModel;
        $this->smsHandle = $smsHandle;
	}

	function viewAllUsers() {

		$allRecords = $this->usersModel->where(IS_VIRTUAL_USER, '=', false)
						->take(100)->orderBy(TABLE_ID, 'DESC')->get();
		
		$numOfRealUsers = $this->usersModel->getTotalNumOfRealUsers();
		$totalMainBalance = $this->usersModel->getTotalUsersBalance();
		
		return view('admin/view_all_users', [
				'data' => $this->data,
				'allRecords' => $allRecords,
				'isAdmin' => true,
				'numOfRealUsers' => $numOfRealUsers,
    		    'totalMainBalance' => number_format($totalMainBalance, 2),
		]);
	}
	
	/**
	 * Search user database by Name, username, email or phone 
	 * @param unknown $param
	 */	
	function searchUsers() {
		
		$param = getValue($_GET, 'search_user');
		$allRecords = $this->usersModel->where(USERNAME, 'LIKE', "%$param%")
			->orWhere(FULLNAME, 'LIKE', "%$param%")
			->orWhere(EMAIL, 'LIKE', "%$param%")
			->orWhere(PHONE_NO, 'LIKE', "%$param%")
			->orderBy(TABLE_ID, 'DESC')
			->take(100)->get();
		
		return view('admin/view_all_users', [
				'data' => $this->data,
				'allRecords' => $allRecords,
		]);
	}

	function suspendUser($table_id) {

	    $success = $this->usersModel->where(TABLE_ID, '=', $table_id)
			->update([SUSPENDED => true]);
		
		if ($success)
		{
			$this->session->flash('success', 'User has been suspended');
		}
		else
		{
			$this->session->flash('success', 'Error suspending user');
		}
		
		redirect_(getAddr('admin_view_all_users'));
		
	}

	function unSuspendUser($table_id) {

	    $success = $this->usersModel->where(TABLE_ID, '=', $table_id)
			->update([SUSPENDED => false]);
		
		if ($success){
			$this->session->flash('success', 'User has been unsuspended');
		}else{
			$this->session->flash('success', 'Error unsuspending user');
		}
		
		redirect_(getAddr('admin_view_all_users'));
		
	}
	
	function viewUserProfile($table_id_or_username) {
		
	    $userData = $this->usersModel->where(TABLE_ID, '=', $table_id_or_username)
			->orWhere(USERNAME, '=', $table_id_or_username)->first();
		
		return view('admin/user_profile', [
				'data' => $this->data,
				'userData' => $userData,
				'isAdmin' => true,
		]);
	}

	function deleteUser($table_id) 
	{
	    if($this->usersModel->where(TABLE_ID, '=', $table_id)->delete())
		{
			$this->session->flash('success', 'User deleted successfully');
		}
		else
		{
			$this->session->flash('error', 'Error deleting user');
		}
		redirect_(getAddr('admin_view_all_users'));
	}

	function sendBroadcastSMS()
	{
	    if (!isset($_POST['send_sms']))
	    {
	        return view('admin/sms_form', [ 'isBroadcast' => true, 'post' =>  $this->session->getFlash('post', []), 'data' => $this->data ]);
	    }
	    
	    //Check CSRF token match
	    if (!$this->session->isCsrfValid(getValue($_POST, CSRF_TOKEN)))
	    {
	        $this->session->flash('error', 'Token Mismatch');
	        
	        redirect_(getAddr(null));
	    }
	    
	    $usersPhoneNos = $this->usersModel->all([PHONE_NO])->toArray();
	    
	    $ret = $this->smsHandle->sendMultiSMS($usersPhoneNos, $_POST[MESSAGE]);
	    
	    $this->session->flash($ret[SUCCESSFUL] ? 'success' : 'error', $ret[MESSAGE]);
	    
	    if(!$ret[SUCCESSFUL]) $this->session->flash('post', $_POST);
	    
	    redirect_(getAddr(null));
	}
	

}





