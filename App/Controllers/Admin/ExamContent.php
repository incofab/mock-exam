<?php
namespace App\Controllers\Admin;

class ExamContent extends BaseAdmin
{
    private $examContenModel;
    private $courseModel;
    private $outputJson;

	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session,
	    \App\Models\Courses $courseModel,
	    \App\Models\ExamContent $examContentModel,
	    \App\Parser\OutputJson $outputJson
    ){
	    parent::__construct($c, $session);
	    
	    $this->examContenModel = $examContentModel;
	    
	    $this->courseModel = $courseModel;
	    
	    $this->outputJson = $outputJson;
	}
	
	function all()
	{
	    $allRecords = $this->examContenModel->all();
	   
	    return  $this->view('admin/exam_content/all', [
	        'allRecords' => $allRecords,
	        'outputJson' => $this->outputJson,
	    ]);
	}
	
	function create()
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
	    if(!$_POST)
	    {
	        return $this->view('admin/exam_content/add', [ 'post' => $this->session->getFlash('post') ]);
	    }
	    
	    $ret = $this->examContenModel->insert($_POST);
	    
	    if($ret[SUCCESSFUL])
	    {
	        $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);
	        
	        redirect_(getAddr('admin_exam_contents'));
	    }
	    
	    if(isset($ret['val_errors']))
	    {
	        $this->session->flash('val_errors', $ret['val_errors']);
	    }else{
	        $this->session->flash('error', $ret[MESSAGE]);
	    }
	    
	    $this->session->flash('post', $_POST);
	    
	    redirect_(null);
	}
	
	function edit($tableID)
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
	    if(!$_POST)
	    {
	        $post = $this->session->getFlash('post', $this->examContenModel->where(TABLE_ID, '=', $tableID)->first());
	        
	        return $this->view('admin/exam_content/add', [
	            'post' => $post,
	            'edit' => true,
	            'tableID' => $tableID,
	        ]);
	    }
	    
	    $_POST[TABLE_ID] = $tableID;
	    
	    $ret = $this->examContenModel->edit($_POST);
	    
	    if($ret[SUCCESSFUL])
	    {
	        $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);
	        
	        redirect_(getAddr('admin_exam_contents'));
	    }
	    
	    if(isset($ret['val_errors']))
	    {
	        $this->session->flash('val_errors', $ret['val_errors']);
	    }else{
	        $this->session->flash('error', $ret[MESSAGE]);
	    }
	    
	    $this->session->flash('post', $_POST);
	    
	    redirect_(null);
	}
	
	function delete($table_id)
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
	    $success = $this->examContenModel->where(TABLE_ID, '=', $table_id)->delete();
	    
	    if ($success)
	    {
	        $this->session->flash(SUCCESSFUL, 'Record deleted successfully');
	    }else {
	        $this->session->flash('error', 'Delete failed');
	    }
	    
	    redirect_(getAddr('admin_exam_contents'));
	}
	
	function outputJson($table_id)
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
	    $ret = $this->outputJson->outputJson($table_id, true);
	    
        $this->session->flash($ret[SUCCESSFUL]?SUCCESSFUL:'error', $ret[MESSAGE]);
	    
	    redirect_(getAddr('admin_exam_contents'));	    
	}
	
	function deleteOutputJson($table_id)
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
	    $ret = $this->outputJson->deleteOutputJson($table_id);
	    
        $this->session->flash($ret[SUCCESSFUL]?SUCCESSFUL:'error', $ret[MESSAGE]);
	    
	    redirect_(getAddr('admin_exam_contents'));	    
	}

}