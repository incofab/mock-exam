<?php
namespace App\Controllers\Admin;


class Centers extends BaseAdmin
{
	private $centersModel; 
	private $codeGenerator; 
	private $smsHandle;
	
	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session,
	    \App\Models\ExamCenters $centersModel,
	    \App\Core\CodeGenerator $codeGenerator,
	    \App\Core\SMSHandler $smsHandle
    ){
        parent::__construct($c, $session);
        
        $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
        
        $this->centersModel = $centersModel;
        
        $this->codeGenerator = $codeGenerator; 
        
        $this->smsHandle = $smsHandle;
	}

	function all() 
	{
	    $allRecords = $this->centersModel->skip($this->num * ($this->page - 1))->take($this->num)->get();
		
		return $this->view('admin/centers/all', [
			'allRecords' => $allRecords,
		    'count' => $this->centersModel->getCount(),
		]);
	}

	function add() 
	{
	    if(!$_POST)
	    {
    		return $this->view('admin/centers/add', [
    		    'post' =>  $this->session->getFlash('post', []),
    		]);
	    }
	    
	    $ret = $this->centersModel->insert($_POST, $this->data, $this->codeGenerator);
	    
	    if($ret[SUCCESS])
	    {
	        $this->session->flash('success', $ret[MESSAGE]);
	        
	        redirect_(getAddr('admin_view_all_centers'));
	    }
	    
	    $this->session->flash('val_errors', getValue($ret, 'val_errors'));
	    $this->session->flash('error', getValue($ret, MESSAGE));
	    $this->session->flash('post', $_POST);
	    
	    redirect_(getAddr(null));
	}

	function edit($tableId) 
	{
	    if(!$_POST)
	    {
    		return $this->view('admin/centers/add', [
    		    'post' =>  $this->session->getFlash('post', $this->centersModel->where(TABLE_ID, '=', $tableId)->first()),
    		    'edit' => true,
    		    'tableId' => $tableId
    		]);
	    }
	    
	    $ret = $this->centersModel->edit($_POST);
	    
	    if($ret[SUCCESS])
	    {
	        $this->session->flash('success', $ret[MESSAGE]);
	        
	        redirect_(getAddr('admin_view_all_centers'));
	    }
	    
	    $this->session->flash('val_errors', getValue($ret, 'val_errors'));
	    $this->session->flash('post', $_POST);
	    
	    redirect_(getAddr(null));
	}
	
	function suspend($table_id) 
	{
	    $success = $this->centersModel->where(TABLE_ID, '=', $table_id)
			->update([STATUS => STATUS_SUSPENDED]);
		
		if ($success)
		{
			$this->session->flash('success', 'Exam Center has been suspended');
		}
		else
		{
			$this->session->flash('success', 'Error suspending Exam Center');
		}
		
		redirect_(getAddr('admin_view_all_centers'));
	}

	function unSuspend($table_id) 
	{
	    $success = $this->centersModel->where(TABLE_ID, '=', $table_id)
	           ->update([STATUS => STATUS_ACTIVE]);
		
		if ($success){
			$this->session->flash('success', 'Exam Center has been unsuspended');
		}else{
			$this->session->flash('success', 'Error unsuspending Exam Center');
		}
		
		redirect_(getAddr('admin_view_all_centers'));
	}
	
	function viewCenterDetail($table_id_or_username) 
	{
	    $centerData = $this->centersModel->where(TABLE_ID, '=', $table_id_or_username)
			->orWhere(USERNAME, '=', $table_id_or_username)->first();
		
		return view('admin/centers/profile', [
		    'centerData' => $centerData
		]);
	}

	function delete($table_id) 
	{
	    if($this->centersModel->where(TABLE_ID, '=', $table_id)->delete())
		{
			$this->session->flash('success', 'Exam Center deleted successfully');
		}
		else
		{
			$this->session->flash('error', 'Error deleting Exam Center');
		}
		redirect_(getAddr('admin_view_all_centers'));
	}


}





