<?php
namespace App\Controllers\Admin;

class Admin extends BaseAdmin{

	private $adminModel;
	private $userModel;
	private $smsHandle;
	private $cookieManager;
	private $passwordChange;
	private $usersChart;

	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session,
	    \App\Models\Admin $adminModel, 
	    \App\Models\Users $userModel, 
	    \App\Core\SMSHandler $smsHandle, 
	    \App\Core\CookieManager $cookieManager,
	    \App\Core\Password\PasswordChange $passwordChange,
	    \App\Core\UsersChart $usersChart
    ){
	    parent::__construct($c, $session);
	    
	    $this->adminModel = $adminModel;
	    $this->userModel  = $userModel;
	    $this->smsHandle  = $smsHandle;
	    $this->cookieManager = $cookieManager;
	    $this->passwordChange = $passwordChange;
	    $this->usersChart = $usersChart;
	}
	
	function index()
	{
// 	    die('You are logged in');
// 	    $numOfRealUsers = $this->userModel->getTotalNumOfRealUsers();
		
// 		$this->usersChart->generateChartData();
		
		return view('admin/index', [
				'data' => $this->data,
		]);
	}
	
	function all()
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_GOLD);
	    
	    $allRecords = $this->adminModel->orderBy(TABLE_ID, 'DESC')->take(50)->get();
	    
	    return $this->view('admin/admin/all_admin', [ 'allRecords' => $allRecords ]);
	}
	
	function addNew()
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
	    if(!$_POST)
	    {
	        return $this->view('admin/admin/add_admin', [ 'post' => $this->session->getFlash('post') ]);
	    }
	    
	    $ret = $this->adminModel->insert($_POST);
	    
	    if($ret[SUCCESSFUL])
	    {
	        $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);
	        
	        redirect_(getAddr('admin_all'));
	    }
	    
	    if(isset($ret['val_errors']))
	        $this->session->flash('val_errors', $ret['val_errors']);
        else
            $this->session->flash('error', $ret[MESSAGE]);
        
        $this->session->flash('post', $_POST);
        
        redirect_(null);
	}
	
	function edit()
	{
	    $tableID = $this->data[TABLE_ID];
	    
	    if(!$_POST)
	    {
	        $post = $this->session->getFlash('post', $this->adminModel->where(TABLE_ID, '=', $tableID)->first());
	        
	        return $this->view('admin/admin/add_admin', [ 'post' => $post, 'edit' => true]);
	    }
	    
	    $_POST[TABLE_ID] = $tableID;
	    
	    $ret = $this->adminModel->edit($_POST);
	    
	    if($ret[SUCCESSFUL])
	    {
	        $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);
	        
	        redirect_(getAddr('admin_all'));
	    }
	    
	    if(isset($ret['val_errors']))
	        $this->session->flash('val_errors', $ret['val_errors']);
        else
            $this->session->flash('error', $ret[MESSAGE]);
        
        $this->session->flash('post', $_POST);
        
        redirect_(null);
	}
	
	function delete($tableID)
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
	    $adminUser = $this->adminModel->where(TABLE_ID, '=', $tableID)->first();
	    
	    if($adminUser[USERNAME] == $this->data[USERNAME])
	    {
	        $this->session->flash('error', 'You cannot delete yourself');
	        
	        redirect_(getAddr('admin_all'));
	    }
	    
	    if($adminUser)
	    {
	        $adminUser[LEVEL] = \App\Models\Admin::ACCESS_LEVEL_NON_GRATA;
	        
	        $success = $adminUser->save();
	        
	    }else { $success = false; }
	    
	    $this->session->flash($success ? 'success' : 'error', $success ? 'Record Deleted' : 'Error: Delete failed');
	    
	    redirect_(getAddr('admin_all'));
	}

	function changePassword() 
	{
		if(!$_POST)
		{
		    return $this->view('admin/change_password');
		}
		
	    $ret = $this->passwordChange->changeAdminPassword($_POST, $this->getAdminObj());
	    
	    $this->session->flash($ret[SUCCESSFUL] ? SUCCESSFUL : 'error', $ret[MESSAGE]);
		
	    if($ret[SUCCESSFUL]) redirect_(getAddr('admin_dashboard'));
	    
		redirect_(null);
	}
	
	function sendSMS($phone = null) 
	{
		if (!isset($_POST['send_sms']))
		{
			if(empty($phone))
			{
				$this->session->flash('error', 'User did not supply a phone number');
				
				redirect_(getAddr('admin_dashboard'));
			}
			
			return view('admin/sms_form', [ 'phone' => $phone , 'post' =>  $this->session->getFlash('post', []), 'data' => $this->data ]);
		}
		
		//Check CSRF token match
		if (!$this->session->isCsrfValid(getValue($_POST, CSRF_TOKEN)))
		{
			$this->session->flash('error', 'Token Mismatch');
			
			redirect_(getAddr(null));
		}
		
		$ret = $this->smsHandle->sendSMS($_POST[PHONE_NO], $_POST[MESSAGE]);
		
		$this->session->flash($ret[SUCCESSFUL] ? 'success' : 'error', $ret[MESSAGE]);
		
		if(!$ret[SUCCESSFUL]) $this->session->flash('post', $_POST);
		
		redirect_(getAddr(null)); 
	}
	
	function logout() 
	{
		// Delete remeber login cookie
		$this->cookieManager->deleteLoginCookie('admin'); 
		
		return $this->logout_('admin_login');
	}

}