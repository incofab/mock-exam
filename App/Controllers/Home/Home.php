<?php
namespace App\Controllers\Home;

use App\Controllers\BaseController;

class Home extends BaseController{
	
    private $usersModel;
    private $points;
    private $carbon;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c,
        \Session $session,
        \App\Models\Users $usersModel,
        \App\Core\Points $points,
        \Carbon\Carbon $carbon
    ){
        parent::__construct($c, $session);
        
        $this->usersModel = $usersModel;
        $this->points = $points;
        $this->carbon = $carbon;  
    }
    
	function index() 
	{
// 	    redirect_(getAddr('center_login'));
	    
// 	    if($this->isUserLoggedIn())
// 	    {
// 	        redirect_(getAddr('user_dashboard'));
// 	    }
	    return view('home/index', []); 
    }
	
	function developers() { return $this->view('home/developers', []); }
	
	function contactUs() { return $this->view('home/contact_us', []); }
	
	function aboutUs() { return $this->view('home/about_us', []); }
	
	function plus18() { return $this->view('home/18_plus', []); }
	
	function transactionSecurity() { return $this->view('home/transaction_security', []); }
	
	function privacyPolicy() { return $this->view('home/privacy_policy', []); }
	
	function terms() { return $this->view('home/terms', []); }
	
	function responsibleGambling() { return $this->view('home/responsible_gambling', []); }

	function howItWorks() { return $this->view('home/how_it_works', []); }
	
	function rules() 
	{ 
	    return $this->view('home/rules', ['rules' => $this->points->getGamePointsArr()]); 
	}
	
	function registerUser($referral = NULL) 
	{
	    if($this->isUserLoggedIn())
	    {
	        redirect_(getAddr('user_dashboard'));
	    }
	    
		if (!$_POST)
		{
			return view('home/register',[ 
			      'post' =>  $this->session->getFlash('post', []),
			    
				  'referral' => $referral,
			]);
		}
		
		//Check CSRF token match
		if (!$this->session->isCsrfValid(getValue($_POST, CSRF_TOKEN)))
		{
			$this->session->flash('error', 'Token Mismatch');
			
			$this->session->flash('post', $_POST);
			
			redirect_(getAddr(null));
		}
		
		$ret = $this->usersModel->insert($_POST);
		
		if($ret[SUCCESS])
		{
			$this->session->flash('success', $ret[MESSAGE]);
			
			redirect_(getAddr('user_login'));
		}
		
		$this->session->flash('error', $ret[MESSAGE]);
		$this->session->flash('val_errors', getValue($ret, 'val_errors'));
		$this->session->flash('post', $_POST);
		
		redirect_(getAddr(null)); 
	}
	
	function message($type = null) 
	{ 
	    $messageTitle = SITE_TITLE;
	    
	    $messageBody = $this->session->getFlash('message', 'No Message');
	    
	    if($type == 'exam_submitted')
	    {
    	    $messageTitle = 'Congratulations!';
	        
    	    $messageBody = 'You have successfully completed your test.';
	    }
	    else if($type == 'exam_paused')
	    {	        
    	    $messageTitle = 'Exam Paused!';
	        
    	    $messageBody = 'This exam has been paused.';
	    }
	    else if($type == 'unsupported_browser')
	    {	        
    	    $messageTitle = 'Unsupported Browser!';
	        
    	    $messageBody = 'For optimal performance, Use your Native Device '
    	        .' browser, Google Chrome or Mozilla firefox to access this site. '
	            .'Opera Mini and UC Web are not supported';
	    }
	    
        return $this->view('home/message', [
            'messageTitle' => $messageTitle,
            'messageBody' => $messageBody,
        ]); 
	}

}




