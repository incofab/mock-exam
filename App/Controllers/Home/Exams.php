<?php
namespace App\Controllers\Home;

use App\Controllers\BaseController;

class Exams extends BaseController
{
    private $examModel;
    private $questionModel;
    private $questionAttemptsModel;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c, 
        \Session $session,
        \App\Models\Exams $examModel,
        \App\Models\Questions $questionModel,
        \App\Models\QuestionAttempts $questionAttemptsModel
    ){
        parent::__construct($c, $session);
        
        $this->examModel = $examModel;
        
        $this->questionAttemptsModel = $questionAttemptsModel;
    }
    
    function examLogin(\App\Models\Exams $examModel)
    {
        $this->cehckForUnsupportedBrowser();
        
        if (!$_POST)
        {
            return $this->view('exams/exam_login', []);
        }
//         $studentID = array_get($_POST, STUDENT_ID);
        $examNo = array_get($_POST, EXAM_NO);
        
        /** @var \App\Models\Exams $examData */
        $examData = $examModel->where(EXAM_NO, '=', $examNo)->first();
        
        if(!$examData)
        {
            $this->session->flash('error', 'Exam not found');
            
            redirect_(getAddr(null));
        }
        
        $studentID = $examData[STUDENT_ID];
        
        if($examData[STATUS] != STATUS_ACTIVE && $examData[STATUS] != STATUS_PAUSED)
        {
            $this->session->flash('error', 'login: Exam Inactive');
            
            redirect_(getAddr(null));
        }
        
        /** @var \App\Models\Students $student */
        $student = $examData->student()->first();

        if(!$student)
        {
            $this->session->flash('error', 'Student record not found');
            
            redirect_(getAddr(null));
        }
        
        $student->clearPreviousExamLoginSession($examNo);
        
        // Login student
        $this->session->put(STUDENT_SESSION_DATA, [
            EXAMS_TABLE => $examData->toArray(),
            STUDENTS_TABLE => $student->toArray(),
            EXAM_NO => $examNo
        ]);
        
        redirect_(getAddr('student_start_exam'));
    }
    
    function viewExamResult($examNo=null, $studentID=null)
    {
        $examNo = array_get($_REQUEST, EXAM_NO, $examNo);
        
        $studentID = array_get($_REQUEST, STUDENT_ID, $studentID);
        
        if(empty($examNo))
        {
            return $this->view('exams/view_result_form', []);
        }
        
        $exam = \App\Models\Exams::where(EXAM_NO, '=', $examNo)->first();
        
        $studentID = array_get($exam, STUDENT_ID);
        
        return $this->displayExamResult($examNo, $studentID);
    }
    
    function demoExam()
    {
        $this->cehckForUnsupportedBrowser();
        
        if(!$_POST)
        {
            return $this->view('exams/exam_login', ['is_demo' => true]);
        }
        
        $examNo = array_get($_POST, EXAM_NO);
        
        if($examNo !== '00000000')
        {
            $this->session->flash('error', 'Incorrect Exam No');
            
            redirect_(null);
        }
        
        $studentData = [
            FIRSTNAME => 'Ciroma',
            LASTNAME => 'Chukwuma Adekunle',
            STUDENT_ID => '11111111',
            EXAM_NO => $examNo,
        ];
        
        $examData = [];
        /* Select 4 courses */
        $courses = \App\Models\Courses::take(4)->get();
        $sessions = [];
        foreach ($courses as $course) 
        {
            $sessions[] = \App\Models\Sessions::where(COURSE_ID, '=', $course[TABLE_ID])
                ->with(['course'])->first();
        }
        $i = 0;
        foreach ($sessions as $session) 
        {
            if(!$session) continue;
            
            $sessionQuestions = $session->questions();
            
            if(!$sessionQuestions) continue;
            
            $course = $session['course'];
            
            $i++;
            $subjectDataFormatted = [
                EXAM_SUBJECT_ID => $i,
                'session_id' => $session[TABLE_ID],
                'course_code' => $course[COURSE_CODE],
                'course_id' => $session[COURSE_ID],
                'course_title' => $course[COURSE_TITLE],
                'year' => $session[SESSION],
                GENERAL_INSTRUCTIONS => 'Answer all questions',
                'instructions' => [],
                'passages' => [],
                'attempted_questions' => [],
                'questions' => $this->formatDemoQuestions($sessionQuestions->get()),
            ];
            
            $examData['all_exam_subject_data'][] = $subjectDataFormatted;            
        }
        
        $examData['meta']['time_remaining'] = 45 * 60;
//         dlog($examData);
        return $this->view('exams/exam_page', [
            'exam_data' => $examData,
            'student_data' => $studentData,
            'data' => $studentData,
            'is_demo' => true
        ]);
    }
    
    private function getDemoQuestions($sessionId, $courseCode) 
    {
        $questionsFormated = [];
        for($i = 0; $i<10; $i++)
        {
            $questionsFormated[] = [
                QUESTION_ID => "$sessionId.$courseCode.$i",
                QUESTION_NO => $i+1,
                QUESTION => 'Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium rem!',
                OPTION_A => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                OPTION_B => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                OPTION_C => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                OPTION_D => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                OPTION_E => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
            ];
        }
        return $questionsFormated;
    }
    
    private function formatDemoQuestions($questions) 
    {
        $questionsFormated = [];
        foreach ($questions as $question) 
        {
            $questionsFormated[] = [
                QUESTION_ID => $question[TABLE_ID],
                QUESTION_NO => $question[QUESTION_NO],
                QUESTION => $question[QUESTION],
                OPTION_A => $question[OPTION_A],
                OPTION_B => $question[OPTION_B],
                OPTION_C => $question[OPTION_C],
                OPTION_D => $question[OPTION_D],
                OPTION_E => $question[OPTION_E],
            ];
        }
        return $questionsFormated;
    }
    
}




