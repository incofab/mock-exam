<?php
namespace App\Controllers\Home;

use App\Controllers\BaseController;

class Login extends BaseController{

    private $userModel; 
    private $adminModel; 
    private $PasswordReset;  
    private $cookieManager;  
    private $loginHelper;  
    
	function __construct(
    	    \Bootstrap\Container\MyContainer $c, 
    	    \Session $session, 
    	    \App\Models\Users $userModel,   
    	    \App\Models\Admin $adminModel,
	        \App\Core\Password\PasswordReset $PasswordReset,
	        \App\Core\CookieManager $cookieManager,
	        \App\Core\Login $loginHelper
    ){
		parent::__construct($c, $session);
		
		$this->adminModel = $adminModel;
		$this->userModel  = $userModel;
		$this->PasswordReset  = $PasswordReset;
		$this->cookieManager  = $cookieManager;
		$this->loginHelper  = $loginHelper;
		
		// First check if user session is still active
		if($this->session->get(USER_SESSION_DATA))
		{
// 			redirect_(getAddr('user_dashboard'));
		}
		else if($this->session->get(CENTER_SESSION_DATA))
		{
// 			redirect_(getAddr('center_dashboard'));
		}
		else if($this->session->get(ADMIN_SESSION_DATA))
		{
// 			redirect_(getAddr('admin_dashboard'));
		}
		
		// Check if the remember login is still active
		if($remLogin = array_get($_COOKIE, USER_REMEBER_LOGIN))
		{
// 			$this->loginFromCookie($remLogin, 'user');
		}
		elseif($remLogin = array_get($_COOKIE, ADMIN_REMEBER_LOGIN))
		{
// 			$this->loginFromCookie($remLogin, 'admin'); 
		}
		
	}
	
	
	function userLogin() 
	{
		if (!$_POST)
		{
			$this->session->keepFlash();
			
			return view('home/login', ['username' => array_get($_COOKIE, USERNAME)]);
		}
		
		$this->checkCSRFToken(array_get($_POST, CSRF_TOKEN), true);
		
		$ret = $this->loginHelper->loginUser($this->userModel, $_POST);
		
		if(isset($ret['to_activate']))
		{
		    $this->session->flash('error', $ret[MESSAGE]);
		    
		    redirect_($ret['activation_address']);
		}
		
		if(!$ret[SUCCESSFUL])
		{
		    $this->session->flash('error', $ret[MESSAGE]);
		    
		    redirect_(getAddr(null));
		}
		
		setcookie(USERNAME, $ret['data'][USERNAME], time() + (14 * 24 * 60 * 60)); // 2 weeks
		
		$this->session->put(USER_SESSION_DATA, $ret['data']->getRequiredUserData($ret['data']));
		
		redirect_(array_get($_GET, 'next', getAddr('user_dashboard')));
	}
	
	function centerLogin(\App\Models\ExamCenters $centerModel) 
	{
		if (!$_POST)
		{
			$this->session->keepFlash();
			
			return view('centers/login', []);
		}
		
		$this->checkCSRFToken(array_get($_POST, CSRF_TOKEN), true);
		
		$ret = $this->loginHelper->loginExamCenter($centerModel, $_POST);
		
		if(!$ret[SUCCESSFUL])
		{
		    $this->session->flash('error', $ret[MESSAGE]);
		    
		    redirect_(getAddr(null));
		}
		
		$this->session->put(CENTER_SESSION_DATA, $ret['data']->toArray());
		
		redirect_(array_get($_GET, 'next', getAddr('center_dashboard')));
	}
	
	function adminLogin() 
	{
		if (!$_POST)
		{
			return view('admin/login', ['username' => array_get($_COOKIE, 'admin_username')]);
		}
		
		$this->checkCSRFToken(array_get($_POST, CSRF_TOKEN), true);
		
		$ret = $this->loginHelper->loginAdmin($this->adminModel, $_POST);
		
		if(!$ret[SUCCESSFUL])
		{
		    $this->session->flash('error', $ret[MESSAGE]);
		    
		    redirect_(getAddr(null));
		}
		
		setcookie('admin_username', $ret['data'][USERNAME], time() + (14 * 24 * 60 * 60)); // 2 weeks
		
		if($ret[REMEMBER_LOGIN])
		{
		    $this->cookieManager->setLoginCookie('admin', $ret[REMEMBER_LOGIN]);
		}
		
		$this->session->put(ADMIN_SESSION_DATA, $ret['data']->toArray());
		
		redirect_(array_get($_GET, 'next', getAddr('admin_dashboard')));
	}
	
	function forgotPassword() 
	{
		if(!isset($_POST['forgot_password']))
		{
			return view('home/forgot_password', ['forgotPassword' => true]);
		}
		
		//Check CSRF token match
		if (!$this->session->isCsrfValid(array_get($_POST, CSRF_TOKEN)))
		{
			$this->session->flash('error', 'Token Mismatch');
			
			redirect_(getAddr(null));
		}
		
		$ret = $this->PasswordReset->sendResetCode($_REQUEST);

		if($ret[SUCCESS]) redirect_(getAddr('reset_password'));
		
		$this->session->flash('error', $ret[MESSAGE]);
		
		redirect_(null);
	}
	
	function passwordReset() 
	{
		if(!isset($_POST['reset_password']))
		{
			return view('home/forgot_password', ['resetPassword' => true]);
		}
		
		//Check CSRF token match
		if (!$this->session->isCsrfValid(array_get($_POST, CSRF_TOKEN)))
		{
			$this->session->flash('error', 'Token Mismatch');
		
			redirect_(null);
		}
		
		$ret = $this->PasswordReset->resetPassword($_POST);
		
		if($ret[SUCCESSFUL])
		{
			$this->session->flash('success', 'Password has been reset, You can now login');
		
			redirect_(getAddr('user_login'));			
		}

		$this->session->flash('error', $ret[MESSAGE]);
		
		redirect_(null);	
	}
	
}