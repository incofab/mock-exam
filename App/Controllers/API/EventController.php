<?php
namespace App\Controllers\API;

use App\Models\BaseModel;
use App\Models\Exams;
use App\Controllers\BaseController;

class EventController extends BaseController
{
	private $resultsDir = APP_DIR.'../public/files/';
	
	private $eventHelper;
	
	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session,
	    \App\Controllers\Helpers\EventHelper $eventHelper
    ){
        parent::__construct($c, $session);
        
        $this->eventHelper = $eventHelper;
	}
	
	function apiList()
	{
	    $ret = $this->eventHelper->list(null, $_REQUEST[CENTER_CODE], $this->num, $this->page, 0);
	    
	    die(json_encode($ret));
	}
	
	function downloadEventContent(\App\Controllers\Helpers\EventContentHelper $eventContentHelper)
	{
	    $ret = $eventContentHelper->downloadEventContent($_REQUEST);
	    
	    die(json_encode($ret));
	}
	
	function apiUploadEventResult()
	{
	    $exams = $_REQUEST['exams'];
	    
	    BaseModel::beginTransaction();
	    
	    foreach ($exams as $exam) 
	    {
	        Exams::whereId($exam['id'])->update($exam);
	    }
	    
	    BaseModel::commit();
	    
	    die(json_encode(retS('Exam records updated')));
	}

}





