<?php
namespace App\Controllers\Exams;


use App\Controllers\BaseController;
use App\Core\ErrorCodes;

class BaseExam extends BaseController
{
	protected $allExamData = null;
	
	protected $data = null;    
	
	protected $examsModel;
	
	protected $studentModel;     
	 
	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session, 
	    \App\Models\Exams $examsModel,  
	    \App\Models\Students $studentModel,  
	    $fromAPI = false
    ){	
	    parent::__construct($c, $session);
	    
	    $this->examsModel = $examsModel;
	    
	    $this->studentModel = $studentModel;
	    
	    $this->allExamData = $this->session->get(STUDENT_SESSION_DATA, null);
	    
	    $this->data = $this->allExamData[EXAMS_TABLE];
		
		if (!$this->data)
		{
			$this->session->flash('error', 'You are not logged in');
			
		    if($fromAPI) die(json_encode([SUCCESSFUL => false, MESSAGE => 'You are not logged in',
		        ERROR_CODE => ErrorCodes::UNATHORIZED_ACCESS]));
			
		    redirect_(getAddr('student_exam_login', '?next=' . getAddr('')));
		}
	}
	
	protected function view($view, array $data = [])
	{
	    $data['data'] = isset($data['data']) ? $data['data'] : $this->data;
	    
	    $data['isExam'] = isset($data['isExam']) ? $data['isExam'] : true;
	    
	    return parent::view($view, $data);
	}
	
	protected function exam()
	{
	    return $this->data;
	}

	protected function student()
	{
	    return $this->allExamData[STUDENTS_TABLE];;
	}
		
	
}