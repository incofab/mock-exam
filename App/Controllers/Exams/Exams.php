<?php
namespace App\Controllers\Exams;

class Exams extends BaseExam
{
    protected $studentsModel;  
    protected $examsModel;  
	protected $examCenterModel;  
	private $examSubjectsModel;  
	private $sessionsModel;  
	private $questionsModel;  
	private $questionAttemptsModel;  
	private $coursesModel;  
	private $smsHandle;
	private $carbon;
	
	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session,
	    \App\Models\ExamCenters $examCenterModel, 
	    \App\Models\Students $studentsModel, 
	    \App\Models\Exams $examsModel, 
	    \App\Models\ExamSubjects $examSubjectsModel, 
	    \App\Models\Sessions $sessionsModel, 
	    \App\Models\Courses $coursesModel, 
	    \App\Models\Questions $questionsModel, 
	    \App\Models\QuestionAttempts $questionAttemptsModel, 
	    \Carbon\Carbon $carbon,
	    \App\Core\SMSHandler $smsHandle
    ){
        parent::__construct($c, $session, $examsModel, $studentsModel);
        
        $this->studentsModel = $studentsModel;
        
        $this->examsModel = $examsModel;
        
        $this->examCenterModel = $examCenterModel;
        
        $this->examSubjectsModel = $examSubjectsModel;
        
        $this->sessionsModel = $sessionsModel;
        
        $this->coursesModel = $coursesModel;
        
        $this->questionsModel = $questionsModel;
        
        $this->questionAttemptsModel = $questionAttemptsModel;
        
        $this->carbon = $carbon; 
        
        $this->smsHandle = $smsHandle;
	}

	function startExam(
	    \App\Models\Passages $passagesModel,
	    \App\Models\Instructions $instructionsModel,
	    \App\Models\Questions $questionsModel,
	    \App\Models\QuestionAttempts $questionAttemptsModel
    ){
	    $ret = $this->examsModel->startExam($this->data[EXAM_NO], 
	        $this->student(), $this->examSubjectsModel, $passagesModel, 
	        $instructionsModel, $this->carbon, $questionsModel, $questionAttemptsModel);
	    
	    if(!$ret[SUCCESSFUL])
	    {
           $this->session->flash('error', $ret[MESSAGE]);
           
           $this->session->put(STUDENT_SESSION_DATA, null);

           redirect_(getAddr('student_exam_login'));
	    }
	    
	    $student = $this->student();
	    $exam = $this->exam();
	    
	    $studentDataFormated = [
	        FIRSTNAME => $student[FIRSTNAME],
	        LASTNAME => $student[LASTNAME],
	        STUDENT_ID => $exam[STUDENT_ID],
	        EXAM_NO => $exam[EXAM_NO],
	    ];
	    
// 	    dlog($ret['data']);
		return $this->view('exams/exam_page', [
		    'exam_data' => $ret['data'],
		    'student_data' => $studentDataFormated,
		]);
	}

	function attemptQuestion(
	    \App\Models\QuestionAttempts $questionAttemptsModel,
	    \App\Models\Questions $questionsModel
    ){
	    $ret = $questionAttemptsModel->insert($_POST, $this->student(), 
	        $this->examSubjectsModel, $questionsModel, $this->carbon);
// 	    dlog($ret);
        die(json_encode($ret, JSON_PRETTY_PRINT));
	}

	function multiAttemptQuestion(
	    \App\Models\QuestionAttempts $questionAttemptsModel,
	    \App\Models\Questions $questionsModel
    ){
	    $success = [];
	    $failure = [];
	    $allAttempts = $_POST['attempts'];
	    foreach ($allAttempts as $post) 
	    {
    	    $ret = $questionAttemptsModel->insert($post, $this->student(), 
    	        $this->examSubjectsModel, $questionsModel, $this->carbon);
    	    
    	    $ret[QUESTION_ID] = array_get($post, QUESTION_ID);
    	    
    	    if($ret[SUCCESSFUL]) $success[] = $ret;
    	    else $failure[] = $ret;
	    }
//         dlog($success);
//         dlog($failure);
        die(json_encode([SUCCESSFUL => true, 'data' => ['success' => $success, 'failure' => $failure]], JSON_PRETTY_PRINT));
	}

	function pauseExam(
	    \App\Models\QuestionAttempts $questionAttemptsModel,
	    \App\Models\Questions $questionsModel
	){
	    $ret = $this->examsModel->pauseExam($this->data[EXAM_NO], $this->student(), $this->carbon,
	        $questionsModel, $questionAttemptsModel);
        
	    if($ret[SUCCESSFUL]) 
	    {
	        // Login student
	        $this->session->put(STUDENT_SESSION_DATA, null);
	    }
	        
        die(json_encode($ret, JSON_PRETTY_PRINT));
	}

	function endExam() 
	{
	    $ret = $this->examsModel->endExam($this->data[EXAM_NO], 
	        $this->student(), $this->questionsModel, $this->questionAttemptsModel);
	    
	    if($ret[SUCCESSFUL])
	    {
	        // Login student
	        $this->session->put(STUDENT_SESSION_DATA, null);
	    }
	    
        die(json_encode($ret, JSON_PRETTY_PRINT));
	}



}





