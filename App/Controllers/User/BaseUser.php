<?php
namespace App\Controllers\User;


use App\Controllers\BaseController;
use App\Core\ErrorCodes;

class BaseUser extends BaseController
{

	private $userData = null;
	
	private $data = null;    
	
	protected $usersModel;    
	 
	function __construct(
	    \Bootstrap\Container\MyContainer $c,
	    \Session $session, 
	    \App\Models\Users $usersModel, 
	    $fromAPI = false
    ){	    
	    parent::__construct($c, $session);
	    
	    $this->usersModel = $usersModel;
	    
		$this->data = $this->session->get(USER_SESSION_DATA, null);
		
		if (!$this->data)
		{
			$this->session->flash('error', 'You are not logged in');
			
		    if($fromAPI) die(json_encode([SUCCESSFUL => false, MESSAGE => 'You are not logged in',
		        ERROR_CODE => ErrorCodes::UNATHORIZED_ACCESS]));
			
		    redirect_(getAddr('user_login', '?next=' . getAddr('')));
		}
	}
	
	function __get($name) 
	{
	    if($name == 'data') return $this->getUserData();
	    
	    if($name == 'userData') return $this->getUserDataObj();
	    
	    throw new \Exception("Variable '$name' undefined");
	}
	
	function getUserData() 
	{
	    return $this->data;
	}
	
	function setUserData($data) 
	{
	    $this->data = $data;
	}
	/**
	 * @return \App\Models\Users
	 */
	function getUserDataObj() 
	{
	    if($this->userData) return $this->userData;
	    
	    $this->userData = $this->usersModel->where(USERNAME, '=', getValue($this->data, USERNAME))
	               ->first();
	    
       if(!$this->userData)
       {
           $this->session->put(USER_SESSION_DATA, null);
           die(json_encode([SUCCESSFUL => false, 
               MESSAGE => 'Unknow error, cannot retrieve user data',
               ERROR_CODE => ErrorCodes::UNATHORIZED_ACCESS]));
       }
       
	    return $this->userData;
	}
	
		
	
}