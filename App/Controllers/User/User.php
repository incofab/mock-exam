<?php
namespace App\Controllers\User;

class User extends BaseUser{
	
    private $passwordChange;
    private $browserDetector;
    private $cookieManager;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c,
        \Session $session,
        \App\Models\Users $usersModel,
        \App\Core\Password\PasswordChange $passwordChange,
        \App\Core\BrowserDetector $browserDetector,
        \App\Core\CookieManager $cookieManager
    ){ 
        parent::__construct($c, $session, $usersModel);
        
        $this->cookieManager   = $cookieManager;
        $this->passwordChange  = $passwordChange;
        $this->browserDetector = $browserDetector;
    }
	
	/**
	 * Users are directed to this page after successful login
	 */
	function index() 
	{
	    // If user does not have balance, go to deposit else got to events page
	    $data = $this->getUserData(); 
	    
	    if(empty($data[BANK_NAME]) || empty($data[ACCOUNT_NUMBER]))
	    {
	        $this->session->flash('alertMsg', 'Update your Bank Account details '
	            . 'for easy payment you request for withdrawal.');
	        
	        redirect_(getAddr('user_edit_profile'));
	    }
	    
		return view('dashboard/index', ['data' => $data ]);
	}
		
	function profile() 
	{
	    $myData = $this->getUserData();
	    
		return view('dashboard/profile', ['data' => $myData ]);
	}
	
	function editProfile() 
	{
	    if(!$_POST)
	    {
	        return view('dashboard/edit_profile', [
	            
	            'alertMsg' => $this->session->getFlash('alertMsg'),
	            
	            'post' => $this->session->getFlash('post', $this->getUserData()),
	            
	            'data' => $this->getUserData(),
	            
	            'valErrors' => $this->session->getFlash('val_errors'),
	        ]);
	    }
	    
	    $userData = $this->getUserDataObj();
		
	    $_POST[USERNAME] = $userData[USERNAME];
	    
		$ret = $userData->edit($_POST);
		
		$this->session->flash($ret[SUCCESSFUL] ? SUCCESSFUL : 'error', $ret[MESSAGE]);
		
		if($ret[SUCCESSFUL]) redirect_(getAddr('user_dashboard'));
		
		if(!empty($ret['val_errors'])) $this->session->flash('val_errors', $ret['val_errors']);
		
		$this->session->flash('post', $_POST);
		
		redirect_(null);
				
	}

	function logout() 
	{
		// Delete remeber login cookie
		$this->cookieManager->deleteLoginCookie('user'); 
		
		return $this->logout_('user_login');
	}
	
	function changePassword() 
	{
		if(!$_POST)
		{
		    return $this->view('dashboard/change_password');
		}
		
		$ret = $this->passwordChange->changeUserPassword($_POST, $this->getUserDataObj());
		
		$this->session->flash($ret[SUCCESSFUL] ? SUCCESSFUL : 'error', $ret[MESSAGE]);
		
		if($ret[SUCCESSFUL]) redirect_(getAddr('user_dashboard'));
		
		redirect_(null);
	}	

	function getProfileUploadForm() 
	{ 
		return view('api/profile_pic_upload_form', ['data' => $this->getUserData()]); 
	}
	
	function welcomeLetter() 
	{ 
		return view('dashboard/welcome_letter', ['data' => $this->getUserData()]); 
	}
	
	function stats() 
	{ 
	    $users = $this->usersModel->orderBy(TABLE_ID, 'DESC')->get();
	    
	    return view('dashboard/users', ['data' => $this->getUserData(), 'users' => $users ]); 
	}

}