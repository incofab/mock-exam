<?php
namespace App\Controllers\CCD;

use App\Controllers\Admin\BaseAdmin;

class BaseCCD extends BaseAdmin{
	
    protected $session;
	
    function __construct(\Bootstrap\Container\MyContainer $c, \Session $session) 
	{
	    parent::__construct($c, $session);
	    
	    $this->session = $session;
	    
// 	    if (!$session->get(ADMIN_SESSION_DATA, false))
// 	    {
// 	        $session->flash('error', 'You are not logged in');
	        
// 	        redirect_(getAddr('admin_login', '?next=' . getAddr('')));
// 	    }
	}	

	static function getPassage($allPassages, $QuestionNo) {
		foreach ($allPassages as $passage) {
			if($QuestionNo >= $passage[FROM_] && $QuestionNo <= $passage[TO_])
				return $passage;
		}
		return null;
	}
	
	static function getInstruction($allInstructions, $QuestionNo) {
		foreach ($allInstructions as $instruction) {

			if($QuestionNo >= $instruction[FROM_] && $QuestionNo <= $instruction[TO_]){

				return $instruction;
			}
		}
		return null;
	}
}