<?php
namespace App\Controllers\CCD;

class UploadContent extends BaseCCD
{    
    private $sessionModel;
    private $multiContentUploader;
    private $courseInstaller;
    private $exportContent;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c,
        \Session $session, 
        \App\Models\Sessions $sessionModel,
        \App\Core\UploadMultipleContent $multiContentUploader,
        \App\Controllers\Helpers\ExportContent $exportContent,
        \App\Core\CourseInstaller $courseInstaller
    ){
        parent::__construct($c, $session);
        
        $this->sessionModel = $sessionModel;

        $this->multiContentUploader = $multiContentUploader;
        
        $this->courseInstaller = $courseInstaller;
        
        $this->exportContent = $exportContent;
    }
	
    function installCourse($courseId = null)
    {
        $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
        
        if(empty($courseId))
        {
            $examContentId = array_get($_REQUEST, 'exam_content');
            $courses = new \App\Models\Courses();
            if($examContentId){
                $courses = $courses->where(EXAM_CONTENT_ID, '=', $examContentId);
            }
            
            $courses = $courses->with(['examContent'])->get();
            
            return  $this->view('admin/view_courses', [
                'allExamContent' => \App\Models\ExamContent::all(),
                'courses' => $courses,
                'courseInstaller' => $this->courseInstaller
            ]);
        }
        
        $course = \App\Models\Courses::where(TABLE_ID, '=', $courseId)->first();
        
        if(!$course)
        {
            $this->session->flash('error', htmlentities($courseId).' not found');
            
            redirect_(array_get($_REQUEST, 'next', getAddr('ccd_home')));
        }
        
        if(!$_POST)
        {
            return  $this->view('admin/upload_content', [
                'course' => $course,
            ]);
        }
        
        $filename = "{$this->courseInstaller->coursesFolder}$courseId.zip";
        
        if(file_exists($filename)) unlink($filename);
        
        $ret = $this->uploadFile($_FILES, $filename);
        
        if(!$ret[SUCCESSFUL])
        {
            $this->session->flash('error', $ret[MESSAGE]);
            
            redirect_(null);
        }
        
        $ret = $this->courseInstaller->installCourse($courseId);
        
        $this->session->flash($ret[SUCCESSFUL] ? SUCCESSFUL : 'error', $ret[MESSAGE]);
        
        redirect_(array_get($_REQUEST, 'next', getAddr('admin_dashboard')));
    }
    
    function unInstallCourse($courseId)
    {
        $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
        
        if(!$_POST)
        {
            $course = \App\Models\Courses::where(TABLE_ID, '=', $courseId)->first();
            
            if(!$course)
            {
                $this->session->flash('error', htmlentities($courseId).' not found');
                
                redirect_(array_get($_REQUEST, 'next', getAddr('ccd_home')));
            }
            
            return  $this->view('admin/confirm_uninstall', [
                'courseId' => $courseId,
                'courseCode' => $course[COURSE_CODE],
                'post' => $this->session->getFlash('post', [])
            ]);
        }
        
        if(!comparePasswords($this->getAdminObj()[PASSWORD], $_POST[PASSWORD]))
        {
            $this->session->flash('error', 'Invalid Password');
            
            redirect_(null);
        }
        
        $ret = $this->courseInstaller->unInstallCourse($courseId);
        
        $this->session->flash($ret[SUCCESSFUL] ? SUCCESSFUL : 'error', $ret[MESSAGE]);
        
        redirect_(array_get($_REQUEST, 'next', getAddr('ccd_home')));
    }
    
    function exportCourse($courseId = null)
    {
        $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
        
        if(empty($courseId))
        {
            $courses = \App\Models\Courses::with(['examContent'])->get();
            
            return  $this->view('admin/view_courses', [
                'allExamContent' => \App\Models\ExamContent::all(),
                'courses' => $courses,
                'courseInstaller' => $this->courseInstaller
            ]);
        }
        
        $course = \App\Models\Courses::where(TABLE_ID, '=', $courseId)->first();
        
        if(!$course)
        {
            $this->session->flash('error', htmlentities($courseId).' not found');
            
            redirect_(array_get($_REQUEST, 'next', getAddr('ccd_home')));
        }
        
        $this->exportContent->exportCourse($course);
        
        die('Done');
    }
    
    
    
    
    
    
	function uploadContent() 
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
        if(!$_POST)
        {
            $courses = \App\Models\Courses::orderBy(TABLE_ID, 'DESC')->get();
            
            if(!$courses->first())
            {
                $this->session->flash('error', 'No subject registered yet');
                
                redirect_(getAddr('ccd_register_course'));
            }
            
    		return  $this->view('ccd/upload_content', [
				'error' => \Session::getFlash('error') ,
				'success' => \Session::getFlash('success'),  
				'report' => \Session::getFlash('report'),
				'courses' => $courses,
    		]);
        }
        
        $courseId = array_get($_REQUEST, COURSE_ID);
        
        $course = \App\Models\Courses::where(TABLE_ID, '=', $courseId)->first();
        
        if(!$course)
        {
            $this->session->flash('error', 'Subject record not found');
            
            redirect_(getAddr('ccd_upload_content'));            
        }
        
        $subject = array_get($course, COURSE_CODE);
        $subjectFullname = array_get($course, COURSE_TITLE);
        $year = array_get($_REQUEST, SESSION);
        $extractionFolder = APP_DIR . "../public/files/content/extracted";
        $ret = $this->uploadFile($_FILES);
        
        if(!$ret[SUCCESSFUL])
        {
            $this->session->flash('error', $ret[MESSAGE]);
            
            redirect_(getAddr('ccd_upload_content'));            
        }
        
        $zip = new \ZipArchive();
        $res = $zip->open($ret['full_path']);
        $folderName = pathinfo($ret['full_path'], PATHINFO_FILENAME);
        
        if($res !== TRUE) die('<h2>File could not open</h2><h4>'.array_get($ret, MESSAGE).'</h4>');

        $subjectDir = "$extractionFolder/$folderName";
        
        $zip->extractTo($subjectDir);
        $zip->close();
//         die('We\'re done.');
        $obj = new \App\Parser\GenericParse($year, $subjectDir);
        
        $ret = $obj->parse($course);
        
        if(!$ret[SUCCESSFUL])
        {
            $this->session->flash('error', $ret[MESSAGE]);
            
            $this->session->flash('post', $_REQUEST);
        }
        
        $ret = $obj->insertSessionAndQuestionsRecord($ret['session_data'], $ret['questions']);
        
        if(!$ret[SUCCESSFUL])
        {
            $this->session->flash('error', $ret[MESSAGE]);
            
            $this->session->flash('post', $_REQUEST);
        }
        else 
        {
            $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);            
        }
        redirect_(null);
	} 
	
	private function uploadFile($files, $destinationPath = null) 
	{
	    if(!isset($files['content'])) return [SUCCESSFUL => false, MESSAGE => 'Invalid File'];
	    
	    // First check if file type is image
	    $validExtensions = array("zip");
	    $maxFilesize = 50000000; // 50mb
	    
	    $name = $files['content']["name"];
	    
	    $ext = pathinfo($name, PATHINFO_EXTENSION);
	    $originalFilename = pathinfo($name, PATHINFO_FILENAME);
	    
	    if($files['content']["size"] > $maxFilesize) return [SUCCESSFUL => false, MESSAGE => 'File greater than 50mb'];
	    
	    if(!in_array($ext, $validExtensions)) return [SUCCESSFUL => false, MESSAGE => 'Invalid file Extension'];
	    
	    // Check if the file contains errors
	    if($files['content']["error"] > 0) return [SUCCESSFUL => false, MESSAGE => "Return Code: " . $files['content']["error"]];
	    
	    $filename = $destinationPath;
	    
	    // Now upload the file
	    if(!$destinationPath)
	    {
	        $originalFilename = pathinfo($name, PATHINFO_FILENAME);
	        $filename = "$originalFilename".'_'.uniqid().".$ext";
	        $destinationFolder = APP_DIR . "../public/files/content/";
	        if(!file_exists($destinationFolder))mkdir($destinationFolder, 0777, true);
	        $destinationPath = $destinationFolder.$filename;
	    }
	    
	    $tempPath = $files['content']['tmp_name'];
	    
	    $move = move_uploaded_file($tempPath, $destinationPath); // Moving Uploaded file
	    
	    return [SUCCESSFUL => true, MESSAGE => "File uploaded successfully", 'full_path' => $destinationPath];
	}
	
	function installBonusContent($which = 1)
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
	    if($which == 2) 
	    {
	        $ret = $this->multiContentUploader->installBonusContent2();
	    }
        else 
        {
            $ret = $this->multiContentUploader->installBonusContent();
        }

        $this->session->flash($ret[SUCCESSFUL] ? SUCCESSFUL : 'error', $ret[MESSAGE]);

        redirect_(getAddr('ccd_home'));
	}
	
		
}
