<?php
namespace App\Controllers\CCD;

use Illuminate\Database\Capsule\Manager as Capsule;

class Courses extends BaseCCD{
    
    private $examContentModel;
    private $courseModel;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c,
        \Session $session,
        \App\Models\ExamContent $examContentModel,
        \App\Models\Courses $courseModel
    ){
        parent::__construct($c, $session);
        
        $this->courseModel = $courseModel;
        
        $this->examContentModel = $examContentModel;
    }
    
	function index() {
		die('index');
	}
	
	function viewAll($examContentId = null) 
	{ 
	    if (!Capsule::schema()->hasTable($this->courseModel->table)){
	        return  $this->view('layout', [
	            'error' => '<h3>Seems like a fresh installation, <a href="' .
	            getAddr('') . '">Click Here</a> to initialize database</h3>']);
	    }
	    
	    $allRegdCourses = $this->courseModel;
// 	    if($examContentId)
// 	    {
// 	        $allRegdCourses = $allRegdCourses->where(EXAM_CONTENT_ID, '=', $examContentId);
// 	    }
	    
	    $allRegdCourses = $allRegdCourses->orderBy(TABLE_ID, 'DESC')->get();
	    
	    return  $this->view('ccd/courses/view_all', [
	        'allRegdCourses' => $allRegdCourses,
	        'error' => \Session::getFlash('error') ,
	        'success' => \Session::getFlash('success'),
	        'report' => \Session::getFlash('report')
	    ]);
	}
	
	function registerCourses() 
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
	    if(!$_POST)
	    {
	        return $this->view('ccd/courses/register_course', [
	            'errorMsg' => \Session::getFlash('errorMsg'),
	            'success' => \Session::getFlash(SUCCESSFUL),
	            'errors' => \Session::getFlash('errors'),
	            'post' =>  \Session::getFlash('post', []),
	            'examContents' => $this->examContentModel->all()
	        ]);
	    }
	    
	    $ret = $this->courseModel->insert($_POST);
	    
	    if (array_get($ret, SUCCESSFUL))
	    {
	        $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);
	        redirect_(getAddr('ccd_all_courses'));
	    }
	    
	    $this->session->flash('errorMsg', $ret[MESSAGE]);
	    if(!empty($ret['val_errors']))
	        $this->session->flash('errors', $ret['val_errors']);
	        
        redirect_(null);
	}
	
	function editCourse($table_id) 
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
	    if (!$_POST)
	    {
	        $course = $this->courseModel->where(TABLE_ID, '=', $table_id)->first();
	        
	        if (empty($course))
	        {
	            \Session::flash('error', 'Record not found or invalid or expired');
	            
	            redirect_(getAddr('ccd_all_courses'));
	        }
	        
	        return $this->view('ccd/courses/register_course', [
	            'errorMsg' => \Session::getFlash('errorMsg'),
	            'errors' => \Session::getFlash('errors'),
	            'post' =>  \Session::getFlash('post', $course),
	            'edit' => 'true',
	            'table_id' => $table_id,
	            'examContents' => $this->examContentModel->all()
	        ]);
	    }
	    
	    $ret = $this->courseModel->updateRecord($_POST);
	    
	    if (array_get($ret, SUCCESSFUL))
	    {
	        $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);
	        redirect_(getAddr('ccd_all_courses'));
	    }
	    
	    $this->session->flash('errorMsg', $ret[MESSAGE]);
	    
	    if(!empty($ret['val_errors']))
	        $this->session->flash('errors', $ret['val_errors']);
	        
        redirect_(null);
	}
	
	function delete($table_id) 
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
	    $success = $this->courseModel->where(TABLE_ID, '=', $table_id)->delete();
	    
	    if ($success)
	    {
	        \Session::flash('success', 'Record deleted successfully');
	    }else {
	        \Session::flash('error', 'Delete failed');
	    }
	    
	    redirect_(getAddr('ccd_all_courses'));
	}
	
	
	
}
