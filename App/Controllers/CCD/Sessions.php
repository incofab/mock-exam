<?php
namespace App\Controllers\CCD;

class Sessions extends BaseCCD{
    private $courseModel;
    private $academicSessionModel;
    private $uploadPDFContentHandler;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c,
        \Session $session,
        \App\Models\Sessions $academicSessionModel,
        \App\Models\Courses $courseModel
    ){
        parent::__construct($c, $session);
        
        $this->academicSessionModel = $academicSessionModel;
        
        $this->courseModel = $courseModel;
    }
    
    function viewAllSessions($courseId)
    {
        $course = $this->courseModel->where(TABLE_ID, '=', $courseId)->first();
        
        if(!$course)
        {
            die("Course not found");
        }
        
        $allCoursesYears =  $this->academicSessionModel->where(COURSE_ID, '=', $courseId)->get();
        
        return  $this->view('ccd/session/view_all_sessions', [
            'allCoursesYears' => $allCoursesYears,
            'courseId' => $courseId,
            'course' => $course,
            'error' => \Session::getFlash('error') ,
            'success' => \Session::getFlash('success'),
            'report' => \Session::getFlash('report')
        ]);
        
    }
    
    function registerSession($courseId)
    {
        if (!$_POST)
        {
            $post = \Session::getFlash('post', []);
            $allInstructions = \App\Models\Sessions::joinUpInstruction($post);
            $allPassages = \App\Models\Sessions::joinUpPassage($post);
            
            return $this->view('ccd/session/register_session', [
                'errorMsg' => \Session::getFlash('errorMsg'),
                'errors' => \Session::getFlash('errors'),
                'post' => $post,
                'allInstructions' =>  $allInstructions,
                'allPassages' =>  $allPassages,
                'courseId' => $courseId
            ]);
        }
        
        $ret = $this->academicSessionModel->insert($_POST);
        
        if($ret[SUCCESSFUL])
        {
            $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);
            
            redirect_(getAddr('ccd_all_sessions', $courseId));
        }
        
        $this->session->flash('errorMsg', $ret[MESSAGE]);
        $this->session->flash("post", $_POST);
        redirect_(null);
    }
    
    function editSession($courseId, $session_id)
    {
        if (!$_POST)
        {
            $post = $this->session->getFlash('post');
            
            if(!empty($post))
            {
                $allInstructions = \App\Models\Sessions::joinUpInstruction($post);
                
                $allPassages = \App\Models\Sessions::joinUpPassage($post);
            }
            else
            {
                $post = $this->academicSessionModel->where(TABLE_ID, '=', $session_id)->first();
                
                if (empty($post))
                {
                    $this->session->flash('error', 'Record not found or invalid or expired');
                    
                    redirect_(getAddr('ccd_all_sessions', $courseId));
                }
                
                $allInstructions = $post->instructions()->get();
                $allPassages 	 = $post->passages()->get();
            }
            
            return $this->view('ccd/session/register_session', [
                'errorMsg' => $this->session->getFlash('errorMsg'),
                'errors' => $this->session->getFlash('errors'),
                'post' =>  $post,
                'allInstructions' =>  $allInstructions,
                'allPassages' =>  $allPassages,
                'edit' => 'true',
                'year_id' => $session_id, 'courseId' => $courseId
            ]);
        }
        
        $ret = $this->academicSessionModel->updateRecord($_POST);
        
        if($ret[SUCCESSFUL])
        {
            $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);
            
            redirect_(getAddr('ccd_all_sessions', $courseId));
        }
        
        $this->session->flash('errorMsg', $ret[MESSAGE]);
        $this->session->flash("post", $_POST);
        redirect_(null);
    }
    
    function delete($courseId, $session_id)
    {
        $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
        
        $success = $this->academicSessionModel->where(TABLE_ID, '=', $session_id)->delete();
        
        if ($success){
            $this->session->flash('success', 'Record deleted successfully');
        }else {
            $this->session->flash('error', 'Delete failed');
        }
        
        redirect_(getAddr('ccd_all_sessions', $courseId));
    }
    
    function previewAllSesssionQuestions($courseId, $session_id)
    {
        $sessionDetails = $this->academicSessionModel->where(TABLE_ID, '=', $session_id)
        ->with(['course', 'questions', 'instructions', 'passages', 'questions'])->first();
        
        if (!$sessionDetails)
        {
            return $this->view('ccd/layout', [ 'error' => 'No Record found' ]);
        }
        
        $allSessionQuestions = $sessionDetails['questions'];
        // 		$allSessionQuestions = $sessionDetails->questions()->get();
        
        $allPassages = $sessionDetails['passages'];
        $allInstructions = $sessionDetails['instructions'];
        $course = $sessionDetails['course'];
        
        if (!$allSessionQuestions->first())
        {
            return $this->view('ccd/layout', [ 'error' => 'No question recorded for this course. '
                . '<a href="' . getAddr('ccd_new_session_question', [$courseId, $session_id])
                . '">Click here to start recording question</a>'
            ]);
        }
        
        return $this->view('ccd/session/preview_session', [
            'year' => $sessionDetails[SESSION],
            'courseId' => $courseId,
            'courseName' => $course[COURSE_CODE],
            'course' => $course,
            'allCourseYearQuestions' =>  $allSessionQuestions,
            'year_id' => $session_id,
            'session' => $sessionDetails[SESSION],
            'allPassages' => $allPassages,
            'allInstructions' => $allInstructions,
        ]);
    }
    
}
