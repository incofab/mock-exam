<?php
namespace App\Controllers\CCD;

class ExamContent extends BaseCCD
{	
    private $examContenModel;
    private $courseModel;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c, 
        \Session $session,
        \App\Models\Courses $courseModel,
        \App\Models\ExamContent $examContentModel
    ){
        parent::__construct($c, $session);
        
        $this->examContenModel = $examContentModel;
        
        $this->courseModel = $courseModel;
    }
	
	function all() 
	{
		$allRecords = $this->examContenModel->all(); 
		
		return  $this->view('ccd/exam_content/all', [
				'allRecords' => $allRecords, 
		]);
		
	}
	
	function create() 
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
	    if(!$_POST)
	    {
	        return $this->view('ccd/exam_content/add', [ 'post' => $this->session->getFlash('post') ]);
	    }
	    
	    $ret = $this->examContenModel->insert($_POST);
	    
	    if($ret[SUCCESSFUL])
	    {
	        $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);
	        
	        redirect_(getAddr('ccd_exam_contents'));
	    }
	    
	    if(isset($ret['val_errors']))
	    {
	        $this->session->flash('val_errors', $ret['val_errors']);
	    }else{
            $this->session->flash('error', $ret[MESSAGE]);
	    }
	    
        $this->session->flash('post', $_POST);
            
        redirect_(null);
	}
	
	function edit($tableID) 
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
	    if(!$_POST)
	    {
	        $post = $this->session->getFlash('post', $this->examContenModel->where(TABLE_ID, '=', $tableID)->first());
	        
	        return $this->view('ccd/exam_content/add', [
	            'post' => $post, 
	            'edit' => true,
	            'tableID' => $tableID,
	        ]);
	    }
	    
	    $_POST[TABLE_ID] = $tableID;
	    
	    $ret = $this->examContenModel->edit($_POST);
	    
	    if($ret[SUCCESSFUL])
	    {
	        $this->session->flash(SUCCESSFUL, $ret[MESSAGE]);
	        
	        redirect_(getAddr('ccd_exam_contents'));
	    }
	    
	    if(isset($ret['val_errors']))
	    {
	        $this->session->flash('val_errors', $ret['val_errors']);
	    }else{
            $this->session->flash('error', $ret[MESSAGE]);
	    }
	    
        $this->session->flash('post', $_POST);
        
        redirect_(null);
	}
	
	function delete($table_id) 
	{
	    $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
	    
		$success = $this->examContenModel->where(TABLE_ID, '=', $table_id)->delete();
		
		if ($success)
		{
			$this->session->flash(SUCCESSFUL, 'Record deleted successfully');
		}else {
			$this->session->flash('error', 'Delete failed');
		}
		
		redirect_(getAddr('ccd_exam_contents'));
	}	
	
}
