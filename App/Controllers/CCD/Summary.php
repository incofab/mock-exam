<?php

namespace App\Controllers\CCD;

class Summary extends BaseCCD{
    
    function __construct(
        \Bootstrap\Container\MyContainer $c,
        \Session $session
        ){
            parent::__construct($c, $session);
    }
    
    function viewAll($courseId)
    {
        $allCourseSummary = \App\Models\Summary::where(COURSE_ID, '=', $courseId)->get();
        
        return  $this->view('ccd/summary/view_all', [
            'allCourseChapterSummary' => $allCourseSummary,
            'error' => \Session::getFlash('error') ,
            'success' => \Session::getFlash('success'),
            'report' => \Session::getFlash('report'),
            'courseName' => $courseId,
            'courseId' => $courseId,
        ]);
    }
    
    function newChapter($courseId)
    {
        if (!$_POST)
        {
            return $this->view('ccd/summary/add_new_chapter', [
                'errorMsg' => \Session::getFlash('errorMsg'),
                'errors' => \Session::getFlash('errors'),
                'post' =>  \Session::getFlash('post', []),
                'courseName' =>  $courseId,
                'courseId' =>  $courseId,
            ]);
        }
        
        $ret = (new \App\Models\Summary())->insert($_POST);
        
        if ($ret[SUCCESSFUL])
        {
            $this->session->flash('success', $ret[MESSAGE]);
            redirect_(getAddr('ccd_view_all_chapter', $courseId));
        }
        
        $this->session->flash('success', $ret[MESSAGE]);
        
        if(!empty($ret['val_errors']))
            $this->session->flash('errors', $ret['val_errors']);
            
            redirect_(null);
    }
    
    function editChapter($courseId, $chapter_id)
    {
        if (!$_POST)
        {
            $post = \Session::getFlash('post', \App\Models\Summary::where(TABLE_ID, '=', $chapter_id)->first());
            
            if (empty($post))
            {
                \Session::flash('error', 'Record not found or invalid or expired');
                redirect_(getAddr('ccd_view_all_chapter', $courseId));
            }
            
            return $this->view('ccd/summary/add_new_chapter', [
                'errorMsg' => \Session::getFlash('errorMsg'),
                'errors' => \Session::getFlash('errors'),
                'post' =>  $post, 'edit' => 'true',
                'chapter_id' => $chapter_id,
                'courseId' => $courseId,
            ]);
        }
        
        $ret = (new \App\Models\Summary())->updateRecord($_POST);
        
        if ($ret[SUCCESSFUL])
        {
            $this->session->flash('success', $ret[MESSAGE]);
            
            redirect_(getAddr('ccd_view_all_chapter', $courseId));
        }
        
        $this->session->flash('errorMsg', $ret[MESSAGE]);
        
        if(!empty($ret['val_errors']))
            $this->session->flash('errors', $ret['val_errors']);
            
            redirect_(null);
    }
    
    function preview($courseId, $chapter_id)
    {
        $previewData = \App\Models\Summary::where(TABLE_ID, '=', $chapter_id)->first();
        
        return $this->view('ccd/summary/preview_chapter', [
            'previewData' =>  $previewData,
            'chapter_id' => $chapter_id,
            'courseId' => $courseId,
        ]);
    }
    
    function delete($courseId, $chapter_id)
    {
        $this->checkAccessLevel(\App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS);
        
        $success = \App\Models\Summary::where(TABLE_ID, '=', $chapter_id)->delete();
        
        if ($success){
            \Session::flash('success', 'Record deleted successfully');
        }else {
            \Session::flash('error', 'Delete failed');
        }
        
        redirect_(getAddr('ccd_view_all_chapter', $courseId));
    }
    
    
    
}
