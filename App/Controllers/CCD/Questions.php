<?php
namespace App\Controllers\CCD;

class Questions extends BaseCCD
{
    private $courseModel;
    private $questionModel;
    
    function __construct(
        \Bootstrap\Container\MyContainer $c,
        \Session $session,
        \App\Models\Questions $questionModel,
        \App\Models\Courses $courseModel
    ){
        parent::__construct($c, $session);
        
        $this->questionModel = $questionModel;
        $this->courseModel = $courseModel;
    }
    
    function viewAllQuestions($courseId, $session_id)
    {
        $course = $this->courseModel->where(TABLE_ID, '=', $courseId)->first();
        
        $allSessionQuestions =  $this->questionModel->where(COURSE_SESSION_ID, '=', $session_id)->get();
        
        $acadSession = [];
        if($allSessionQuestions->first()){
            $acadSession = $allSessionQuestions->first()->session()->first();
        }
        
        return  $this->view('ccd/question/view_session_questions', [
            'allCourseYearQuestions' => $allSessionQuestions,
            'error' => \Session::getFlash('error') ,
            'success' => \Session::getFlash('success'),
            'report' => \Session::getFlash('report'),
            'year_id' => $session_id,
            'course_id' => $courseId,
            'courseId' => $courseId,
            'courseName' => array_get($course, COURSE_CODE),
            'year' => getValue($acadSession, SESSION),
        ]);
        
    }
    
    function addNewQuestion($courseId, $session_id, $num = 1)
    {
        if(!$_POST)
        {
            $courseSession = \App\Models\Sessions::where(TABLE_ID, '=', $session_id)
            ->with(['course'])->first();
            
            $allSessionQuestions =  $this->questionModel
            ->where(COURSE_SESSION_ID, '=', $session_id)->get();
            
            if(!$allSessionQuestions->first()) $num = 1;
            
            else $num = $allSessionQuestions->last()[QUESTION_NO] + 1;
            
            $course = $courseSession['course'];
//             $allTopics = $course['topics'];
            
            return $this->view('ccd/question/add_question', [
                'errorMsg' => \Session::getFlash('errorMsg'),
                'errors' => \Session::getFlash('errors'),
                'post' =>  \Session::getFlash('post', []),
                'year_id' =>  $session_id, 'num' => $num,
                'courseName' => $course[COURSE_CODE],
//                 'allTopics' => $allTopics,
//                 'topic' => null,
                'courseId' => $courseId,
                'year' => $courseSession[SESSION]
            ]);
        }
        
        $ret = (new \App\Models\Questions())->insert($_POST);
        
        \Session::flash($ret[SUCCESSFUL] ? 'success' : 'errorMsg', $ret[MESSAGE]);
        
        if ($ret[SUCCESSFUL])
        {
            redirect_(getAddr('ccd_new_session_question', [$courseId, $session_id, $_POST[QUESTION_NO] + 1 ]));
        }
        
        \Session::flash('post', $_POST);
        
        redirect_(null);
    }
    
    function addNewQuestionAPI()
    {
        $ret = (new \App\Models\Questions())->insert($_POST);
        
        return json_encode($ret);
    }
    
    function editQuestion($courseId, $session_id, $table_id)
    {
        if (!$_POST)
        {
            $question = \App\Models\Questions::where(TABLE_ID, '=', $table_id)->first();
            
            if (empty($question))
            {
                // 		        \Session::flash('error', 'Record not found or invalid or expired');
                
                redirect_(getAddr('ccd_all_session_questions', [$courseId, $session_id]));
            }
            
            $post = \Session::getFlash('post', $question);
            
            $courseSession = \App\Models\Sessions::where(TABLE_ID, '=', $session_id)
            ->with(['course'])->first();
            
            $course = $courseSession['course'];
            
//             $topic = $question['topic'];
//             $post[TOPIC_ID] = array_get($topic, TABLE_ID);
//             $post['topic_title'] = array_get($topic, TITLE);
//             $allTopics = $course['topics'];
            
            return $this->view('ccd/question/add_question', [
                'errorMsg' => \Session::getFlash('errorMsg'),
                'errors' => \Session::getFlash('errors'),
                'post' =>  $post,
                'year_id' => $session_id,
                'edit' => 'true', 'num' => $post[QUESTION_NO],
                'courseName' => $course[COURSE_CODE],
                'table_id' => $table_id,
                'year' => $courseSession[SESSION],
//                 'allTopics' => $allTopics,
//                 'topic' => $topic,
                'courseId' => $courseId,
                'next' => array_get($_REQUEST, 'next')
            ]);
        }
        
        $ret = $this->questionModel->updateRecord($_POST);
        
        if($ret[SUCCESSFUL])
        {
            // If user uses the "save_and_goto_next" button, then he wants to edit the next question immediately
            $nextQuestion = \App\Models\Questions::getNextQuestion($session_id, $_POST[QUESTION_NO]);
            
            if(empty($_POST['save_and_goto_next_question']) || empty($nextQuestion))
            {
                $this->session->flash("success", $ret[MESSAGE]);
                
                redirect_(array_get($_REQUEST, 'next', getAddr('ccd_all_session_questions', [$courseId, $session_id])));
                //     			redirect_(getAddr('ccd_all_session_questions', [$courseId, $session_id]));
            }
            
            $this->session->flash("success", "Question {$_POST[QUESTION_NO]} updated, Now in question ({$nextQuestion[QUESTION_NO]})");
            
            redirect_(getAddr('ccd_edit_session_question', [$courseId, $session_id, $nextQuestion[TABLE_ID] ]));
        }
        
        $this->session->flash("errorMsg", $ret[MESSAGE]);
        
        if(!empty($ret['val_errors'])){
            $this->session->flash('errors', $ret['val_errors']);
        }
        redirect_(null);
    }
    
    function delete($courseId, $session_id, $table_id)
    {
        $success = \App\Models\Questions::where(TABLE_ID, '=', $table_id)->delete();
        
        if ($success)
        {
            \Session::flash('success', 'Record deleted successfully');
        }
        else
        {
            \Session::flash('error', 'Delete failed');
        }
        
        redirect_(getAddr('ccd_all_session_questions', [$courseId, $session_id]));
    }
    
    function previewSingleSessionQuestion($courseId, $session_id, $table_id)
    {
        $questionObj = \App\Models\Questions::where(TABLE_ID, '=', $table_id)
        ->with(['session', 'session.course'])->first();
        
        if (!$questionObj) return $this->view('common/message', [ 'errors' => 'No Record found' ]);
        
        // 		$session = $questionObj->session()->first();
        $session = $questionObj['session'];
        $course = $session['course'];
        
        $allPassages = $questionObj->session()->first()->passages()->get();
        $allInstructions = $questionObj->session()->first()->instructions()->get();
        
        return $this->view('ccd/question/preview_single_question', [
            'year' => $session[SESSION],
            'courseName' => $course[COURSE_CODE],
            'courseId' => $course[TABLE_ID],
            'questionObj' =>  $questionObj,
            // 			'topic' =>  $questionObj['topic'],
            'year_id' => $session[TABLE_ID],
            'allPassages' => $allPassages,
            'allInstructions' => $allInstructions,
            'courseCode' => $course[COURSE_CODE],
        ]);
        
    }
    
    
    
}
