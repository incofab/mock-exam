<?php


// require_once __DIR__ . '/Faker/src/autoload.php';
// require_once 'C:/Users/Incofab/Desktop/libraries/php library/Faker-master/Faker/src/autoload.php';
require_once 'D:/Users/user/Downloads/Compressed/Faker-master/Faker-master/src/autoload.php';


$faker = Faker\Factory::create();

 echo '<br />';
// seed_users_table($faker);
// seedPlayersTable($faker);
// seedEventsTable($faker);



function seed_users_table($faker) 
{
	for ($i = 0; $i < 50; $i++)
	{
		$obj = new \App\Models\Users();
		
		$arr = [
		    FIRSTNAME => $faker->firstname,
		    LASTNAME => $faker->lastname,
		    PASSWORD => 'password',
		    EMAIL => $faker->email,
		    USERNAME => $faker->username,
		    PHONE_NO => $faker->unique()->phoneNumber,
		    ADDRESS => $faker->address,
		    COUNTRY => $faker->randomElement(\App\Models\Users::COUNTRIES),
		    'day' => $faker->numberBetween(1, 30),
		    'year' => $faker->numberBetween(1930, 1999),
		    'month' => $faker->numberBetween(1, 11),
		    GENDER => $faker->randomElement(['male', 'female']),
		    IS_VIRTUAL_USER => true,
		];
		
		$ret = $obj->insert($arr);
		if(!getValue($ret, SUCCESSFUL)) dDie($ret);
	}
	echo 'Users table seeded <br />';
}

function seedPlayersTable($faker) 
{
    for ($i = 0; $i < 50; $i++) 
    {
        $obj = new \App\Models\Players();
        $arr = [
            PLAYER_NAME => $faker->name,
            PLAYER_POSITION => $faker->randomElement(\App\Models\Players::POSSIBLE_POSITIONS),
            PLAYER_CLUB => $faker->country,
            PLAYER_COUNTRY => $faker->country,
            PLAYER_RATING => $faker->numberBetween(40, 99),
        ];

        $ret = $obj->insert($arr);
        
        if(!getValue($ret, SUCCESSFUL)) dDie($ret);
    }
    echo 'Players table seeded <br />';
}

function seedEventsTable($faker) 
{
    $c = new \Carbon\Carbon();
    $c->addDays(40);
    for ($i = 0; $i < 50; $i++) 
    {
        $obj = new \App\Models\Events();
        
        $arr = [
            MATCH_DAY_ID => 1,
            PLAYER_1 => $faker->numberBetween(1, 25),
            PLAYER_2 => $faker->numberBetween(26, 50),
            PLAYER_1_ODD => $faker->numberBetween(1, 6),
            PLAYER_2_ODD => $faker->numberBetween(1, 6),
            DRAW_ODD => $faker->numberBetween(1, 7),
            END_TIME => $c->toDateTimeString(),
            START_TIME => $c->toDateTimeString(),
        ];
        
        $ret = $obj->insert($arr);
        
        if(!getValue($ret, SUCCESSFUL)) dDie($ret);
    }
    echo 'Events table seeded <br />';
}


	

echo 'done <br />';





