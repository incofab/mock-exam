<?php
namespace App\Core;

class UsersChart
{
    private $carbon;
    private $usersModel;
    
    function __construct(\App\Models\Users $usersModel, \Carbon\Carbon $carbon) 
    {
        $this->carbon = $carbon;
        
        $this->usersModel = $usersModel;
    }
    
    private $last12days = [];
    private $last12weeks = []; 
    private $last12months = [];
    
    private $growth = [];
    
    function generateChartData() 
    {
        $users = $this->usersModel->where(IS_VIRTUAL_USER, '=', false)->get([TABLE_ID, CREATED_AT]);
        
        $numOfRealUsers = $this->usersModel->getTotalNumOfRealUsers();
        
        $lineGrowth = [];
        $now = $this->carbon->now();
        foreach ($users as $user) 
        {
            /** @var \Carbon\Carbon $createdAt */
            $createdAt = $user[CREATED_AT];
            $daysDiff = $createdAt->diffInDays($now, false);
            
            if($daysDiff <= 12) 
            {
                $this->recordLast12days($daysDiff);
            }
            
            $weekDiff = $createdAt->diffInWeeks($now);
            if($weekDiff <= 12)
            {
                $this->recordLast12weeks($weekDiff); 
            }
            
            $monthYear = $createdAt->format('M Y');
            
            // For Last 12 Months
            $this->recordLast12months($monthYear);
        }
    }
    
    private $last12monthsSize = 0;
    private function recordLast12months($monthYear) 
    {
        if(isset($this->last12months[$monthYear]))
        {
            $this->last12months[$monthYear]['num_users'] += 1;
        }
        else
        {
            if($this->last12monthsSize > 12) return;
            
            $this->last12monthsSize++;
            
            $this->last12months[$monthYear] = [
                'num_users' => 1,
                'label' => $monthYear,
            ];
        }
    }
    
    private function recordLast12weeks($weekDiff)  
    {
        $week = "week$weekDiff";
        if(isset($this->last12weeks[$week]))
        {
            $this->last12weeks[$week]['num_users'] += 1;
        }
        else
        {
            $label = "Last $weekDiff Weeks";
            if($weekDiff == 0) $label = 'This Week';
            if($weekDiff == 1) $label = 'Last Week';
            $this->last12weeks[$week] = [
                'num_users' => 1,
                'label' => $label,
            ];
        }
    }
    
    private function recordLast12days($daysDiff) 
    {
        $day = "day$daysDiff"; 
        if(isset($this->last12days[$day]))
        {
            $this->last12days[$day]['num_users'] += 1;
        }
        else
        {
            $label = "Last $daysDiff Days";
            if($daysDiff == 0) $label = 'Today';
            if($daysDiff == 1) $label = 'Yesterday';
            $this->last12days[$day] = [
                'num_users' => 1,
                'label' => $label,
            ];
        }
    }
    
    private function recordGrowth($label, $timestamp) 
    {
        $last = last($this->growth);
        
        $this->growth[] = [
            'num_users' => array_get($last, 'num_users', 0) + 1,
            'label' => $label,
            'timestamp' => $timestamp
        ];
    }
    
    private $formattedLast12daysChartData = [];    
    function getLast12daysChartData()
    {
        if(!empty($this->formattedLast12daysChartData)) return $this->formattedLast12daysChartData;
        
        $this->formattedLast12daysChartData = $this->formatForChart($this->last12days);
        
        $this->formattedLast12daysChartData['label'] = 'Users registered in the last 12 Days';
        
        return $this->formattedLast12daysChartData;
    }
    
    private $formattedLast12weeksChartData = [];    
    function getLast12weeksChartData()
    {
        if(!empty($this->formattedLast12weeksChartData)) return $this->formattedLast12weeksChartData;
        
        $this->formattedLast12weeksChartData = $this->formatForChart($this->last12weeks);
        
        $this->formattedLast12weeksChartData['label'] = 'Users registered in the last 12 Weeks';
        
        return $this->formattedLast12weeksChartData;
    }
    
    private $formattedLast12monthsChartData = [];    
    function getLast12monthsChartData()
    {
        if(!empty($this->formattedLast12monthsChartData)) return $this->formattedLast12monthsChartData;
        
        $this->formattedLast12monthsChartData = $this->formatForChart($this->last12months);
        
        $this->formattedLast12monthsChartData['label'] = 'Users registered in the last 12 Months';
        
        return $this->formattedLast12monthsChartData;
    }
    
    private $formattedGrowthChartData = [];    
    function getGrowthChartData()
    {
        if(!empty($this->formattedGrowthChartData)) return $this->formattedGrowthChartData;
        
        $this->formattedGrowthChartData = $this->formatForChart($this->growth);
        
        $this->formattedGrowthChartData['label'] = 'Growth Graph';
        $this->formattedGrowthChartData['type'] = 'line';
        
        return $this->formattedGrowthChartData;
    }
    
    private function formatForChart($records) 
    {
        $labels = [];
        $datasets = [];
        $colors = [];
        
        $maxYAxis = 10; 
        foreach ($records as $data)
        {
            $labels[] = $data['label'];
            $datasets[] = $data['num_users'];
            if($data['num_users'] > $maxYAxis) $maxYAxis = $data['num_users'];
            $colors[] = '#ee4fde';
        }
        
        return [
            'type' => 'bar',
            'labels' => $labels,
            'datasets' => $datasets,
            'bg-colors' => $colors,
            'max' => $maxYAxis,
        ];
    }
    
    
}