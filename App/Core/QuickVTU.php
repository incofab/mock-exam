<?php
namespace App\Core;
/**
 *
 * PHP version 5
 *
 * @category Authentication
 * @package  Quick VTU
 * @author   Mbanusi Ikenna <b>Incofabikenna@gmail.com</b>
 * @license  http://opensource.org/licenses/BSD-3-Clause 3-clause BSD
 * @link     https://github.com/incofab
 */
class QuickVTU {

	private static $apiKey = 'qv_live_RnbajkJb7eyGv2HlWcH4olt4s',
					$merchantId = '2007753978';


	/**
	 * Send airtime to the suppliend phone number
	 * @param unknown $amount
	 * @param unknown $recipientPhoneNo
	 * @return mixed
	 */
	static function sendAirtime($amount, $recipientPhoneNo) {
		
		$serviceURL = 'http://quickvtu.com/api/v1/';
				
		$curl_post_data = array
		(
				'amount' => $amount, ///// amount you want to send to customer
				'apiKey' => self::$apiKey, ////// your secret API key
				'merchantId' => self::$merchantId, ///// your merchant ID
				'recipientMobile' => $recipientPhoneNo ////////must be a valid mobile number, all networks are available
				
		);

		$curl_response = self::execute_curl($serviceURL, $curl_post_data);

		return json_decode($curl_response, true);

	}

	/**
	 * Checks if a previously made airtime transfer has been credited
	 * @param unknown $reference_id The reference number return when the deposit was made
	 * @return mixed|string|array
	 */
	static function statusRequest($reference_id) {
		
		$serviceURL = 'http://quickvtu.com/api/v1/status';
		
		$curl_post_data = array
		(
				'apiKey' => self::$apiKey,
				'merchantId' => self::$merchantId,
				'TransactionReference' =>  $reference_id, ////////// returned after every transaction
		);

		$curl_response = self::execute_curl($curl_post_data);

		return json_decode($curl_response, true);

	}

	/**
	 * Helper function to create and execute curl requests
	 * @param unknown $curl_post_data
	 * @return mixed
	 */
	private static function execute_curl($serviceURL, $curl_post_data) {

		$curl = curl_init($serviceURL);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);

		$curl_response = curl_exec($curl);

		curl_close($curl);

		return $curl_response;
	}


}







