<?php
namespace App\Core;

class SubscriptionPlan
{
    const SUBSCRIPTION_PLAN_PER_EVENT = 'Per Event';
    const SUBSCRIPTION_PLAN_ONE_MONTH = '1 Month';
    const SUBSCRIPTION_PLAN_TWO_MONTH = '2 Months';
    const SUBSCRIPTION_PLAN_THREE_MONTH = '3 Months';
    const SUBSCRIPTION_PLAN_SIX_MONTH = '6 Months';
    const SUBSCRIPTION_PLAN_ONE_YEAR = '1 Year';
    
    private $jwt;
    private $sysID;
    private $centerSettingsModel;
    private $subscriptionData;
    private $salt = "&df?><1!";
    
    public function __construct(
        \App\Models\CenterSettings $centerSettingsModel,
        \App\Core\JWT $jwt,
        \App\Core\SystemIdentificator $sysID
    ){
        $this->jwt = $jwt;
        $this->sysID = $sysID;
        $this->centerSettingsModel = $centerSettingsModel;
    }

    private $centerCode;
    private $redirectAddr;
    
    function init($centerCode, $redirectAddr = null)
    {
        $this->centerCode = $centerCode;
        
        $this->redirectAddr = $redirectAddr;
    }
    
    private function verifyInitCall()
    {
        if(empty($this->centerCode)) 
        {
            throw new \Exception('You must first initialize this class by calling init()');
        }
    }
    
    private function exitNow($message)
    {
        if($this->redirectAddr)
        {
            \Session::flash('error', $message);
            
            redirect_($this->redirectAddr);
        }
        
        die("<h2>$message</h2>");
    }
    
    function getSubscriptionData()
    {
        $this->verifyInitCall();
        
        if(empty($this->subscriptionData))
        {
            $subscription = $this->centerSettingsModel
                ->where(CENTER_CODE, '=', $this->centerCode)
                ->where(KEY, '=', SUBSCRIPTION_DATA)
                ->orderBy(TABLE_ID, '=', 'DESC')->first();
            
            if(!$subscription)
            {
                return null;
//                 $this->exitNow("You are not subscribed to any plan", $redirectAddr);  
            }
            
            $this->subscriptionData = (array)$this->jwt->decode(array_get($subscription, VALUE), SECRET_KEY_JWT);
        }
        
        if(array_get($this->subscriptionData, 'system_id') !== $this->sysID->getId())
        {
            $this->exitNow("Mismatched Identifier, Contact administrator", $this->redirectAddr);  
        }
    
        return $this->subscriptionData;
    }
    
    function getPlan() 
    {
        return array_get($this->getSubscriptionData(), 'plan');
    }
    
    function isPerEventSubscription() 
    {
        return array_get($this->getSubscriptionData(), 'plan') 
            == self::SUBSCRIPTION_PLAN_PER_EVENT;
    }
    
    function getExpiry()
    {
        return array_get($this->getSubscriptionData(), 'expiry');
    }
    
    function getSubscriptionDate() 
    {
        return array_get($this->getSubscriptionData(), 'subscription_date');
    }
    
    function isExpired() 
    {
        $now = \Carbon\Carbon::now();
        
        $subscriptionDate = \Carbon\Carbon::parse($this->getSubscriptionDate());
        
        $expiryDate = \Carbon\Carbon::parse($this->getExpiry());
        
        if($now->timestamp >= $expiryDate->timestamp) return true;
        
        if($now->timestamp < $subscriptionDate->timestamp) return true;
        
        return false;
    }
    
    function generateSubscriptionData($pin, $event=0)
    {
        $url = DEV ? 'http://localhost/formula1/activate-cbt-mock-app' 
            : 'http://formula1autozone.com.ng/activate-cbt-mock-app';
        
        $now = \Carbon\Carbon::now();
        
        $minTime = \Carbon\Carbon::parse(\Config::MIN_SYSTEM_TIME);
        
        if($now->timestamp < $minTime->timestamp)
        {
            return [SUCCESSFUL => false, MESSAGE => 'This PC Time is incorrect'];
        }
        
        $data = [
            'system_id' => $this->sysID->getId(),
            'event' => $event,
            'pin' => $pin,
            'subscription_date' => $now->toDateTimeString()
        ];
        
        
        
        
        if($pin < '333')
        {
            return [SUCCESSFUL => false, MESSAGE => 'Invalid pin'];
        }
        
        $plan = '1 Month';
        $arr = [
            'system_id' => $this->sysID->getId(),
            'plan' => $plan,
            'pin' => $pin,
            'subscription_date' => $now->toDateTimeString(),
            'expiry' => $now->copy()->addMonth(1)->toDateTimeString(),
        ];
        
        $subscriptionData = $this->jwt->encode($arr, SECRET_KEY_JWT);
        
        return [
            SUCCESSFUL => true,
            MESSAGE => 'successfully activated',
            'subscription_data' => $subscriptionData,
            'event' => $event,
            'plan' => $plan,
            'activation_quantity' => 100
        ];
        
        
        
        
        
        
        
        
        
        
        
        
        
        $ret = $this->executeCurl($url, $data);
        
        return $ret;
    }
    
    private function executeCurl($url, $data)
    {
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // the SOAP request
        
        // converting
        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        
        if($error) return [SUCCESSFUL => false, MESSAGE => "Error connecting to server"];
        
        return $response;
        
//         if(empty($response[SUBSCRIPTION_DATA])) return [SUCCESSFUL => false, MESSAGE => 'Subscription Data not found' ];
        
//         return [SUCCESSFUL => true, MESSAGE => '', SUBSCRIPTION_DATA => $response[SUBSCRIPTION_DATA] ];
    }
    
}




