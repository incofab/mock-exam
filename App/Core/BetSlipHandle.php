<?php
namespace App\Core;

class BetSlipHandle
{
    private $session;
    
    private $myBets = [];
    
    const MY_BETS = 'my_bets';
    
    const LAST_UPDATE = 'last_update';
    
    function __construct(\Session $session) 
    {
        $this->session = $session;
        
        $this->loadMyBets();
    }
    
    private function loadMyBets() 
    {
        $this->myBets = $this->session->get(self::MY_BETS, []);
        
        $lastUpdated = array_get($this->myBets, self::LAST_UPDATE, 0);
        
        if((time() - $lastUpdated) > (30 * 60))
        {
            $this->myBets = [];
            
            $this->clearSlip();
        }
        
        return $this->myBets;        
    }
    
    private function updateBetSession($myBets)
    {
        $this->myBets[self::MY_BETS] = $myBets;
        
        $this->myBets[self::LAST_UPDATE] = time();
        
        $this->session->put(self::MY_BETS, $this->myBets);
    }
    
    function clearSlip()
    {
        $this->myBets = [];
        
        $this->session->put(self::MY_BETS, null);
    }
    
    function getMyBets()
    {
        return array_get($this->myBets, self::MY_BETS, []);
    }
 
    function addBet($eventID, $userBet)
    {
        $myBets = array_get($this->myBets, self::MY_BETS, []);
        
        $len = count($myBets);  
        
        $isMatched = false;
        for ($i = 0; $i < $len; $i++) 
        {
            if($myBets[$i][EVENT_ID] == $eventID)
            {
                // if Eventd ID and userbet matches the selected, then 
                // Deselect the bet.
                if($myBets[$i][USERS_BET] == $userBet)
                {
                    return $this->removeBet($eventID);
                }
                
                $myBets[$i][USERS_BET] = $userBet;
                
                $isMatched = true;
            }
        }
        
        if(!$isMatched)
        {
            $myBets[] = [
                EVENT_ID => $eventID,
                USERS_BET => $userBet,
            ];
        }
        
        $this->updateBetSession($myBets);
        
        return $myBets;
    }
    
    function removeBet($eventID) 
    { 
        $myBets = array_get($this->myBets, self::MY_BETS, []);
        
        $len = count($myBets);
        
        for ($i = 0; $i < $len; $i++)
        {
            if($myBets[$i][EVENT_ID] == $eventID)
            {
                array_splice($myBets, $i, 1);
                break;
            }
        }
        
        $this->updateBetSession($myBets);
        
        return $myBets; 
    }
    
    function removeMultipleBets($eventIDsArr) 
    {
        $myBets = array_get($this->myBets, self::MY_BETS, []);
        
        foreach ($eventIDsArr as $eventID)
        {
            $myBets = $this->removeBet($eventID, false);
        }
        
        return $myBets;
    }
    
    
    
}



