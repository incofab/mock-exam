<?php
namespace App\Core;

use App\Models\BaseModel;

class CourseInstaller
{
    const INSTALL_FROM_HTML = false;
    
    public $coursesFolder = APP_DIR.'../public/courses/'.(self::INSTALL_FROM_HTML ? 'html/' : 'json/');
    public static $imagesBaseFolder = APP_DIR."../public/img/content/";
    
    const IS_INSTALLED_SUFFIX = '.installed.zip';
    
    private $extractionFolder = APP_DIR."../public/files/content/extracted/installed-courses/";
    
    private $coursesModel;
    private $sessionModel;
    private $passagesModel;
    private $instructionsModel;
    private $questionsModel;
    private $summaryModel;
    
    function __construct(
        \App\Models\Courses $coursesModel,
        \App\Models\Sessions $sessionModel,
        \App\Models\Passages $passagesModel,
        \App\Models\Instructions $instructionsModel,
        \App\Models\Questions $questionsModel,
        \App\Models\Summary $summaryModel
    ){
        $this->coursesModel = $coursesModel;
        
        $this->sessionModel = $sessionModel;
        
        $this->passagesModel = $passagesModel;
        
        $this->instructionsModel = $instructionsModel;
        
        $this->questionsModel = $questionsModel;
        
        $this->summaryModel = $summaryModel;
        
        if(!file_exists($this->coursesFolder)){
            mkdir($this->coursesFolder, 0777, true);
        }
    }
    
    function isCourseInstalled($courseId) 
    {
        return file_exists($this->coursesFolder.$courseId.self::IS_INSTALLED_SUFFIX);
    }
    
    function canInstallCourse($courseId) 
    {
        return file_exists($this->coursesFolder.$courseId.'.zip');
    }
    
    function installCourse($courseId) 
    {
        BaseModel::beginTransaction();
        
        $ret = $this->startInstallation($courseId);
//         dlog($ret);
        if($ret[SUCCESSFUL])
        {
            BaseModel::commit();
            
            rename($this->coursesFolder.$courseId.'.zip', $this->coursesFolder.$courseId.self::IS_INSTALLED_SUFFIX);
            
            // Delete the folder with the extracted files
            $extractedFilesDir = $this->extractionFolder.$courseId;
            
            if(is_dir($extractedFilesDir)) \App\Core\Helper::deleteDir($extractedFilesDir);
            
        }else{
            BaseModel::rollBack();
        }
        
        return $ret;
    }
    
    private function startInstallation($courseId)
    {
        $contentPath = $this->coursesFolder.$courseId.'.zip';
        
//         if(file_exists($this->coursesFolder.$courseId.self::IS_INSTALLED_SUFFIX)) 
//         {
//             return [SUCCESSFUL => false, MESSAGE => 'Content already installed'];
//         }
        
        if(!file_exists($contentPath)) return [SUCCESSFUL => false, MESSAGE => 'Error: File not found'];
            
        ini_set('max_execution_time', 1440);  
        
        if(self::INSTALL_FROM_HTML)
        {
            $ret = $this->extractAndFormatHTMLContent($contentPath, $courseId);
        }
        else 
        {
            $ret = $this->extractAndFormatJSONContent($contentPath, $courseId);
        }
//         dDie($ret);    
        return $ret;
    }
	
    private function extractAndFormatHTMLContent($contentPath, $courseId)
    {
        $course = \App\Models\Courses::where(TABLE_ID, '=', $courseId)->first();
        
        if(empty($course)) return [SUCCESSFUL => false, MESSAGE => 'Error: Course not found'];
        
	    $zip = new \ZipArchive();
	    
	    $res = $zip->open($contentPath);
	    
	    if($res !== TRUE) die('<h2>File could not open</h2>');
	    
	    $contentDir = $this->extractionFolder.$courseId;
	    
	    $zip->extractTo($contentDir);
	    $zip->close();
	    
	    $formatedSessionsAndQuestions = $this->formatSessionsAndQuestions($contentDir, $course);
	    
	    if(empty($formatedSessionsAndQuestions)) 
	    {
	        return [SUCCESSFUL => false, MESSAGE => 'Error: No content to install or Content formatting failed'];
	    }
	    
	    // Validate content
	    foreach ($formatedSessionsAndQuestions as $sessionAndQuestions)
	    {
	        $sessionData = $sessionAndQuestions['session_data'];
	        $questionsData = $sessionAndQuestions['questions'];
	        
	        $sessionValidation = $this->sessionModel->validateInsert($sessionData);
	        
	        if(!$sessionValidation[SUCCESSFUL])
	        {
	            dlog($sessionData);
	            
	            dDie($sessionValidation);
	        }
	        
	        // Validate questions
	        foreach ($questionsData as $question) 
	        {
	            $questionValidation = $this->questionsModel->validateData($question);
	            
	            if(!$questionValidation[SUCCESSFUL])
	            {
    	            dlog($sessionData);
    	            
    	            dlog($question);
    	            
    	            dDie($questionValidation);
	            }
	        }	        
	    }
	    // // Validation Complete
	    
	    // Start inserting to DB
	    foreach ($formatedSessionsAndQuestions as $sessionAndQuestions)
	    {
	        $this->insertSessionAndQuestionsRecord(
	            $sessionAndQuestions['session_data'], $sessionAndQuestions['questions']);
	    }
	    
	    return [SUCCESSFUL => true, MESSAGE => 'Data recorded'];
	}
	
	private function formatSessionsAndQuestions($courseDir, $courseData)
	{
	    $allSessions = [];
	    
	    $sessionsList = scandir($courseDir);
	    
	    foreach ($sessionsList as $file)
	    {
	        if(in_array($file, ['.', '..'])) continue;

	        $subjectDir = $courseDir.'/'.$file;
	        
	        if(!is_dir($subjectDir)) continue;
	        
	        $arr = str_getcsv($file, '_');
	        
	        if(empty($arr[0]) || empty($arr[1])) continue;
	        
	        $courseCode = $arr[0];
	        
	        $year = $arr[1];
	        
	        $obj = new \App\Parser\GenericParse($year, $subjectDir);
	        
	        $ret = $obj->parse($courseData);
	        
	        if(!$ret[SUCCESSFUL])
	        {
	            dlog($ret[SUCCESSFUL]);
	            
	            continue;
	        }
	        
	        $allSessions[] = $ret;
	    }
	    
	    return $allSessions;
	}
	
	private function insertSessionAndQuestionsRecord($sessionData, $formattedQuestions)
	{
	    $session = new \App\Models\Sessions();
	    
	    $session = $session->insert($sessionData);
	    
	    if(empty($session[SESSION]))
	    {
	        return [SUCCESSFUL => FALSE, MESSAGE => "Failed to record Acad Session"];
	    }
	    
	    foreach ($formattedQuestions as $questionsArr)
	    {
	        $questionsArr[COURSE_SESSION_ID] = $session[TABLE_ID];
	        
	        $questionRet = (new \App\Models\Questions())->insert($questionsArr);
	        
	        if(empty($questionRet[SUCCESSFUL]))
	        {
	            dlog(array_merge($questionsArr, $questionRet));
	            
	            dDie($questionRet);
	        }
	    }
	    
	    return [SUCCESSFUL => TRUE, MESSAGE => "Data recorded"];
	}
	
	
	
	
	
	

	
	private function extractAndFormatJSONContent($contentPath, $courseId)
	{
	    $course = \App\Models\Courses::where(TABLE_ID, '=', $courseId)->first();
	    
	    if(empty($course)) return [SUCCESSFUL => false, MESSAGE => 'Error: Course not found'];
	    
	    $contentDir = $this->extractionFolder.$courseId;

	    /*
	    $zip = new \ZipArchive();
	    
	    $res = $zip->open($contentPath);
	    
	    if($res !== TRUE) die('<h2>File could not open</h2>');
	    
	    $zip->extractTo($contentDir);
	    $zip->close();
	    */
	    $ret = \App\Core\Helper::unzip($contentPath, $contentDir);
	    
	    if(!$ret[SUCCESSFUL]) return $ret;
	    
	    $courseFilename = "$contentDir/course.json";
	    $sessionsFilename = "$contentDir/sessions.json";
	    $summaryFilename = "$contentDir/summary.json";
	    $summaryData = null;
	    
	    if(!file_exists($courseFilename))
	    {
	        return [SUCCESSFUL => false, MESSAGE => 'Error: Course file not found'];	        
	    }
	    
	    if(!file_exists($sessionsFilename))
	    {
	        return [SUCCESSFUL => false, MESSAGE => 'Error: Sessions file not found'];	        
	    }
	    
	    if(file_exists($summaryFilename))
	    {
    	    $summaryData = json_decode(file_get_contents($summaryFilename), true);
    	    
    	    $summaryVal = $this->validateSummary($summaryData, $courseId);
    
    	    if(!$summaryVal[SUCCESSFUL]) return $summaryVal;
	    }
	    
// 	    $courseData = json_decode(file_get_contents($courseFilename), true);
	    
// 	    $validateCourse = $this->coursesModel->validateInsert($courseData);

// 	    if(!$validateCourse[SUCCESSFUL]) return $validateCourse;
	    
	    $allSessionData = json_decode(file_get_contents($sessionsFilename), true);
	    
	    $sessionVal = $this->validateSessions($allSessionData, $contentDir, $courseId);

	    if(!$sessionVal[SUCCESSFUL]) return $sessionVal;
	    
	    if(empty($allSessionData))
	    {
	        return [SUCCESSFUL => false, MESSAGE => 'Error: Sessions file content empty'];
	    }
	    
// 	    $firstSession = array_get($allSessionData, 0);
// 	    if(array_get($firstSession, COURSE_ID) !== $courseId
// 	        && array_get($firstSession, COURSE_CODE) !== $course[COURSE_CODE])
// 	    {
// 	        return [SUCCESSFUL => false, MESSAGE => 'Error: Course code mismatch. '
// 	            .'Edit the course code to match what is on the file'];
// 	    }
	    
// 	    $sessionIDMappingOldToNew = [];
	    $allUpdatedSessions = [];
	    $allSessionDataCopy = [];
	    // Now start saving records to database
        foreach ($allSessionData as $sessionData) 
        {
            $updatedSessionData = $this->sessionModel->where(COURSE_ID, '=', $courseId)
            ->where(SESSION, '=', $sessionData[SESSION])->first();
            
            //Check if session already exists
            if($updatedSessionData) continue;
            
            // $updatedSessionData updated with the new DB record
    	    $updatedSessionData = $this->sessionModel->create([
                COURSE_ID => $courseId,
                SESSION => $sessionData[SESSION],
                CATEGORY => $sessionData[CATEGORY],
                GENERAL_INSTRUCTIONS => $sessionData[GENERAL_INSTRUCTIONS],
    	    ]);
            
//             $sessionIDMappingOldToNew[$sessionData[TABLE_ID]] = $updatedSessionData[TABLE_ID];
            $allUpdatedSessions[] = $updatedSessionData;
            $allSessionDataCopy[] = $sessionData;
            
    	    $passages = $sessionData['passages'];
    	    
    	    foreach ($passages as $passage) 
    	    {
    	        $this->passagesModel->create([
                    COURSE_ID => $courseId,
    	            COURSE_SESSION_ID => $updatedSessionData[TABLE_ID],
    	            PASSAGE => $passage[PASSAGE],
    	            FROM_ => $passage[FROM_],
    	            TO_ => $passage[TO_],
    	        ]);
    	    }
    	    
    	    $instructions = $sessionData['instructions'];
        
    	    foreach ($instructions as $instruction) 
    	    {
    	        $this->instructionsModel->create([
    	            COURSE_ID => $courseId,
    	            COURSE_SESSION_ID => $updatedSessionData[TABLE_ID],
    	            INSTRUCTION => $instruction[INSTRUCTION],
    	            FROM_ => $instruction[FROM_],
    	            TO_ => $instruction[TO_],
    	        ]);
    	    }
    	    
    	    $questionsFilename = "$contentDir/questions_{$sessionData[SESSION]}_{$sessionData[TABLE_ID]}.json";
    	    
    	    if(!file_exists($questionsFilename)) continue;
    	    
    	    $questionsData = json_decode(file_get_contents($questionsFilename), true);
    	    
    	    foreach ($questionsData as $question) 
    	    {
    	        $question[COURSE_SESSION_ID] = $updatedSessionData[TABLE_ID];
    	        
    	        $questionInstallRet = (new \App\Models\Questions())->insert($question);
    	        
    	        if(!$questionInstallRet[SUCCESSFUL])
    	        {
    	            dDie($questionInstallRet);
    	        }
    	    }
        }
	    
	    // Copy images
	    $imageLocation = "$contentDir/img";
// 	    $this->handleImagesForJSON($sessionIDMappingOldToNew, $imageLocation, $courseId);
	    $this->handleImagesForJSON($allSessionDataCopy, $allUpdatedSessions, $imageLocation, $courseId);
	    /*
        $imagesFolder = APP_DIR."../public/img/content/$courseId";
        
        if(is_dir($imageLocation))
        {
            if(is_dir($imagesFolder))
            {
                \App\Core\Helper::deleteDir($imagesFolder, false);
            }
            else
            {
                mkdir($imagesFolder, 0777, true);
            }
            
            \App\Core\Helper::copy($imageLocation, $imagesFolder);
        }
        */
        // Insert Summary Data
        if($summaryData)
        {
            foreach ($summaryData as $summary) 
            {
                (new \App\Models\Summary())->insert($summary);
            }
        }
        
        return [SUCCESSFUL => true, MESSAGE => "{$course[COURSE_CODE]} installed successfully"];
	}
	
    // Copy images
	private function handleImagesForJSON($oldSessionsData, $updatedSessionsData, $imageLocation, $courseId)
	{
// 	    $imagesFolder = APP_DIR."../public/img/content/$courseId";
	    $imagesFolder = self::$imagesBaseFolder.$courseId;
	    
	    if(!is_dir($imageLocation)) return;
	    
	    $i = -1;
	    foreach ($oldSessionsData as $oldSession) 
	    {
	        $i++;
	        $updatedSession = $updatedSessionsData[$i];
	        
	        
	        $destinationFolder = "$imagesFolder/{$updatedSession[TABLE_ID]}";
	        
	        $sourceFolder = "$imageLocation/{$oldSession[TABLE_ID]}";
	        $sourceFolder2 = "$imageLocation/{$oldSession[SESSION]}";
	        
	        if(!is_dir($sourceFolder)) {
	            $sourceFolder = $sourceFolder2;
	        }
	        
	        if(!is_dir($sourceFolder)) continue;
	        
	        // If the destination folder is available, skip (means it has already been handled)
	        // else create the folder and copy images
	        if(!is_dir($destinationFolder)){
	            mkdir($destinationFolder, 0777, true);
	        }else continue;
	        
	        \App\Core\Helper::copy($sourceFolder, $destinationFolder);
	    }
	    
// 	    foreach ($sessionIDMappingOldToNew as $oldSessionId => $newSessionId) 
// 	    {
// 	        $destinationFolder = "$imagesFolder/$newSessionId";
	        
// 	        $sourceFolder = "$imageLocation/$oldSessionId";
	        
// 	        if(!is_dir($sourceFolder)) continue;
            
// 	        if(!is_dir($destinationFolder)) mkdir($destinationFolder, 0777, true);
	        
//     	    \App\Core\Helper::copy($sourceFolder, $destinationFolder);
// 	    }
	}
	
	private function validateSessions($allSessionData, $contentDir, $courseId)
	{
	    foreach ($allSessionData as $sessionData) 
	    {
	        $sessionData[COURSE_ID] = $courseId;
	        
            $val = $this->sessionModel->validateInsert($sessionData);
	        
            if(!$val[SUCCESSFUL]) return $val;
            
            $questionsFilename = "$contentDir/questions_{$sessionData[SESSION]}_{$sessionData[TABLE_ID]}.json";
            
            if(!file_exists($questionsFilename))
            {
//                 return [SUCCESSFUL => false, MESSAGE => "Error: Questions file for {$sessionData[SESSION]} not found"];
                continue;
            }
            
            $questionsData = json_decode(file_get_contents($questionsFilename), true);
            
            $questionsVal = $this->validateQuestions($questionsData, $contentDir);

            if(!$questionsVal[SUCCESSFUL]) return $questionsVal;
	    }
	    
	    return [SUCCESSFUL => true, MESSAGE => 'Validation successful'];
	}
	
	private function validateQuestions($questionsData, $contentDir)
	{
	    foreach ($questionsData as $question) 
	    {
	        $ret = $this->questionsModel->validateInsert($question);
	        
	        if(!$ret[SUCCESSFUL])
	        {
	            return $ret;
	        }
	    }
	    
	    return [SUCCESSFUL => true, MESSAGE => 'Validation successful'];
	}
	
	private function validateSummary($summayData, $courseId)
	{
	    foreach ($summayData as $summary)
	    {
	        $summary[COURSE_ID] = $courseId;
	        
	        $ret = $this->summaryModel->validateInsert($summary);
	        
	        if(!$ret[SUCCESSFUL])
	        {
	            return $ret;
	        }
	    }
	    
	    return [SUCCESSFUL => true, MESSAGE => 'Validation successful'];
	}
		
	
	function unInstallCourse($courseId)
	{
	    $course = $this->coursesModel->where(TABLE_ID, '=', $courseId)->first();
	    
	    if(!$course) return [SUCCESSFUL => false, MESSAGE => htmlentities($courseId).' not found'];
	    
	    $allSessions = $this->sessionModel->where(COURSE_ID, '=', $courseId)->get();
	    
	    /** @var \App\Models\Sessions $session */
	    foreach ($allSessions as $session) 
	    {
// 	        $session->questions()->delete();
	        
	        $this->questionsModel
	           ->where(COURSE_SESSION_ID, '=', $session[TABLE_ID])->delete();

	        $this->instructionsModel->where(COURSE_ID, '=', $courseId)
	        ->where(COURSE_SESSION_ID, '=', $session[TABLE_ID])->delete();

	        $this->passagesModel->where(COURSE_ID, '=', $courseId)
	        ->where(COURSE_SESSION_ID, '=', $session[TABLE_ID])->delete();

	        $this->summaryModel->where(COURSE_ID, '=', $courseId)->delete();
	           
	        $session->delete();
	    }
	    
	    $imagesFolder = APP_DIR."../public/img/content/$courseId";
	    
        if(is_dir($imagesFolder))
        {
            \App\Core\Helper::deleteDir($imagesFolder, true);
        }
        
        rename($this->coursesFolder.$courseId.self::IS_INSTALLED_SUFFIX, $this->coursesFolder.$courseId.'.zip');
        
        return [SUCCESSFUL => true, MESSAGE => $course[COURSE_CODE].' uninstalled successfully'];
	}
	
	static function deleteImg($courseId, $course_session_id = null, $filename = null) 
	{
	    $file = self::$imagesBaseFolder."$courseId/$course_session_id/$filename";
	    
	    if($course_session_id) $file = "$file/$course_session_id";
	    if($filename) $file = "$file/$filename";
	    
	    if(!file_exists($file)) return;
	    
	    if(is_dir($file)) \App\Core\Helper::deleteDir($file, true);
	    else unlink($file);
	}
	
}
