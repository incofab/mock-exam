<?php
namespace App\Core;

class Points
{

        const ACTION_GOAL = 'goal_scored';
        const ACTION_ASSIST = 'assist';
        const ACTION_YELLOW_CARD = 'yellow_card';
        const ACTION_RED_CARD = 'red_card';
        const ACTION_PK_MISS = 'pk_miss';
        const ACTION_CLEAN_SHEET = 'clean_sheet';
        const ACTION_GOAL_CONCEDED = 'goal_conceded';
        const ACTION_WIN = 'win';
        const ACTION_PK_SAVE = 'pk_save';
        const ACTION_PLAY_TIME = 'play_time';
        const ACTION_OWN_GOAL = 'own_goal';
        const ACTION_FULL_PLAY = 'full_play';
        const ACTION_COMEBACK_WIN = 'comeback_win';
        const ACTION_SUBTITUTE_GOAL = 'subtitute_goal';
        
        const POSITION_COACH = 'coach';
        const POSITION_GOALIE = 'goalie';
        const POSITION_DEENDER = 'defender';
        /**
         * Utility player refers to midfielders, Attackers and strikers
         * @var string
         */
        const POSITION_UTILITY_PLAYER = 'utility player';
        
        const POSITION_MIDFIELDER = 'midfielder';
        
        const POSITION_FORWARD = 'forward';
        
        
        static function getPositionCode($position)
        {
            $positionCode = 'UT';
            switch ($position) 
            {
                case self::POSITION_COACH: $positionCode = 'HC'; break;
                
                case self::POSITION_DEENDER: $positionCode = 'DF'; break;
                
                case self::POSITION_FORWARD: $positionCode = 'FD'; break;
                
                case self::POSITION_GOALIE: $positionCode = 'GK'; break;
                
                case self::POSITION_MIDFIELDER: $positionCode = 'MD'; break;
            
                case self::POSITION_UTILITY_PLAYER: $positionCode = 'UT'; break;
            }
            
            return $positionCode;
        }
        
        static function getGamePointsArr()
        {
            return self::$gamePoints;
        }
        
        static function getPoints($action, $position)
        {
            $gamePoints = self::$gamePoints;
            if(!isset($gamePoints[$action]))
            {
                throw new \Exception("Invalid Action '$action' supplied");
            }
            
            $action = $gamePoints[$action];
            
            if(!isset($action[$position]))
            {
                throw new \Exception("Invalid Position '$position' supplied");
            }
            
            return $action[$position];
        } 
        
        static function calculateScore($playerPerformance, $position)
        {
            $score = 0;
            foreach ($playerPerformance as $action => $value)
            {
                $value = (int)$value;
                
                if(!empty(self::$gamePoints[$action]))
                {
                    $score += $value * self::getPoints($action, $position);
                }
            }
            
            return $score;
        }
        
        private static $gamePoints = [
            
            self::ACTION_GOAL =>[
                self::POSITION_COACH => 10,
                self::POSITION_GOALIE => 15,
                self::POSITION_DEENDER => 12,
                self::POSITION_UTILITY_PLAYER => 10,
                self::POSITION_MIDFIELDER => 10,
                self::POSITION_FORWARD => 10,
            ],
            
            self::ACTION_ASSIST =>[
                self::POSITION_COACH => 0,
                self::POSITION_GOALIE => 12,
                self::POSITION_DEENDER => 10,
                self::POSITION_UTILITY_PLAYER => 6,
                self::POSITION_MIDFIELDER => 6,
                self::POSITION_FORWARD => 6,
            ],
            
            self::ACTION_YELLOW_CARD =>[
                self::POSITION_COACH => -1.5,
                self::POSITION_GOALIE => -1.5,
                self::POSITION_DEENDER => -1.5,
                self::POSITION_UTILITY_PLAYER => -1.5,
                self::POSITION_MIDFIELDER => -1.5,
                self::POSITION_FORWARD => -1.5,
            ],
            
            self::ACTION_RED_CARD =>[
                self::POSITION_COACH => -3,
                self::POSITION_GOALIE => -3,
                self::POSITION_DEENDER => -3,
                self::POSITION_UTILITY_PLAYER => -3,
                self::POSITION_MIDFIELDER => -3,
                self::POSITION_FORWARD => -3,
            ],
            
            self::ACTION_PK_MISS =>[
                self::POSITION_COACH => -5,
                self::POSITION_GOALIE => -1,
                self::POSITION_DEENDER => -3,
                self::POSITION_UTILITY_PLAYER => -6,
                self::POSITION_MIDFIELDER => -6,
                self::POSITION_FORWARD => -6,
            ],
            
            self::ACTION_CLEAN_SHEET =>[
                self::POSITION_COACH => 5,
                self::POSITION_GOALIE => 5,
                self::POSITION_DEENDER => 3,
                self::POSITION_UTILITY_PLAYER => 0,
                self::POSITION_MIDFIELDER => 0,
                self::POSITION_FORWARD => 0,
            ],
            
            self::ACTION_GOAL_CONCEDED =>[
                self::POSITION_COACH => -10,
                self::POSITION_GOALIE => -2,
                self::POSITION_DEENDER => 0,
                self::POSITION_UTILITY_PLAYER => 0,
                self::POSITION_MIDFIELDER => 0,
                self::POSITION_FORWARD => 0,
            ],
            
            self::ACTION_WIN =>[
                self::POSITION_COACH => 5,
                self::POSITION_GOALIE => 5,
                self::POSITION_DEENDER => 5,
                self::POSITION_UTILITY_PLAYER => 5,
                self::POSITION_MIDFIELDER => 5,
                self::POSITION_FORWARD => 5,
            ],
            
            self::ACTION_PK_SAVE =>[
                self::POSITION_COACH => 3,
                self::POSITION_GOALIE => 3,
                self::POSITION_DEENDER => 0,
                self::POSITION_UTILITY_PLAYER => 0,
                self::POSITION_MIDFIELDER => 0,
                self::POSITION_FORWARD => 0,
            ],
            
//             self::ACTION_PLAY_TIME =>[
//                 self::POSITION_COACH => 0,
//                 self::POSITION_GOALIE => 9,
//                 self::POSITION_DEENDER => 9,
//                 self::POSITION_UTILITY_PLAYER => 9,
//                 self::POSITION_MIDFIELDER => 9,
//                 self::POSITION_FORWARD => 9,
//             ],
            
            self::ACTION_OWN_GOAL =>[
                self::POSITION_COACH => -5,
                self::POSITION_GOALIE => 0,
                self::POSITION_DEENDER => -5,
                self::POSITION_UTILITY_PLAYER => -5,
                self::POSITION_MIDFIELDER => -5,
                self::POSITION_FORWARD => -5,
            ],
            
            self::ACTION_FULL_PLAY =>[
                self::POSITION_COACH => 0,
                self::POSITION_GOALIE => 0,
                self::POSITION_DEENDER => 4,
                self::POSITION_UTILITY_PLAYER => 4,
                self::POSITION_MIDFIELDER => 4,
                self::POSITION_FORWARD => 4,
            ],
            
            self::ACTION_COMEBACK_WIN =>[
                self::POSITION_COACH => 6,
                self::POSITION_GOALIE => 0,
                self::POSITION_DEENDER => 0,
                self::POSITION_UTILITY_PLAYER => 0,
                self::POSITION_MIDFIELDER => 0,
                self::POSITION_FORWARD => 0,
            ],
            
            self::ACTION_SUBTITUTE_GOAL =>[
                self::POSITION_COACH => 3,
                self::POSITION_GOALIE => 3,
                self::POSITION_DEENDER => 3,
                self::POSITION_UTILITY_PLAYER => 3,
                self::POSITION_MIDFIELDER => 3,
                self::POSITION_FORWARD => 3,
            ],
            
        ];
    
    
}







