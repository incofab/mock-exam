<?php
namespace App\Core;

use Valitron\Validator as Valitron;

class Validator extends Valitron{
	
	function __construct($data, $fields = array(), $lang = null, $langDir = null) {
		parent::__construct($data, $fields, $lang, $langDir);
		$this->addCustomRules();
	}
	
	private function addCustomRules() 
	{
		$msg = 'field should contain only alpha-numeric characters, dashes, dots and underscores';
		$this->addRule('username', [\App\Core\Validator::class, 'validateUsername'], $msg);
		$msg = 'field should contain only digits';
		$this->addRule('digits', [\App\Core\Validator::class, 'validateDigits'], $msg);
	}
	
	/**
	 * Validate that a field contains only alpha-numeric characters, dashes, dots and underscores
	 *
	 * @param  string $field
	 * @param  mixed  $value
	 * @return bool
	 */
	static function validateUsername($field, $value)
	{
	    $value = trim($value);
	    
		return preg_match('/^([a-z0-9\._-])+$/i', $value);
	}

	/**
	 * Validate that a field contains only digits
	 *
	 * @param  string $field
	 * @param  mixed  $value
	 * @return bool
	 */
	static function validateDigits($field, $value)
	{
	    $value = trim($value);
	    
		return preg_match('/^([0-9])+$/i', $value);
	}
	
	
	
}