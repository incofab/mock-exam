<?php
namespace App\Core;

class ValidatorWrapper{
    
//     private $container;
    
    /**
     * @var \App\Core\Validator
     */
    private $val;
    
    function __construct() 
    {
//         $this->container = $c->getContainerInstance();
    }
    
    /**
     * Initialize validator with wrapper
     * @return \App\Core\Validator
     */
    function initValidator($data, $fields = array(), $lang = null, $langDir = null) 
    {        
        $this->val = new \App\Core\Validator($data, $fields, $lang, $langDir);
        
        return $this->val;
    }
    
}