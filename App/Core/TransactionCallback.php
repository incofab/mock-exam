<?php
namespace App\Core;

class TransactionCallback
{
    function callFeedbackURL(\App\Models\Transactions $tData) 
    {
        if(    $tData[CHOICE_PLATFORM] != CHOICE_PLATFORM_API
            || $tData[DELIVERY_STATUS] != STATUS_PENDING 
            || $tData[TRANSACTION_STATUS] == STATUS_PENDING 
            || $tData[DELIVERY_ATTEMPTS] >= 3 
        ){
            return [SUCCESSFUL => false, 'in-code' => true, MESSAGE => 'Transaction already handled'];
        }
        
        /** @var \App\Models\Users $userData */
        $userData = $tData->user(); 
        
        if(!$userData) return [SUCCESSFUL => false, 'in-code' => true, MESSAGE => 'User data not found'];
        
        /** @var \App\Models\Merchants */
        $merchantData = $userData->merchant();
        
        if(!$merchantData) return [SUCCESSFUL => false, 'in-code' => true, MESSAGE => 'Merchant Data not found'];
        
        $url = $merchantData[CALLBACK_URL];
        
        if(empty($url)) return [SUCCESSFUL => false, 'in-code' => true, MESSAGE => 'No callback url set'];
        
        $curl_post_data = [
            
            PRIVATE_KEY => $merchantData[PRIVATE_KEY],
            
            PUBLIC_KEY => $merchantData[PUBLIC_KEY],
            
            ORDER_ID => $tData[ORDER_ID],
            
            'status' => $tData[TRANSACTION_STATUS],
            
            AMOUNT => $tData[AMOUNT],
            
            // Other parameters like the ones below should be included as well
            // Contains pretty much every parameter that is returned on status request
            
            PHONE_NO => $tData[PHONE_NO],
            
            NETWORK => $tData[NETWORK],
            
            'reference' => $tData[TABLE_ID],
            
            TRANSACTION_CHARGE => $tData[TRANSACTION_CHARGE],
            
            BALANCE_BEFORE_TRANSACTION => $tData[BALANCE_BEFORE_TRANSACTION],
            
            BALANCE_AFTER_TRANSACTION => $tData[BALANCE_AFTER_TRANSACTION],
        ];
        
        $ret = $this->execute_curl($url, $curl_post_data);
        
        if($ret['success'])
        {
            $tData->markAsDelivered();
        }
        else 
        {
            $tData->incrementAttempts();
        }
        
        return $ret;
    }
    
    private function execute_curl($url, $curl_post_data)
    {
        $curl = curl_init($url);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        
        $curl_response = curl_exec($curl);
        
        $httpErrorCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        $error = curl_error($curl);
        curl_close($curl);
        
        if($error)
        {
            return ['success' => false, 'result_code' => $httpErrorCode,
                'message' => $error,  'response' => $curl_response];
        }
        
        if(empty($curl_response) && $httpErrorCode != 200)
        {
            return ['success' => false, 'result_code' => $httpErrorCode, 'response' => $curl_response,
                'message' => "Error, trying to reach destination: $httpErrorCode, try again later"];
        }
        
        return ['success' => true, 'response' => $curl_response];
    }
    
    function callDevFeedbackURL(\App\Models\DevTransactions $tData)
    {
        if(    $tData[CHOICE_PLATFORM] != CHOICE_PLATFORM_API
            || $tData[DELIVERY_STATUS] != STATUS_PENDING
            || $tData[TRANSACTION_STATUS] == STATUS_PENDING
            || $tData[DELIVERY_ATTEMPTS] >= 3
            ){
                return [SUCCESSFUL => false, 'in-code' => true, MESSAGE => 'Transaction already handled'];
        }
        
        /** @var \App\Models\Users $userData */
        $userData = $tData->user();
        
        if(!$userData) return [SUCCESSFUL => false, 'in-code' => true, MESSAGE => 'User data not found'];
        
        /** @var \App\Models\Merchants */
        $merchantData = $userData->merchant();
        
        if(!$merchantData) return [SUCCESSFUL => false, 'in-code' => true, MESSAGE => 'Merchant Data nit found'];
        
        $url = $merchantData[CALLBACK_URL];
        
        if(empty($url)) return [SUCCESSFUL => false, 'in-code' => true, MESSAGE => 'No callback url set'];
        
        $curl_post_data = [
            
            PRIVATE_KEY => $merchantData[PRIVATE_KEY],
            
            PUBLIC_KEY => $merchantData[PUBLIC_KEY],
            
            ORDER_ID => $tData[ORDER_ID],
            
            'status' => $tData[TRANSACTION_STATUS],
            
            AMOUNT => $tData[AMOUNT],
            
            // Other parameters like the ones below should be included as well
            // Contains pretty much every parameter that is returned on status request
            
            PHONE_NO => $tData[PHONE_NO],
            
            NETWORK => $tData[NETWORK],
            
            'reference' => $tData[TABLE_ID],
            
            TRANSACTION_CHARGE => $tData[TRANSACTION_CHARGE],
            
            BALANCE_BEFORE_TRANSACTION => $tData[BALANCE_BEFORE_TRANSACTION],
            
            BALANCE_AFTER_TRANSACTION => $tData[BALANCE_AFTER_TRANSACTION],
        ];
        
        $ret = $this->execute_curl($url, $curl_post_data);
        
        return $ret;
    }
    
}