<?php

use Interop\Container\ContainerInterface;

return [
    
    
    
    \Session::class => function (ContainerInterface $c) { return new \Session(); },
    
    \App\Core\BetSlipHandle::class => function (ContainerInterface $c) { return new \App\Core\BetSlipHandle($c->get(\Session::class)); },
    \App\Core\BrowserDetector::class => function (ContainerInterface $c) { return new \App\Core\BrowserDetector(); },
    \App\Core\CodeGenerator::class => function (ContainerInterface $c) { return new \App\Core\CodeGenerator(); },
    \App\Core\Emitter::class => function (ContainerInterface $c) { return new \App\Core\Emitter(); },
    \App\Core\ErrorCodes::class => function (ContainerInterface $c) { return new \App\Core\ErrorCodes(); },
    \App\Core\JWT::class => function (ContainerInterface $c) { return new \App\Core\JWT(); },
    //     \App\Core\Login::class => function (ContainerInterface $c) { return new \App\Core\Login([], ''); },
    \App\Core\Points::class => function (ContainerInterface $c) { return new \App\Core\Points(); },
    \App\Core\Settings::class => function (ContainerInterface $c) { return new \App\Core\Settings(); },
    \App\Core\ValidatorWrapper::class => function (ContainerInterface $c) 
             { return (new \App\Core\ValidatorWrapper(/*new \Bootstrap\Container\MyContainer()*/)); },

    \Carbon\Carbon::class => function (ContainerInterface $c) { return new \Carbon\Carbon(); },
    
    
    
    
    
    \App\Models\Activation::class => function (ContainerInterface $c) { return new \App\Models\Activation(INJECTED); },
    \App\Models\Admin::class => function (ContainerInterface $c) { return new \App\Models\Admin(INJECTED); },
    \App\Models\BankDeposits::class => function (ContainerInterface $c) { return new \App\Models\BankDeposits(INJECTED); },
    \App\Models\Complaints::class => function (ContainerInterface $c) { return new \App\Models\Complaints(INJECTED); },
    \App\Models\Notifications::class => function (ContainerInterface $c) { return new \App\Models\Notifications(INJECTED); },
    \App\Models\PasswordReset::class => function (ContainerInterface $c) { return new \App\Models\PasswordReset(INJECTED); },
    \App\Models\Session::class => function (ContainerInterface $c) { return new \App\Models\Session(INJECTED); },
    \App\Models\Users::class => function (ContainerInterface $c) { return new \App\Models\Users(INJECTED); },     

    
    
    
];



















