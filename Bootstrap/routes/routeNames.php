<?php

$routesArr = [
    
    /* Login Routes */
    'user_login' => '/login',
    'admin_login' => '/admin/login',
    'forgot_password' => '/forgot_password',
    'reset_password' => '/reset_password',
    
    'home' => '/',
    'home_message' => '/message',
    'home_developers' => '/developers',
    'register_user' => '/register',
    'home_activate_user' => '/activate',
    'home_resend_activation' => '/resend-activation-code',
    'home_how_it_works' => '/how-it-works',
    'home_privacy_policy' => '/privacy-policy',
    'home_about_us' => '/about-us',
    'home_contact_us' => '/contact-us',
    'home_terms' => '/terms-and-conditions',
    'home_quick_cash' => '/quick-cash',
    
    'home_view_exam_result' => '/single-exam-result',
    
    
    /* Admin Routes */
    'admin_dashboard' => '/admin',
    'admin_all' => '/admin/all-admin-users',
    'admin_add' => '/admin/add-new-admin-user',
    'admin_edit' => '/admin/edit-admin-user',
    'admin_delete' => '/admin/delete-admin-user',
    'admin_changepassword' => '/admin/changepassword',
    'admin_logout' => '/admin/logout',
    
    'admin_view_all_users' => '/admin/all_users',
    'admin_search_user' => '/admin/search_users',
    'admin_send_user_broadcast' => '/admin/users/send-broadcast-sms',
    'admin_export_users' => '/admin/export_users',
    'admin_view_user_profile' => '/admin/user_profile',
    'admin_user_suspend' => '/admin/suspend_user',
    'admin_user_unsuspend' => '/admin/unsuspend_user',
    'admin_delete_user' => '/admin/delet_user',    
    
    'admin_view_all_centers' => '/admin/centers/all',
    'admin_view_add_center' => '/admin/centers/add',
    'admin_edit_center' => '/admin/centers/edit',
    'admin_view_center' => '/admin/centers/view-detail',
    'admin_center_suspend' => '/admin/centers/suspend',
    'admin_center_unsuspend' => '/admin/centers/unsuspend',
    'admin_delete_center' => '/admin/centers/delete',

    'admin_install_courses' => '/admin/install-uninstall-courses',
    'admin_uninstall_course' => '/admin/uninstall-course',
    'admin_export_content' => '/admin/export-course-content',
    
    'admin_user_withdrawals' => '/admin/user/transactions/withdrawals',
    'admin_cancel_withdrawal_request' => '/admin/transaction/cancel-withdrawal-request',
    'admin_confirm_withdrawal_request' => '/admin/transaction/confirm-withdrawal-requests',
    
    'admin_deposits' => '/admin/transaction/airtime-deposits',
    
    'admin_bank_deposits' => '/admin/transaction/bank-deposits',
    'admin_cancel_bank_deposit' => '/admin/bank/cancel-deposits',
    'admin_confirm_bank_deposit' => '/admin/bank/confirm-deposit',
    'admin_delete_bank_deposit' => '/manage/backend/admin/dashboard/bank/delete-deposit',

    'admin_merchants' => '/admin/transaction/merchants',
    
    'admin_quick_cash_deposits' => '/admin/transaction/quick-cash-deposits',
    
    
    'admin_user_complaints' => '/admin/user-complaints',
    'admin_complaints' => '/admin/all-complaints',
    'admin_sort_complaints' => '/admin/sort-complaints',
    'admin_delete_complaint' => '/admin/delete-complaint',
    'admin_resolved_complaint' => '/admin/resolve-complaint',
    
    'admin_send_sms' => '/admin/send-sms',
    'admin_send_notification' => '/admin/send-notification',
    
    'admin_exam_contents' => '/admin/exam-content/all',
    'admin_exam_content_create' => '/admin/exam-content/create',
    'admin_exam_content_edit' => '/admin/exam-content/edit',
    'admin_exam_content_delete' => '/admin/exam-content/delete',
    'admin_output_json' => '/admin/exam-content/output-json',
    'admin_output_json_delete' => '/admin/exam-content/output-json-delete',
    
        
    'user_logout' => '/user/logout',
    'user_changepassword' => '/user/changepassword',
    'user_dashboard' => '/user',
    'user_profile' => '/user/profile',
    'user_edit_profile' => '/user/edit_profile',
    'user_notifications' => '/user/notifications',
    
    'user_create_merchant_account' => '/user/create-merchant-account',
    'user_edit_merchant_account' => '/user/edit-merchant-account',
    'user_merchant_account_profile' => '/user/my-merchant-account-data',

    'user_airtime_deposit' => '/user/airtime/bank-deposit',
    'user_deposit_history' => '/user/airtime/deposit-history',
    
    'user_withdraw' => '/user/withdraw',
    'user_withdrawal_history' => '/user/airtime/withdrawal-history',

    'user_bank_deposit' => '/user/bank-deposit',
    
    'user_register_complaint' => '/user/register-complaint',
    
    'confirm_airtime' => '/secure/confirm-airtime-deposit',
    'confirm_quick_cash' => '/secure/confirm-quickcash-deposit',
    
    'api_v1' => '/api/v1',
    'api_v1_test' => '/api/test/v1',
    'api_v1_evaluate_test' => '/api/test/v1/evaluate',
    
    
    'center_login' => '/center/login',
    'center_logout' => '/center/logout',
    'center_changepassword' => '/center/changepassword',
    'center_dashboard' => '/center',
    'center_profile' => '/center/profile',
    'center_edit_profile' => '/center/edit_profile',
    'center_subscribe' => '/center/subscribe',
    
    'center_view_all_students' => '/center/students/all',
    'center_add_student' => '/center/students/add',
    'center_edit_student' => '/center/students/edit',
    'center_view_student' => '/center/students/view-detail',
    'center_student_suspend' => '/center/students/suspend',
    'center_student_unsuspend' => '/center/students/unsuspend',
    'center_delete_student' => '/center/students/delete',
    'center_upload_students' => '/center/students/upload',
    'center_multi_delete_student' => '/center/students/multi-delete',
    
    'center_view_all_events' => '/center/events/all',
    'center_add_event' => '/center/events/add',
    'center_edit_event' => '/center/events/edit',
    'center_preview_event' => '/center/events/preview',
    'center_view_event' => '/center/events/view-detail',
    'center_event_suspend' => '/center/events/suspend',
    'center_event_unsuspend' => '/center/events/unsuspend',
    'center_delete_event' => '/center/events/delete',
    'center_event_result' => '/center/events/result',
    'center_sms_event_result' => '/center/events/sms-event-result',
    'center_sms_invite' => '/center/events/sms-invite',
    'center_event_result_download' => '/center/events/download-result',
    'center_event_pause_all_exams' => '/center/events/pause-all-exams',
    
    'center_view_all_exams' => '/center/exams/all',
    'center_register_exam' => '/center/exams/register',
    'center_multi_register_exam' => '/center/exams/multi-register',
    'center_edit_exam' => '/center/exams/edit',
    'center_force_end_exam' => '/center/exams/force-end-exam',
    'center_exam_suspend' => '/center/exams/suspend',
    'center_exam_unsuspend' => '/center/exams/unsuspend',
    'center_delete_exam' => '/center/exams/delete',
    'center_extend_exam_time' => '/center/exams/extend-exam-time',
    
    'center_view_exam_result' => '/center/single-exam-result',
    
    'student_exam_login' => '/student/exam',
    'student_start_exam' => '/student/start-exam',
    'student_pause_exam' => '/student/pause-exam',
    'student_end_exam' => '/student/end-exam',
    'student_attempt_question' => '/student/exam/attempt-question',
    'student_multi_attempt_question' => '/student/exam/multi-attempt-question',
    
    /* CCD */
    'ccd_home' => '/ccd/home',
    'ccd_upload_question_images' => '/ccd/home/upload/question/images',
    'ccd_all_courses' => '/ccd/all_courses',
    'ccd_register_course' =>'/ccd/register_course',
    'ccd_edit_course' =>'/ccd/edit_course',
    'ccd_delete_course' => '/ccd/delete_course',
    
    'ccd_all_sessions' => '/ccd/view_all_sessions',
    'ccd_register_session' => '/ccd/register_session',
    'ccd_edit_session' => '/ccd/edit_session',
    'ccd_delete_session' => '/ccd/delete_session',
    'ccd_preview_session' => '/ccd/preview_session', // Views all the questions recorded for the year
    
    'ccd_all_session_questions' => '/ccd/all_session_questions',
    'ccd_new_session_question' => '/ccd/new_session_question',
    'ccd_edit_session_question' => '/ccd/edit_session_question',
    'ccd_delete_session_question' => '/ccd/delete_session_question',
    'ccd_preview_session_question' => '/ccd/preview_session_question', //Just preview a particular question
    'ccd_new_session_question_api' => '/ccd/api/new_session_question',
    
    'ccd_new_chapter' => '/ccd/new_chapter',
    'ccd_view_all_chapter' => '/ccd/view_all_chapter',
    'ccd_edit_chapter' => '/ccd/edit_chapter',
    'ccd_delete_chapter' => '/ccd/delete_chapter',
    'ccd_preview_chapter' => '/ccd/preview_chapter', 	
    
    'ccd_upload_content' => '/ccd/upload-content', 	
    'ccd_installed_bonus_content' => '/ccd/install-bonus-content', 	
    /* // CCD */
    
    'api_center_event_index' => '/center/api/events/index',
    'api_center_upload_exam' => '/center/api/events/upload-exams',
    'api_center_event_download' => '/center/api/events/download-content',
    
    'exam_demo' => '/exam/demo',
];


return $routesArr;
