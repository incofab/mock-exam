<?php

if(isset($dontRoute)) return;
    
global $routesArr;
$routesArr = require_once APP_DIR . '../Bootstrap/routes/routeNames.php';


$addr = rtrim(ADDR, '/');

/**
 * @var \FastRoute\Dispatcher
 */
$dispatcher = \FastRoute\simpleDispatcher(function (\FastRoute\RouteCollector $r)use ($routesArr, $addr) {

    /* Home Routes */
//     $r->addRoute(['GET'], $addr.'/', function(){ dDie('Welcome to homepage'); });
    $r->addRoute(['GET'], $addr.$routesArr['home'], ['\App\Controllers\Home\Home', 'index']);
    $r->addRoute(['GET'], $addr.$routesArr['home_message'].'[/{messageType}]', [ '\App\Controllers\Home\Home', 'message']);
    $r->addRoute(['GET'], $addr.$routesArr['home_developers'], [ '\App\Controllers\Home\Home', 'developers']);
//     $r->addRoute(['POST','GET'], $addr.$routesArr['home_quick_cash'], [ \App\Controllers\Home\QuickCash::class, 'deposit']);
    $r->addRoute(['GET'], $addr.$routesArr['home_view_exam_result'].'[/{examNo}[/{studentID}]]', [ \App\Controllers\Home\Exams::class, 'viewExamResult']);
    
    $r->addRoute(['POST','GET'], $addr.$routesArr['user_login'], [ '\App\Controllers\Home\Login', 'userLogin']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_login'], [ '\App\Controllers\Home\Login', 'adminLogin']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['forgot_password'], [ '\App\Controllers\Home\Login', 'forgotPassword']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['reset_password'], [ '\App\Controllers\Home\Login', 'passwordReset']);
  
    $r->addRoute(['POST','GET'], $addr.$routesArr['register_user'].'[/{referral}]', [ '\App\Controllers\Home\Home', 'registerUser']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['home_activate_user'], [ '\App\Controllers\Home\Home', 'activateUserAccount']);
    $r->addRoute(['GET'], $addr.$routesArr['home_resend_activation'], [ '\App\Controllers\Home\Home', 'resendActivationCode']);
    $r->addRoute(['GET'], $addr.$routesArr['home_how_it_works'], [ '\App\Controllers\Home\Home', 'howItWorks']);
    $r->addRoute(['GET'], $addr.$routesArr['home_privacy_policy'], [ '\App\Controllers\Home\Home', 'privacyPolicy']);
    $r->addRoute(['GET'], $addr.$routesArr['home_about_us'], [ '\App\Controllers\Home\Home', 'aboutUs']);
    $r->addRoute(['GET'], $addr.$routesArr['home_contact_us'], [ '\App\Controllers\Home\Home', 'contactUs']);
    $r->addRoute(['GET'], $addr.$routesArr['home_terms'], [ '\App\Controllers\Home\Home', 'terms']);
    
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_dashboard'], [ '\App\Controllers\Admin\Admin', 'index']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_all'], [ '\App\Controllers\Admin\Admin', 'all']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_add'], [ '\App\Controllers\Admin\Admin', 'addNew']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_edit'], [ '\App\Controllers\Admin\Admin', 'edit']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_delete'] . '/{table_id}', [ '\App\Controllers\Admin\Admin', 'delete']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_changepassword'], [ '\App\Controllers\Admin\Admin', 'changePassword']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_logout'], [ '\App\Controllers\Admin\Admin', 'logout']);
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_view_all_users'], [ '\App\Controllers\Admin\Users', 'viewAllUsers']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_search_user'], [ '\App\Controllers\Admin\Users', 'searchUsers']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_send_user_broadcast'], [ '\App\Controllers\Admin\Users', 'sendBroadcastSMS']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_export_users'], [ '\App\Controllers\Admin\Users', 'exportUsers']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_view_user_profile'] . '/{table_id}', [ '\App\Controllers\Admin\Users', 'viewUserProfile']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_user_suspend'] . '/{table_id}', [ '\App\Controllers\Admin\Users', 'suspendUser']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_user_unsuspend'] . '/{table_id}', [ '\App\Controllers\Admin\Users', 'unSuspendUser']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_delete_user'] . '/{table_id}', [ '\App\Controllers\Admin\Users', 'deleteUser']);
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_view_all_centers'], [\App\Controllers\Admin\Centers::class, 'all']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['admin_view_add_center'], [\App\Controllers\Admin\Centers::class, 'add']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['admin_edit_center'] . '/{tableid}', [\App\Controllers\Admin\Centers::class, 'edit']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_view_center'] . '/{tableid}', [\App\Controllers\Admin\Centers::class, 'viewDetail']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_center_suspend'] . '/{tableid}', [\App\Controllers\Admin\Centers::class, 'suspend']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_center_unsuspend'] . '/{tableid}', [\App\Controllers\Admin\Centers::class, 'unSuspend']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_delete_center'] . '/{tableid}', [\App\Controllers\Admin\Centers::class, 'delete']);
    
    $r->addRoute(['GET','POST'], $addr.$routesArr['admin_install_courses'].'[/{course}]', [\App\Controllers\CCD\UploadContent::class, 'installCourse']);
    $r->addRoute(['GET','POST'], $addr.$routesArr['admin_uninstall_course'].'/{course}', [\App\Controllers\CCD\UploadContent::class, 'unInstallCourse']);
    $r->addRoute(['GET','POST'], $addr.$routesArr['admin_export_content'].'[/{course}]', [\App\Controllers\CCD\UploadContent::class, 'exportCourse']);

    $r->addRoute(['GET'], $addr.$routesArr['admin_user_withdrawals'] . '[/{username}]', [ '\App\Controllers\Admin\Withdrawals', 'listWithdrawals']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_cancel_withdrawal_request'] . '/{tableID}', [ '\App\Controllers\Admin\Withdrawals', 'cancelWithdrawalRequest']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_confirm_withdrawal_request'] . '/{username}/{tableID}', [ '\App\Controllers\Admin\Withdrawals', 'confirmWithdrawal']);
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_deposits']. '/{status}[/{lastIndex}[/{forwards}]]', [\App\Controllers\Admin\Transactions::class, 'all']);
    
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_merchants']. '[/{lastIndex}[/{forwards}]]', [\App\Controllers\Admin\Merchants::class, 'all']);

    $r->addRoute(['GET'], $addr.$routesArr['admin_quick_cash_deposits']. '/{status}[/{lastIndex}[/{forwards}]]', [\App\Controllers\Admin\QuickCash::class, 'all']);

    
    $r->addRoute(['GET'], $addr.$routesArr['admin_bank_deposits']. '[/{username}]', [ '\App\Controllers\Admin\BankDeposits', 'listDeposits']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_cancel_bank_deposit'] . '/{tableID}', [ '\App\Controllers\Admin\BankDeposits', 'cancelBankDeposit']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_confirm_bank_deposit'] . '/{username}/{tableID}', [ '\App\Controllers\Admin\BankDeposits', 'creditUser']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_delete_bank_deposit'] . '/{tableID}', [ '\App\Controllers\Admin\BankDeposits', 'deleteBankDeposit']);
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_complaints'], [ '\App\Controllers\Admin\Complaints', 'getComplaints']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_sort_complaints'], [ '\App\Controllers\Admin\Complaints', 'sortByCategory']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_delete_complaint'] . '/{table_id}[/{username}]', [ '\App\Controllers\Admin\Complaints', 'deleteComplaint']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_resolved_complaint'] . '/{table_id}[/{username}]', [ '\App\Controllers\Admin\Complaints', 'markAsResolved']);
    $r->addRoute(['GET'], $addr.$routesArr['admin_user_complaints'] . '/{username}', [ '\App\Controllers\Admin\Complaints', 'getUsersComplaints']);

    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_send_sms'] . '[/{phoneNo}]', [ '\App\Controllers\Admin\Admin', 'sendSMS']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_send_notification'] . '/{username}', [ '\App\Controllers\Admin\Admin', 'sendNotification']);
    
    $r->addRoute(['GET'], $addr.$routesArr['admin_exam_contents'], [ \App\Controllers\Admin\ExamContent::class, 'all']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_exam_content_create'], [ \App\Controllers\Admin\ExamContent::class, 'create']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_exam_content_edit'].'/{id}', [ \App\Controllers\Admin\ExamContent::class, 'edit']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_exam_content_delete'].'/{id}', [ \App\Controllers\Admin\ExamContent::class, 'delete']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_output_json'].'/{id}', [ \App\Controllers\Admin\ExamContent::class, 'outputJson']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['admin_output_json_delete'].'/{id}', [ \App\Controllers\Admin\ExamContent::class, 'deleteOutputJson']);
    
    
    
    $r->addRoute(['GET'], $addr.$routesArr['user_dashboard'], [ '\App\Controllers\User\User', 'index']);
    $r->addRoute(['GET'], $addr.$routesArr['user_profile'], [ '\App\Controllers\User\User', 'profile']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['user_edit_profile'], [ '\App\Controllers\User\User', 'editProfile']);
    $r->addRoute(['GET'], $addr.$routesArr['user_logout'], [ '\App\Controllers\User\User', 'logout']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['user_changepassword'], [ '\App\Controllers\User\User', 'changePassword']);
    $r->addRoute(['GET'], $addr.$routesArr['user_notifications'], [ '\App\Controllers\User\User', 'viewNotifications']);

    $r->addRoute(['POST','GET'], $addr.$routesArr['user_create_merchant_account'], [ \App\Controllers\User\Merchant::class, 'create']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['user_edit_merchant_account'], [ \App\Controllers\User\Merchant::class, 'edit']);
    $r->addRoute(['GET'], $addr.$routesArr['user_merchant_account_profile'], [ \App\Controllers\User\Merchant::class, 'merchantProfile']);

    $r->addRoute(['GET'], $addr.$routesArr['user_deposit_history'], [ \App\Controllers\User\Transactions::class, 'depositHistory']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['user_airtime_deposit'], [ \App\Controllers\User\Transactions::class, 'deposit']);
    
    $r->addRoute(['POST','GET'], $addr.$routesArr['user_withdraw'], [ \App\Controllers\User\Withdrawals::class, 'withdraw']);
    $r->addRoute(['GET'], $addr.$routesArr['user_withdrawal_history'], [ \App\Controllers\User\Withdrawals::class, 'withdrawalHistory']);

    $r->addRoute(['POST','GET'], $addr.$routesArr['user_bank_deposit'], [ '\App\Controllers\User\BankDeposits', 'deposit']);
    
    $r->addRoute(['POST','GET'], $addr.$routesArr['user_register_complaint'], [ '\App\Controllers\User\User', 'registerComplaint']);

    $r->addRoute(['POST','GET'], $addr.$routesArr['confirm_airtime'] . '/{table_id}/{status}', [ \App\Controllers\Admin\TransactionsConfirmation::class, 'confirmTransaction']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['confirm_quick_cash'] . '/{table_id}/{status}', [ \App\Controllers\Admin\TransactionsConfirmation::class, 'confirmQuickCash']);

    
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_login'], [ \App\Controllers\Home\Login::class, 'centerLogin']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_changepassword'], [ \App\Controllers\Centers\Center::class, 'changePassword']);
    $r->addRoute(['GET'], $addr.$routesArr['center_logout'], [ \App\Controllers\Centers\Center::class, 'logout']);
    $r->addRoute(['GET'], $addr.$routesArr['center_dashboard'], [ \App\Controllers\Centers\Center::class, 'index']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_subscribe'], [ \App\Controllers\Centers\Subscription::class, 'subscribe']);
    $r->addRoute(['GET'], $addr.$routesArr['center_profile'], [ \App\Controllers\Centers\Center::class, 'profile']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_edit_profile'], [ \App\Controllers\Centers\Center::class, 'editProfile']);
    
    $r->addRoute(['GET'], $addr.$routesArr['center_view_all_students'], [ \App\Controllers\Centers\Students::class, 'all']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_add_student'], [ \App\Controllers\Centers\Students::class, 'add']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_edit_student'] . '/{id}', [ \App\Controllers\Centers\Students::class, 'edit']);
    $r->addRoute(['GET'], $addr.$routesArr['center_view_student'] . '/{id}', [ \App\Controllers\Centers\Students::class, 'viewStudentDetail']);
    $r->addRoute(['GET'], $addr.$routesArr['center_student_suspend'] . '/{id}', [ \App\Controllers\Centers\Students::class, 'suspend']);
    $r->addRoute(['GET'], $addr.$routesArr['center_student_unsuspend'] . '/{id}', [ \App\Controllers\Centers\Students::class, 'unSuspend']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_delete_student'] . '/{id}', [ \App\Controllers\Centers\Students::class, 'delete']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_upload_students'], [ \App\Controllers\Centers\Students::class, 'uploadStudents']);
    $r->addRoute(['POST'], $addr.$routesArr['center_multi_delete_student'], [ \App\Controllers\Centers\Students::class, 'multiDeleteStudents']);
    
    $r->addRoute(['GET'], $addr.$routesArr['center_view_all_events'], [ \App\Controllers\Centers\Events::class, 'all']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_add_event'], [ \App\Controllers\Centers\Events::class, 'createEvent']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_edit_event'] . '/{id}', [ \App\Controllers\Centers\Events::class, 'edit']);
    $r->addRoute(['GET'], $addr.$routesArr['center_preview_event'] . '/{id}', [ \App\Controllers\Centers\Events::class, 'previewEvent']);
    $r->addRoute(['GET'], $addr.$routesArr['center_event_suspend'] . '/{id}', [ \App\Controllers\Centers\Events::class, 'suspend']);
    $r->addRoute(['GET'], $addr.$routesArr['center_event_unsuspend'] . '/{id}', [ \App\Controllers\Centers\Events::class, 'unSuspend']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_delete_event'] . '/{id}', [ \App\Controllers\Centers\Events::class, 'delete']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_event_result'] . '/{id}', [ \App\Controllers\Centers\Events::class, 'eventResult']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_sms_event_result'] . '/{id}', [ \App\Controllers\Centers\Events::class, 'smsEventResult']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_sms_invite'] . '/{id}', [ \App\Controllers\Centers\Events::class, 'smsInvite']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_event_result_download'] . '/{id}', [ \App\Controllers\Centers\Events::class, 'downloadEventResult']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_event_pause_all_exams'] . '/{eventId}', [ \App\Controllers\Centers\Events::class, 'pauseAllExams']);
    
    
    $r->addRoute(['GET'], $addr.$routesArr['center_view_all_exams'].'[/{eventID}]', [ \App\Controllers\Centers\Exams::class, 'all']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_register_exam'].'[/{studenID}]', [ \App\Controllers\Centers\Exams::class, 'registerExam']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_multi_register_exam'].'/{eventID}', [ \App\Controllers\Centers\Exams::class, 'multiRegisterExam']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_edit_exam'] . '/{id}', [ \App\Controllers\Centers\Exams::class, 'edit']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_force_end_exam'] . '/{examNo}', [ \App\Controllers\Centers\Exams::class, 'forceEndExam']);
    $r->addRoute(['GET'], $addr.$routesArr['center_exam_suspend'] . '/{id}', [ \App\Controllers\Centers\Exams::class, 'suspend']);
    $r->addRoute(['GET'], $addr.$routesArr['center_exam_unsuspend'] . '/{id}', [ \App\Controllers\Centers\Exams::class, 'unSuspend']);
    $r->addRoute(['GET'], $addr.$routesArr['center_delete_exam'] . '/{id}', [ \App\Controllers\Centers\Exams::class, 'delete']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['center_extend_exam_time'].'/{examNo}', [ \App\Controllers\Centers\Exams::class, 'extendExamTime']);

    $r->addRoute(['GET'], $addr.$routesArr['center_view_exam_result'] . '/{examNo}/{studentID}', [ \App\Controllers\Centers\Exams::class, 'viewExamResult']);
    
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['student_exam_login'], [ \App\Controllers\Home\Exams::class, 'examLogin']);
    
    $r->addRoute(['GET'], $addr.$routesArr['student_start_exam'], [ \App\Controllers\Exams\Exams::class, 'startExam']);
    $r->addRoute(['GET'], $addr.$routesArr['student_pause_exam'], [ \App\Controllers\Exams\Exams::class, 'pauseExam']);
    $r->addRoute(['GET'], $addr.$routesArr['student_end_exam'], [ \App\Controllers\Exams\Exams::class, 'endExam']);
    $r->addRoute(['POST'], $addr.$routesArr['student_attempt_question'], [ \App\Controllers\Exams\Exams::class, 'attemptQuestion']);
    $r->addRoute(['POST'], $addr.$routesArr['student_multi_attempt_question'], [ \App\Controllers\Exams\Exams::class, 'multiAttemptQuestion']);

    /* CCD */
    // View all the registered courses without need to log in
    $r->addRoute(['GET'], $addr.$routesArr['ccd_home'], [ \App\Controllers\CCD\Courses::class, 'viewAll']);
    $r->addRoute(['POST'], $addr.$routesArr['ccd_upload_question_images'].'/{courseCode}/{year_id}', [ \App\Controllers\CCD\Home::class, 'uploadImage']);
    $r->addRoute(['GET'], $addr.$routesArr['ccd_all_courses'].'[/{id}]', [ \App\Controllers\CCD\Courses::class, 'viewAll']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_register_course'], [ \App\Controllers\CCD\Courses::class, 'registerCourses']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_edit_course'].'/{id}', [ \App\Controllers\CCD\Courses::class, 'editCourse']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_delete_course'].'/{id}', [ \App\Controllers\CCD\Courses::class, 'delete']);
    
    $r->addRoute(['GET'], $addr.$routesArr['ccd_all_sessions'].'/{courseName}', [ \App\Controllers\CCD\Sessions::class, 'viewAllSessions']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_register_session'].'/{courseName}', [ \App\Controllers\CCD\Sessions::class, 'registerSession']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_edit_session'].'/{courseName}/{session_id}', [ \App\Controllers\CCD\Sessions::class, 'editSession']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_delete_session'].'/{courseName}/{session_id}', [ \App\Controllers\CCD\Sessions::class, 'delete']);
    $r->addRoute(['GET'], $addr.$routesArr['ccd_preview_session'].'/{courseName}/{session_id}', [ \App\Controllers\CCD\Sessions::class, 'previewAllSesssionQuestions']);
    
    $r->addRoute(['GET'], $addr.$routesArr['ccd_all_session_questions'].'/{courseName}/{year_id}', [ \App\Controllers\CCD\Questions::class, 'viewAllQuestions']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_new_session_question'].'/{courseName}/{year_id}[/{num}]', [ \App\Controllers\CCD\Questions::class, 'addNewQuestion']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_edit_session_question'].'/{courseName}/{year_id}/{id}', [ \App\Controllers\CCD\Questions::class, 'editQuestion']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_delete_session_question'].'/{courseName}/{year_id}/{id}', [ \App\Controllers\CCD\Questions::class, 'delete']);
    $r->addRoute(['GET'], $addr.$routesArr['ccd_preview_session_question'].'/{courseName}/{year_id}/{id}', [ \App\Controllers\CCD\Questions::class, 'previewSingleSessionQuestion']);
    
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_new_session_question_api'], [ \App\Controllers\CCD\Questions::class, 'addNewQuestionAPI']);

    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_new_chapter'].'/{courseName}', [ \App\Controllers\CCD\Summary::class, 'newChapter']);
    $r->addRoute(['GET'], $addr.$routesArr['ccd_view_all_chapter'].'/{courseName}', [ \App\Controllers\CCD\Summary::class, 'viewAll']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_edit_chapter'].'/{courseName}/{chapter_id}', [ \App\Controllers\CCD\Summary::class, 'editChapter']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_delete_chapter'].'/{courseName}/{chapter_id}', [ \App\Controllers\CCD\Summary::class, 'delete']);
    $r->addRoute(['GET'], $addr.$routesArr['ccd_preview_chapter'].'/{courseName}/{chapter_id}', [ \App\Controllers\CCD\Summary::class, 'preview']);

    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_upload_content'], [ \App\Controllers\CCD\UploadContent::class, 'uploadContent']);
    $r->addRoute(['POST','GET'], $addr.$routesArr['ccd_installed_bonus_content'].'[/{which}]', [ \App\Controllers\CCD\UploadContent::class, 'installBonusContent']);
    /* // CCD */
    
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['api_center_event_index'], [ \App\Controllers\API\EventController::class, 'apiList']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['api_center_upload_exam'], [ \App\Controllers\API\EventController::class, 'apiUploadEventResult']);
    $r->addRoute(['GET', 'POST'], $addr.$routesArr['api_center_event_download'], [ \App\Controllers\API\EventController::class, 'downloadEventContent']);
    
    $r->addRoute(['POST','GET'], $addr.$routesArr['exam_demo'], [ \App\Controllers\Home\Exams::class, 'demoExam']);

    
    require_once APP_DIR . 'rough.php';
    
});

    
    // Fetch method and URI from somewhere
    $httpMethod = $_SERVER['REQUEST_METHOD'];
    
    $uri = $_SERVER['REQUEST_URI'];
    
    // Strip query string (?foo=bar) and decode URI
    if (false !== $pos = strpos($uri, '?')) 
    {
        $uri = substr($uri, 0, $pos);
    }
    
    $uri = rawurldecode($uri);
    
    $routeInfo = $dispatcher->dispatch($httpMethod, $uri);
    
    switch ($routeInfo[0]) 
    {
        case FastRoute\Dispatcher::NOT_FOUND:
//             require APP_DIR . '../resources/views/home/404.php';
            echo view('home/404');
            break;
            
        case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
            $allowedMethods = $routeInfo[1];
            echo '405 Method Not Allowed';
            break;
            
        case FastRoute\Dispatcher::FOUND:
            $handler = $routeInfo[1];
            $parameters = $routeInfo[2];
            $parameters = array_values($parameters);
//             dlog($uri); 
//             dlog($parameters); 
//             dDie($handler); 
//             call_user_func_array([new $handler[0], $handler[1]], $parameters);
            try { 
                $container = (new \Bootstrap\Container\MyContainer())->getContainerInstance();
                $content = $container->call($handler, $parameters);
                if(is_array($content)) $content = json_encode($content);
                echo $content;
            } catch (Exception $e) {
//                 dlog("Error in url $uri, SERVER['REQUEST_URI'] = {$_SERVER['REQUEST_URI']}");
//                 dlog("Error in file {$e->getFile()}, line = {$e->getLine()}");
//                 dlog($e->getTrace());
                throw $e;
            }
            break;
    }
