<?php

/**
 * The directory to the app files
 */
define('APP_DIR', __DIR__ . '/App/');

/** $courseId here is the Course code */
$courseId = $_REQUEST['course_id'];
$course_session_id = $_REQUEST['course_session_id'];
$year = $_REQUEST['session'];
$filename = $_REQUEST['filename'];

// dlog_22("Filename 1 = $filename");
if(stripos($filename, '?')){
    $filename = substr($filename, 0, stripos($filename, '?'));
}

// dlog_22("Filename 2 = $filename");
if($slashPositon = strripos($filename, '/')){
    $filename = substr($filename, $slashPositon+1);
}
// dlog_22("Filename 3 = $filename");
// $filename = pathinfo($filename, PATHINFO_FILENAME);

$file = APP_DIR . "../public/img/content/$courseId/$course_session_id/$filename";
$file2 = APP_DIR . "../public/img/content/$courseId/$year/$filename";

// dlog_22("File = $file");
// dlog_22("File 2 = $file2");

if (!file_exists($file)) $file = $file2;
if (!file_exists($file)) return null;


$type = 'image/jpeg';
header('Content-Type:'.$type);
header('Content-Length: ' . filesize($file));
readfile($file);





function dlog_22($msg) {
    $str = '';
    
    if (is_array($msg)) $str = json_encode($msg, JSON_PRETTY_PRINT);
    
    else $str = $msg;
    
    error_log(
        '*************************************' . PHP_EOL .
        '     Date Time: ' . date('Y-m-d h:m:s') . PHP_EOL .
        '------------------------------------' . PHP_EOL .
        $str . PHP_EOL . PHP_EOL .
        '*************************************' . PHP_EOL,
        
        3, APP_DIR . '../public/errorlog.txt');
    
}