require.config({
    baseUrl: ADDR + 'public/ts/exams_0.608',
    optimize: 'none', // 'uglify'
    waitSeconds: 60,
    paths: {
        // If moduleID starts with "app", File is loaded from the public/ts/app directory
        app: 'app',
        jquery:[
        	'https://code.jquery.com/jquery-3.2.1.min',
        	'../../js/lib/jquery.min',
    	],
    	bootstrap:[
//            'https://code.jquery.com/jquery-3.2.1.min',
            '../../lib/bootstrap4/js/bootstrap.bundle.min',
//            '../../js/lib/bootstrap.min',
        ],
        handlebars: [
            'https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.10/handlebars.min',
            '../../js/lib/handlebars-v4.0.10',
        ],
        'socket.io-client': [
        	'https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js',
        	'../../js/lib/socket.io.min',
        	],
//        K: 'config/k',
    },
    shim: {
    	bootstrap: {
    		deps: ["jquery"],
        }
    },
    packages: {
        
    },
});

var dispatcher = $({});

require(['app'], function(app) {
//	console.log('Init app');
	new app().start(dispatcher);
});
$(function() {	
	// Bind to bootstrap tab change events
	$('#exam-tabs #nav-tab').on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
		dispatcher.trigger("shown.bs.tab", e);
	});
	
});


