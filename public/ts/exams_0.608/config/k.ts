/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/handlebars/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />

import $ = require('jquery');
import Handlebars = require('handlebars');
import Dialog = require('../util/dialog');

import {IExamData} from '../exam_interfaces';

declare var ADDR: string;
declare var Pace:any; // From Pace.js library

declare var examData:IExamData;
var exam = examData.exam;

class K{
    dummyproperty: 'Dummy';
    public static toAskBeforeUnloadEvent:boolean = true;

    public dummy(){
        K.dd('This is a dummy method');
    }
    // Alert messages
    static ALERT_DANGER = 'alert alert-danger';
    static ALERT_SUCCESS = 'alert alert-success';
    static ALERT_INFO = 'alert alert-info';
    static ALERT_WARNING = 'alert alert-warning';
    static ERROR_UNATHORISED_ACCESS = 1;
    static ERROR_INSUFFICIENT_BALANCE = 2;
    static timeOut = null;
    
    static Pace = Pace;
    
    static $sidebar = $('#sidebar > div');
    static $mainPage = $('#exam-layout'); 
    static $loading = $('#loading');
    
    static dismissLoading():void { K.$loading.hide(); };
    static showLoading(msg:string):void {
        K.$loading.find('#text').text(msg || 'Loading...');
        K.$loading.show();
    };
    
    static getMessageHTML(msg:string, alertType:string):string {
        alertType = alertType || K.ALERT_INFO; 
        var html = '<div class="' + alertType + ' text-center ">' + msg + '</div>';
        return html;
    };
    
    static getTimeStamp():number {
        return Math.floor(Date.now() / 1000);
    };
    
    static registerHandlebarHelperFunctions():void{
        // Register Handlebar helper functions globally
        Handlebars.registerHelper('serialNo', function(value:any) {
            return parseInt(value) + 1;
        });
        Handlebars.registerHelper('formatNumber', function(value:number) {
            return K.formatNumber(value);
        });
        Handlebars.registerHelper('arrayCount', function(arr:any[]) {
            return arr.length;
        });
        Handlebars.registerHelper('compare', function(val1:any, val2:any, ret:string, orElse:string, options:any) {
//            K.dd('orElse = '+orElse);
            if(K.isEmpty(val1) || K.isEmpty(val2)) return '';
            return ((val1 == val2) ? ret : orElse);
        });
        Handlebars.registerHelper('in_array', function(arr:any[], val:any, ret:string, options:any) {
            if(!(arr instanceof Array)) return '';
            return (arr.indexOf(val) >= 0) ? ret : '';
        });
        Handlebars.registerHelper('isEmpty', function(val:any[], ret?:string) {
            if(K.isEmpty(val)) return (ret) ? ret : true;
            return false;
        });
    };
    
    static formatNumber(number:any):any {
        if(!number) return 0;
        var arr= number.toString().split('.');
        number = arr[0];
        var dec = arr[1];
        var formatedNum = [];
        var temp = '';
        while (number >= 100) {
            temp = (number % 1000) + '';
            if(temp.length == 1) temp = '00' + temp;
            if(temp.length == 2) temp = '0' + temp;
            formatedNum.push(temp);         
            number = number/1000;
            number = parseInt(number);
        }
        if(number > 0)   formatedNum.push(number);
        formatedNum = formatedNum.reverse();
        return formatedNum.join(', ') + (dec ? ('.' + dec) : '');
    };
    
    static formatTime(time_in_secs:number):string {
        if(isNaN(time_in_secs) || time_in_secs < 0) time_in_secs = 0 
        var total_mins = time_in_secs / 60;
        var hour:any = parseInt((total_mins / 60) + '');
        var min:any = parseInt((total_mins % 60) + '');
        var sec:any = parseInt((time_in_secs % 60) + '');
        sec  = sec  < 10 ? ('0' + sec)  : sec;
        
        if(hour < 1 && min < 1) return sec;

        min  = min  < 10 ? ('0' + min)  : min;
        
        if(hour < 1) return min + ':' + sec;
        
        hour = hour < 10 ? ('0' + hour) : hour;
        return hour + ':' + min + ':' + sec;
    };
    // Returns false is the val is not empty
    static isEmpty(val:any):boolean {
        if(val === 0) return false; // Ensure that its not a number
        if(val){
            if(val instanceof Array) return val.length <= 0;
            return false
        } 
        return true;
    }
    static addSpinner($target: JQuery) {
        var spinnerHTML = '<div class="waiting-spinner h-center v-center"><i class="fa fa-spinner fa-spin"></i></div>';
        $target.html(spinnerHTML);
    };
    static removeSpinner($target:JQuery):void{
        $target.html('');
    };
    private static $handleBarsTemplates = $('#handlebars-templates');
    static loadTemplate(id:string):HandlebarsTemplateDelegate {
        var source   = K.$handleBarsTemplates.find('script#mus-' + id).html();
        return Handlebars.compile(source);
    };
    static render(musData:any, $target:JQuery, musID:string):string {
        var template = K.loadTemplate(musID);
        var musHTML = template(musData);
        $target.html(musHTML);
        return musHTML;
    };
    /**
     *  Send ajax request, action should be in url format
     */
    static sendAjax(url:string, success:any, data?:any, type?:string, onFail?:any, processData?:any): void {
        data = data ? data : {};
        data.exam_no = exam.exam_no;
        data.event_id = exam.event_id;
        data.student_id = exam.student_id;
        var options = {
                url: ADDR + url,            
                dataType: 'json',
                data: data,
                success: success, 
                error: onFail || K.onAjaxFail,
                type: type || 'GET',
//      beforeSend: function(xhr) {
//          xhr.setRequestHeader('Authorization', 'Bearer ' + K.getToken());
//      },
        };
//  if(processData === false){
//      options.cache = false; // To unable request pages to be cached
//      options.contentType = false; // The content type used when sending data to the server.
//      options.processData = false;  // To send DOMDocument or non processed data file it is set to false
//  }
        $.ajax(options);
    };
    static onAjaxFail(jqHRX:any, textStatus:string):void {
        alert("Request failed: " + textStatus + ' : ' + jqHRX.responseText);
        K.dd(jqHRX);
        K.dd(textStatus);
        K.dismissLoading();
    };
    
    static OverLay = class {
//        $div = $main.find('.overlay-page-div');
        static $div = K.$mainPage.parent().find('.overlay-page-div');
        static show(title: string):void {
            K.render({title: title}, this.$div, 'overlay-page');
            this.$div.parent().find('> #content').hide();
            this.$div.fadeIn('fast');
        };
        static loadContent(musData:any, musID:string):void {
            K.render(musData, this.$div.find('#overlay-page-content'), musID);
        };
        static remove():void {
            this.$div.parent().find('> #content').show();
            this.$div.fadeOut('fast');
        };
    };
//    static $customAlert = $('#custom-alert');
    static aa(msg:string, title?:string): void {
          alert(msg);
//        var dialogObj = Dialog.getInstance(K);
//        dialogObj.showCustomAlert(msg);
    }; 

    static dd(msg:any) { console.log(msg); };
    
    static getURL(url:string):string {
        return ADDR+url;
    };
    static randomIntFromInterval(min:number,max:number) // min and max included
    {
        return Math.floor(Math.random()*(max-min+1)+min);
    };
    
    static URL = {
        logout: 'api/user/dashboard/logout', 
        login: 'student/exam', 
        demoLogin: 'exam/demo',
        endExam: 'student/end-exam', 
        pauseExam: 'student/pause-exam', 
        attemptSingleQuestion: 'attempt_question.php?type=single',//'student/exam/attempt-question', 
        attemptMultiQuestion: 'attempt_question.php?type=multi',//'student/exam/multi-attempt-question', 
        messageSubmitted: 'message/exam_submitted', 
        messagePaused: 'message/exam_paused', 
        
    };
    
    static KEYBOARD_KEYS = {
        letter_A: 65,
        letter_a: 97,
        
        letter_B: 66,
        letter_b: 98,
        
        letter_C: 67,
        letter_c: 99,
        
        letter_D: 68,
        letter_d: 100,
        
        // Previous
        letter_P: 80, 
        letter_p: 112,
        // Next
        letter_N: 78, 
        letter_n: 110,
        // Submit
        letter_S: 83, 
        letter_s: 115,
        // Reverse
        letter_R: 82, 
        letter_r: 114, 
    }
    
};

export = K;
