/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />
define(["require", "exports", "config/k", "app/container", "app/stack_manager"], function (require, exports, K, Container, StackManager) {
    "use strict";
    //import Page = require('app/league/page');
    //import League = require('app/league/league');
    var config = {
        'K': {
            'object': K,
            'params': [],
        },
        'Container': {
            'object': Container,
            'params': []
        },
        'StackManager': {
            'object': StackManager,
            'params': [Container]
        },
    };
    return class LeagueConfig {
        constructor() {
            this.container = new Container(config);
        }
        getContainer() {
            return this.container;
        }
        // Not really necessary
        //We use a singleton to ensure the constructor is called only once
        static getInstance() {
            if (LeagueConfig.instance)
                return LeagueConfig.instance;
            LeagueConfig.instance = new LeagueConfig();
            return LeagueConfig.instance;
        }
        onClick(callableName, event, methodParams) {
            let splitCallable = callableName.split('.');
            if (splitCallable.length < 2)
                throw new Error('Invalid callable supplied');
            var arr = [];
            arr.push(event);
            methodParams.split(',').forEach(function (value, i) {
                if (!value && value != '0')
                    return;
                arr.push(value);
            });
            this.container.call(splitCallable[0], splitCallable[1], arr);
        }
    };
});
