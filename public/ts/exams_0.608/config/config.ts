/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />

import K = require('config/k');
import $ = require('jquery');

import Container = require('app/container');
import StackManager = require('app/stack_manager');

//import Page = require('app/league/page');
//import League = require('app/league/league');

var config:any = {
    'K' : {
        'object': K,
        'params': [],
    },
    'Container': {
        'object': Container, 
        'params': []
    },
    'StackManager': {
        'object': StackManager, 
        'params': [Container]
    },
//    'Page': {
//        'object': Page, 
//        'params': [StackManager, League]
//    },
//    'League': {
//        'object': League, 
//        'params': [Container, StackManager, Socket]
//    },
};

export = class LeagueConfig{
    
    public container:Container;
    private static instance:LeagueConfig;
    
    constructor() {
        this.container = new Container(config);
    }
    
    getContainer(){
        return this.container;
    }
    
    // Not really necessary
    //We use a singleton to ensure the constructor is called only once
    static getInstance():LeagueConfig{
        if(LeagueConfig.instance) return LeagueConfig.instance;
        LeagueConfig.instance = new LeagueConfig();
        return LeagueConfig.instance;
    }
    
    onClick(callableName:string, event:JQuery, methodParams:string)
    {
        let splitCallable = callableName.split('.');
        
        if(splitCallable.length < 2) throw new Error('Invalid callable supplied');
        
        var arr:any[] = [];
        arr.push(event);
        
        methodParams.split(',').forEach(function (value:string, i:number) {
           if(!value && value != '0') return; 
           arr.push(value);
        });
        
        this.container.call(splitCallable[0], splitCallable[1], arr);
    }
    

}
