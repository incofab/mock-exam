/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />
define(["require", "exports", "config/k", "jquery", "handlebars", "app/exam_page", "app/exam_events", "app/exam_comm"], function (require, exports, K, $, Handlebars, ExamPage, ExamEvents, ExamsComm) {
    "use strict";
    class Exam {
        constructor(examData, studentData, examTimer, modal) {
            this.studentData = studentData;
            this.examTimer = examTimer;
            this.modal = modal;
            this.examPages = [];
            //    private baseImgPath = `${ADDR}public/img/jamb/`;
            this.baseImgPath = `${ADDR}public/img/content/`;
            this.examData = examData;
            this.allExamSubjectData = examData.all_exam_subject_data;
            this.examEvents = new ExamEvents(this.modal);
            this.examsComm = new ExamsComm(this);
        }
        init(dispatcher) {
            var tabs = '';
            var questionBody = '';
            this.registerExamsHandlebarEvents();
            var self = this;
            this.allExamSubjectData.forEach((examData, i) => {
                var examPage = new ExamPage(this, this.examsComm);
                examPage.init(examData, i);
                self.examPages.push(examPage);
                tabs += K.loadTemplate('exam-tab')(examPage);
                questionBody += K.loadTemplate('exam-question-container')(examPage);
                if (i == 0) {
                    self.currentPage = examPage;
                }
            });
            $('#exam-layout #exam-tabs .nav.nav-tabs').html(tabs);
            $('#exam-layout #questions-content').html(questionBody);
            this.$timer = $('.app-header #timer');
            this.examTimer.start(this.examData.meta.time_remaining, this);
            this.examEvents.registerEvents(this, dispatcher);
            this.examsComm.init();
            // Set up base path for images
            self.examPages.forEach((examPage, i) => {
                var $img = $('#exam-layout #questions-content').find(`#nav-${examPage.getSubjectID()} .question-main img`);
                $img.each(function (i, ele) {
                    var $imgObj = $(this);
                    var imgUrl = $imgObj.attr('src');
                    self.filterImgUrl(imgUrl, $imgObj, examPage);
                });
            });
            this.currentPage.reinvalidateCurrentQuestion();
        }
        getExamPages() { return this.examPages; }
        getImageAddr(courseCode, course_session_id, fileName, year) {
            var imgAddr = ADDR + 'exam-img.php'
                + `?course_id=${courseCode}&course_session_id=${course_session_id}&filename=${fileName}&session=${year}`;
            // console.log(`imgAddr = ${imgAddr}`);
            return imgAddr;
        }
        filterImgUrl(imgUrl, $imgObj, currentPage) {
            // while (imgUrl.substr(0, 3) == '../') {
            //     imgUrl = imgUrl.substr(3);
            // }
            // if(imgUrl.substr(0, this.baseImgPath.length) == this.baseImgPath){
            //     return;
            // }
            // if(imgUrl.substr(0, 1) == '/'){
            //     imgUrl = imgUrl.substr(1);
            // }
            // var courseCode = currentPage.getCourseCode();
            var courseId = currentPage.getCourseId();
            var year = currentPage.getYear();
            var imgAddr = this.getImageAddr(courseId + "", currentPage.getSessionID(), imgUrl, year);
            // console.log("Image Address", imgAddr);
            $imgObj.attr('src', imgAddr);
            // $imgObj.attr('src', `${this.baseImgPath}${courseCode}/${year}/${imgUrl}`); 
        }
        timeElapsed() {
            this.stopExam();
            //        this.examsComm.examEnd();
        }
        timeLessThan5Mins() {
            this.$timer.addClass('.text-danger');
        }
        timeLessThan1Min() {
            this.$timer.addClass('.blink');
        }
        updateTime(timeString) {
            this.$timer.text(timeString);
        }
        timerStopped() {
        }
        tabSwitched(currentTabIndex, previousTabIndex) {
            if (this.currentPage.tabIndex == currentTabIndex)
                return;
            if (currentTabIndex >= this.examPages.length)
                return;
            this.currentPage = this.examPages[currentTabIndex];
            this.currentPage.reinvalidateCurrentQuestion();
        }
        questionSelected(questionIndex, questionId) {
            this.currentPage.gotoQuestion(questionIndex, questionId);
        }
        answerSelected(selectedAns) {
            this.currentPage.addSelectedAns(selectedAns);
        }
        answerSelectedKeyboard(selectedAns) {
            $('#exam-layout #questions-content')
                .find(`#nav-${this.currentPage.getSubjectID()} .question-main ` +
                ` .option input[data-selection="${selectedAns}"]`).click();
        }
        nextQuestion() {
            this.currentPage.gotoNextQuestion();
        }
        previousQuestion() {
            this.currentPage.gotoPreviousQuestion();
        }
        pauseExam() {
            K.toAskBeforeUnloadEvent = false;
            // Show Loading
            K.showLoading('Signing out...');
            this.examsComm.examEnd();
            if (isDemo) {
                window.location.href = K.getURL(K.URL.demoLogin);
                return;
            }
            K.sendAjax(K.URL.pauseExam, success);
            function success(res) {
                K.dismissLoading();
                if (!res.success) {
                    console.log(res.message);
                    return K.aa("Pause Failed, Contact Admin");
                }
                window.location.href = K.getURL(K.URL.messagePaused);
            }
        }
        stopExam() {
            K.toAskBeforeUnloadEvent = false;
            // Show Loading
            //        K.showLoading('Closing Exam Session...');
            if (isDemo) {
                window.location.href = K.getURL(K.URL.demoLogin);
                return;
            }
            this.examsComm.examEnd(function () {
                K.sendAjax(K.URL.endExam, success);
                function success(res) {
                    //                K.dismissLoading();                
                    if (!res.success) {
                        console.log(res.message);
                        return K.aa("Submit failed, contact admin");
                    }
                    window.location.href = K.getURL(K.URL.messageSubmitted);
                }
            });
        }
        forceDeliverAttemptedQuestions() {
            this.examsComm.forceDeliverAttemptedQuestions();
        }
        registerExamsHandlebarEvents() {
            var self = this;
            Handlebars.registerHelper('isOptionSelected', function (tabIndex, questionId, selection, ret) {
                var page = self.examPages[tabIndex];
                if (!page)
                    return null;
                var question = page.getFormattedAttemptedQuestions()[questionId];
                if (!question)
                    return null;
                if (question.attempt == selection) {
                    return new Handlebars.SafeString('checked="checked"');
                }
                return null;
            });
            Handlebars.registerHelper('isAttemptedQuestion', function (tabIndex, questionId, ret) {
                var page = self.examPages[tabIndex];
                if (!page)
                    return null;
                var question = page.getFormattedAttemptedQuestions()[questionId];
                if (question) {
                    return 'attempted';
                }
                return null;
            });
            Handlebars.registerHelper('getPassage', function (tabIndex) {
                var page = self.examPages[tabIndex];
                if (!page)
                    return null;
                return page.getCurrentPassage();
            });
            Handlebars.registerHelper('getInstruction', function (tabIndex) {
                var page = self.examPages[tabIndex];
                if (!page)
                    return null;
                return page.getCurrentInstruction();
            });
        }
        static toString() {
            return 'Exam';
        }
        toString() {
            return Exam.toString();
        }
    }
    ;
    return Exam;
});
