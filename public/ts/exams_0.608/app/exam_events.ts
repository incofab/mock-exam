/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />

import K = require('config/k'); 
import $ = require('jquery');
import Modal = require('util/modal');
import KeysHandler = require('app/keys_handler');

import {IExamEventsHooks} from '../exam_interfaces'; 

class ExamEvents{   
            
    private examEventsHook:IExamEventsHooks;
    private keysHandler:KeysHandler;
    // private askBeforeUnload:boolean = true;

    constructor(private modal:Modal){
        
    }
    
    registerEvents(examEventsHook:IExamEventsHooks, dispatcher:JQuery){
        
        this.keysHandler = new KeysHandler(examEventsHook, this.modal);
        
        var self = this;
        
        this.examEventsHook = examEventsHook;
        
        dispatcher.on('shown.bs.tab', function (event:any, data:any) {
            
            var $target = $(data.target) // newly activated tab

            examEventsHook.tabSwitched($target.data('tab_index'), $(data.relatedTarget).data('tab_index'));
        });
        
        dispatcher.on('hidden.bs.modal', function (event:any, data:any) {
            
            K.dd('hidden.bs.modal');
            var $target = $(data.target) // modal
            
            var modalFor = $target.data('modal_for');
            K.dd('modalFor = '+modalFor);
            if(modalFor == KeysHandler.toString()){
                self.keysHandler.clearLastPressedKey();
            }
            
        });
                
        $('#questions-content').on('click', '.question-numbers-tab li', function (e:any) {
            
            var questionIndex = $(e.target).data('question_index');
            
            var questionId = $(e.target).data('question_id');
            
            examEventsHook.questionSelected(questionIndex, questionId);
        });
        
        $('#questions-content').on('change', '.option input[name="option"]', function (e:any) {
            var selectedAns = $(e.currentTarget).data('selection');
            
            examEventsHook.answerSelected(selectedAns);
        });

        $('#exam-layout #previous-question').on('click', function (e:any) {
            examEventsHook.previousQuestion();
        });

        $('#exam-layout #next-question').on('click', function (e:any) {
            examEventsHook.nextQuestion();
        }); 

        $('.app-header #pause-exam').on('click', function (e:any) {
            // self.askBeforeUnload = false;
            if(!confirm('Do you want to pause this exam?')){
            //     self.askBeforeUnload = true;
                 return;
            }
            examEventsHook.pauseExam();
        });

        $('#exam-layout #stop-exam').on('click', function (e:any) {
            // self.askBeforeUnload = false;
            if(!confirm('End this exam?')){
                // self.askBeforeUnload = true;
                return;
            } 
            examEventsHook.stopExam();
        });

        $(window).on('beforeunload', function (e:any) {
            if(K.toAskBeforeUnloadEvent){
                examEventsHook.forceDeliverAttemptedQuestions();
                return "Do you want exit this exam?";
            } 
        });

        $('.toggleCalculator').on('click', function (e:any) {
            $('#examdriller-calculator').toggleClass('show');
        });
        
        $('#examModal .negative-btn').on('click', function (e:any) {
            self.modal.executeNegativeAction();
        });

        $('#examModal .positive-btn').on('click', function (e:any) {
            self.modal.executePositiveAction();
        });
        
        $(document).keypress(function(e:any) {
            self.keysHandler.handleKeys(e.which);
        });
        
    }

    public static toString() {
        return 'ExamEvents';
    }
    
    public toString() {
        return ExamEvents.toString();
    }
    

    
};
export = ExamEvents;

