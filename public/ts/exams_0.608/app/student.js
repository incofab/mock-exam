/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />
define(["require", "exports", "jquery"], function (require, exports, $) {
    "use strict";
    class Student {
        constructor(student) {
            this.student = student;
            this.init();
        }
        init() {
            $('.app-header #name').text(this.student.lastname + ' ' + this.student.firstname);
            $('#exam-layout #exam-no').text(this.student.exam_no);
            //        console.log('this.student.exam_no = '+this.student.exam_no);
        }
        static toString() {
            return 'Student';
        }
        toString() {
            return Student.toString();
        }
    }
    ;
    return Student;
});
