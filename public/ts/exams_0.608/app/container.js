/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />
define(["require", "exports"], function (require, exports) {
    "use strict";
    class Container {
        constructor(configData) {
            this.configData = configData;
            this.container = [];
            this.backStack = [];
            this.resolvedClasses = [];
            this.container[this.toString()] = this;
        }
        resolveInstance(className) {
            if (!(className in this.configData))
                throw new Error('Class Name "' + className + "\" not registered");
            let instance;
            if (className in this.container) {
                instance = this.container[className];
            }
            else {
                if (this.resolvedClasses.indexOf(className) > -1)
                    throw new Error('Circular dependency in ' + className);
                else
                    this.resolvedClasses.push(className);
                let constructorParams = this.configData[className]['params'];
                if (constructorParams.length > 0) {
                    let resolvedConstructorParams = this.resolveConstructorParameters(constructorParams);
                    instance = new this.configData[className]['object'](...resolvedConstructorParams);
                }
                else {
                    instance = new this.configData[className]['object']();
                }
                this.container[className] = instance;
            }
            return instance;
        }
        resolveConstructorParameters(constructorParams) {
            let resolvedParams = [];
            for (var i = 0; i < constructorParams.length; i++) {
                var param = constructorParams[i];
                if (typeof (param) == 'function') {
                    var className = param.toString();
                    resolvedParams.push(this.resolveInstance(className));
                }
                else {
                    resolvedParams.push(param);
                }
            }
            return resolvedParams;
        }
        call(className, methodName, methodParams) {
            this.resolvedClasses = [];
            if (!(className in this.configData))
                throw new Error('Class Name "' + className + '.' + methodName
                    + "\" not registered OR Did not override the toString()");
            let instance = this.resolveInstance(className);
            instance[methodName](...methodParams);
        }
        static toString() {
            return 'Container';
        }
        toString() {
            return Container.toString();
        }
    }
    ;
    return Container;
});
