/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />

import K = require('../config/k');
import $ = require('jquery');
import {IPage} from '../interfaces';

class Container{    
    
    private container:any = [];
    private backStack:IPage[] = [];
    
    constructor(private configData:any) {
        this.container[this.toString()] = this;
    }

    public resolveInstance(className: string) {
        
        if(!(className in this.configData)) 
            throw new Error('Class Name "' + className + "\" not registered");
        
        let instance;
        if(className in this.container){
            instance = this.container[className];
        }else{
            if(this.resolvedClasses.indexOf(className) > -1) throw  new Error('Circular dependency in ' + className);
            else this.resolvedClasses.push(className);
            let constructorParams: any[] = this.configData[className]['params'];
            if(constructorParams.length > 0){
                let resolvedConstructorParams = this.resolveConstructorParameters(constructorParams);
                instance = new this.configData[className]['object'](...resolvedConstructorParams);
            }else{
                instance = new this.configData[className]['object']();
            }
            this.container[className] = instance;
        }
        return instance;
    }
    
    private resolveConstructorParameters(constructorParams: any[]):any[]{
        let resolvedParams = [];
        for (var i = 0; i < constructorParams.length; i++) {
            var param = constructorParams[i];
            if(typeof(param) == 'function'){
                var className = param.toString();
                resolvedParams.push(this.resolveInstance(className));
            }else{
                resolvedParams.push(param);
            }
        }
        return resolvedParams;
    }
    
    private resolvedClasses:string[] = [];
    public call(className:string, methodName:string, methodParams:any[])
    {
        this.resolvedClasses = [];
        if(!(className in this.configData)) 
            throw new Error('Class Name "' + className + '.' + methodName 
                    + "\" not registered OR Did not override the toString()");
        
        let instance = this.resolveInstance(className);
        
        instance[methodName](...methodParams);
    }

    public static toString() {
        return 'Container';
    }
    public toString() {
        return Container.toString();
    }        
    
};
export = Container;

