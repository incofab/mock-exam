/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />

import K = require('config/k'); 
import $ = require('jquery');
import Handlebars = require('handlebars');
import StackManager = require('stack_manager');
import Container = require('app/container');
import Modal = require('util/modal');
import ExamPage = require('app/exam_page');
import ExamTimer = require('app/exam_timer');
import ExamEvents = require('app/exam_events');
import ExamsComm = require('app/exam_comm');


import {IExamTimerHooks, IExamEventsHooks, IExamData, IExamSubjectData, IQuestions, 
    IAttemptedQuestions, IPassages, IInstructions} from '../exam_interfaces';
    
import {IStudentData} from '../interfaces';

declare var ADDR: string;
declare var isDemo: boolean;

class Exam implements IExamTimerHooks, IExamEventsHooks{   
    
    private examPages: ExamPage[] = [];

    private currentPage:ExamPage;

    private examEvents:ExamEvents;

    private examsComm:ExamsComm;

    private allExamSubjectData:IExamSubjectData[];
    private examData:IExamData;
    private $timer:JQuery;
//    private baseImgPath = `${ADDR}public/img/jamb/`;
    private baseImgPath = `${ADDR}public/img/content/`;
        
    constructor(examData:IExamData, private studentData:IStudentData, 
            private examTimer:ExamTimer, private modal:Modal){
        this.examData = examData;
        this.allExamSubjectData = examData.all_exam_subject_data;
        
        this.examEvents = new ExamEvents(this.modal);
        
        this.examsComm = new ExamsComm(this);
    }
    
    init(dispatcher:JQuery){
        
        var tabs:string = '';
        
        var questionBody:string = '';
            
        this.registerExamsHandlebarEvents();
        
        var self = this;
        
        this.allExamSubjectData.forEach((examData:IExamSubjectData, i:number) => {
            
            var examPage = new ExamPage(this, this.examsComm);
            
            examPage.init(examData, i);
            
            self.examPages.push(examPage);          
            
            tabs += K.loadTemplate('exam-tab')(examPage);
            
            questionBody += K.loadTemplate('exam-question-container')(examPage);
            
            if(i == 0){
                self.currentPage = examPage;
            }            
        });
        
        $('#exam-layout #exam-tabs .nav.nav-tabs').html(tabs);
        
        $('#exam-layout #questions-content').html(questionBody);
        
        this.$timer = $('.app-header #timer');
        
        this.examTimer.start(this.examData.meta.time_remaining, this);
        
        this.examEvents.registerEvents(this, dispatcher);
        
        this.examsComm.init();
        
        // Set up base path for images
        self.examPages.forEach((examPage:ExamPage, i:number) => {

          var $img = $('#exam-layout #questions-content').find(`#nav-${examPage.getSubjectID()} .question-main img`);
          
          $img.each(function (this:HTMLElement, i:number,  ele:HTMLElement):void|false {
              
              var $imgObj = $(this);
              
              var imgUrl = <string>$imgObj.attr('src');

              self.filterImgUrl(imgUrl, $imgObj, examPage); 
          });
        });    
        this.currentPage.reinvalidateCurrentQuestion();
    }

    getExamPages(): ExamPage[] { return this.examPages; }
    
    getImageAddr(courseCode:string, course_session_id:number, fileName:string, year:string) {
        var imgAddr = ADDR+'exam-img.php'
            +`?course_id=${courseCode}&course_session_id=${course_session_id}&filename=${fileName}&session=${year}`;
        // console.log(`imgAddr = ${imgAddr}`);
        return imgAddr;
    }

    filterImgUrl(imgUrl:string, $imgObj:JQuery, currentPage:ExamPage) {
        
        // while (imgUrl.substr(0, 3) == '../') {
        //     imgUrl = imgUrl.substr(3);
        // }
        
        // if(imgUrl.substr(0, this.baseImgPath.length) == this.baseImgPath){
        //     return;
        // }
        
        // if(imgUrl.substr(0, 1) == '/'){
        //     imgUrl = imgUrl.substr(1);
        // }
        
        // var courseCode = currentPage.getCourseCode();
        var courseId = currentPage.getCourseId();
        var year = currentPage.getYear();

        var imgAddr = this.getImageAddr(courseId+"", currentPage.getSessionID(), imgUrl, year);
        // console.log("Image Address", imgAddr);
        $imgObj.attr('src', imgAddr); 
        // $imgObj.attr('src', `${this.baseImgPath}${courseCode}/${year}/${imgUrl}`); 
    }
        
    timeElapsed(): void {
        this.stopExam();
//        this.examsComm.examEnd();
    }
    timeLessThan5Mins(): void {
        this.$timer.addClass('.text-danger');
    }
    timeLessThan1Min(): void {
        this.$timer.addClass('.blink');
    }
    updateTime(timeString: string): void {
        this.$timer.text(timeString);
    }
    timerStopped(): void {
        
    }

    tabSwitched(currentTabIndex: number, previousTabIndex: number): void {
        if(this.currentPage.tabIndex == currentTabIndex) return;
                
        if(currentTabIndex >= this.examPages.length) return;
        
        this.currentPage = this.examPages[currentTabIndex];
        
        this.currentPage.reinvalidateCurrentQuestion();
    }
    questionSelected(questionIndex: number, questionId: number): void {
        this.currentPage.gotoQuestion(questionIndex, questionId);
    }
    answerSelected(selectedAns: string): void {
        this.currentPage.addSelectedAns(selectedAns);
    }
    answerSelectedKeyboard(selectedAns: string): void {
        $('#exam-layout #questions-content')
            .find(`#nav-${this.currentPage.getSubjectID()} .question-main `+
                    ` .option input[data-selection="${selectedAns}"]`).click();
    }
    nextQuestion(): void {
        this.currentPage.gotoNextQuestion();
    }
    previousQuestion(): void {
        this.currentPage.gotoPreviousQuestion();
    }
    pauseExam(): void {
        K.toAskBeforeUnloadEvent = false;
        // Show Loading
        K.showLoading('Signing out...');
        this.examsComm.examEnd();
        if(isDemo){
            window.location.href = K.getURL(K.URL.demoLogin);
            return;
        }
        K.sendAjax(K.URL.pauseExam, success);
        
        function success(res:any) {
            K.dismissLoading();
            
            if(!res.success){
                
                console.log(res.message);
                
                return K.aa("Pause Failed, Contact Admin");
            }
            
            window.location.href = K.getURL(K.URL.messagePaused);
        }
    } 
    
    stopExam(): void {
        K.toAskBeforeUnloadEvent = false;
        // Show Loading
//        K.showLoading('Closing Exam Session...');

        if(isDemo){
            window.location.href = K.getURL(K.URL.demoLogin);
            return;
        }
        
        this.examsComm.examEnd(function () {	
            K.sendAjax(K.URL.endExam, success);
            
            function success(res:any) {
//                K.dismissLoading();                
                if(!res.success){
                    console.log(res.message);
                    
                    return K.aa("Submit failed, contact admin");
                }
                
                window.location.href = K.getURL(K.URL.messageSubmitted);
            }
        });
    }

    forceDeliverAttemptedQuestions():void{
        this.examsComm.forceDeliverAttemptedQuestions();
    }
    
    private registerExamsHandlebarEvents() {
        var self = this;
        
        Handlebars.registerHelper('isOptionSelected', 
                function(tabIndex:number, questionId:number, selection:string, ret?:string) {
            
            var page = self.examPages[tabIndex];
            
            if(!page) return null;
            
            var question = page.getFormattedAttemptedQuestions()[questionId];
            
            if(!question) return null;
            
            if(question.attempt == selection){
                return new Handlebars.SafeString('checked="checked"');
            }
            
            return null;
        });
        
        Handlebars.registerHelper('isAttemptedQuestion', 
                function(tabIndex:number, questionId:number, ret?:string) {
            
            var page = self.examPages[tabIndex];
            
            if(!page) return null;
            
            var question = page.getFormattedAttemptedQuestions()[questionId];
            
            if(question) {
                return 'attempted';
            }
            
            return null;
        });
        
        Handlebars.registerHelper('getPassage', function(tabIndex:number) {
            
            var page = self.examPages[tabIndex];
            
            if(!page) return null;
            
            return page.getCurrentPassage();
        });
        
        Handlebars.registerHelper('getInstruction', function(tabIndex:number) {
            
            var page = self.examPages[tabIndex];
            
            if(!page) return null;
            
            return page.getCurrentInstruction();
        });
    }
    
    public static toString() {
        return 'Exam';
    }
    
    public toString() {
        return Exam.toString();
    }
    

    
};
export = Exam;

