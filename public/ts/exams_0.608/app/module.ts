/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />
/// <reference path='../node_modules/@types/socket.io-client/index.d.ts' />

import K = require('../config/k');
import $ = require('jquery'); 
import {IPage} from '../interfaces';
import Container = require('app/container');

abstract class Module{    
    
    public abstract id:number;
    
    public abstract shutDown():void;
    
    public abstract load():void;
    
    public static toString() {
        return 'Module';
    }
    
    public toString() {
        return Module.toString();
    }

};

export = Module;

