/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />
/// <reference path='../node_modules/@types/socket.io-client/index.d.ts' />
define(["require", "exports"], function (require, exports) {
    "use strict";
    class StackManager {
        constructor(container) {
            this.container = container;
            this.backStack = [];
            this.MAX_STACK_SIZE = 10;
            this.ACTIVITY_REAL_FOOTBALL = 1;
            this.ACTIVITY_VIRTUAL_LEAGUE = 2;
            this.ACTIVITY_SPECIALS = 3;
            //    private currentActivity:number = this.ACTIVITY_REAL_FOOTBALL;
            this.currentModule = null;
        }
        gotoPage($this, nextPageClassName) {
            try {
                let currentPage = this.getCurrentPage();
                var newPageObj = this.container.resolveInstance(nextPageClassName);
                if (currentPage) {
                    if (currentPage.toString() == 'nextPageClassName')
                        return;
                    currentPage.close();
                }
                newPageObj.open();
                this.addToStack(newPageObj);
            }
            catch (e) {
                throw new Error('Page not found');
            }
        }
        switchPages(oldPage, nextPageClassName) {
            try {
                var newPageObj = this.container.resolveInstance(nextPageClassName);
                if (oldPage)
                    oldPage.close();
                newPageObj.open();
                this.addToStack(newPageObj);
            }
            catch (e) {
                throw new Error('Page not found');
            }
        }
        back() {
            let len = this.backStack.length;
            if (len < 2)
                return;
            let currentPage = this.backStack.splice(len - 1, 1)[0];
            let nextPage = this.backStack[len - 2];
            currentPage.close();
            nextPage.open();
        }
        static toString() {
            return 'StackManager';
        }
        toString() {
            return StackManager.toString();
        }
        getCurrentPage() {
            let len = this.backStack.length;
            if (len < 1)
                return null;
            return this.backStack[len - 1];
        }
        addToStack(page) {
            if (!page)
                return;
            var currentPage = this.getCurrentPage();
            if (currentPage && (page.toString() == currentPage.toString()))
                return;
            this.trimStackSize();
            this.backStack.push(page);
        }
        clearStack() {
            this.backStack = [];
        }
        trimStackSize() {
            while (this.backStack.length >= this.MAX_STACK_SIZE) {
                this.backStack.pop();
            }
        }
        reinvalidateCurrentPage() {
            // Then reinvalidate current page if available or load up a new Games page
            var currentPage = this.getCurrentPage();
            if (!currentPage) {
                //            currentPage = this.container.resolveInstance(Games.toString());
                //            currentPage.open();
                this.addToStack(currentPage);
            }
            else {
                currentPage.reInvalidate();
            }
        }
        boot(moduleClassName) {
            var modules = ['ActivityPage', 'Specials', 'League'];
            moduleClassName = (modules.indexOf(moduleClassName) >= 0)
                ? moduleClassName : 'ActivityPage';
            var module = this.container.resolveInstance(moduleClassName);
            this.setCurrentActivity(module);
        }
        setCurrentActivity(module) {
            if (this.currentModule != null && this.currentModule.id == module.id)
                return;
            module.load();
            if (this.currentModule != null) {
                this.currentModule.shutDown();
            }
            this.currentModule = module;
        }
        getCurrentActivity() {
            if (this.currentModule == null)
                return this.ACTIVITY_REAL_FOOTBALL;
            else
                return this.currentModule.id;
        }
    }
    ;
    return StackManager;
});
