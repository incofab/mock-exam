/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />

import K = require('config/k'); 
import $ = require('jquery');

import {IExamTimerHooks} from '../exam_interfaces'; 

class ExamTimer{   

    private duration:number;
    private timeRemaining:number;
    private startTime:number;
    private intervalId:number = 0;

    private timeElapsed:number = 0;

    private examTimerHooks:IExamTimerHooks;

    private lessThan5MinsCalled:boolean;
    private lessThan1MinCalled:boolean;
        
    constructor(){
        
    }
    
    start(duration:number, examTimerHooks:IExamTimerHooks){
        
        this.duration = duration;
        
        this.examTimerHooks = examTimerHooks;
        
        this.lessThan5MinsCalled = false;
        
        this.lessThan1MinCalled = false;
        
        this.startTime = K.getTimeStamp();
        
        this.evaluateTime();
        
        this.intervalId = setInterval(() => {

            this.evaluateTime();
            
        }, 1000); 
    }
    
    private evaluateTime(){

        this.timeElapsed = K.getTimeStamp() - this.startTime;
        
        this.timeRemaining = this.duration - this.timeElapsed;
        
        if(this.timeRemaining < 1){
            // Trigger Time Elapsed --- End Exam
            this.examTimerHooks.timeElapsed();
            
            clearInterval(this.intervalId);
        }
        
        if(!this.lessThan5MinsCalled && this.timeRemaining < (5 * 60)){
            this.lessThan5MinsCalled = true;
            // Time less than 5 minutes, turn timer color red
            this.examTimerHooks.timeLessThan5Mins();
        }
        
        if(!this.lessThan1MinCalled && this.timeRemaining < (1 * 60)){
            this.lessThan1MinCalled = true;
            // Time less than 1 minute, start blinking
            this.examTimerHooks.timeLessThan1Min();
        }
        
        this.examTimerHooks.updateTime(K.formatTime(this.timeRemaining));
    }
    
    stop(){
        
        if(this.intervalId === 0) clearInterval(this.intervalId);
        
        if(this.examTimerHooks) this.examTimerHooks.timerStopped();
    }
    
    public static toString() {
        return 'ExamTimer';
    }
    
    public toString() {
        return ExamTimer.toString();
    }
    

    
};
export = ExamTimer;

