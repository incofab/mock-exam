/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />
define(["require", "exports", "config/k", "jquery", "app/keys_handler"], function (require, exports, K, $, KeysHandler) {
    "use strict";
    class ExamEvents {
        // private askBeforeUnload:boolean = true;
        constructor(modal) {
            this.modal = modal;
        }
        registerEvents(examEventsHook, dispatcher) {
            this.keysHandler = new KeysHandler(examEventsHook, this.modal);
            var self = this;
            this.examEventsHook = examEventsHook;
            dispatcher.on('shown.bs.tab', function (event, data) {
                var $target = $(data.target); // newly activated tab
                examEventsHook.tabSwitched($target.data('tab_index'), $(data.relatedTarget).data('tab_index'));
            });
            dispatcher.on('hidden.bs.modal', function (event, data) {
                K.dd('hidden.bs.modal');
                var $target = $(data.target); // modal
                var modalFor = $target.data('modal_for');
                K.dd('modalFor = ' + modalFor);
                if (modalFor == KeysHandler.toString()) {
                    self.keysHandler.clearLastPressedKey();
                }
            });
            $('#questions-content').on('click', '.question-numbers-tab li', function (e) {
                var questionIndex = $(e.target).data('question_index');
                var questionId = $(e.target).data('question_id');
                examEventsHook.questionSelected(questionIndex, questionId);
            });
            $('#questions-content').on('change', '.option input[name="option"]', function (e) {
                var selectedAns = $(e.currentTarget).data('selection');
                examEventsHook.answerSelected(selectedAns);
            });
            $('#exam-layout #previous-question').on('click', function (e) {
                examEventsHook.previousQuestion();
            });
            $('#exam-layout #next-question').on('click', function (e) {
                examEventsHook.nextQuestion();
            });
            $('.app-header #pause-exam').on('click', function (e) {
                // self.askBeforeUnload = false;
                if (!confirm('Do you want to pause this exam?')) {
                    //     self.askBeforeUnload = true;
                    return;
                }
                examEventsHook.pauseExam();
            });
            $('#exam-layout #stop-exam').on('click', function (e) {
                // self.askBeforeUnload = false;
                if (!confirm('End this exam?')) {
                    // self.askBeforeUnload = true;
                    return;
                }
                examEventsHook.stopExam();
            });
            $(window).on('beforeunload', function (e) {
                if (K.toAskBeforeUnloadEvent) {
                    examEventsHook.forceDeliverAttemptedQuestions();
                    return "Do you want exit this exam?";
                }
            });
            $('.toggleCalculator').on('click', function (e) {
                $('#examdriller-calculator').toggleClass('show');
            });
            $('#examModal .negative-btn').on('click', function (e) {
                self.modal.executeNegativeAction();
            });
            $('#examModal .positive-btn').on('click', function (e) {
                self.modal.executePositiveAction();
            });
            $(document).keypress(function (e) {
                self.keysHandler.handleKeys(e.which);
            });
        }
        static toString() {
            return 'ExamEvents';
        }
        toString() {
            return ExamEvents.toString();
        }
    }
    ;
    return ExamEvents;
});
