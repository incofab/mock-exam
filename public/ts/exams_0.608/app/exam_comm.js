/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />
define(["require", "exports", "config/k"], function (require, exports, K) {
    "use strict";
    class ExamComm {
        constructor(exam) {
            this.exam = exam;
            this.attemptedQuestionsSent = {};
            this.attemptedQuestionsPending = {};
            this.networkFailCount = 0;
            this.pingInterval = 3.5 * 60 * 1000; //3.5mins
            this.pingInterval = K.randomIntFromInterval(180, 420) * 1000; // Interval between 3-7mins
            //        K.dd('this.pingInterval = '+this.pingInterval);
        }
        init() {
            var o = Object;
            this.exam.getExamPages().forEach((val, i) => {
                // Add all attempted questions to the variable this.attemptedQuestionsSent
                o.assign(this.attemptedQuestionsSent, val);
            });
            this.initPing();
        }
        initPing() {
            this.intervalId = setInterval(() => {
                this.sendQuestionAttempts();
            }, this.pingInterval);
        }
        examEnd(callable) {
            clearInterval(this.intervalId);
            this.intervalId = 0;
            this.sendQuestionAttempts(callable);
        }
        forceDeliverAttemptedQuestions(callable) {
            // K.dd('forceDeliverAttemptedQuestions() called');
            clearInterval(this.intervalId);
            this.intervalId = 0;
            var self = this;
            this.sendQuestionAttempts(function () {
                self.initPing();
                if (callable)
                    callable();
            });
        }
        sendQuestionAttempts(callable) {
            if (isDemo)
                return;
            var data = [];
            for (let question_id in this.attemptedQuestionsPending) {
                if (!this.attemptedQuestionsPending.hasOwnProperty(question_id))
                    continue;
                if (K.isEmpty(this.attemptedQuestionsPending[question_id]))
                    continue;
                data.push(this.attemptedQuestionsPending[question_id]);
            }
            if (data.length < 1) {
                if (callable)
                    callable();
                return;
            }
            K.sendAjax(K.URL.attemptMultiQuestion, success, { 'attempts': this.attemptedQuestionsPending }, 'POST', fail);
            var self = this;
            function success(res) {
                self.networkFailCount = 0;
                // K.dd(res);
                if (!res.success)
                    return K.aa(res.message);
                if (callable)
                    callable();
                var successes = res.data.success;
                var failures = res.data.failure;
                if (successes) {
                    successes.forEach((val, i) => {
                        var sentAttempt = self.attemptedQuestionsPending[val.question_id];
                        if (!sentAttempt) {
                            return console.log('Attempted questions returned from server not found');
                        }
                        self.attemptedQuestionsSent[val.question_id] = self.attemptedQuestionsPending[val.question_id];
                        self.attemptedQuestionsPending[val.question_id] = undefined;
                    });
                }
            }
            function fail(jqHRX, textStatus) {
                //            K.dd(jqHRX);
                if (callable)
                    callable();
                if (self.intervalId == 0)
                    return;
                if (jqHRX.status == 200) {
                    console.log('Network Error, jqHRX.status = ' + jqHRX.status);
                    console.log(jqHRX);
                    console.log(textStatus + '\n' + jqHRX.responseText);
                    // return;
                }
                self.networkFailCount += 1;
                if (self.networkFailCount > 3) {
                    console.log('No Internet available');
                    alert("It seems your network is disconnected. Inform admin now to check it out.");
                    return;
                }
                if (self.networkFailCount > 1) {
                    console.log('Possible Network Problem');
                }
            }
        }
        addToPending(questionAttempt) {
            this.attemptedQuestionsPending[questionAttempt.question_id] = questionAttempt;
            var size = 0;
            for (let question_id in this.attemptedQuestionsPending) {
                if (!this.attemptedQuestionsPending.hasOwnProperty(question_id))
                    continue;
                if (K.isEmpty(this.attemptedQuestionsPending[question_id]))
                    continue;
                size++;
                // Once the user has answered up to 3 questions, Forward them to the server
                if (size > 2) {
                    this.forceDeliverAttemptedQuestions();
                    break;
                }
            }
        }
        addToSent(questionAttempt) {
            this.attemptedQuestionsSent[questionAttempt.question_id] = questionAttempt;
        }
        static toString() {
            return 'ExamComm';
        }
        toString() {
            return ExamComm.toString();
        }
    }
    ;
    return ExamComm;
});
