/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />

import K = require('config/k'); 
import $ = require('jquery');

import Exam = require('app/exam');
import ExamPage = require('app/exam_page');

import {IAttemptedQuestions} from '../exam_interfaces'; 

declare var isDemo: boolean;

class ExamComm{   

    private attemptedQuestionsSent:{[question_id:number]:IAttemptedQuestions|undefined} = {};
    private attemptedQuestionsPending:{[question_id:number]:IAttemptedQuestions|undefined} = {};

    private intervalId:number;

    private networkFailCount:number = 0;
    
    private pingInterval:number = 3.5 * 60 * 1000; //3.5mins

    constructor(private exam:Exam){
        this.pingInterval = K.randomIntFromInterval(180, 420) * 1000; // Interval between 3-7mins
//        K.dd('this.pingInterval = '+this.pingInterval);
    }
    
    public init() {
        var o:any = Object;
        this.exam.getExamPages().forEach((val:ExamPage, i:number) => {
            
            // Add all attempted questions to the variable this.attemptedQuestionsSent
            o.assign(this.attemptedQuestionsSent, val);
        });
        
        this.initPing();
    }
    
    initPing(){
        this.intervalId = setInterval(() => {
            this.sendQuestionAttempts();
        }, this.pingInterval);
    }  
    
    examEnd(callable?:Function){
        clearInterval(this.intervalId);
        
        this.intervalId = 0;
        
        this.sendQuestionAttempts(callable);
    }
    
    forceDeliverAttemptedQuestions(callable?:Function){
        // K.dd('forceDeliverAttemptedQuestions() called');
        clearInterval(this.intervalId);
        
        this.intervalId = 0;
        
        var self = this;
        this.sendQuestionAttempts(function() {
            self.initPing();
            if(callable) callable();
        });
    }
    
    private sendQuestionAttempts(callable?:Function){

        if(isDemo) return;
        
        var data:IAttemptedQuestions[] = [];
        
        for(let question_id in this.attemptedQuestionsPending){
            
            if(!this.attemptedQuestionsPending.hasOwnProperty(question_id)) continue;
            
            if(K.isEmpty(this.attemptedQuestionsPending[question_id])) continue;
            
            data.push(<IAttemptedQuestions>this.attemptedQuestionsPending[question_id])
        }
        
        if(data.length < 1){
            if(callable) callable();
            
            return;
        }
        
        K.sendAjax(K.URL.attemptMultiQuestion, success, {'attempts': this.attemptedQuestionsPending}, 'POST', fail);
        
        var self = this;
        function success(res:any) {
            self.networkFailCount = 0;
            // K.dd(res);

            if(!res.success) return K.aa(res.message);
            
            if(callable) callable();
            
            var successes:any[] = res.data.success;
            var failures:any[] = res.data.failure;
            
            if(successes){
                successes.forEach((val:any, i:number) => {
                    var sentAttempt = self.attemptedQuestionsPending[val.question_id];
                    
                    if(!sentAttempt){
                        return console.log('Attempted questions returned from server not found');
                    }
                    self.attemptedQuestionsSent[val.question_id] = self.attemptedQuestionsPending[val.question_id];
                    
                    self.attemptedQuestionsPending[val.question_id] = undefined;
                });
            }
        }
        
        function fail(jqHRX:any, textStatus:string) {
//            K.dd(jqHRX);
            if(callable) callable();
            
            if(self.intervalId == 0) return;
            
            if(jqHRX.status == 200){
                console.log('Network Error, jqHRX.status = '+jqHRX.status);
                
                console.log(jqHRX);
                
                console.log(textStatus+'\n'+jqHRX.responseText);
                // return;
            }
            
            self.networkFailCount += 1;
            
            if(self.networkFailCount > 3){
                console.log('No Internet available');
                alert("It seems your network is disconnected. Inform admin now to check it out.");
                return;
            }                
            
            if(self.networkFailCount > 1){
                console.log('Possible Network Problem');
            }                
        }
    }
    
    addToPending(questionAttempt:IAttemptedQuestions){
        this.attemptedQuestionsPending[questionAttempt.question_id] = questionAttempt;
    
        var size:number = 0;
        for(let question_id in this.attemptedQuestionsPending){
            
            if(!this.attemptedQuestionsPending.hasOwnProperty(question_id)) continue;
            
            if(K.isEmpty(this.attemptedQuestionsPending[question_id])) continue;
            
            size++;
            // Once the user has answered up to 3 questions, Forward them to the server
            if(size > 2){
                this.forceDeliverAttemptedQuestions();
                break;
            }
        }
    }
    
    addToSent(questionAttempt:IAttemptedQuestions){
        this.attemptedQuestionsSent[questionAttempt.question_id] = questionAttempt;
    }
    
    public static toString() {
        return 'ExamComm';
    }
    
    public toString() {
        return ExamComm.toString();
    }
    

    
};
export = ExamComm;

