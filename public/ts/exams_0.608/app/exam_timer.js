/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />
define(["require", "exports", "config/k"], function (require, exports, K) {
    "use strict";
    class ExamTimer {
        constructor() {
            this.intervalId = 0;
            this.timeElapsed = 0;
        }
        start(duration, examTimerHooks) {
            this.duration = duration;
            this.examTimerHooks = examTimerHooks;
            this.lessThan5MinsCalled = false;
            this.lessThan1MinCalled = false;
            this.startTime = K.getTimeStamp();
            this.evaluateTime();
            this.intervalId = setInterval(() => {
                this.evaluateTime();
            }, 1000);
        }
        evaluateTime() {
            this.timeElapsed = K.getTimeStamp() - this.startTime;
            this.timeRemaining = this.duration - this.timeElapsed;
            if (this.timeRemaining < 1) {
                // Trigger Time Elapsed --- End Exam
                this.examTimerHooks.timeElapsed();
                clearInterval(this.intervalId);
            }
            if (!this.lessThan5MinsCalled && this.timeRemaining < (5 * 60)) {
                this.lessThan5MinsCalled = true;
                // Time less than 5 minutes, turn timer color red
                this.examTimerHooks.timeLessThan5Mins();
            }
            if (!this.lessThan1MinCalled && this.timeRemaining < (1 * 60)) {
                this.lessThan1MinCalled = true;
                // Time less than 1 minute, start blinking
                this.examTimerHooks.timeLessThan1Min();
            }
            this.examTimerHooks.updateTime(K.formatTime(this.timeRemaining));
        }
        stop() {
            if (this.intervalId === 0)
                clearInterval(this.intervalId);
            if (this.examTimerHooks)
                this.examTimerHooks.timerStopped();
        }
        static toString() {
            return 'ExamTimer';
        }
        toString() {
            return ExamTimer.toString();
        }
    }
    ;
    return ExamTimer;
});
