/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />

import K = require('config/k'); 
import $ = require('jquery'); 
import {IExamEventsHooks} from '../exam_interfaces'; 
import Modal = require('util/modal');

class KeysHandler{    
     
    private lastPressedKey:number = -1;
    
    constructor(private examEventsHook:IExamEventsHooks, 
            private modal:Modal) {
        
    }
    
    public clearLastPressedKey() {
        this.lastPressedKey = -1;
    }
    
    public handleKeys( pressedKey: number ) {
        var toClear = false;
        if ( pressedKey == 13 ) {
            // enter pressed
        } else if ( pressedKey == K.KEYBOARD_KEYS.letter_A || pressedKey == K.KEYBOARD_KEYS.letter_a ) {
            this.examEventsHook.answerSelectedKeyboard( 'A' );
        } else if ( pressedKey == K.KEYBOARD_KEYS.letter_B || pressedKey == K.KEYBOARD_KEYS.letter_b ) {
            this.examEventsHook.answerSelectedKeyboard( 'B' );
        } else if ( pressedKey == K.KEYBOARD_KEYS.letter_C || pressedKey == K.KEYBOARD_KEYS.letter_c ) {
            this.examEventsHook.answerSelectedKeyboard( 'C' );
        } else if ( pressedKey == K.KEYBOARD_KEYS.letter_D || pressedKey == K.KEYBOARD_KEYS.letter_d ) {
            this.examEventsHook.answerSelectedKeyboard( 'D' );
        } else if ( pressedKey == K.KEYBOARD_KEYS.letter_P || pressedKey == K.KEYBOARD_KEYS.letter_p ) {
            this.examEventsHook.previousQuestion();
        } else if ( pressedKey == K.KEYBOARD_KEYS.letter_N || pressedKey == K.KEYBOARD_KEYS.letter_n ) {
            this.examEventsHook.nextQuestion();
        } else if ( pressedKey == K.KEYBOARD_KEYS.letter_S || pressedKey == K.KEYBOARD_KEYS.letter_s ) {
            if( this.lastPressedKey == K.KEYBOARD_KEYS.letter_S || this.lastPressedKey == K.KEYBOARD_KEYS.letter_s ){
                toClear = true;
                this.examEventsHook.stopExam();
            }else{
                // show dialog
                var callback = ()=>{
                    this.examEventsHook.stopExam();
                };
                var negativeCallback = ()=>{
                    this.clearLastPressedKey();
                };
                
                this.modal.setModalFor(this.toString())
                .setNegativeCallback(negativeCallback);
                
                this.modal.show("Submit Exam", "Do you want to end this exam?", callback, "Yes", "No");
            }
        } else if ( pressedKey == K.KEYBOARD_KEYS.letter_R || pressedKey == K.KEYBOARD_KEYS.letter_r ) {
            if( this.lastPressedKey == K.KEYBOARD_KEYS.letter_S || this.lastPressedKey == K.KEYBOARD_KEYS.letter_s ){
                // Reverse
                toClear = true;
                this.modal.dismiss();
            }else{
                toClear = true;
                this.modal.dismiss();
            }            
        }
        
        if(toClear)this.clearLastPressedKey();
        
        else this.lastPressedKey = pressedKey;
    }
    
    
    public static toString() {
        return 'KeysHandler';
    }
    
    public toString() {
        return KeysHandler.toString();
    }

    
};
export = KeysHandler;

