/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />

import K = require('config/k'); 
import $ = require('jquery'); 
import StackManager = require('stack_manager');
import {IPage} from '../interfaces';

abstract class Page implements IPage{    
    
    toString(): string {
        throw new Error("Method not implemented.");
    }
    
    constructor(protected stackManager:StackManager) {
        
    }
    
    setCurrentTab(currentTabID:string){
        
    }
    
    entryAnimation(): void {
//        this.$main.find('#' + this.getPageID()).slideDown(500, 'linear');
    }

    outAnimation(): void {
        
    }
    
    abstract open(): void;
    
    abstract reInvalidate(): void;
    
    abstract setActive(): void;
    
    close(): void{
        this.outAnimation();
    };
    
    abstract getPageID(): string;

    
};
export = Page;

