/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />

import K = require('config/k'); 
import $ = require('jquery'); 

import {IStudentData} from '../interfaces';

class Student{   
        
    constructor(private student:IStudentData){
        this.init();
    }
    
    init(){        
        $('.app-header #name').text(this.student.lastname + ' ' + this.student.firstname);
        $('#exam-layout #exam-no').text(this.student.exam_no);
//        console.log('this.student.exam_no = '+this.student.exam_no);
    }

    public static toString() {
        return 'Student';
    }
    
    public toString() {
        return Student.toString();
    }
    

    
};
export = Student;

