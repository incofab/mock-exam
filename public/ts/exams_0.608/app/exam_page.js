/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />
define(["require", "exports", "config/k", "jquery"], function (require, exports, K, $) {
    "use strict";
    class ExamPage {
        constructor(exam, examsComm) {
            this.exam = exam;
            this.examsComm = examsComm;
            this.attemptedQuestionsFormatted = {};
        }
        init(examData, tabIndex) {
            this.exam_subject_id = examData.exam_subject_id;
            this.session_id = examData.session_id;
            this.course_code = examData.course_code;
            this.course_id = examData.course_id;
            this.course_title = examData.course_title;
            this.year = examData.year;
            this.general_instructions = examData.general_instructions;
            this.questions = examData.questions;
            this.attempted_questions = examData.attempted_questions;
            this.passages = examData.passages;
            this.instructions = examData.instructions;
            this.questionsLength = examData.questions.length;
            this.tabIndex = tabIndex;
            var startIndex = 0;
            this.current_question = this.questions[startIndex];
            this.current_question.index = startIndex;
            this.formatAttemptedQuestions();
        }
        formatAttemptedQuestions() {
            this.attempted_questions.forEach((val, i) => {
                this.attemptedQuestionsFormatted[val.question_id] = val;
            });
        }
        displayQuestion(questionIndex) {
            var self = this;
            var $currentTarget = $('#exam-layout #questions-content').find(`#nav-${this.exam_subject_id} .question-main`);
            var $targetQuestion = $currentTarget.find(`.question-main`);
            K.render(this, $targetQuestion, 'exam-question');
            this.highlightCurrentQuestion();
            var $img = $('#exam-layout #questions-content').find(`#nav-${this.exam_subject_id} .question-main img`);
            $img.each(function (i, ele) {
                var $imgObj = $(this);
                var imgUrl = $imgObj.attr('src');
                self.exam.filterImgUrl(imgUrl, $imgObj, self);
            });
            //        var $targetGridQuestionNumbers = $currentTarget.find(`.question-numbers-tab`);
            //        
            //        K.render(this, $targetGridQuestionNumbers, 'exam-grid-question-numbers');
        }
        reinvalidateCurrentQuestion() {
            this.displayQuestion(this.current_question.index);
        }
        gotoQuestion(questionIndex, questionId) {
            //        var previousIndex = this.current_question.index;
            //        if(this.current_question.index == questionIndex) return;
            if (questionIndex >= this.questionsLength || this.questionsLength < 0)
                return;
            this.current_question = this.questions[questionIndex];
            this.current_question.index = questionIndex;
            //        this.displayQuestion(previousIndex);
            this.displayQuestion(questionIndex);
        }
        gotoNextQuestion() {
            //        var previousIndex = this.current_question.index;
            var newIndex = this.current_question.index + 1;
            if (newIndex >= this.questionsLength) {
                newIndex = this.questionsLength - 1;
                return;
            }
            this.current_question = this.questions[newIndex];
            this.current_question.index = newIndex;
            //        this.displayQuestion(previousIndex);
            this.displayQuestion(newIndex);
        }
        gotoPreviousQuestion() {
            //        var previousIndex = this.current_question.index;
            var newIndex = this.current_question.index - 1;
            if (newIndex < 0) {
                newIndex = 0;
                return;
            }
            this.current_question = this.questions[newIndex];
            this.current_question.index = newIndex;
            this.displayQuestion(newIndex);
        }
        highlightCurrentQuestion() {
            var $container = $('#exam-layout #questions-content').find(`#nav-${this.exam_subject_id}`);
            $container.find(`li`).removeClass('current');
            $container.find(`li[data-question_index="${this.current_question.index}"]`).addClass('current');
        }
        addSelectedAns(selectedAns) {
            var questionAttemptFormatted = {
                exam_subject_id: this.exam_subject_id,
                question_id: this.current_question.question_id,
                attempt: selectedAns,
            };
            this.attemptedQuestionsFormatted[this.current_question.question_id] = questionAttemptFormatted;
            this.examsComm.addToPending(questionAttemptFormatted);
            $('#exam-layout #questions-content').find(`#nav-${this.exam_subject_id} 
            li[data-question_index="${this.current_question.index}"]`).addClass('attempted');
        }
        getCurrentQuestion() {
            return this.current_question;
        }
        getFormattedAttemptedQuestions() {
            return this.attemptedQuestionsFormatted;
        }
        getCurrentPassage() {
            var passageText = null;
            var self = this;
            this.passages.forEach(function (passage, i) {
                if (self.course_code == passage.course_code && self.session_id == passage.session_id) {
                    if (self.current_question.question_no >= passage.from_
                        && self.current_question.question_no <= passage.to_) {
                        passageText = passage.passage;
                        return passageText;
                    }
                }
            });
            return passageText;
        }
        getCurrentInstruction() {
            var instructionText = null;
            var self = this;
            this.instructions.forEach(function (instruction, i) {
                if (self.course_code == instruction.course_code && self.session_id == instruction.session_id) {
                    if (self.current_question.question_no >= instruction.from_
                        && self.current_question.question_no <= instruction.to_) {
                        instructionText = instruction.instruction;
                        return instructionText;
                    }
                }
            });
            return instructionText;
        }
        getCourseCode() { return this.course_code; }
        getCourseId() { return this.course_id; }
        getSessionID() { return this.session_id; }
        getYear() { return this.year; }
        getSubjectID() { return this.exam_subject_id; }
        static toString() {
            return 'ExamPage';
        }
        toString() {
            return ExamPage.toString();
        }
    }
    ;
    return ExamPage;
});
