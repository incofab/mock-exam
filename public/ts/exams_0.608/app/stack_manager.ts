/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />
/// <reference path='../node_modules/@types/socket.io-client/index.d.ts' />

import K = require('../config/k');
import $ = require('jquery'); 
import {IPage} from '../interfaces';
import Container = require('app/container');
import Module = require('app/module');

class StackManager{    
    
    private backStack:IPage[] = [];
    
    constructor(private container:Container) {
        
    }
        
    public gotoPage($this:JQuery|null, nextPageClassName:string):void{
        try {
            let currentPage = this.getCurrentPage();
            var newPageObj = this.container.resolveInstance(nextPageClassName);
            if(currentPage){
                if(currentPage.toString() ==  'nextPageClassName') return;
                currentPage.close();
            }
            newPageObj.open();
            this.addToStack(newPageObj);
        } catch (e) {
        	throw new Error('Page not found');
        }        
    }
        
    public switchPages(oldPage:IPage, nextPageClassName:string):void{
        try {
            var newPageObj = this.container.resolveInstance(nextPageClassName);
            if(oldPage)oldPage.close();
            newPageObj.open();
            this.addToStack(newPageObj);
        } catch (e) {
        	throw new Error('Page not found');
        }
        
    }
    
    public back():void{
        let len = this.backStack.length;
        if(len < 2) return;
        let currentPage:IPage = this.backStack.splice(len-1, 1)[0];
        let nextPage:IPage = this.backStack[len - 2];
        currentPage.close();
        nextPage.open();
    }    
    
    public static toString() {
        return 'StackManager';
    }
    public toString() {
        return StackManager.toString();
    }

    public getCurrentPage():IPage|null{
        let len = this.backStack.length;
        if(len < 1) return null;
        return this.backStack[len-1];
    }    

    private addToStack(page:IPage):void{
        if(!page) return;
        var currentPage = <IPage>this.getCurrentPage();
        if(currentPage && (page.toString() == currentPage.toString())) return;
        this.trimStackSize();
        this.backStack.push(page);
    }
    private clearStack():void{
        this.backStack = [];
    }
    
    readonly MAX_STACK_SIZE = 10;
    
    private trimStackSize(){
        while(this.backStack.length >= this.MAX_STACK_SIZE){
            this.backStack.pop();
        }
    }
    
    reinvalidateCurrentPage(){
        // Then reinvalidate current page if available or load up a new Games page
        var currentPage:IPage = <IPage>this.getCurrentPage();
    
        if(!currentPage){
//            currentPage = this.container.resolveInstance(Games.toString());
//            currentPage.open();
            this.addToStack(currentPage);
        }else{
            currentPage.reInvalidate();
        }
    }
    
    readonly ACTIVITY_REAL_FOOTBALL = 1;
    readonly ACTIVITY_VIRTUAL_LEAGUE = 2;
    readonly ACTIVITY_SPECIALS = 3;
//    private currentActivity:number = this.ACTIVITY_REAL_FOOTBALL;
    
    private currentModule:Module|null = null;
    
    boot(moduleClassName:string){
        
        var modules = ['ActivityPage', 'Specials', 'League'];
        
        moduleClassName = (modules.indexOf(moduleClassName) >= 0 )
                            ? moduleClassName : 'ActivityPage';
        
        var module = <Module>this.container.resolveInstance(moduleClassName);
        
        this.setCurrentActivity(module);
    }
    
    setCurrentActivity(module:Module)
    {
        if(this.currentModule != null && this.currentModule.id == module.id) return;
        
        module.load();
        
        if(this.currentModule != null){
            this.currentModule.shutDown();
        } 
        
        this.currentModule = module;
    }
    
    getCurrentActivity(){
        if(this.currentModule == null) return this.ACTIVITY_REAL_FOOTBALL;
        
        else return this.currentModule.id;
    }
};

export = StackManager;

