/// <reference path='node_modules/@types/jquery/index.d.ts' />
/// <reference path='node_modules/@types/requirejs/index.d.ts' />
define(["require", "exports", "config/k", "jquery", "util/modal", "app/exam", "app/exam_timer", "app/student"], function (require, exports, K, $, Modal, Exam, ExamTimer, Student) {
    "use strict";
    // Init container
    //var config = new Config();
    //var container = config.getContainer();
    //var stackManager = <StackManager>container.resolveInstance(StackManager.toString());
    //var dialogObj = Dialog.getInstance(K);
    var modalObj = new Modal();
    var examTimer = new ExamTimer();
    var student = new Student(studentData);
    var exam = new Exam(examData, studentData, examTimer, modalObj);
    return class app {
        constructor() {
            //        console.log('Inside app construsctor');
            var self = this;
            this.inMobileMode = this.getWidth() < 700;
            $(window).resize(function () {
                if (self.getWidth() < 700) {
                    self.inMobileMode = true;
                }
                else {
                    self.inMobileMode = false;
                }
            });
        }
        getWidth() { return window.innerWidth || $(window).width(); }
        start(dispatcher) {
            //        K.Pace.options = { ajax: { ignoreURLs: [Socket.URL] } }
            var self = this;
            K.$mainPage = $('#main #content');
            $('#custom-alert').on('click', '#exit-message', function (event) {
                //            dialogObj.closeButtonClicked();
            });
            $('#custom-alert').on('click', function (event) {
                if (event.target != self)
                    return;
                //            dialogObj.outsideTouched();
            });
            $(document).ready(function () {
                // console.log('HTML loaded');
                K.registerHandlebarHelperFunctions();
                exam.init(dispatcher);
            });
            //        K.$mainPage.on('submit', 'form[name="pod_bet_form"]', function(event) {
            //            var amount = <number>$(this).find('input[name="amount"]').val();
            //            PODBets.getInstance().placeBet(amount, Sidebar);
            //            return false;
            //        });
            /*
            K.$mainPage.on('click', '.generic-click', function(event) {
               let callableName = $(this).data('caller');
               let methodParams = $(this).data('params');
    //           K.dd('generic-click called');
    //           K.dd(callableName);
    //           K.dd(methodParams);
               leagueConfig.onClick(callableName, $(this), methodParams);
               return false;
            });
            K.$mainPage.on('keyup', '.generic-keyup', function(event) {
                let callableName = $(this).data('caller');
                let methodParams = $(this).data('params');
                leagueConfig.onClick(callableName, $(this), methodParams);
                return false;
            });
            */
        }
    };
});
