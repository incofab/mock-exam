
export interface IStudentData{
    firstname:string;
    lastname:string;
    student_id:string;
    exam_no:string;
}

export interface IPage{
    open():void;
    close():void;
    reInvalidate():void;
    setActive():void;
    toString():string;
} 


export interface IMatchday{
    id:number;
    title:string;
}

export interface ILeagueGames{
    game_id:number;

    team_1_logo:string;
    team_2_logo:string;

    team_1_name:string;
    team_2_name:string;
    
    team_1_id:number;
    team_2_id:number;
    
    team_1_odd:number;
    team_2_odd:number;
    draw_odd:number;
}

export interface ILeagueTable{
    team_logo:string;
    team_name:string;
    team_id:string;
    
    num_played:number;
    num_won:number;
    num_drawn:number;
    num_lost:number;
    num_points:number;
}

export interface ILeagueResults{
    team_1_logo:string;
    team_2_logo:string;

    team_1_name:string;
    team_2_name:string;
    
    team_1_score:number;
    team_2_score:number;
}

export interface ILeagueBetSlip{
    game_id:number;
    odd:number;
    users_bet:string;
}

export interface IWinnersBalance{
    username:string;
    balance:string;
    zoranga_balance:string;
    bonus_balance:string;
}

