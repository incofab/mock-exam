
export interface IExamData{
    meta:IExamMeta;
    all_exam_subject_data:IExamSubjectData[];
    exam:IExam;
}
export interface IExamMeta{
    time_remaining:number;
}
export interface IExam{
    event_id:number;
    student_id:string;
    exam_no:string;
    start_time:string;
    end_time:string;
    paused_time:string;
}
export interface IExamSubjectData{
    tabIndex:number;
    exam_subject_id:number;
    session_id:number;
    course_code:string;
    course_id:number;
    course_title:string;
    year:string;
    general_instructions:string;
    questions:IQuestions[];
    current_question:IQuestions;
    attempted_questions:IAttemptedQuestions[];
    passages:IPassages[];
    instructions:IInstructions[];
}
export interface IQuestions{
    index:number;
    question_id:number;
    question_no:number;
    question:string;
    option_a:string;
    option_b:string;
    option_c:string;
    option_d:string;
    option_e:string;
}

export interface IAttemptedQuestions{
    exam_subject_id:number;
    question_id:number;
    attempt:string;
}

export interface IPassages{
    id:number;
    course_code:string;
    session_id:number;
    passage:string;
    from_:number;
    to_:number;
}

export interface IInstructions{
    id:number;
    course_code:string;
    session_id:number;
    instruction:string;
    from_:number;
    to_:number;    
}

export interface IExamTimerHooks{
    timeElapsed():void;
    timeLessThan5Mins():void;
    timeLessThan1Min():void;
//    timerStarted():void;
    updateTime(timeString:string):void;
    timerStopped():void;
}
export interface IExamEventsHooks{
    tabSwitched(currentTabIndex:number, previousTabIndex:number):void;
    questionSelected(questionIndex:number, questionId:number):void;
    answerSelected(selectedAns:string):void;
    answerSelectedKeyboard(selectedAns:string):void;
    nextQuestion():void;
    previousQuestion():void;
    pauseExam():void;
    stopExam():void;
    forceDeliverAttemptedQuestions():void;
}

