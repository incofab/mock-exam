/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/bootstrap/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />
define(["require", "exports", "jquery", "bootstrap"], function (require, exports, $) {
    "use strict";
    //import Bootstrap = require('bootstrap'); 
    class Modal {
        constructor() {
            this.isShown = false;
            this.$modal = $('#examModal');
            var self = this;
            this.$modal.on('hidden.bs.modal', function (e) {
                self.executeNegativeAction();
            });
        }
        setModalFor(modalFor) {
            this.$modal.attr('data-modal_for', modalFor);
            return this;
        }
        show(title, message, callback, positiveButtonText, negativeButtonText) {
            this.setTitle(title);
            this.setMessage(message);
            if (!positiveButtonText)
                positiveButtonText = 'Ok';
            this.setPositiveButtonText(positiveButtonText);
            if (!negativeButtonText)
                negativeButtonText = 'Cancel';
            this.setNegativeButtonText(negativeButtonText);
            this.$modal.modal('show');
            this.isShown = true;
            this.modalPositiveCallback = callback;
        }
        dismiss() {
            if (!this.isShown)
                return;
            this.$modal.modal('hide');
            this.$modal.attr('data-modal_for', '');
            this.isShown = false;
        }
        executePositiveAction() {
            if (this.modalPositiveCallback) {
                this.modalPositiveCallback();
            }
            this.dismiss();
        }
        executeNegativeAction() {
            if (this.modalNegativeCallback) {
                this.modalNegativeCallback();
            }
            this.dismiss();
        }
        setTitle(title) {
            this.$modal.find('.modal-title').text(title);
            return this;
        }
        setNegativeButtonText(message) {
            this.$modal.find('.negative').text(message);
            return this;
        }
        setPositiveButtonText(message) {
            this.$modal.find('.positive').text(message);
            return this;
        }
        setNegativeCallback(callback) {
            this.modalNegativeCallback = callback;
            return this;
        }
        setPositiveCallback(callback) {
            this.modalPositiveCallback = callback;
            return this;
        }
        setMessage(message) {
            this.$modal.find('.modal-body').html(message);
            return this;
        }
        static toString() {
            return 'Modal';
        }
        toString() {
            return Modal.toString();
        }
    }
    ;
    return Modal;
});
