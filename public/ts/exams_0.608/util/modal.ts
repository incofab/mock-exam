/// <reference path='../node_modules/@types/jquery/index.d.ts' />
/// <reference path='../node_modules/@types/bootstrap/index.d.ts' />
/// <reference path='../node_modules/@types/requirejs/index.d.ts' />

import K = require('config/k');
import $ = require('jquery');
import "bootstrap";
//import Bootstrap = require('bootstrap'); 

class Modal{      
    private isShown:boolean = false;
    private $modal:JQuery = $('#examModal');
    private modalPositiveCallback:Function|null;
    private modalNegativeCallback:Function|null;
    
    constructor() {
        var self = this;
        this.$modal.on('hidden.bs.modal', function (e:any) {
            self.executeNegativeAction();
        });
    }
    
    public setModalFor(modalFor:string):Modal{
        this.$modal.attr('data-modal_for', modalFor);
        return this;
    }
    
    public show(title:string, message:string, callback:Function,
            positiveButtonText?:string, negativeButtonText?:string) {
        this.setTitle(title);
        this.setMessage(message);
        if(!positiveButtonText) positiveButtonText = 'Ok';
        this.setPositiveButtonText(positiveButtonText);
        if(!negativeButtonText) negativeButtonText = 'Cancel';
        this.setNegativeButtonText(negativeButtonText);
        this.$modal.modal('show');
        this.isShown = true;
        this.modalPositiveCallback = callback;
    }
    
    public dismiss() {
        if(!this.isShown) return;
        this.$modal.modal('hide'); 
        this.$modal.attr('data-modal_for', '');   
        this.isShown = false;
    }
    
    public executePositiveAction() {
        if(this.modalPositiveCallback){
            this.modalPositiveCallback();
        }
        this.dismiss();
    }
    
    public executeNegativeAction() {
        if(this.modalNegativeCallback){
            this.modalNegativeCallback();
        }
        this.dismiss();
    }
        
    setTitle(title:string):Modal{
        this.$modal.find('.modal-title').text(title);
        return this;
    }
    
    setNegativeButtonText(message:string):Modal{
        this.$modal.find('.negative').text(message);   
        return this;     
    }
    
    setPositiveButtonText(message:string):Modal{
        this.$modal.find('.positive').text(message); 
        return this;       
    }
    
    setNegativeCallback(callback:Function):Modal{
        this.modalNegativeCallback = callback;   
        return this;     
    }
    
    setPositiveCallback(callback:Function):Modal{
        this.modalPositiveCallback = callback;   
        return this;     
    }
    
    setMessage(message:string):Modal{
        this.$modal.find('.modal-body').html(message); 
        return this;       
    }
    
    public static toString() {
        return 'Modal';
    }
    
    public toString() {
        return Modal.toString();
    }

    
};
export = Modal;

