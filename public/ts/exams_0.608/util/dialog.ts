/// <reference path='../node_modules/@types/jquery/index.d.ts' />

import $ = require('jquery');

class Dialog{

    static obj:Dialog;
    $dialog:JQuery;
    $dialogTemplate:HandlebarsTemplateDelegate;
    
    private closeOnTouchOutside:boolean;
    
    private musData:IMusdata; 
    
    constructor(K:any) {
        this.$dialogTemplate = K.loadTemplate('custom-dialog');
        this.$dialog = $('#custom-alert');
        let self = this;
//        if(!Dialog.currentObj){
//            $('#custom-alert #exit-message').on('click', function(event) {
//                self.closeButtonClicked();
//            });
//            $('#custom-alert').on('click', function(event) {
//                if(event.target != this) return;
//                self.outsideTouched();
//            });
//        }
//        Dialog.currentObj = this;
        this.reset();
    }
    getContainerObj():JQuery{
        return this.$dialog;
    }
    static getInstance(K:any){
        if(!Dialog.obj) Dialog.obj = new Dialog(K);
        return Dialog.obj;
    }
    
    showCustomAlert(msg:string, title?:string): void {
        if(title) this.musData.title = title;
        this.musData.content = msg;
        var musHTML = this.$dialogTemplate(this.musData);
        this.$dialog.html(musHTML);
        this.displayDialog();
    };
    
    showDialog(html:string, title?:string){
        if(title) this.musData.title = title;
        this.musData.content = html;
        this.musData.hideOkButton = true;
        this.musData.isHTML = true;
        this.closeOnTouchOutside = false;
        var musHTML = this.$dialogTemplate(this.musData);
        this.$dialog.html(musHTML);
        this.displayDialog();
    }
    
    private displayDialog(){
        this.$dialog.fadeIn('fast');
    }
    
    private reset(){
        this.musData = {
            title: 'Gidigada Message',
            isHTML: false,
            hideOkButton: false,
            content: '',
        };
        this.closeOnTouchOutside = true;
    }
    
    dismissDialog(){
        this.$dialog.fadeOut('fast');
        this.reset();
    }
    
    closeButtonClicked(){
        this.$dialog.fadeOut('fast');
    }
    
    outsideTouched(){
        if(!Dialog.obj.closeOnTouchOutside) return;
        this.$dialog.fadeOut('fast');
    }
    
}
export = Dialog;
interface IMusdata{
    title: string,
    isHTML: boolean,
    hideOkButton: boolean,
    content: string,
}