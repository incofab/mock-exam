/// <reference path='../node_modules/@types/jquery/index.d.ts' />
define(["require", "exports", "jquery"], function (require, exports, $) {
    "use strict";
    class Dialog {
        constructor(K) {
            this.$dialogTemplate = K.loadTemplate('custom-dialog');
            this.$dialog = $('#custom-alert');
            let self = this;
            //        if(!Dialog.currentObj){
            //            $('#custom-alert #exit-message').on('click', function(event) {
            //                self.closeButtonClicked();
            //            });
            //            $('#custom-alert').on('click', function(event) {
            //                if(event.target != this) return;
            //                self.outsideTouched();
            //            });
            //        }
            //        Dialog.currentObj = this;
            this.reset();
        }
        getContainerObj() {
            return this.$dialog;
        }
        static getInstance(K) {
            if (!Dialog.obj)
                Dialog.obj = new Dialog(K);
            return Dialog.obj;
        }
        showCustomAlert(msg, title) {
            if (title)
                this.musData.title = title;
            this.musData.content = msg;
            var musHTML = this.$dialogTemplate(this.musData);
            this.$dialog.html(musHTML);
            this.displayDialog();
        }
        ;
        showDialog(html, title) {
            if (title)
                this.musData.title = title;
            this.musData.content = html;
            this.musData.hideOkButton = true;
            this.musData.isHTML = true;
            this.closeOnTouchOutside = false;
            var musHTML = this.$dialogTemplate(this.musData);
            this.$dialog.html(musHTML);
            this.displayDialog();
        }
        displayDialog() {
            this.$dialog.fadeIn('fast');
        }
        reset() {
            this.musData = {
                title: 'Gidigada Message',
                isHTML: false,
                hideOkButton: false,
                content: '',
            };
            this.closeOnTouchOutside = true;
        }
        dismissDialog() {
            this.$dialog.fadeOut('fast');
            this.reset();
        }
        closeButtonClicked() {
            this.$dialog.fadeOut('fast');
        }
        outsideTouched() {
            if (!Dialog.obj.closeOnTouchOutside)
                return;
            this.$dialog.fadeOut('fast');
        }
    }
    return Dialog;
});
