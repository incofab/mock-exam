/// <reference path='node_modules/@types/jquery/index.d.ts' />
/// <reference path='node_modules/@types/requirejs/index.d.ts' />

import K = require('config/k');
import $ = require('jquery');
import Dialog = require('util/dialog');
import Modal = require('util/modal');
import StackManager = require('app/stack_manager');
import Exam = require('app/exam');
import ExamTimer = require('app/exam_timer');
import Student = require('app/student');
import Config = require('config/config');

import {IStudentData} from 'interfaces';
import {IExamData} from 'exam_interfaces';

// Init container
//var config = new Config();
//var container = config.getContainer();
//var stackManager = <StackManager>container.resolveInstance(StackManager.toString());

//var dialogObj = Dialog.getInstance(K);

var modalObj = new Modal();

declare var studentData:IStudentData; 
declare var examData:IExamData;

var examTimer = new ExamTimer();

var student = new Student(studentData);

var exam = new Exam(examData, studentData, examTimer, modalObj);

export = class app{
    
    inMobileMode:boolean;
    
    constructor() {
//        console.log('Inside app construsctor');
        var self:app = this;
    
        this.inMobileMode = this.getWidth() < 700;
        
        $(window).resize(function() {
            if(self.getWidth() < 700){
                self.inMobileMode = true;
            }else {
                self.inMobileMode = false;
            }
        });	
    }
    
    getWidth():number{ return window.innerWidth || <number>$(window).width(); }

    start(dispatcher:JQuery):void{
//        K.Pace.options = { ajax: { ignoreURLs: [Socket.URL] } }
        var self:app = this; 
        K.$mainPage = $('#main #content');

        $('#custom-alert').on('click', '#exit-message', function(event:any) {
//            dialogObj.closeButtonClicked();
        });
        
        $('#custom-alert').on('click', function(event:any) {
            if(event.target != self) return;
//            dialogObj.outsideTouched();
        });

        $(document).ready(function() {
            // console.log('HTML loaded');
            K.registerHandlebarHelperFunctions();
            
            exam.init(dispatcher);
        });

//        K.$mainPage.on('submit', 'form[name="pod_bet_form"]', function(event) {
//            var amount = <number>$(this).find('input[name="amount"]').val();
//            PODBets.getInstance().placeBet(amount, Sidebar);
//            return false;
//        });
        
        /*
        K.$mainPage.on('click', '.generic-click', function(event) {
           let callableName = $(this).data('caller'); 
           let methodParams = $(this).data('params');
//           K.dd('generic-click called');
//           K.dd(callableName);
//           K.dd(methodParams);
           leagueConfig.onClick(callableName, $(this), methodParams);
           return false;
        });
        K.$mainPage.on('keyup', '.generic-keyup', function(event) {
            let callableName = $(this).data('caller'); 
            let methodParams = $(this).data('params');
            leagueConfig.onClick(callableName, $(this), methodParams);
            return false;
        });
        */
    }
}