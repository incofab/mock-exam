
require.config({
    baseUrl: ADDR + 'public/ts/exams_0.608',
    optimize: 'none', // 'uglify'
    waitSeconds: 60,
    paths: {
        // If moduleID starts with "app", File is loaded from the public/ts/app directory
        app: 'app',
        jquery:[
            'https://code.jquery.com/jquery-3.2.1.min',
            '../../js/lib/jquery.min',
        ],
        handlebars: [
            'https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.10/handlebars.min',
            '../../js/lib/handlebars-v4.0.10',
        ],
//        'socket.io-client': [
//        	'https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js',
//        	'../../js/lib/socket.io.min',
//        	],
//        K: 'config/k',
    },
    packages: {
        
    },
});

require(['app'], function(app) {
	new app().start();
});

