<?php

$dontRoute = true;

require '../../index.php';

$container = (new \Bootstrap\Container\MyContainer())->getContainerInstance();

$container->call(function(
    \App\Models\DevTransactions $devTransactionsModel, 
    \App\Core\TransactionCallback $transactionsCallback,
    \Carbon\Carbon $carbon
){
    $transactions = $devTransactionsModel->getPendingTransactions();
    
    $allRes = [];
    
    /** @var \App\Models\DevTransactions $t */
    foreach ($transactions as $t) 
    {
        // Validate These Transactions
        
        $post = [];
        
        if(empty($t[PIN]))
        {
            // It is an airtime deposit
            $post[TRANSACTION_STATUS] = ($t[USER_ENTERED_AMOUNT] == \Config::DEV_VALID_TRANSFER_AMOUNT) 
                                        ? STATUS_CREDITED : STATUS_INVALID;
        }
        else 
        {
            // It is a Pin deposit
            $post[TRANSACTION_STATUS] = ($t[PIN] == \Config::DEV_VALID_PIN) ? STATUS_CREDITED : STATUS_INVALID;
        }
        
        $post[TABLE_ID] = $t[TABLE_ID];
        
        $post[AMOUNT] = $t[USER_ENTERED_AMOUNT];
        
        $ret = $t->confirmTransaction($post, $carbon, $transactionsCallback);
        
        $allRes[] = $ret;
        
        $t->delete();
    }
    
    echo 'Done <br />';
    
    die(json_encode($allRes, JSON_PRETTY_PRINT));
});



