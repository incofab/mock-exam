<?php

$dontRoute = true;

require '../../index.php';

$container = (new \Bootstrap\Container\MyContainer())->getContainerInstance();

$container->call(function(
    \App\Models\Transactions $transactionsModel,
    \App\Core\TransactionCallback $transactionsCallback
){
    $transactions = $transactionsModel->getUndeliveredTransactions();
    
    $allRes = [];
    
    foreach ($transactions as $t) 
    {
        $ret = $transactionsCallback->callFeedbackURL($t);
        
        $allRes[] = $ret;
        
//         /** @var \App\Models\Users $userData */
//         $userData = $t->user();
        
//         if(!$userData) continue;
        
//         /** @var \App\Models\Merchants */
//         $merchantData = $userData->merchant();

//         if(!$merchantData) continue;
        
//         $url = $merchantData[CALLBACK_URL];
        
//         $curl_post_data = [
            
//             PRIVATE_KEY => $merchantData[PRIVATE_KEY], 
            
//             PUBLIC_KEY => $merchantData[PUBLIC_KEY], 
            
//             ORDER_ID => $t[ORDER_ID],
            
//             'status' => $t[TRANSACTION_STATUS],
            
//             AMOUNT => $t[AMOUNT],
            
//             // Other parameters like the ones below should be included as well
//             // Contains pretty much every parameter that is returned on status request
            
//             PHONE_NO => $t[PHONE_NO],
            
//             NETWORK => $t[NETWORK],
            
//             'reference' => $t[TABLE_ID],
            
//             TRANSACTION_CHARGE => $t[TRANSACTION_CHARGE],
            
//             BALANCE_BEFORE_TRANSACTION => $t[BALANCE_BEFORE_TRANSACTION],
            
//             BALANCE_AFTER_TRANSACTION => $t[BALANCE_AFTER_TRANSACTION],
//         ];
        
//         $ret = execute_curl($url, $curl_post_data);
        
//         if(!$ret['success'])
//         {
//             $t->incrementAttempts();
            
//             continue;
//         }
        
//         $t->markAsDelivered();
    }
    
    echo 'Done <br />';
    
    die(json_encode($allRes, JSON_PRETTY_PRINT));
});

// function execute_curl($url, $curl_post_data)
// {
//     $curl = curl_init($url);
    
//     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//     curl_setopt($curl, CURLOPT_POST, true);
//     curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
    
//     $curl_response = curl_exec($curl);
    
//     $httpErrorCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    
//     $error = curl_error($curl);
//     curl_close($curl);
    
//     if($error)
//     {
//         return ['success' => false, 'result_code' => $httpErrorCode,
//             'message' => $error,  'response' => $curl_response];
//     }
    
//     if(empty($curl_response) && $httpErrorCode != 200)
//     {
//         return ['success' => false, 'result_code' => $httpErrorCode, 'response' => $curl_response,
//             'message' => "Error, trying to reach destination: $httpErrorCode, try again later"];
//     }
    
//     return ['success' => true, 'response' => $curl_response];
// }


