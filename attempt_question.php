<?php

/**
 * The directory to the app files
 * @var unknown
 */
define('APP_DIR', __DIR__ . '/App/');


// require APP_DIR . '../config/config.php';

// require APP_DIR . 'session.php';
require APP_DIR . 'Controllers/Helpers/ExamHandler.php';

// require APP_DIR . 'init.php';

$examHandler = new \App\Controllers\Helpers\ExamHandler();
// $this->allExamData = $this->session->get(STUDENT_SESSION_DATA, null);
// die('Welcome');
$type = $_GET['type'];
// dlog_22($_REQUEST);
if($type === 'single')
{
    $attempt = $_POST['attempts'];
    
    $ret = $examHandler->attemptQuestion([$attempt], 
        $_POST['event_id'], $_POST['exam_no'], $_POST['student_id']);
    
    emitResponse($ret);
}
else if($type === 'multi')
{
    $allAttempts = $_POST['attempts'];
    
    $ret = $examHandler->attemptQuestion($allAttempts, 
        $_POST['event_id'], $_POST['exam_no'], $_POST['student_id']);
    
    if($ret['success'] !== true) emitResponse($ret);
    
    emitResponse([
        'success' => true, 
        'data' => ['success' => array_values($allAttempts), 'failure' => []]
    ]);
}
else 
{
    emitResponse(['success' => false, 'message' => 'Unknown type']);
}

function emitResponse($data) 
{
    die(json_encode($data));
}


function dlog_22($msg) {
    $str = '';
    
    if (is_array($msg)) $str = json_encode($msg, JSON_PRETTY_PRINT);
    
    else $str = $msg;
    
    error_log(
        '*************************************' . PHP_EOL .
        '     Date Time: ' . date('Y-m-d h:m:s') . PHP_EOL .
        '------------------------------------' . PHP_EOL .
        $str . PHP_EOL . PHP_EOL .
        '*************************************' . PHP_EOL,
        
        3, APP_DIR . '../public/errorlog.txt');
    
}