<?php
 require_once 'inst.php';
 require_once 'col_names.php';

if(!defined('SITE_TITLE')) define('SITE_TITLE', 'Examdriller');

// Zoranga
define('ZORANGA_API_KEY', 'zoR1_live_G6oovurJOvJkiI2HlWgH4olfNr');
define('ZORANGA_MERCHANT_ID', '299009081');


define('OUR_ACCOUNT_NO', '0000000000');
define('OUR_ACCOUNT_NAME', 'Axxxxxx Nxxx');
define('OUR_BANK_NAME', 'Bank');


define("K_NEWLINE", PHP_EOL);
define("CSRF_TOKEN", 'csrf_token');
define("ADMIN_SESSION_DATA", SITE_TITLE . '_admin_session_data');
define("USER_SESSION_DATA", SITE_TITLE . '_user_session_data');
define("USER_REMEBER_LOGIN", md5(SITE_TITLE . '_user_remember_login'));
define("ADMIN_REMEBER_LOGIN", md5(SITE_TITLE . '_admin_remember_login'));
define("STUDENT_SESSION_DATA", SITE_TITLE . '_student_session_data');
define("CENTER_SESSION_DATA", SITE_TITLE . '_center_session_data');

//column
// For users table
define("PASSWORD", "password");
define('BALANCE', 'balance');
define('QUEUE_BALANCE', 'queue_balance');
define('BONUS_BALANCE', 'bonus_balance');
define('ZORANGA_BALANCE', 'zoranga_balance');
define('BITCOIN_BALANCE', 'bitcoin_balance');
define("DEPOSIT_METHOD", 'deposit_method');
define('ACCOUNT_NUMBER', 'account_number');
define('TRANSACTION_ID', 'transaction_id'); 
define('ACCOUNT_NAME', 'account_name');
define('BANK_NAME', 'bank_name');
define('ENTRY_CASH', 'entry_cash');
/**
 * This is initially the same as the username but can be changes at anytime while the 
 * users main username cannot be changes. Consider this as a kinda title for the user
 * @var unknown
 */
define('DISPLAY_USERNAME', 'display_username');

define("FULLNAME", "fullname");
define("FIRSTNAME", "firstname");
define("LASTNAME", "lastname");
define("OTHERNAMES", "othernames");
define("GENDER", "gender");
define("ADDRESS", "address");
define("EMAIL", "email");
define("PHONE_NO", "phone");
define("ACTIVATED", "is_activated");
define("SUSPENDED", "is_suspended");
define("AGE", "age");
define("DOB", "dob");
define("CREATED_BY", "created_by");
define("UPDATED_BY", "updated_by");
define("IS_VIRTUAL_USER", 'is_virtual_user');
define("FAVOURITES", 'favourites');
define("FB_ACCESS_TOKEN", 'facebook_access_token');
define("FB_ID", 'facebook_id');
define("PROFILE_PIC", 'profile_pic');
define("REMEMBER_LOGIN", 'remember_login');

define('SUCCESS', 'success');
define("SUCCESSFUL", 'success');
define("SUBSCRIPTION_DATA", 'subscription_data');

define('PASSWORD_RESET_CODE', 'password_reset_code');
define("NUM_OF_ATTEMPTS", "num_of_attempts");
define("EXPIRY_TIME", 'expiry_time');
define("IS_STILL_VALID", 'is_still_valid');
define("CITY", 'city');
define("STATE", 'state');



/**
 * from pagination
 * @var unknown
 */
define("PAGE_NUM", "pageNum");

// Queue table
define("IS_ACTIVE", 'is_active');
define("IS_APPROVED", 'is_approved');

//Used to specify the url a user will be redirected to after log in
define("REDIRECT_TO", "redirect_to");
define("PASSWORD_CONFIRMATION", "password_confirmation");
define("SECRET_KEY_JWT", '`1~.,ckjd26@sa7@)+>?%ds$*dw7/ew,.;');
define("TOKEN", 'token');
define("COMMAND", 'command');
define("REFERRAL", 'referral');


define("STATUS_ACTIVE", 'active');
define("STATUS_INACTIVE", 'inactive');
define("STATUS_WON", 'won');
define("STATUS_LOST", 'lost');
define("STATUS_CANCELLED", 'cancelled');
define("STATUS_ENDED", 'ended');
define("STATUS_EXPIRED", 'expired');
define("STATUS_PAUSED", 'paused');
define("STATUS_PENDING", 'pending');
define("STATUS_INVALID", 'invalid');
define("STATUS_CREDITED", 'credited');
define("STATUS_DELIVERED", 'delivered');
define("STATUS_PAID", 'paid');
define("STATUS_SUSPENDED", 'suspended');

define("DEPOSIT_METHOD_BANK", 'bank deposits');
define("DEPOSIT_METHOD_ONLINE", 'online deposits');
define("DEPOSIT_METHOD_BITCOIN", 'bitcoin deposits');
define("DEPOSIT_METHOD_ZORANGA", 'airtime and zora deposits');

define("NETWORK_MTN", 'MTN');
define("NETWORK_GLO", 'GLO');
define("NETWORK_AIRTEL", 'AIRTEL');
define("NETWORK_9_MOBILE", '9 MOBILE');
define("NETWORK_MTN_TRANSFER", 'MTN TRANSFER');

define("CHOICE_PLATFORM_API", 'API');
define("CHOICE_PLATFORM_WEBSITE", 'WEBSITE');

/** 
 * Signifies the percentage zoranga charges for airtime transactions
 * @var unknown
 */
define('ZORANGA_CHARGE_PERCENTAGE', 30);

/** Percent bonus for every bank deposit*/
define("BANK_DEPOSIT_BONUS", 42.857);

/** Tag to mark a row as deleted */
define("DELETED", "deleted");

define("NAIRA_SIGN", '₦');
define("CURRENCY_SIGN", NAIRA_SIGN);

define("INJECTED", 'injected');

define("WHATSAPP_NO", '+2348034715310');
define("FACEBOOK_PAGE", 'https://web.facebook.com/cheetahpay.ng');
define("TWITTER_PAGE", 'https://twitter.com/cheetahpay');
define("INSTAGRAM_PAGE", '#');
define("GOOGLE_PLUS_PAGE", '#');
define("SITE_EMAIL", 'cheetahfastpay@gmail.com');

define('PERCENT_CHARGE', 20);

define("DEPOSIT_EMAIL", 'cheetahpay.deposit@yahoo.com');
define("SHARE_AND_SELL_NUMBER", '08034715310');

define("APP_VERSION", '0.608');

class Config
{
    const NETWORKS = [NETWORK_9_MOBILE, NETWORK_AIRTEL, NETWORK_GLO, NETWORK_MTN, NETWORK_MTN_TRANSFER];
    
    const CHOICE_PLATFORMS = [CHOICE_PLATFORM_API, CHOICE_PLATFORM_WEBSITE];
    
    const TRANSACTION_STATUSES = [STATUS_PENDING, STATUS_CREDITED, STATUS_INVALID];
    
    const DELIVERY_STATUSES = [STATUS_PENDING, STATUS_DELIVERED];
    
    const MINIMUM_WITHDRAWAL = 1000;
    
    const DEV_VALID_PIN = '1111222233334444';
    
    const DEV_VALID_TRANSFER_AMOUNT = 200;
    
    const MIN_SYSTEM_TIME = '2018-01-05 10:47:05';
}



