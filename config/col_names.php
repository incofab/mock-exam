<?php

// Tables
define('ADMIN_TABLE', 'admins');
define("STUDENTS_TABLE", 'students');

define("EXAM_CONTENT_TABLE", "exam_contents");
define("COURSES_TABLE", "courses");
define("SUMMARY_TABLE", "summary");
define("SESSIONS_TABLE", "academic_sessions");
define("QUESTIONS_TABLE", "questions");
define("INSTRUCTIONS_TABLE", "instructions");
define("PASSAGES_TABLE", "passages");
define('EXAM_CENTERS_TABLE', 'exam_centers');
define('EXAMS_TABLE', 'exams');
define('EXAM_SUBJECTS_TABLE', 'exam_subjects');
define('EVENT_SUBJECTS_TABLE', 'event_subjects');
define('QUESTION_ATTEMPTS_TABLE', 'question_attempts');
define('EVENTS_TABLE', 'events');
define('SETTINGS_TABLE', 'settings');
define('CENTER_SETTINGS_TABLE', 'center_settings');

define("USERS_TABLE", 'users');
define('PASSWORD_RESET_TABLE', 'password_reset');
define('ZORANGA_DEPOSITS_TABLE', 'zoranga_deposits');
define('WITHDRAWALS_TABLE', 'withdrawals');
define('NOTIFICATION_TABLE', 'notifications');
define('COMPLAINTS_TABLE', 'complaints');
define('ACTIVATION_TABLE', 'activation');
define('BANK_DEPOSITS_TABLE', 'bank_deposits');
define('TRANSACTIONS_TABLE', 'tansactions');
define('QUICK_CASH_TABLE', 'quick_cash');
define('DEV_TRANSACTIONS_TABLE', 'dev_tansactions');



// Course list table
define("COURSE_ID", "course_id");
define("COURSE_CODE", "course_code");
define("COURSE_TITLE", "course_title");
define("DESCRIPTION", "description");
define("NUM_OF_ACTIVATIONS", "activation_quantity");

// Course Summaary
define("SUMMARY", "summary");

// Sesssions
define("SESSION", "session");
define("GENERAL_INSTRUCTIONS", "general_instructions");
define("NUM_OF_QUESTIONS", "num_of_questions");

define("TITLE", "title");
define("CHAPTER_ID", "chapter_id");
define("CHAPTER_NO", "chapter_no");
define("KEY", "key");
define("VALUE", "value");


define("CATEGORY", "category");
define("ADDED_BY", "added_by");
define("CENTER_CODE", "center_code");
define("CENTER_NAME", "center_name");
define("EXAM_NO", "exam_no");
define("EXAM_SUBJECT_ID", "exam_subject_id");
define("QUESTION_ID", "question_id");
define("ATTEMPT", "attempt");
define("DURATION", "duration");
define("EVENT_ID", "event_id");
define("SCORE", "score");

define("INSTRUCTION", "instruction");
define("PASSAGE", "passage");
define("FROM_", "from_");
define("TO_", "to_");

define("ALL_PASSAGES", "all_passages");
define("ALL_INSTRUCTION", "all_instructions");

// Questions table
define("SESSION_ID", "session_id");
define("COURSE_SESSION_ID", "course_session_id");
define("QUESTION_NO", "question_no");
define("QUESTION", "question");
define("OPTION_A", "option_a");
define("OPTION_B", "option_b");
define("OPTION_C", "option_c");
define("OPTION_D", "option_d");
define("OPTION_E", "option_e");
define("ANSWER", "answer");
define("ANSWER_META", "answer_meta");


//columns
define('TABLE_ID', 'id');
define('USERNAME', 'username');
define('CREATED_AT', 'created_at');
define('UPDATED_AT', 'updated_at');
define('FAILED_LOGIN_COUNT', 'failed_login_count');
define('FIRST_FAILED_LOGIN', 'first_failed_login');

define('STUDENT_ID', 'student_id');


define('START_TIME', 'start_time');
define('PAUSED_TIME', 'paused_time');
define('END_TIME', 'end_time');
define('IS_CANCELLED', 'is_cancelled');
define('IS_CREDITED', 'is_credited');
define('IS_EXPIRED', 'is_expired');
define('IS_RESOLVED', 'is_resolved');
define('IS_READ', 'is_read');
define('RESULT', 'result');
define('ACCOUNT_TYPE', 'account_type');

// define('TRANSACTION_ID', 'transaction_id');
define('USER_ID', 'user_id');
define("DOMAIN_URL", "domain_url");
define('CALLBACK_URL', 'callback_url');
define('PUBLIC_KEY', 'public_key');
define('PRIVATE_KEY', 'private_key');

define('USER_ENTERED_AMOUNT', 'user_entered_amount');
define('TRANSACTION_TIME', 'transaction_time');
define('CHOICE_PLATFORM', 'choice_platform');
define('TRANSACTION_STATUS', 'transaction_status');
define('TRANSACTION_CHARGE', 'transaction_charge');
define('ORDER_ID', 'order_id');
define('DELIVERY_STATUS', 'delivery_status');
define('DELIVERY_ATTEMPTS', 'delivery_attempts');


define("PIN", 'pin');
define('WITHDRAWAL_MODE', 'withdrawal_mode');
define('BALANCE_BEFORE_TRANSACTION', 'balance_before_transaction');
define('BALANCE_AFTER_TRANSACTION', 'balance_after_transaction');
// define('BALANCE_BEFORE_DEPOSIT', 'balance_before_deposit');
// define('BALANCE_AFTER_DEPOSIT', 'balance_after_deposit');
define('CONFIRMED_BY', 'confirmed_by');
define('DEPOSITORS_NAME', 'depositors_name');
define("NETWORK", 'network');
define("AMOUNT", 'amount');
define("REFERENCE_ID", 'reference_id');
define("AMOUNT_IN_DOLLARS", 'amount_in_dollars');
define("AMOUNT_IN_NAIRA", 'amount_in_naira');

define("COUNTRY", "country");
define("INSTITUTION", "institution");
define("REGION", "region");
define("EXAM_NAME", "exam_name");
define("IS_FILE_CONTENT_UPLOADED", "is_file_content_uploaded");
define("EXAM_CONTENT_ID", "exam_content_id");
define("FILE_VERSION", "file_version");
define("FILE_PATH", "file_path");

define('MESSAGE', 'message');
define('ERROR_CODE', 'error_code');
define('COMPLAINT', 'complaint');
define('IMAGE_1', 'image1');
define('IMAGE_2', 'image2');

define("STATUS", "status");
define("POINTS", "points");

define("LEVEL", "level");

define("IP_ADDRESS", "ip_address");
define("FORWARDED_FOR_IP_ADDRESS", "forwarded_for_ip_address");
define("COUNT", "count");

define("ACTIVATION_CODE", "activation_code");



