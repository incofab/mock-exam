<?php
$title = "Extend Student's exam time | " . SITE_TITLE;
$donFlashMessages = true;
$errors = isset($errors) ? $errors : $sessionModel->getFlash('error');
$post = isset($post) ? $post : [];
$valErrors = $sessionModel->getFlash('val_errors', []);
if($valErrors) $errors = null;
?>

@extends('centers.layout')

@section('dashboard_content')

<div class="app-title">
	<div>
		<h1>
			<i class="fa fa-dashboard"></i> Exams
		</h1>
		<p>Extend Student's exam time</p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> <a href="{{getAddr('admin_dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{getAddr('center_view_all_exams')}}">Exams</a></li>
		<li class="breadcrumb-item">Extend time</li>
	</ul>
</div>
<div>
	<div class="tile">
		<h3 class="tile-title">{{$student[FIRSTNAME]}} {{$student[LASTNAME]}}</h3>
		@include('common.form_message')
		<div>
			<p><span>Time Remaining: </span> {{$timeRemaining}}</p>
		</div>
		<form action="" method="post">
    		<div class="tile-body">
				<div class="form-group w-75">
					<label class="control-label">Extended remainig time by (mins):</label> 
					<input class="form-control" type="number" placeholder="Extend time" name="extend_time" 
						value="<?= getValue($post, 'extend_time')  ?>" >
				</div>
    		</div>
    		<div class="tile-footer">
    			<button class="btn btn-primary" type="submit">
    				<i class="fa fa-fw fa-lg fa-check-circle"></i>Submit
    			</button>
    		</div>
		</form>
	</div>
</div>

@endsection