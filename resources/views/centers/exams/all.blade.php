<?php
$title = "Exam Center - All Registered Exams | " . SITE_TITLE;
$page = 'manage';
$subCat = 'admin';
$confirmMsg = 'Are you sure?';
// dDie($allRecords->toArray());
?>
@extends('centers.layout')

@section('dashboard_content')

<div class="app-title">
	<div>
		<h1>
			<i class="fa fa-dashboard"></i> Exams
		</h1>
		<p>List of Student Exams</p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> <a href="{{getAddr('center_dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item">Exams</li>
	</ul>
</div>
@include('common.form_message')
<div class="tile">
    <div class="tile-header clearfix mb-3">
    	<a href="{{getAddr('center_register_exam')}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
    </div>
    <div class="tile-body">
    	<table class="table table-responsive table-hover table-bordered" id="data-table" >
    		<thead>
    			<tr>
    				<th>Student Name</th>
    				<th>Exam No</th>
    				<th>Event</th>
<!--     				<th>Subjects</th> -->
    				<th>Duration</th>
    				<th>Status</th>
    				<th><i class="fa fa-bars p-2"></i></th>
    			</tr>
    		</thead>
			@foreach($allRecords as $record)
			<?php 
			 $examSubjects = $record['examSubjects'];
			 $event = $record['event'];
			 $mySubjects = '';
			 foreach ($examSubjects as $subject) 
			 {
			     $session = $subject['session'];
			 
			     $mySubjects .= $session['course'][COURSE_CODE] . " ({$session[SESSION]}), ";
			 }
			 $mySubjects = rtrim($mySubjects, ', ');
			 $examNo = $record[EXAM_NO];
			 $studentId = $record[STUDENT_ID]; 
			 $student = $record['student'];
			?>
				<tr title="Subjects: [{{$mySubjects}}]" >
					<td>{{$student[LASTNAME]}} {{$student[FIRSTNAME]}}</td>
					<td>{{$record[EXAM_NO]}}</td>
					<td>
						@if(empty($eventId))
						<a href="{{getAddr('center_view_all_exams', [$event[TABLE_ID]])}}" 
							class="btn-link">{{$event[TITLE]}}</a>
						@else
						{{$event[TITLE]}}
						@endif
					</td>
					<?php /*
					<td>{{$mySubjects}}</td>
					*/?>
					<td>{{\App\Core\Settings::splitTime($event[DURATION], true)}}</td>
					<td>
						@if($record[STATUS] == STATUS_ACTIVE)
							@if(empty($record[START_TIME]))
								<button class="btn btn-success">Ready</button>
							@else
								<button class="btn btn-success">{{$record[STATUS]}}</button>
							@endif
						@elseif($record[STATUS] == STATUS_PAUSED)
						<button class="btn btn-warning">{{$record[STATUS]}}</button>						
						@elseif($record[STATUS] == STATUS_SUSPENDED)
						<button class="btn btn-danger">{{$record[STATUS]}}</button>						
						@else
						<a href="{{getAddr('center_view_exam_result', [$examNo, $studentId, '?next='.$next])}}" class="btn btn-link">View Results</a>
						@endif
					</td>
					<td>
						<i class="fa fa-bars p-2 pointer"
						   tabindex="0"
						   role="button" 
                           data-html="true" 
                           data-toggle="popover" 
                           title="Options" 
                           data-placement="bottom"
                           data-content="<div>
                            <?php /*
                            <div><small><i class='fa fa-pencil'></i> 
                            <a onclick='return confirmAction()' href='{{getAddr('center_edit_exam', [$record[TABLE_ID]])}}' class='btn btn-link'>Edit</a></small></div>
                            */?>
                            <div><small><i class='fa fa-circle-o'></i> 
                            <a href='{{getAddr('center_extend_exam_time', [$record[EXAM_NO]])}}' class='btn btn-link'>Extend Exam Time</a></small></div>
                            @if($record[STATUS] == STATUS_SUSPENDED)
                                <div><small><i class='fa fa-circle-o'></i> 
                                <a onclick='return confirmAction()' href='{{getAddr('center_exam_unsuspend', [$record[TABLE_ID]])}}' class='btn btn-link'>Unsuspend</a></small></div>
                            @else
                                <div><small><i class='fa fa-circle-o'></i> 
                                <a onclick='return confirmAction()' href='{{getAddr('center_exam_suspend', [$record[TABLE_ID]])}}' class='btn btn-link'>Suspend</a></small></div>
                            @endif
                            @if($record[STATUS] == STATUS_ACTIVE && !empty($record[START_TIME]))
                            	<div><small><i class='fa fa-power-off'></i> 
                                <a onclick='return confirmAction()' href='{{getAddr('center_force_end_exam', [$record[EXAM_NO]])}}' class='btn btn-link'>End Exam</a></small></div>
                            @endif
                            <div><small><i class='fa fa-trash'></i> 
                            	<a onclick='return confirmAction()' href='{{getAddr('center_delete_exam', [$record[TABLE_ID]])}}' class='btn btn-link text-danger'>Delete</a></small></div>
                            </div>
                            "></i>
					</td>
				</tr>
			@endforeach
		</table>
	</div>
	<div class="tile-footer">
		@include('common.paginate')
	</div>
</div>

<!-- Data table plugin-->
<script type="text/javascript" src="{{assets('lib/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{assets('lib/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
$('#data-table').DataTable({
    "iDisplayLength": 50
});
$(function () {
//   $('[data-toggle="popover"]').popover();
  var popOverSettings = {
// 	    placement: 'bottom',
// 	    container: 'body',
// 	    html: true,
	    selector: '[data-toggle="popover"]', //Sepcify the selector here
// 	    content: function () {
// 	        return $('#popover-content').html();
// 	    }
	}
	
	$('#data-table').popover(popOverSettings);
});
function confirmAction() {
	return confirm('{{$confirmMsg}}');
}
</script>

@stop
