<?php
$title = "Exam Centers - All Students | " . SITE_TITLE;
$page = 'manage';
$subCat = 'admin';
$confirmMsg = 'Are you sure?';
?>
@extends('centers.layout')

@section('dashboard_content')

<div class="app-title">
	<div>
		<h1>
			<i class="fa fa-dashboard"></i> Students
		</h1>
		<p>List of all students in this Center</p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> <a href="{{getAddr('admin_dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item">Students</li>
	</ul>
</div>
	
<div class="tile" id="all-students">
    <div class="tile-header clearfix mb-3">
    	<a href="{{getAddr('center_add_student')}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> New</a>
    </div>
    <div class="tile-body">
    	<table class="table table-hover table-bordered" id="data-table" >
    		<thead>
    			<tr>
    				<th><div class="text-center"><input type="checkbox" /></div></th>
    				<th>Student ID</th>
    				<th>Name</th>
    				<th>Phone</th>
    				<th>Email</th>
    				<th><i class="fa fa-bars p-2"></i></th>
    			</tr>
    		</thead>
			@foreach($allRecords as $record)
				<tr id="_{{$record[STUDENT_ID]}}" >
					<td>
						<div class="input-container text-center">
							<label for="{{$record[STUDENT_ID]}}" class="px-2 py-1 pointer m-0" >
        						<input type="checkbox" class="pointer" id="{{$record[STUDENT_ID]}}" 
        							data-id="{{$record[STUDENT_ID]}}"  />
							</label>
        				</div>
					</td>
					<td>{{$record[STUDENT_ID]}}</td>
					<td>{{$record[LASTNAME]}} {{$record[FIRSTNAME]}}</td>
					<td>{{$record[PHONE_NO]}}</td>
					<td>{{$record[EMAIL]}}</td>
					<td>
						<i class="fa fa-bars p-2"
						   tabindex="0"
						   role="button" 
                           data-html="true" 
                           data-toggle="popover" 
                           title="Options" 
                           data-placement="bottom"
                           data-content="<div>
                            <div><small><i class='fa fa-graduation-cap'></i> 
                            <a href='{{getAddr('center_register_exam', [$record[STUDENT_ID]])}}' class='btn btn-link'>Register Exam</a></small></div>
                            <div><small><i class='fa fa-pencil'></i> 
                            <a href='{{getAddr('center_edit_student', [$record[TABLE_ID]])}}' class='btn btn-link'>Edit</a></small></div>
                            @if($record[STATUS] == STATUS_SUSPENDED)
                                <div><small><i class='fa fa-circle-o'></i> 
                                <a onclick='return confirmAction()'  href='{{getAddr('center_student_unsuspend', [$record[TABLE_ID]])}}' class='btn btn-link'>Unsuspend</a></small></div>
                            @else
                                <div><small><i class='fa fa-circle-o'></i> 
                                <a onclick='return confirmAction()'  href='{{getAddr('center_student_suspend', [$record[TABLE_ID]])}}' class='btn btn-link'>Suspend</a></small></div>
                            @endif
                            <div><small><i class='fa fa-trash'></i> 
                            <a onclick='return confirmAction()'  href='{{getAddr('center_delete_student', [$record[TABLE_ID]])}}' class='btn btn-link text-danger'>Delete</a></small></div>
                            </div>
                            "></i>
					</td>
				</tr>
			@endforeach
		</table>
	</div>
	<div class="tile-footer">
		@include('common.paginate')
	<div class="my-2">
    	<button class="btn btn-danger disabled" id="btn-delete-selected"
    		onclick="handleSubmit('{{STATUS_INVALID}}')" >
    		<i class="fa fa-trash"></i> Delete Selected Students 
    	</button>
    </div>
    <form action="{{getAddr('center_multi_delete_student')}}" method="post" id="multi-delete">
    	<input type="hidden" id="student_ids" name="{{STUDENT_ID}}" value="" />
    </form>
	</div>
</div>

<!-- Data table plugin-->
<script type="text/javascript" src="{{assets('lib/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{assets('lib/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
$('#data-table').DataTable({
    "iDisplayLength": 50
});

$(function () {
  var popOverSettings = {
// 	    placement: 'bottom',
// 	    container: 'body',
// 	    html: true,
	    selector: '[data-toggle="popover"]', //Sepcify the selector here
// 	    content: function () {
// 	        return $('#popover-content').html();
// 	    }
	}
	
	$('#data-table').popover(popOverSettings);
});
function confirmAction() {
	return confirm('{{$confirmMsg}}');
}
$(function() {
	$('#data-table')
    	.on('change', 'th input', function(e) {
    		var $cb = $(e.target);
    		if($cb.prop('checked')){
    			$('#data-table td .input-container input').prop('checked', true).trigger('change');
    		}else{
    			$('#data-table td .input-container input').prop('checked', false).trigger('change');
    		}
    	});
	$('#data-table')
    	.on('change', 'td .input-container input', function(e) {
    		var $cb = $(e.target);
    		var id = $cb.data('id');
    		if($cb.prop('checked')){
    			$('#data-table tr#_' + id).addClass('table-success');
    			addToSelectedIDs(id);
    		}else{
    			$('#data-table tr#_' + id).removeClass('table-success');
    			removeSelectedIDs(id);
    		}

    		if(selectedId.length > 0){
				$('#btn-delete-selected').removeClass('disabled');
    		}else{
				$('#btn-delete-selected').addClass('disabled');
    		}
    	});
});

var selectedId = [];
function removeSelectedIDs(id) {
	var index = selectedId.indexOf(id);
	
	if(index == -1) return;
	
	selectedId.splice(index, 1);
}

function addToSelectedIDs(id) {
	
	if(selectedId.indexOf(id) != -1) return;
	
	selectedId.push(id);	
}

function handleSubmit(status) {

	if(selectedId.length < 1) return;
	
	if(!confirm('Do you want to delete selected students?')) return false;

	var studentIDs = '';

	var len = selectedId.length;
	
	for (var i = 0; i < len; i++) {

		var id = selectedId[i];
		
		if(i == len-1) studentIDs += id;

		else studentIDs += id+',';
	}

	$('form#multi-delete input[name="{{STUDENT_ID}}"]').val(studentIDs);
	
	$('form#multi-delete').submit();	
}

</script>

@stop
