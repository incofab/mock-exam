<?php
$title = "Upload Student - Exam Center | " . SITE_TITLE;
// $donFlashMessages = true;
// $errors = isset($errors) ? $errors : $sessionModel->getFlash('error');
$post = isset($post) ? $post : [];
// $valErrors = $sessionModel->getFlash('val_errors', []);
// if($valErrors) $errors = null;
$subjects = [];
?>

@extends('centers.layout')

@section('dashboard_content')

<div class="app-title">
	<div>
		<h1>
			<i class="fa fa-dashboard"></i> Students
		</h1>
		<p>Upload students record from Excel</p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> <a href="{{getAddr('admin_dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{getAddr('admin_view_all_centers')}}">Students</a></li>
		<li class="breadcrumb-item">Upload Students</li>
	</ul>
</div>
<div>
	<div class="tile">
		<h3 class="tile-title">Upload Students</h3>
    	@include('common.form_message')
		<form action="" method="post" enctype="multipart/form-data" >
    		<div class="tile-body">
    			<div class="form-group w-75">
					<label class="control-label">Event</label>
					<select name="{{EVENT_ID}}" class="form-control" id="selected-event" onchange="eventSelected()" >
						<option value="" >select event</option>
						@foreach($events as $event)
						<option value="{{$event[TABLE_ID]}}" 
						  <?= markSelected(array_get($post, EVENT_ID), $event[TABLE_ID]) ?>
						  >{{$event[TITLE]}}</option>
						  
						  <?php
						      $availableSubjects = $event->eventSubjects()->lists(COURSE_SESSION_ID);
						      $subArr = [];
						      $subjectSessionDetails = \App\Models\Sessions::whereIn(TABLE_ID, $availableSubjects)
						          ->with(['course'])->get();
						      foreach ($subjectSessionDetails as $subjectSessionDetail) {
						          $subArr[] = "<b>{$subjectSessionDetail['course'][COURSE_CODE]}</b>, Session ID = "
						              .$subjectSessionDetail[TABLE_ID];
						      }
// 						      $subjectDetails = \App\Models\Courses::whereIn(TABLE_ID, $availableSubjects)->get();
// 						      foreach ($subjectDetails as $subjectDetail) {
// 						          $subArr[] = $subjectDetail[COURSE_CODE].", ID = ".$subjectDetail[TABLE_ID];
// 						      }
// 						      ->lists(COURSE_CODE);
    						  
    						  $subjects[$event[TABLE_ID]] = implode('<br>', $subArr);
    				      ?>
						@endforeach
					</select>
					<div class="alert alert-info mt-2" id="available-event-subjects-container">
						<div class="mb-1"><b><small><u>Available subjects</u></small></b></div>
						<div id="available-event-subjects"></div>
					</div>
				</div>		
    			<div class="form-group w-75">    		
    				<label for="" >Excel Student Records</label><br />
    				<input type="file" class="form-control" name="content" value="" />
    				<input type="hidden" class="form-control" name="upload_students" value="true" />
	    		</div>
    		</div>
    		<div class="tile-footer">
    			<button class="btn btn-primary" type="submit" onclick="return confirm('Note: This might a few minutes and should not be interrupted. \n\nContinue?')" >
    				<i class="fa fa-fw fa-lg fa-check-circle"></i> Upload
    			</button>
    		</div>
		</form>
	</div>

</div>
<script type="text/javascript">

	var subjects = <?= json_encode($subjects, JSON_PRETTY_PRINT); ?>;

	$('#available-event-subjects-container').hide();

	function eventSelected() {

		var eventId = $('#selected-event').val();
		
		if(!subjects[eventId]){

			$('#available-event-subjects-container').hide();

			 return;
		}else{

			$('#available-event-subjects-container').show();
			
		}
		
		$('#available-event-subjects').html(subjects[eventId]);
	}
</script>
@endsection