<?php
$title = "Add Student - Exam Center | " . SITE_TITLE;
$donFlashMessages = true;
$errors = isset($errors) ? $errors : $sessionModel->getFlash('error');
$post = isset($post) ? $post : [];
$valErrors = $sessionModel->getFlash('val_errors', []);
if($valErrors) $errors = null;
?>

@extends('centers.layout')

@section('dashboard_content')

<div class="app-title">
	<div>
		<h1>
			<i class="fa fa-dashboard"></i> Students
		</h1>
		<p>Register a student in this Exam Center</p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> <a href="{{getAddr('admin_dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{getAddr('admin_view_all_centers')}}">Students</a></li>
		<li class="breadcrumb-item">Register</li>
	</ul>
</div>
<div>
	<div class="tile">
		<h3 class="tile-title">{{ isset($edit) ? 'Edit' : 'Register' }} Student</h3>
    	@if($valErrors)
    	<div class="alert alert-danger">
    		@foreach($valErrors as $vError)
    			<p><i class="fa fa-star" style="color: #cc4141;"></i> {{implode('<br />', $vError)}}</p>
    		@endforeach
    	</div>
    	@endif
    	@if($errors)
    	<div class="alert alert-danger">
   			<div><i class="fa fa-star" style="color: #cc4141;"></i> {{$errors}}</div>
    	</div>
    	@endif
		<form action="" method="post">
    		<div class="tile-body">
				<div class="form-group">
					<label class="control-label">Firstname</label> 
					<input type="text" id="" name="<?= FIRSTNAME ?>" value="<?= getValue($post, FIRSTNAME)  ?>" 
						placeholder="Firstname" class="form-control" >
				</div>
				<div class="form-group">
					<label class="control-label">Lastname</label> 
					<input type="text" id="" name="<?= LASTNAME ?>" value="<?= getValue($post, LASTNAME)  ?>" 
						placeholder="Lastname" class="form-control" >
				</div>
				<div class="form-group">
					<label class="control-label">Email [optional]</label> 
					<input type="email" id="" name="<?= EMAIL ?>" value="<?= getValue($post, EMAIL)  ?>" 
						placeholder="Email" class="form-control">
				</div>
				<div class="form-group">
					<label class="control-label">Phone [optional]</label> 
					<input type="text" id="" name="<?= PHONE_NO ?>" value="<?= getValue($post, PHONE_NO)  ?>" 
						placeholder="Reachable Mobile number" class="form-control">
				</div>
				<div class="form-group">
					<label class="control-label">Address [optional]</label>
					<textarea class="form-control" rows="3" name="<?= ADDRESS ?>"
						placeholder="Address of the exam center"><?= getValue($post, ADDRESS)  ?></textarea>
				</div>
				@if(isset($edit))
				<div>
					<input type="hidden" name="{{TABLE_ID}}" value="$tableId" />
				</div>
				@endif
    		</div>
    		<div class="tile-footer">
    			<button class="btn btn-primary" type="submit">
    				<i class="fa fa-fw fa-lg fa-check-circle"></i>Register
    			</button>
    			&nbsp;&nbsp;&nbsp;
    			<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
    		</div>
		</form>
	</div>

</div>

@endsection