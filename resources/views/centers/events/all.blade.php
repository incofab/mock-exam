<?php
$title = "Exam Center - All Events | " . SITE_TITLE;
$page = 'manage';
$subCat = 'admin';
$confirmMsg = 'Are you sure?';
?>
@extends('centers.layout')

@section('dashboard_content')

<div class="app-title">
	<div>
		<h1>
			<i class="fa fa-dashboard"></i> Events
		</h1>
		<p>List of all Events</p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> <a href="{{getAddr('center_dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item">Exams</li>
	</ul>
</div>
	
<div class="tile">
	@include('common.form_message')
    <div class="tile-header clearfix mb-3">
    	<a href="{{getAddr('center_add_event')}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> New</a>
    </div>
    <div class="tile-body">
    	<table class="table table-hover table-bordered" id="data-table" >
    		<thead>
    			<tr>
    				<th>S/No</th>
    				<th>Title</th>
    				<th>Description</th>
    				<th>Duration</th>
    				<th><i class="fa fa-bars p-2"></i></th>
    			</tr>
    		</thead>
    		<?php $i = 0; ?>
			@foreach($allRecords as $record)
			<?php 
			 $eventSubjects = $record['eventSubjects'];
			 $subjects = '';
			 foreach ($eventSubjects as $subject) {
			     $subjects .= $subject[COURSE_CODE] . ',';
			 }
			 $subjects = rtrim($subjects, ',');
			 $dur = \App\Core\Settings::splitTime($record[DURATION]);
			 $i++;
			?>
				<tr title="Subjects: [{{$subjects}}]" >
					<td>{{$i}}</td>
					<td>{{$record[TITLE]}}</td>
					<td>{{$record[DESCRIPTION]}}</td>
					<td><?= "{$dur['hours']}hrs, {$dur['minutes']}mins, {$dur['seconds']}secs" ?></td>
					<td>
						<i class="fa fa-bars p-2 pointer"
						   tabindex="0"
						   role="button" 
                           data-html="true" 
                           data-toggle="popover" 
                           title="Options" 
                           data-placement="left"
                           data-content="<div>
                            <div><small><i class='fa fa-eye'></i> 
                            <a href='{{getAddr('center_preview_event', [$record[TABLE_ID]])}}' class='btn btn-link'>Preview</a></small></div>
                            @if($record[STATUS] == STATUS_ACTIVE)
                            <div><small><i class='fa fa-pause'></i> 
                            <a onclick='return confirmAction()' href='{{getAddr('center_event_pause_all_exams', [$record[TABLE_ID]])}}' class='btn btn-link'>Pause All</a></small></div>
                            @endif
                            <div><small><i class='fa fa-graduation-cap'></i> 
                            <a href='{{getAddr('center_view_all_exams', [$record[TABLE_ID]])}}' class='btn btn-link'>Exams</a></small></div>
                            <div><small><i class='fa fa-edit'></i> 
                            <a href='{{getAddr('center_multi_register_exam', [$record[TABLE_ID]])}}' class='btn btn-link'>Register Students for Exam</a></small></div>
                            <div><small><i class='fa fa-pencil'></i> 
                            <a href='{{getAddr('center_edit_event', [$record[TABLE_ID]])}}' class='btn btn-link'>Edit</a></small></div>
                            @if($record[STATUS] == STATUS_SUSPENDED)
                                <div><small><i class='fa fa-circle-o'></i> 
                                <a onclick='return confirmAction()' href='{{getAddr('center_event_unsuspend', [$record[TABLE_ID]])}}' class='btn btn-link'>Unsuspend</a></small></div>
                            @else
                                <div><small><i class='fa fa-circle-o'></i> 
                                <a onclick='return confirmAction()' href='{{getAddr('center_event_suspend', [$record[TABLE_ID]])}}' class='btn btn-link'>Suspend</a></small></div>
                            @endif
                            <div><small><i class='fa fa-bar-chart'></i> 
                            <a class='btn btn-link' href='{{getAddr('center_event_result', [$record[TABLE_ID]])}}' >View Result</a></small></div>
                            
                            @if($record[STATUS] == STATUS_ACTIVE || $record[STATUS] == STATUS_SUSPENDED)
                            <div><small><i class='fa fa-envelope'></i> 
                            <a class='btn btn-link' href='{{getAddr('center_sms_invite', [$record[TABLE_ID]])}}' >SMS Invite</a></small></div>
                            @else
                            <div><small><i class='fa fa-envelope'></i> 
                            <a class='btn btn-link' href='{{getAddr('center_sms_event_result', [$record[TABLE_ID]])}}' >SMS Result</a></small></div>
                            @endif
                            
                            <div><small><i class='fa fa-trash'></i> 
                            	<a onclick='return confirmAction()' href='{{getAddr('center_delete_event', [$record[TABLE_ID]])}}' class='btn btn-link text-danger'>Delete</a></small></div>
                            </div>
                            "></i>
					</td>
				</tr>
			@endforeach
		</table>
	</div>
	<div class="tile-footer">
		@include('common.paginate')
	</div>
</div>

<!-- Data table plugin-->
<script type="text/javascript" src="{{assets('lib/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{assets('lib/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
$('#data-table').DataTable({
    "iDisplayLength": 50
});
$(function () {
//   $('[data-toggle="popover"]').popover();
  var popOverSettings = {
//	 	    placement: 'bottom',
//	 	    container: 'body',
//	 	    html: true,
		    selector: '[data-toggle="popover"]', //Sepcify the selector here
//	 	    content: function () {
//	 	        return $('#popover-content').html();
//	 	    }
	}
	
	$('#data-table').popover(popOverSettings);
});
function confirmAction() {
	return confirm('{{$confirmMsg}}');
}
</script>

@stop
