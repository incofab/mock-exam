<?php
$title = "Preview | " . SITE_TITLE;
// $donFlashMessages = true;
// $errors = isset($errors) ? $errors : $sessionModel->getFlash('error');
// $post = isset($post) ? $post : [];
// $valErrors = $sessionModel->getFlash('val_errors', []);
// if($valErrors) $errors = null;
// $selectedSessionIDs = isset($selectedSessionIDs) ? $selectedSessionIDs : [];

$dur = \App\Core\Settings::splitTime($event[DURATION]);
$eventSubjects = $event['event_subjects'];
?>

@extends('centers.layout')

@section('dashboard_content')

<div class="app-title">
	<div>
		<h1>
			<i class="fa fa-dashboard"></i> Events
		</h1>
		<p>Preview Event</p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> <a href="{{getAddr('center_dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{getAddr('center_view_all_events')}}">Events</a></li>
		<li class="breadcrumb-item">Preview Event</li>
	</ul>
</div>
<div>
	<div class="tile">
		<div class="tile-header clearfix">
        	<a href="{{getAddr('center_edit_event', [$event[TABLE_ID]])}}" class="btn btn-primary btn-sm pull-right">
        		<i class="fa fa-pencil"></i> Edit
        	</a>
        </div>
		<h3 class="tile-title">{{$event[TITLE]}}</h3>
		<div class="tile-body">
			<dl class="row">
				<dt class="col-md-3">Description</dt>
				<dd class="col-md-9">{{empty($event[DESCRIPTION]) ? '-' : $event[DESCRIPTION]}}</dd>
				<dt class="col-md-3 mt-3">Duration</dt>
				<dd class="col-md-9 mt-3"><?= "{$dur['hours']}hrs, {$dur['minutes']}mins, {$dur['seconds']}secs" ?></dd>
			</dl>
			<div class="mt-3 mb-1 h6">Subjects</div>
			<ul class="list-group">
				@foreach($eventSubjects as $subject)
				<?php 
				   $course = $subject['course']; 
				   $acadSession = $subject['session']; 
				?>
				<li class="list-group-item">{{$course[COURSE_TITLE].' - '.$acadSession[SESSION]}}</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>

@endsection