<?php
$title = "Register Event | " . SITE_TITLE;
$donFlashMessages = true;
$errors = isset($errors) ? $errors : $sessionModel->getFlash('error');
$post = isset($post) ? $post : [];
$valErrors = $sessionModel->getFlash('val_errors', []);
if($valErrors) $errors = null;
$selectedSessionIDs = isset($selectedSessionIDs) ? $selectedSessionIDs : [];
?>

@extends('centers.layout')

@section('dashboard_content')

<div class="app-title">
	<div>
		<h1>
			<i class="fa fa-dashboard"></i> Events
		</h1>
		<p>Create Event</p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> <a href="{{getAddr('center_dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{getAddr('center_view_all_events')}}">Events</a></li>
		<li class="breadcrumb-item">Create Event</li>
	</ul>
</div>
<div>
	<div class="tile">
		<h3 class="tile-title">{{ isset($edit) ? 'Edit' : 'Create' }} Event</h3>
    	@if($valErrors)
    	<div class="alert alert-danger">
    		@foreach($valErrors as $vError)
    			<p><i class="fa fa-star" style="color: #cc4141;"></i> {{implode('<br />', $vError)}}</p>
    		@endforeach
    	</div>
    	@endif
    	@if($errors)
    	<div class="alert alert-danger">
   			<div><i class="fa fa-star" style="color: #cc4141;"></i> {{$errors}}</div>
    	</div>
    	@endif
		<form action="" method="post">
    		<div class="tile-body">
    			
				<div class="form-group w-75" >
					<label class="control-label">Title</label>
					<input type="text" name="{{TITLE}}" value="<?= getValue($post, TITLE)  ?>" class="form-control"
						placeholder="Enter title" />
				</div>
				<div class="form-group w-75" >
					<label class="control-label">Description</label>
					<textarea name="{{DESCRIPTION}}" class="form-control" 
						rows="3" placeholder="Enter description" ><?= getValue($post, DESCRIPTION)  ?></textarea>
				</div>
				<div class="form-group w-75" >
    				<label class="control-label"><b>Duration</b></label> 
    				<div class="row mx-0">
    <!-- 					<div class="col-md-10 col-lg-9"> -->
    						<div class="form-group col-3 px-1">
    							<label class="control-label">Hours</label> 
    							<input class="form-control" type="text" placeholder="Hours" name="hours" 
    								value="<?= getValue($post, 'hours', 3)  ?>" >
    						</div>
    						<div class="form-group col-1 text-center px-1">
    							<label class="control-label">&nbsp;</label> 
    							<div class="form-control">:</div>
    						</div>
    						<div class="form-group col-3 px-1">
    							<label class="control-label">Mins</label> 
    							<input class="form-control" type="text" placeholder="Minutes" name="minutes" 
    								value="<?= getValue($post, 'minutes', 0)  ?>" >
    						</div>
    						<div class="form-group col-1 text-center px-1">
    							<label class="control-label">&nbsp;</label> 
    							<div class="form-control">:</div>
    						</div>
    						<div class="form-group col-3 px-1">
    							<label class="control-label">Seconds</label> 
    							<input class="form-control" type="text" placeholder="Seconds" name="seconds" 
    								value="<?= getValue($post, 'seconds', 0)  ?>" >
    						</div>
    <!-- 					</div> -->
    				</div>
				</div>
				<div class="form-group w-75" >
					<label class="control-label">Subjects</label>
					<select name="{{COURSE_SESSION_ID}}[]" id="select-subjects" required="required" 
						class="form-control" multiple="multiple" >
						@foreach($subjects as $subject)
						<?php 
						  $sessions = \App\Models\Sessions::where(COURSE_ID, '=', $subject[TABLE_ID])
						      ->orderBy(TABLE_ID, 'DESC')->get();
						  $i = 0;
						?>
    						@foreach($sessions as $acadSession)
    						<?php $i++;  ?> 
    						<option value="{{$acadSession[TABLE_ID]}}" title="{{str_replace('_', ' ', $subject[COURSE_CODE])}}"
    						<?= in_array($acadSession[TABLE_ID], $selectedSessionIDs) ? ' selected="selected" ' : '' ?>
    							>{{$subject[COURSE_TITLE].' '.$acadSession[SESSION]}}</option>
    						@endforeach
						@endforeach
					</select>
				</div>
				
				@if(isset($edit))
				<div>
					<input type="hidden" name="{{TABLE_ID}}" value="$tableId" />
					<input type="hidden" name="prev_selected_session_ids" value="{{implode(',', $selectedSessionIDs)}}" />
				</div>
				@endif
    		</div>
    		<div class="tile-footer">
    			<button class="btn btn-primary" type="submit">
    				<i class="fa fa-fw fa-lg fa-check-circle"></i> {{isset($edit) ? 'Update' : 'Register'}}
    			</button>
    			&nbsp;&nbsp;&nbsp;
    			<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
    		</div>
		</form>
	</div>
</div>
<script type="text/javascript" src="{{assets('lib/select2.min.js')}}"></script>
<script type="text/javascript">
$('#select-subjects').select2();
</script>

@endsection