<?php
$title = "Exam Center - SMS Events Results | " . SITE_TITLE;

$dur = \App\Core\Settings::splitTime($event[DURATION]);
?>
@extends('centers.layout')

@section('dashboard_content')

<div class="app-title">
	<div>
		<h1>
			<i class="fa fa-dashboard"></i> Events
		</h1>
		<p>Send {{$event[TITLE]}} Results Via SMS</p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> <a href="{{getAddr('center_dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item">Send Results</li>
	</ul>
</div>
	
<div class="tile">
	@include('common.form_message')
    <div class="tile-body">
		<div class="alert alert-info">
			<p>Send SMS to all students that took part in this test ({{$event[TITLE]}}).</p>
			<strong>Note: This should be done only after all students have completed their test</strong>
		</div>
    	<dl class="row">
			<dt class="col-md-3">Event</dt>
			<dd class="col-md-9">{{$event[TITLE]}}</dd>
			<dt class="col-md-3">Description</dt>
			<dd class="col-md-9">{{empty($event[DESCRIPTION]) ? '-' : $event[DESCRIPTION]}}</dd>
			<dt class="col-md-3 mt-3">Duration</dt>
			<dd class="col-md-9 mt-3"><?= "{$dur['hours']}hrs, {$dur['minutes']}mins, {$dur['seconds']}secs" ?></dd>
		</dl>
    	<form action="" method="post">
    		<div class="tile-body">
				<div class="form-group w-75" >
					<label class="control-label">SMS Username</label>
					<input name="{{USERNAME}}" class="form-control" 
						placeholder="Enter SMS Username" value="<?= getValue($post, USERNAME)  ?>" />
				</div>
				<div class="form-group w-75" >
					<label class="control-label">SMS Password</label>
					<input type="password" name="{{PASSWORD}}" class="form-control" 
						placeholder="Enter SMS Password"  />
				</div>
    		</div>
    		<div class="tile-footer">
    			<button class="btn btn-primary" type="submit">
    				<i class="fa fa-fw fa-lg fa-check-circle"></i> Send Now
    			</button>
    			&nbsp;&nbsp;&nbsp;
    			<a class="btn btn-secondary" href="{{getAddr('center_view_all_events')}}" 
    				><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
    		</div>
		</form>
	</div>
</div>


@stop
