<?php
$title = "Subscription | " . SITE_TITLE;
$donFlashMessages = true;
$errors = isset($errors) ? $errors : $sessionModel->getFlash('error');
$post = isset($post) ? $post : [];
$valErrors = $sessionModel->getFlash('val_errors', []);
if($valErrors) $errors = null;
$subjects = [];

/** @var \App\Core\SubscriptionPlan $subscriptionPlan */
$subscriptionPlan->isPerEventSubscription(getAddr('center_dashboard'));
$subscriptionPlan->isExpired(getAddr('center_dashboard'));
$hintMessage = 'Enter subscription pin to subscribe to a plan';
?>

@extends('centers.layout')

@section('dashboard_content')

<div class="app-title">
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> <a href="{{getAddr('center_dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{getAddr('center_dashboard')}}">Subscription</a></li>
		<li class="breadcrumb-item">Activate</li>
	</ul>
</div>
<div>
	<div class="tile">
		@if($subscriptionPlan->isPerEventSubscription())
		<?php $hintMessage = 'Enter subscription pin to upgrade your plan'; ?>
		<div class="alert alert-info">
			You are on {{$subscriptionPlan->getPlan()}} Subscription Plan
		</div>
		@elseif($subscriptionPlan->isExpired())
		<div class="alert alert-danger">
			You do not have an active Subscription
		</div>
		@else
		<?php $hintMessage = 'Enter subscription pin to upgrade your plan'; ?>
		<div class="alert alert-success">
			You are on {{$subscriptionPlan->getPlan()}} Subscription Plan
		</div>
		@endif
    	@if($valErrors)
    	<div class="alert alert-danger">
    		@foreach($valErrors as $vError)
    			<p><i class="fa fa-star" style="color: #cc4141;"></i> {{implode('<br />', $vError)}}</p>
    		@endforeach
    	</div>
    	@endif
    	@if($errors)
    	<div class="alert alert-danger">
   			<div><i class="fa fa-star" style="color: #cc4141;"></i> {{$errors}}</div>
    	</div>
    	@endif
		<form action="" method="post">
    		<div class="tile-body">
				<div class="form-group w-75">
					<label class="control-label">Select Event</label>
					<select name="event" class="form-control">
						<option value="0">All Events</option>
						@foreach($events as $event)
						<option value="{{$event[TABLE_ID]}}" 
						  <?= markSelected($event[TABLE_ID], array_get($post, 'event')) ?>
							>{{$event[TITLE]}}</option>
						@endforeach
					</select>
					<div>
						<label for=""><i class="text-info">Optional if you are subscribing to time-based subscriptions</i></label>
					</div>
				</div>
				<div class="form-group w-75">
					<label class="control-label">Subscription Pin</label>
					<input type="text" name="pin" value="<?= array_get($post, 'pin')?>" 
						class="form-control" required="required" />
					<div>
						<label for=""><i class="text-info">{{$hintMessage}}</i></label>
					</div>
				</div>
				<?php /*
				<div class="form-group w-75">
					<label class="control-label">No of Activations</label>
					<input type="text" name="{{NUM_OF_ACTIVATIONS}}" 
						value="<?= array_get($post, NUM_OF_ACTIVATIONS)?>" 
						class="form-control" required="required" />
					<div>
						<label for=""><i class="text-info">Optional if you are subscribing to time-based subscriptions</i></label>
					</div>
				</div>
				*/ ?>
    		</div>
    		<div class="tile-footer">
    			<button class="btn btn-primary" type="submit">
    				<i class="fa fa-fw fa-lg fa-check-circle"></i> Subscribe Now
    			</button>
    			&nbsp;&nbsp;&nbsp;
    			<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
    		</div>
		</form>
	</div>
</div>

@endsection