<?php
$title = isset($title) ? $title : SITE_TITLE;
?>

@extends('vali_layout')

@section('body')
	
	@include('centers.header')
	
	@include('centers._sidebar')
	
	<main class="app-content">
	@yield('dashboard_content')
	</main>
	
@endsection


