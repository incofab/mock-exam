<?php

?>
<div class="panel-body" style="padding: 0px !important">
	<br />
	<table width="100%" class="table table-striped" 
		style="font-size: 14px;padding: 2px !important" id="dataTables-example">
		<thead>
			<tr class="text-center">
				<th colspan="2"><b>{{$userData[USERNAME]}}'s Details</b></th>
			</tr>
		</thead>
		<tbody >
			<tr>
				<td style="width: 160px"><label>Fullname</label></td>
				<td><?= "{$userData[FIRSTNAME]} {$userData[LASTNAME]}" ?></td>
			</tr>
			<tr>
				<td><label>Email</label></td>
				<td><?= $userData[EMAIL] ?></td>
			</tr>
			<tr>
				<td><label>Phone no</label></td>
				<td><?= $userData[PHONE_NO] ?></td>
			</tr>
			<tr>
				<td><label>Balance</label></td>
				<td><?= CURRENCY_SIGN . $userData->getBalance() ?></td>
			</tr>
			<tr>
				<td><label>Bank</label></td>
				<td><?= $userData[BANK_NAME] ?></td>
			</tr>
			<tr>
				<td><label>Account No</label></td>
				<td><?= $userData[ACCOUNT_NUMBER] ?></td>
			</tr>
		</tbody>
	</table>
</div>






