<?php

	$errors = isset($errors) ? $errors : [];

?>

        
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Change Password</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
            <?= showFormValidationErrors($errors) ?>
            
			<div class="panel-body">
               
                <div class="">
                    
                    <form action="" name="" method="POST">
                    
                       
	                  Old Password <input type="password" name="<?= PASSWORD ?>" class="textbox" required="required">
	                  <br><br>
                       
	                  New Password <input type="password" name="new_password" class="textbox" required="required">
	                  <br><br>
	                       
	                  Confirm Password <input type="password" name="<?= PASSWORD_CONFIRMATION ?>" class="textbox" required="required">
	                  <br><br>

					  <input type="hidden" name="<?= CSRF_TOKEN ?>" value="<?= \Session::getCsrfValue() ?>" />
   					  <input type="hidden" name="change_password" value="true" />
                       
                      <input type="submit" class="btn btn-success" name="submit" value="Change Password">
                  		
                    </form>
                               
                </div>
                            <!-- /.table-responsive -->
			</div>



