<?php
$donFlashMessages = true;
$valErrors = formatValidationErrors($sessionModel->getFlash('val_errors', []));
$Ierror = isset($Ierror) ? $Ierror : $sessionModel->getFlash('error');
$Isuccess = isset($Isuccess) ? $Isuccess : $sessionModel->getFlash('success');
?>
<script type="text/javascript">
var donFlashMessages = true;
</script>
@if($valErrors)
<div class="alert alert-danger rounded-0">
	@foreach($valErrors as $vError)
		<p class="m-0 py-1"><i class="fa fa-star red">*</i> {{$vError}}</p>
	@endforeach
</div>
@endif
<?php if($valErrors) return; ?>
@if($Ierror)
    <div class="alert alert-danger rounded-0">
    <p class="m-0 py-1"><i class="fa fa-star red"></i> {{$Ierror}}</p>
    </div>
@endif

@if($Isuccess)
    <div class="alert alert-success rounded-0">
    <p class="m-0 py-1"><i class="fa fa-check"></i> {{$Isuccess}}</p>
    </div>
@endif