<?php
use App\Core\Points;
?>
<style>
    #abbreviations{
        font-weight: 600;
        color: #001517;
        text-align: left;
        margin-bottom: 5px;
    }
    #abbreviations span{
        padding: 0 8px;
        display: inline-block;
    }
</style>
<div class="main page-id-lego-home__main section-id-lego__main    page-id-merchants-home__main trim-all text-center--handhelds">
	<div class="as-container as-container-stacked as-bg-transparent ">
		<div class="lego-element position-stretch background-cover" id="page-id-merchant-home__hero-background"></div>
		<div class="lego-element container  v-pad">
			<div class="lego-element row v-align child-margin-top-25--handhelds">
				<div class="lego-element col-sm-8">
					<h1 class="as-heading h2 color-black">
						<p>General soccer rules and scoring</p>
					</h1>
					<div class="lego-element row">
						<div class="col-sm-9 color-black">
							<p>
								On Gidigada, players of the same category are matched to
								ensure fairness and equity. Coaches are matched against coaches,
								goalkeepers against goalkeeper, defenders against defenders,
								utility players (midfielders and strikers) against utility
								players. Each performer or player has an odd. Results will be
								determined by the total points accumulated by each performer.
							</p>
						</div>
					</div>
				</div>
				<div class="lego-element col-sm-4">
					<!-- 
					<img src="https://images-na.ssl-images-amazon.com/images/G/01/amazonservices/payments/website/New_Iphone_Black_US_NewButton._V526372914_.png"
						alt="" title="" class="as-img as-img-theme-icon as-img-theme-image-default img-responsive as-responsive-img" />
					 -->
				</div>
			</div>
		</div>
	</div>
	<div class="as-container as-container-stacked as-bg-transparent"
		id="merchants-benefits">
		<div class="lego-element container v-pad">
			<h2 class="as-heading text-center">
				<p>Scoring table - This is how points are awarded for all events</p>
			</h2>
			<div class="as-heading font-default font-size-16 text-center">
				<div id="abbreviations">
					<span>HC - Head Coach</span> 
					<span>GK - GoalKeeper/Goalie</span> 
					<span>DF - Defender</span> 
					<span>MD - Midfielder</span> 
					<span>FW - Forward/Striker</span> 
				</div>
				<table border="1" cellspacing="5" cellpadding="0" width="100%">
					<tr>
						<td><p align="center"> <strong>Action</strong> </p></td>
						<td><p align="center"> <strong>Coach</strong> </p></td>
						<td><p align="center"> <strong>Goalie</strong> </p></td>
						<td><p align="center"> <strong>Defender</strong> </p></td>
						<td title="Midfielder/Forward"><p align="center"> <strong>MD/FD</strong> </p></td>
<!-- 						<td><p align="center"> <strong>Forward</strong> </p></td> -->
					</tr>
					@foreach($rules as $action => $positions)
					<tr>
						<td><p align="center">{{str_replace('_', ' ', ucfirst($action))}}</p></td>
						
						<td><p align="center"><?= $positions[Points::POSITION_COACH] ?></p></td>
						<td><p align="center"><?= $positions[Points::POSITION_GOALIE] ?></p></td>
						<td><p align="center"><?= $positions[Points::POSITION_DEENDER] ?></p></td>
						<td><p align="center"><?= $positions[Points::POSITION_MIDFIELDER] ?></p></td>
					</tr>
					@endforeach
				</table>
			</div>

			<h3>Scoring Notes</h3>
			<ul>
				<li>Utility players on Gidigada refer to all midfeilders and
					forwards. Defenders are not utility players.</li>
				<li>In the instance where a player gets 2 yellow cards, the player
					will NOT be given a red card. The player will get a total of -3
					PTs.</li>
				<li>Win stat is determined by the score after regulation time or
					extra time, if necessary. Penalty shootouts are not considered in
					determining a win.</li>
				<li>A Player qualifies for a win if he plays a part in the game, either
					from the start or subbed on during the game. 
					</li>
				<li>A bet is cancelled if player did not take part in the game or was an
					unused subtitute;
					</li>
				<li>A subtitute goal is awarded when a player scores after coming on from the bench,
					The equivalent point for scoring a goal and additional 
					points for substitute goals are awarded to the player and his manager
					</li>
				<!-- 
				<li>Point for playing time is awarded 1 point for every 10mins
					played. Injury time is not counted in a player's total minutes
					played.</li>
				<li>Players must play at least 60 minutes to qualify for a win. If a
					player gets subbed off, his team must be winning at the time of
					substitution in order to be eligible for the win. The score at the
					time of substitution is not factored in for a player who is subbed
					into a match. His team will need to score more goals than the
					opposing team for him to earn the win point</li>
				 -->
				<li>Stats from Penalty Kick shootouts will not be counted for any
					players.</li>
				<li>Own goals do not count as goals for, but do count as goals
					against for Goalkeepers and do factor into Clean Sheet
					calculations.</li>
			</ul>
			<h2>Cancelled, Postponed, and Rescheduled Matches</h2>
			<p>In the event that a match is canceled, postponed, or a match is
				rescheduled to a time outside of the original Scoring Period,
				players listed on that match will not be eligible to accrue points
				and all bets on it will be cancelled</p>
			<p>Gidigada may choose to leave bets on a postponed or cancelled
				event if a date has been fixed and the match will be played with 72
				hours.
			</p>
			<h2>Maximum and Minimum Stake</h2>
			<p>
				For every bet on Gidigada, the Minimum allowed stake is 
				{{CURRENCY_SIGN}}<?= number_format(round(\App\Core\Settings::getMinimumStake(), 2)) ?>
				while the Maximum is {{CURRENCY_SIGN}}<?= number_format(round(\App\Core\Settings::getMaximumStake(), 2)) ?>. 
				<br />
<!-- 				The official denomination for all transactions is in US dollars. -->
			</p>
		</div>
	</div>
</div>
<style>
    #page-id-merchant-home__hero-background {
    	background-image:
    		url('https://images-na.ssl-images-amazon.com/images/G/01/EPSMarketingJRubyWebsite/assets/merchantredesign/herobg._V279631466_.jpg');
    	background-position: 50% 50%;
    }
</style>