<?php 
$error = isset($error) ? $error : \Session::getFlash('error');
$post = isset($post) ? $post : [];
$valErrors = formatValidationErrors(\Session::getFlash('val_errors', []));
if($valErrors) $error = null;

?>
<style>
    /**
    #phone_no{
        float: left;
        width: 85%;
        border-radius: 0 0.25rem 0.25rem 0;
    }
    #country-code{
        padding: 6px 5px;
        font-weight: 400;
        line-height: 1.73;
        color: #464a4c;
        text-align: center;
        background-color: #eceeef;
        border: 1px solid rgba(0,0,0,.15);
        border-radius: .25rem 0 0 .25rem;
        white-space: nowrap;
        vertical-align: middle;
        height: 39px;
        float: left;
        width: 15%;
        border-right: none;
    }
    */
    #register{
        padding: 5px 10px;
        margin: 0;
        background-image: url("{{assets('img/images/welcome-bg.png')}}");
    }
    #register .col-form-label{
        text-align: right;
        font-weight: normal;
        margin-top: 10px;
    }
    #register label.error{
        background: red;
        padding: 3px 5px;
        color: #fff;
        border-radius: 5px;
        position: relative;
        margin: 0 10px 0 2px;
        margin-top: 10px;
    }
    #register label.error:after{
        position: absolute;
        content: '';
        display: block;
        border: 5px solid red;
        top: -4px; 
        left: 10px;
        transform: rotate(45deg);
    }
    input.error, select.error{
        border: 2px solid red;
        box-shadow: none;
        position: relative;
    }
    div.check-valid{
        display: none;
    }
    input.valid ~ div.check-valid{
        display: block;
        position: absolute;
        right: 20px;
        top: 56%;
        transform: translateY(-50%);
        color: green;        
    }
    input.valid, select.valid{
        border: 1px solid green;
        box-shadow: none;
    }
    .red{color: red;}
    @media (max-width:756px)  {
        #register .error{
            font-weight: normal;
            font-size: 13px;
        }
        #register .form-group {
            margin-bottom: 5px;
        }
        #register .form-control {
            font-size: 13px;
        }
    }
    @media (max-width:480px)  {
        #register .form-control {
            font-size: 12px;
            padding: 3px 5px;
        }
    }
    
</style>
<script type="text/javascript" src="{{ assets('lib/jquery_validation/jquery.validate.min.js') }}"></script>
<?php // <script type="text/javascript" src="{{ assets('lib/jquery_validation/jquery.additional-methods.min.js') }}"></script> ?>
<div id="register" class="footer-margin">
<div class="header-pad"></div>
<div class="row mx-0">
	<div class="col-md-8 offset-md-2 col-sm-10 offset-sm-1">
		<div class="slab p-3 mb-2 mt-4">
		<h3 class="text-center color-primary mb-4" >Registration</h3>
		<form method="post" action="" name="register">
		@if($valErrors)
		<div class="row">
    		<div class="alert col-sm-offset-2">
    			@foreach($valErrors as $vError)
    				<p><i class="fa fa-star" style="color: #cc4141;">*</i> {{$vError}}</p>
    			@endforeach
    		</div>
		</div>
		<br />
		@endif
		@if($error)
		<div class="row">
    		<div class="alert col-sm-offset-2">
    			<p><i class="fa fa-star" style="color: #cc4141;"></i> {{$error}}</p>
    		</div>
		</div>
		<br />
		@endif
          <div class="form-group row">
            <label for="firstname" class="col-sm-4 col-form-label">First name <span class="red">*</span></label>
            <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="First name" name="<?= FIRSTNAME ?>"
              	value="<?= getValue($post, FIRSTNAME) ?>" required="required" >
              	<div class="check-valid"><i class="fa fa-check-circle fa-2x"></i></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="lastname" class="col-sm-4 col-form-label">Last name <span class="red">*</span></label>
            <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Last name" name="<?= LASTNAME ?>"
              	value="<?= getValue($post, LASTNAME) ?>" required >
              	<div class="check-valid"><i class="fa fa-check-circle fa-2x"></i></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="phone" class="col-sm-4 col-form-label">Phone No. <span class="red">*</span></label>
            <div class="col-sm-7"  id="phone">
<!--               <div id="country-code" >+234</div> -->
              <input type="tel" class="form-control" placeholder="Eg. 07034585645" name="<?= PHONE_NO ?>"
              		value="<?= getValue($post, PHONE_NO) ?>" id="phone_no" >
              	<div class="check-valid"><i class="fa fa-check-circle fa-2x"></i></div>
            </div>
          </div>  
          <div class="form-group row">
            <label for="email" class="col-sm-4 col-form-label">Email <span class="red">*</span></label>
            <div class="col-sm-7">
              <input type="email" class="form-control" placeholder="Email" name="<?= EMAIL ?>"
              	value="<?= getValue($post, EMAIL) ?>" required="required" >
              	<div class="check-valid"><i class="fa fa-check-circle fa-2x"></i></div>
            </div>
          </div>  
          <div class="form-group row">
            <label for="username" class="col-sm-4 col-form-label">Enter Username <span class="red">*</span></label>
            <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Username" name="<?= USERNAME ?>"
              	value="<?= getValue($post, USERNAME) ?>" >
              	<div class="check-valid"><i class="fa fa-check-circle fa-2x"></i></div>
            </div>
          </div>  
          <div class="form-group row">
            <label for="username" class="col-sm-4 col-form-label">Enter Password <span class="red">*</span></label>
            <div class="col-sm-7">
              <input type="password" class="form-control" placeholder="password" name="<?= PASSWORD ?>" id="<?= PASSWORD ?>" >
              <div class="check-valid"><i class="fa fa-check-circle fa-2x"></i></div>
            </div>
          </div>  
          <div class="form-group row">
            <label for="username" class="col-sm-4 col-form-label">Confirm Password <span class="red">*</span></label>
            <div class="col-sm-7">
              <input type="password" class="form-control" placeholder="Confirm password" name="<?= PASSWORD_CONFIRMATION ?>"  >
              <div class="check-valid"><i class="fa fa-check-circle fa-2x"></i></div>
            </div>
          </div>  
          <div class="form-group row">
            <label for="" class="col-sm-4 col-form-label">Gender <span class="red">*</span></label>
            <div class="col-sm-7" >
              <select name="<?= GENDER ?>" id="gender" class="form-control" required="required">
        	  	<option value="">Gender</option>
        	  	<option <?= markSelected('Male', getValue($post, GENDER)) ?> >Male</option>
        	  	<option <?= markSelected('Female', getValue($post, GENDER)) ?> >Female</option>
        	  </select>
            </div>
          </div>  
          <div class="form-group">
          	<div class="alert alert-info" id="disclaimer">
          		<input type="checkbox" checked="checked" /> By submitting this form, you agree 
          		to our <a href="#">Terms and Conditions</a>, <a href="#">Privacy Policy</a> 
          		and acknowledge to have full ownership of every airtime you transact with us.
          	</div>
          </div>
          <div class="form-group row">
            <div class="col-sm-4 offset-sm-6">
				<input type="hidden" name="<?= CSRF_TOKEN ?>" value="<?= \Session::getCsrfValue() ?>" />
				<input type="submit"  name="register" class="btn btn-primary pointer pull-right" value="Register">
				<div class="clearfix"></div>
            </div>
          </div>  
        </form>
	</div>
	</div>
</div>	
</div>
<br /><br />
<script type="text/javascript">
$(document).ready(function() {

	disclaimerCheck();
	
	$('form[name="register"]').validate({
	    submitHandler: function(form) {
		    // Run any "after validation" operation here
	    	form.submit();
	    },
		rules: {
			phone: {
		      required: true,
		      digits: true,
		    },
		    username: {
		      required: true,
		      minlength: 4,
		    },
		    password: {
		      required: true,
		      minlength: 6,
		    },
		    password_confirmation: {
   		        required: true,
	        	equalTo: "#password"
	        }
		  },
	});
	/**
	$('select#country').on('change', function(event) {
		var val = $(this).val();
		$('#country-code').text(countriesAndCode[val]);
		$('input[name="phone_"]').val('')
	});
	$('input[name="phone_"]').on('keyup', function(event) {
		var countryCode = $('#country-code').text();
		var _phoneNo = $(this).val();

		if(countryCode == '+234') _phoneNo = _phoneNo.replace(/^0+/,'');
		
		var phoneNo = countryCode + _phoneNo;
// 		console.log('phoneNo = ' + phoneNo);
	    $('input[name="phone"]').val(phoneNo);
	});
	*/
	$('#disclaimer input').on('change', function() {
		disclaimerCheck();
	});
});

function disclaimerCheck() {
	
	if($('#disclaimer input').prop('checked')){
		$('#register input[name="register"]').attr('disabled', false);
	}else{
		$('#register input[name="register"]').attr('disabled', true);
	}
}

</script>
