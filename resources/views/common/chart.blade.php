<?php
// $chartCanvasWidth = isset($chartCanvasWidth) ? $chartCanvasWidth : 600;
// $chartCanvasHeight = isset($chartCanvasHeight) ? $chartCanvasHeight : 400;

$displayCharts = isset($displayCharts) ? $displayCharts : [];

?>

@if(DEV)
<script src="{{assets('lib/Chart.min.js')}}"></script>
@else
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
@endif

</canvas>
<script type="text/javascript">
var options = {
	scales: {
		xAxes: [{
				stacked: false,
				categoryPercentage: 0.9,
				barPercentage: 1.0
		}],
		yAxes: [{
			ticks: {
				beginAtZero: true,
				steps: 10,
				stepValue: 5,
				max: 100
			}
		}]
	},
    legend: {
        display: true,
        position: "top",
        labels: {
            fontColor: "#333",
            fontSize: 12,
        }
    },
    title: {
        display: true,
        position: "top",
//         text: "Doughnut Chardsdsdcddet",
        fontSize: 14,
        fontColor: "#111"
    },
    tooltips: {
//         cornerRadius: 0,
//         caretSize: 0,
        xPadding: 10,
        yPadding: 10,
//         backgroundColor: 'rgba(0, 150, 100, 0.9)',
        titleFontStyle: 'normal',
//         titleMarginBottom: 15
    }
} 

var chartParams = {};
var popCanvas = '';
@foreach($displayCharts as $chart)
	<?php 
	   $data = $chart['data'];  
	   $chartData = [
    	    'type' => $data['type'],
    	    'data' => [
    	        'labels' => $data['labels'],
    	        'datasets' => [ [
    	            'label' => $data['label'],
    	            'data' => $data['datasets'],
    	            'backgroundColor' => $data['bg-colors'],
    	            'borderWidth' => 0,
    	            'fill' => false,
    	        ] ],
    	    ],
    	];
	?>
	popCanvas = document.getElementById("{{$chart['id']}}").getContext("2d");
	chartParams = {{json_encode($chartData)}};
	chartParams.options = options;
	chartParams.options.title.text = "{{$data['label']}}";
	<?php if(isset($data['max'])): ?>
	chartParams.options.scales.yAxes[0].ticks.max = {{$data['max']}};
	<?php endif ?>
	new Chart(popCanvas, chartParams);
@endforeach

/*
var barChart = new Chart(popCanvas, {
	  type: 'bar',
	  data: {
	    labels: ["China", "India", "United States", "Indonesia", "Brazil", "Pakistan", "Nigeria", "Bangladesh", "Russia", "Japan"],
	    datasets: [{
	      label: 'Population',
	      data: [1379302771, 1281935911, 326625791, 260580739, 207353391, 204924861, 190632261, 157826578, 142257519, 126451398],
	      backgroundColor: [
	        'rgba(255, 99, 132, 0.6)',
	        'rgba(54, 162, 235, 0.6)',
	        'rgba(255, 206, 86, 0.6)',
	        'rgba(75, 192, 192, 0.6)',
	        'rgba(153, 102, 255, 0.6)',
	        'rgba(255, 159, 64, 0.6)',
	        'rgba(255, 99, 132, 0.6)',
	        'rgba(54, 162, 235, 0.6)',
	        'rgba(255, 206, 86, 0.6)',
	        'rgba(75, 192, 192, 0.6)',
	        'rgba(153, 102, 255, 0.6)'
	      ]
	    }]
	  }
	});
*/
</script>
	
	
