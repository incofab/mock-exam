<?php

$c = new \Carbon\Carbon();

$destinationRoute = isset($destinationRoute) ? $destinationRoute : '';

?>

	<div>
		<ol class="breadcrumb">
			<li><a href="{{getAddr('admin_dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active"><i class="fa fa-users"></i> Events</li>
		</ol>
		<h4 class="page-header">Select Matchday</h4>
	</div>
	<div>
		<table class="table table-responsive table-striped text-center" >
			<tr> 
				<th>Title</th>
				<th>Starts</th>
				<th>Ends</th>
				<th></th>
			</tr>
			@foreach($matchDays as $matchDay)
			<?php  
        		$cStart = $c->parse($matchDay[START_TIME])->toDayDateTimeString();
        		$cEnd = $c->parse($matchDay[END_TIME])->toDayDateTimeString();
			?>
				<tr>
					<td>
						<strong> {{$matchDay[TITLE]}} </strong>
					</td>
					<td>
						<strong> {{$cStart}} </strong>
					</td>
					<td>
						<strong> {{$cEnd}} </strong>
					</td>
					<td>
						<a href="{{getAddr($destinationRoute, $matchDay[TABLE_ID])}}" 
							class="btn btn-sm btn-success"> select</a>
					</td>
				</tr>
			@endforeach
		</table>
	</div>
	
