<?php
if(isset($donFlashMessages)) return;
/**
 * Used to include neutral messages into another page
 * NOTE: The function controlling this page resides in the page footer 
 */

$error = isset($error) ? $error : \Session::getFlash('error');
$errorMsg = isset($errorMsg) ? $errorMsg : \Session::getFlash('errorMsg');
$report = isset($report) ? $report : \Session::getFlash('report');
$success = isset($success) ? $success : \Session::getFlash('success');
$msg_ = '';
$fa_ = '';
$class_ = '';
// $report = 'Lorem ipsum dolor sit amet.';
    if(!empty($error))   {  $msg_ = $error;    $class_ = 'alert-danger';  $fa_ = 'warning'; }
elseif(!empty($errorMsg)){  $msg_ = $errorMsg; $class_ = 'alert-danger';  $fa_ = 'warning'; }
elseif(!empty($report))  { 	$msg_ = $report;   $class_ = 'alert-warning'; $fa_ = 'exclamation'; }
elseif(!empty($success)) { 	$msg_ = $success;  $class_ = 'alert-success'; $fa_ = 'success'; }
?>

<style>
#error-msgs{
	overflow: hidden;
	margin: auto;
	width: 80%;
	font-weight: 500;
	position: absolute;
	z-index: 5;
    left: 10%;
	@if(isset($isHome))
	margin-top: 80px;
	@endif
}
#error-msgs .alert{
    border-radius: 0;
    border-style: solid;
    border-width: 1px;
    border-left-width: 5px;
}
#error-msgs .alert.alert-success{
    border-color: #3ade60;
    border-left-color: #155724;
}
#error-msgs .alert.alert-danger{
    border-color: #f94052;
    border-left-color: #721c24;
}
#error-msgs .alert.alert-warning{
    border-color: #efb50f;
    border-left-color: #856404;
}
</style>
	
@if(!empty($msg_))	
	<div id="error-msgs" class="py-1">
		<div class="my-0 alert {{$class_}}">
			<i class="pull-left fa fa-{{$fa_}}" style="color: inherit; padding: 2px 5px;"></i>
			<div class="text-center" style="color: inherit;">{{$msg_}}</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<script type="text/javascript">
		setTimeout(function(){
			showErrorMsgs();
		}, 1000);
		function showErrorMsgs() {
			setTimeout(function() {
				$('#error-msgs').fadeOut('slow');
			}, 15000);
		}
// 		$('#error-msgs').on('transitionend webkitTransitionEnd MSTransitionEnd oTransitionEnd', function(e) {
// 			$('#error-msgs').hide();
// 		});
	</script>
@endif
	
	
