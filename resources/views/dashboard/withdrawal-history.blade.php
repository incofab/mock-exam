<?php
$title = "Dashboard - Withdrawal History | " . SITE_TITLE;
?>
@extends('dashboard.layout')

@section('dashboard_content')
<style>
#withdrawal-history{
     
}
</style>
<div id="withdrawal-history" class="p-2">
	<div id="" class="dashboard-slab pb-3">
    	<header class="text-center py-3">
    		<h3 class="p-0 m-0">All Withdrawals</h3>
    	</header>
    	@if($records->first())
    	<div class="dashboard-slab-body">
        	<table class="table table-responsive table-striped text-center">
        		<tr>
        			<th><small>Amount ({{CURRENCY_SIGN}})</small></th>
        			<th><small>B.B.T ({{CURRENCY_SIGN}})</small></th>
        			<th><small>B.A.T ({{CURRENCY_SIGN}})</small></th>
        			<th><small>Status</small></th>
        			<th><small>Date</small></th>
        			<th></th>
        		</tr>
        		@foreach($records as $record)
    			<tr>
    				<td>{{$record[AMOUNT]}}</td>
    				<td>{{$record[BALANCE_BEFORE_TRANSACTION]}}</td>
    				<td>{{$record[BALANCE_AFTER_TRANSACTION]}}</td>
    				<td>{{$record[STATUS]}}</td>
    				<td class="ellipsis">{{$record[CREATED_AT]->format('d-m-Y')}}</td>
    			</tr>
        		@endforeach
        	</table>
        	@include('common.paginate')
    	</div>
    	@else
    	<div class="alert alert-info text-center">
    		No Records found
    	</div>
    	@endif
	</div>
</div>
	

@stop
