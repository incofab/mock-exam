<?php
$title = "Dashboard - Deposit History | " . SITE_TITLE;

?>
@extends('dashboard.layout')

@section('dashboard_content')
<style>
#deposit-history{
     
}
</style>
<div id="deposit-history" class="p-1 p-2">
	<div id="" class="dashboard-slab pb-3">
    	<header class="text-center py-3">
    		<h3 class="p-0 m-0">All Deposits</h3>
    	</header>
    	@if($records->first())
    	<div class="dashboard-slab-body">
        	<table class="table table-responsive table-striped text-center">
        		<tr>
        			<th><small>Order</small></th>
        			<th><small>Amount({{CURRENCY_SIGN}})</small></th>
        			<th><small>B.B.T({{CURRENCY_SIGN}})</small></th>
        			<th><small>B.A.T({{CURRENCY_SIGN}})</small></th>
        			<th><small>Network</small></th>
        			<th><small>Value({{CURRENCY_SIGN}})</small></th>
        			<th><small>Charge({{CURRENCY_SIGN}})</small></th>
        			<th><small>Status</small></th>
        			<th><small>Date</small></th>
        			<th></th>
        		</tr>
        		<?php $i = 0;?>
        		@foreach($records as $record)
        		<?php $i++;?>
        			<tr>
        				<td>{{array_get($record, ORDER_ID, $i)}}</td>
        				<td>{{$record[USER_ENTERED_AMOUNT]}}</td>
        				<td>{{$record[BALANCE_BEFORE_TRANSACTION]}}</td>
        				<td>{{$record[BALANCE_AFTER_TRANSACTION]}}</td>
        				<td>{{$record[NETWORK]}}</td>
        				<td>{{$record[AMOUNT]}}</td>
        				<td>{{$record[TRANSACTION_CHARGE]}}</td>
        				<td>{{$record[TRANSACTION_STATUS]}}</td>
        				<td><div class="ellipsis">{{$record[CREATED_AT]->format('d-m-Y')}}</div></td>
        			</tr>
        		@endforeach
        	</table>
        	@include('common.paginate')
    	</div>
    	@else
    	<div class="alert alert-info text-center">
    		No Records found
    	</div>
    	@endif
	</div>
</div>
	

@stop
