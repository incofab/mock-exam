<?php
$title = "Dashboard - Airtime Deposit | " . SITE_TITLE;

$post = isset($post) ? $post : null;

$activeFormID = '';

if(array_get($post, NETWORK) == NETWORK_MTN_TRANSFER){
    $activeFormID = 'airtime-transfer';
}
else 
{
    $activeFormID = 'pin-deposit';
}

if(empty($post)) $activeFormID = '';
?>
@extends('dashboard.layout')

@section('dashboard_content')
<div id="airtime-deposit" class="px-3 py-5">
	<div class="row">
		<div class="col-md-10 offset-md-1">
			<div id="" class="dashboard-slab pb-3">
    			<header class="text-center py-3 mb-0">
    				<h3 class="p-0 m-0">Airtime Deposit</h3>
    			</header>
    			<div class="form-group px-5 pt-2 pb-3" style="background-color: #efefef;">
    				<label for="select-mode">Mode</label>
					<select name="mode" id="select-mode" class="form-control" required="required" >
						<option value="">Select Mode</option>
						<option value="pin-deposit" <?= markSelected($activeFormID, 'pin-deposit')?>>Deposit with Airtime Pin</option>
						<option value="airtime-transfer" <?= markSelected($activeFormID, 'airtime-transfer')?>>Deposit via Airtime Transfer</option>
					</select>
				</div>
			@if($valErrors)
    		<div class="alert alert-danger" > 
    			@foreach($valErrors as $vError)
    				<p><i class="fa fa-star" style="color: #cc4141;"></i> {{implode('<br />', $vError)}}</p>
    			@endforeach
    		</div>
    		@endif
			<form action="" method="post" id="pin-deposit" class="">
				<div class="form-group px-5">
					<label for="">Pin</label>
					<input type="text" name="{{PIN}}" class="form-control" value="<?= array_get($post, PIN)?>" placeholder="Enter Pin" required="required" />
				</div>
				<div class="form-group px-5">
					<label for="">Amount</label>
					<select name="{{AMOUNT}}" id="amount" class="form-control" required="required" >
						<option value="">Select Amount</option>
						<option value="100" <?= markSelected(100, array_get($post, AMOUNT)) ?>>100</option>
						<option value="200" <?= markSelected(200, array_get($post, AMOUNT)) ?>>200</option>
						<option value="400" <?= markSelected(400, array_get($post, AMOUNT)) ?>>400</option>
						<option value="500" <?= markSelected(500, array_get($post, AMOUNT)) ?>>500</option>
						<option value="750" <?= markSelected(1000, array_get($post, AMOUNT)) ?>>1000</option>
						<option value="1500" <?= markSelected(1500, array_get($post, AMOUNT)) ?>>1500</option>
					</select>
				</div>
				<div class="form-group px-5">
					<label for="">Network</label>
					<select name="{{NETWORK}}" id="{{NETWORK}}" class="form-control" required="required">
						<option <?= markSelected(NETWORK_MTN, array_get($post, NETWORK)) ?>><?= NETWORK_MTN ?></option>
						<option <?= markSelected(NETWORK_AIRTEL, array_get($post, NETWORK)) ?>><?= NETWORK_AIRTEL ?></option>
						<option <?= markSelected(NETWORK_9_MOBILE, array_get($post, NETWORK)) ?>><?= NETWORK_9_MOBILE ?></option>
<?php /*				<option <?= markSelected(NETWORK_GLO, array_get($post, NETWORK)) ?>><?= NETWORK_GLO ?></option> */ ?>
					</select>
				</div>
				<div class="form-group px-5 mt-3">
					<button type="submit" class="form-control text-center py-2 btn submit-btn pointer d-block">Deposit Now</button>
				</div>
				<br />
			</form>
			<form action="" method="post" id="airtime-transfer" class="">
				<div id="transfer-number" class="text-center mx-5 mb-3">
            		Transfer airtime to any of these numbers <b>07037518143, 07036027026, <?= $shareAndSellNo ?></b>
            	</div>
				<div class="form-group px-5">
					<label for="">Amount</label>
					<input type="text" name="{{AMOUNT}}" class="form-control" value="<?= array_get($post, AMOUNT)?>" placeholder="Enter Amount" required="required" />
				</div>
				<div class="form-group px-5">
					<label for=""><small>Phone no with which you will make the transfer</small></label>
					<input type="text" name="{{PHONE_NO}}" class="form-control" value="<?= array_get($post, PHONE_NO)?>" placeholder="Eg. 08067653563" required="required" />
				</div>
				<div class="form-group px-5">
					<label for="">Network</label>
					<select name="{{NETWORK}}" id="{{NETWORK}}" class="form-control" required="required">
						<option value="<?= NETWORK_MTN_TRANSFER ?>">MTN SHARE AND SELL</option>
					</select>
				</div>
				<div class="form-group px-5 mt-3">
					<button type="submit" class="form-control text-center py-2 btn submit-btn pointer d-block">Deposit Now</button>
				</div>
				<br />				
			</form>
			</div>
		</div>
	</div>
<script type="text/javascript">
$(function() {
	var id = '{{$activeFormID}}';
	displayActiveForm(id);  
	$('#airtime-deposit #select-mode').on('change', function() {
		var val = $(this).val();
		displayActiveForm(val); 
	});
	function displayActiveForm(id) {
		$('#airtime-deposit form').hide(); 
		if(!id) return;
		$('#airtime-deposit form#' + id).show();
	}
});
</script>
<style>
    #airtime-deposit{
/*         background-color: #33333399; */
    }
        
    #airtime-deposit #transfer-number{
        background-color: #fda2c1;
        padding: 10px;
        border: 1px solid #e20553;
        color: #6c0227;
    }
    
    @media (max-width:480px)  {
		
	}	
	
</style>
</div>	

@endsection