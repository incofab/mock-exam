<?php
$title = 'Dashboard - Change Password | ' . SITE_TITLE;

$error = isset($error) ? $error : \Session::getFlash('error');
$post = isset($post) ? $post : [];
$valErrors = formatValidationErrors(\Session::getFlash('val_errors', []));
if($valErrors) $error = null;

?>

@extends('dashboard.layout')

@section('dashboard_content')

<style>
    #changepassword{
        
    }
</style>
<div id="changepassword"  class="px-3 py-5">
	<div class="row mx-0">
		<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2">
			<div class="dashboard-slab pb-3">
			<header class="text-center py-3">
				<h3 class="">Change Password</h3>
			</header>
			<form method="POST" action="" name="changepassword" class="px-5">
				<br />
				<div class="form-group" >
					<label for="">Old Password</label>
					<input type="password" name="<?= PASSWORD ?>" 
						placeholder="Old password" required class="form-control" >
				</div>
				<div class="form-group" >
					<label for="">New Password</label>
					<input type="Password" name="new_password" placeholder="New password" 
						required="required" class="form-control" />
				</div>
				<div class="form-group" >
					<label for="">Confirm Password</label>
					<input type="Password" name="<?= PASSWORD_CONFIRMATION ?>" placeholder="Confirm password" 
						required="required" class="form-control" />
				</div>
				<div class="form-group" >
    				<input type="hidden" name="login" value="true" />
    				<input type="hidden" name="<?= CSRF_TOKEN ?>" value="<?= \Session::getCsrfValue() ?>" />
    				<button type="submit"  name="changepassword" class="btn submit-btn pointer d-block w-100" >Change Password</button>
    				<div class="clearfix"></div>
				</div>
			</form>
			</div>
		</div>
	</div>
	<br /><br />
</div>

@endsection