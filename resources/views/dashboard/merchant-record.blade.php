<?php
$title = "Dashboard - Merchant Account | " . SITE_TITLE;
?>

@extends('dashboard.layout')

@section('dashboard_content')

<style>
    
</style>
<div id="merchant-record"  class="px-2 py-5" >
<div class="row mx-0" >
	<div class="col-sm-12 offset-sm-0 col-md-10 offset-md-1">
    	<div id="" class="dashboard-slab pb-3" >
    		<header class="text-center py-3">
    			<h4 class="p-0 m-0">Developer Account</h4>
    		</header>
    		<table class="table table-striped">
    			<tr>
    				<td><b>Public Key:</b></td>
    				<td>{{$merchantData[PUBLIC_KEY]}}</td>
    			</tr>
    			<tr>
    				<td><b>Private Key:</b></td>
    				<td>{{$merchantData[PRIVATE_KEY]}}</td>
    			</tr>
    			<tr>
    				<td><b>Domain:</b></td>
    				<td>{{$merchantData[DOMAIN_URL]}}</td>
    			</tr>
    			<tr>
    				<td><b>Callback:</b></td>
    				<td>{{$merchantData[CALLBACK_URL]}}</td>
    			</tr>
    		</table>
    		<a href="{{getAddr('user_edit_merchant_account')}}" class="btn submit-btn pointer pull-right px-2"><i class="fa fa-pencil"></i> Edit</a>
    		<div class="clearfix"></div> 
    	</div>
	</div>
</div>
</div>

@stop