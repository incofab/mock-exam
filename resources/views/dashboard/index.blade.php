@extends('dashboard.layout')

@section('dashboard_content')

<div class="d-sm-none text-center" style="font-weight: 500;">
	<div class="slab py-2 mx-4 my-3"><a href="{{getAddr('user_airtime_deposit')}}">Deposit</a></div>
	<div class="slab py-2 mx-4 my-3"><a href="{{getAddr('user_withdraw')}}">Withdraw</a></div>
	<div class="slab py-2 mx-4 my-3"><a href="#">Buy Airtime</a></div>
	<div class="slab py-2 mx-4 my-3"><a href="{{getAddr('user_deposit_history')}}">Deposit History</a></div>
	<div class="slab py-2 mx-4 my-3"><a href="{{getAddr('user_withdrawal_history')}}">Withdrawal History</a></div>
</div>

@endsection