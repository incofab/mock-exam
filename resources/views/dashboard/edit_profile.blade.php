<?php
$title = "Dashboard - Edit Profile | " . SITE_TITLE;
$errors = isset($errors) ? $errors : [];
$post = isset($post) ? $post : [];
$banks = \App\Core\Settings::BANKS;
?>

@extends('dashboard.layout')

@section('dashboard_content')

<style>
#edit_profile{
    
}
</style>
<div id="edit_profile"  class="px-2 py-5" >
<div class="row mx-0" >
	<div class="col-sm-12 offset-sm-0 col-md-10 offset-md-1">
	<div id="" class="dashboard-slab pb-3" >
		<header class="text-center py-3">
			<h3 class="p-0 m-0">Update Profile</h3>
		</header>
    	<form action="" method="post" class="px-4">
    		@if($valErrors)
    		<div class="alert alert-danger" > 
    			@foreach($valErrors as $vError)
    				<p><i class="fa fa-star" style="color: #cc4141;"></i> {{implode('<br />', $vError)}}</p>
    			@endforeach
    		</div>
    		@endif
    		@if(!empty($alertMsg))
    		<div class="alert alert-success" > 
    			{{$alertMsg}}
    		</div>
    		@endif
			<div class="form-group">
				<label for="">Firstname</label>
				<input type="text" name="<?= FIRSTNAME ?>" value="<?= getValue($post, FIRSTNAME) ?>" 
					class="form-control" placeholder="First name" >
			</div>
			<div class="form-group">
				<label for="">Lastname</label>
				<input type="text" name="<?= LASTNAME ?>" value="<?= getValue($post, LASTNAME) ?>" 
					class="form-control" placeholder="Last name" >
			</div>
			<div class="form-group" >
				<label >Email</label>
				<input type="email" class="form-control" name="<?= EMAIL ?>" placeholder="Email" value="<?= getValue($post, EMAIL) ?>" /> 
			</div>
			 <div class="form-group">
				<label for="">Gender</label>
				<select name="<?= GENDER ?>" id="gender" class="form-control" required="required">
            	  <option value="">Gender</option>
            	  <option <?= markSelected('Male', array_get($post, GENDER))?>>Male</option>
            	  <option <?= markSelected('Female', array_get($post, GENDER))?>>Female</option>
            	</select>
			</div>
			<div class="form-group" >
				<label >Bank</label>
				<select name="<?= BANK_NAME ?>" id="" class="form-control">
					<option value="">select bank</option>
					@foreach($banks as $bank)
					<option <?= markSelected(array_get($post, BANK_NAME), $bank)?>>{{$bank}}</option>
					@endforeach
				</select> 
			</div>
			<div class="form-group" >
				<label >Account Number</label>
				<input type="text" class="form-control" name="<?= ACCOUNT_NUMBER ?>" placeholder="Your bank account number" value="<?= getValue($post, ACCOUNT_NUMBER) ?>" /> 
			</div>
			<br />
			<div class="form-group" >
				<button type="submit" class="btn submit-btn pointer pull-right" name="update" >Update</button>
				<input type="hidden" name="edit_profile" value="true" />
				<div class="clearfix"></div>
			</div>
    	</form>
    	</div>
	</div>
</div>
</div>

@stop