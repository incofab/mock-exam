<?php 
?>
<header class="py-2">
    <div class="px-4 d-none d-sm-block">
    	<div id="company_name" class="pull-left"><i class="fa fa-adjust"></i> {{SITE_TITLE}}</div>
    	<div class="pull-right pr-1">
        	Bal: <b>{{NAIRA_SIGN}} {{number_format($data[BALANCE])}}</b>
    	</div>
    	<div class="clearfix"></div>
    </div>
    <div class="d-sm-none">
    	<div class="row m-0">
    		<div class="col-1 p-0 pl-2">
    			<i class="fa fa-bars pointer"  onclick="showSideMenu()"></i>
    		</div>
    		<div class="col-6 text-center p-0 pl-2">
    			<i class="fa fa-user"></i> {{$data[USERNAME]}}
    		</div>
    		<div class="col-5 p-0">
            	<div class="text-right pr-1 ellipsis">
                	Bal: <b>{{NAIRA_SIGN}} {{number_format($data[BALANCE])}}</b>
            	</div>
    		</div>
    	</div>
    </div>
</header>
