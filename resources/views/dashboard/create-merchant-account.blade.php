<?php
$title = "Dashboard - Merchant Account | " . SITE_TITLE;
$errors = isset($errors) ? $errors : [];
$post = isset($post) ? $post : [];
$banks = \App\Core\Settings::BANKS;
?>

@extends('dashboard.layout')

@section('dashboard_content')

<style>
#create-merchant-account{
    
}
</style>
<div id="create-merchant-account"  class="px-2 py-5" >
<div class="row mx-0" >
	<div class="col-sm-12 offset-sm-0 col-md-10 offset-md-1">
	<div id="" class="dashboard-slab pb-3" >
		<header class="text-center py-3">
			<h4 class="p-0 m-0">Create Developer Account</h4>
		</header>
		@if(!isset($edit))
		<div class="alert alert-info mx-2">
			<p>To set up a developer account</p>
			<p>Supply a callback URL which will be called as soon as your deposit is verified</p>
			<p>A public and Private key will be generated in the process, Keep it safe and confidential</p>
		</div>
		@endif
    	<form action="" method="post" class="px-4">
    		@if($valErrors)
    		<div class="alert alert-danger" > 
    			@foreach($valErrors as $vError)
    				<p><i class="fa fa-star" style="color: #cc4141;"></i> {{implode('<br />', $vError)}}</p>
    			@endforeach
    		</div>
    		@endif
    		<div class="form-group form-check" >
				<input type="checkbox" name="" value="" id="using-plugin" class="form-check-input font-italic">
    			<label for="using-plugin" class="form-check-label">Check this if you are using Cheetahpay WordPress Plugin</label>
				<div class="clearfix"></div>
    		</div>
			<div class="form-group">
				<label for="">Domain URL</label>
				<input type="text" name="<?= DOMAIN_URL ?>" value="<?= getValue($post, DOMAIN_URL) ?>" 
					class="form-control" placeholder="Your web address" >
			</div>
			<div class="form-group">
				<label for="">Callback URL <span class="alert-danger">*</span></label>
				<input type="text" name="<?= CALLBACK_URL ?>" value="<?= getValue($post, CALLBACK_URL) ?>" 
					class="form-control" placeholder="Callback web address" >
			</div>
			<br />
			<div class="form-group" >
				<button type="submit" class="btn submit-btn pointer pull-right">{{isset($edit) ? 'Update' : 'Save'}}</button>
				<div class="clearfix"></div>
			</div>
    	</form>
    	</div>
	</div>
</div>
</div>
<script type="text/javascript">
$(function() {
	
	var $domainUrl = $('#create-merchant-account input[name="{{DOMAIN_URL}}"]');
	
	var $callbackUrl = $('#create-merchant-account input[name="{{CALLBACK_URL}}"]');

	var wp_plugin = '?wc-api=wc_cheetahpay';
	
	$domainUrl.on('keyup', function(e) {
		if(!$('#using-plugin').prop('checked')) return;
		setCallbackValue();
	});
	
	$('#using-plugin').on('change', function(e) {
		if(!$(this).prop('checked')) return;
		$callbackUrl.attr('readonly', 'readonly');
		setCallbackValue();
	});
	
    function setCallbackValue() {
    	$callbackUrl.val($domainUrl.val() + wp_plugin);
    }
});

</script>

@stop




