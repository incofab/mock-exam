<div class="menu-item">
	<div class="header">Main</div>
	<ul>
		<li class="items"><a href="{{getAddr('user_dashboard')}}">
			<i class="fa fa-dashboard"></i> Dashboard
		</a></li>
		<li class="items"><a href="{{getAddr('user_airtime_deposit')}}">
			<i class="fa fa-money"></i> Make Deposit
		</a></li>
		<li class="items"><a href="{{getAddr('user_withdraw')}}">
			<i class="fa fa-money"></i> Withdraw
		</a></li>
	</ul>
</div>
<div class="menu-item">
	<div class="header">History</div>
	<ul>
		<li class="items"><a href="{{getAddr('user_deposit_history')}}">
			<i class="fa fa-history"></i> Deposit History 
		</a></li>
		<li class="items"><a href="{{getAddr('user_withdrawal_history')}}">
			<i class="fa fa-history"></i> Withdrawals
		</a></li>
		<li class="items">
			<a href="#">
				<i class="fa fa-database"></i> All Transactions
			</a>
		</li>
	</ul>
</div>
<div class="menu-item">
	<div class="header">Account</div>
	<ul>
		<li class="items">
			<a href="{{getAddr('user_edit_profile')}}">
				<i class="fa fa-user"></i> Edit Profile
			</a>
		</li>
		<li class="items">
			<a href="{{getAddr('user_merchant_account_profile')}}" >
				<i class="fa fa-code"></i> Developers
			</a>
		</li>
		<li class="items">
			<a href="{{getAddr('user_changepassword')}}">
				<i class="fa fa-key"></i> Change Password
			</a>
		</li>
		<li class="items">
			<a href="{{getAddr('user_logout')}}">
				<i class="fa fa-sign-out"></i> Logout
			</a>
		</li>
	</ul>
</div>