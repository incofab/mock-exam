<?php
$title = isset($title) ? $title : " Dashboard - " . SITE_TITLE;
?>
<!doctype html>
<html lang="en">
<head>
@if(!DEV) @include('common.google-analytics') @endif
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="{{assets('favicon.ico')}}">
<title>{{$title}}</title>
<link href="{{assets('lib/bootstrap4/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{ assets('lib/font-awesome-4.6.3/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{assets('css/style.css')}}" rel="stylesheet">
<link href="{{assets('css/inc/responsive.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{ assets('lib/jquery.min.js')}}"></script>
<link href="{{assets('css/dashboard.css')}}" rel="stylesheet">
@yield('meta')
</head>
<style>
/* .alert, .alert *{font-weight: 400;} */
p{font-weight: normal;}
</style>
<body>
    <div>
		@include('dashboard._drop-menu')
        <div class="row mx-0">
        	<div class="col-sm-4 col-md-3 px-0 d-none d-sm-block" id="sidebar-box"> 
      			@include('dashboard.sidebar')
        	</div>
        	<div class="col-sm-4 col-md-3 px-0 d-none d-sm-block dummy"></div>
        	<div class="col-sm-8 col-md-9 px-0">
		        @include('dashboard.header')
			    @include('common.message')
            	<div id="main">
          			@yield('dashboard_content')
            	</div>
        	</div>
        </div>
    </div>
    <!-- Bootstrap Core JavaScript -->
	<script type="text/javascript" src="{{assets('lib/bootstrap4/js/bootstrap.min.js')}}"></script>
<script type="text/javascript">
var height = $(window).height();
$(function() {
	setSidebarHeight();		
});
$(window).on('resize', function(e) {
	height = $(window).height();
	setSidebarHeight();		
});
function setSidebarHeight() {
	$('#sidebar').css('height', height);		
}
</script>
</body>
</html>
