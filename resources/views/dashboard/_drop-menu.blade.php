<?php

?>

<div id="drop-menu" class="" style="overflow: auto;">
	<div id="side-menu-header" class="py-1">
		<div class="pull-left pl-2">Menu</div>
		<div class="pull-right pr-2 pointer" onclick="closeSideMenu()" style="color: red;">
			<i class="fa fa-close"></i>
		</div>
		<div class="clearfix"></div>
	</div>	
	@include('dashboard._sidebar')
</div>

<script type="text/javascript">
	function showSideMenu(){
		$('#drop-menu').addClass('show');
	}
	function closeSideMenu(){
		$('#drop-menu').removeClass('show');
	}
	$('#drop-menu .menu-item .items').on('click', function() {
		closeSideMenu();
	});
</script>



