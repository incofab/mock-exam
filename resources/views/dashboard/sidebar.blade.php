<?php

?>
<div id="sidebar" >
    <div id="side-menu">
    	<div id="user-details" class="py-2 px-2">
    		<div id="avatar" class="pull-left text-center">
    			<span style="letter-spacing: 2px;">{{substr($data[FIRSTNAME], 0, 1)}}{{substr($data[LASTNAME], 0, 1)}}</span>
    		</div>
    		<div id="user-name" class="pull-left pl-2">
    			<div><b>{{$data[FIRSTNAME]}} {{$data[LASTNAME]}}</b></div>
    			<div>{{$data[PHONE_NO]}}</div>
    		</div>
    		<div class="clearfix"></div>
    	</div>
    	@include('dashboard._sidebar')
    </div>
</div>



