<?php
$title = 'Dashboard - Withdraw | ' . SITE_TITLE;
$post = isset($post) ? $post : null;
$banks = \App\Core\Settings::BANKS;
?>
@extends('dashboard.layout')

@section('dashboard_content')
<div id="withdrawal" class="px-3 py-5">
	<div class="row">
		<div class="col-md-10 offset-md-1">
			<div id="" class="dashboard-slab pb-3">
    			<header class="text-center py-3">
    				<h3 class="p-0 m-0">Withdrawal</h3>
    			</header>
    			<form action="" method="post" id="" class="">
        			@if($valErrors)
            		<div class="alert alert-danger" > 
            			@foreach($valErrors as $vError)
            				<p><i class="fa fa-star" style="color: #cc4141;"></i> {{implode('<br />', $vError)}}</p>
            			@endforeach
            		</div>
            		@endif
            		<div class="alert alert-info px-5">
        				<b>Note: </b> <span>Minimum withdrawal is {{CURRENCY_SIGN . Config::MINIMUM_WITHDRAWAL}}</span>
        			</div>
    				<div class="form-group px-5">
    					<label for="">Amount</label>
    					<input type="text" name="{{AMOUNT}}" class="form-control" value="<?= array_get($post, AMOUNT)?>" placeholder="Enter Amount" required="required" />
    				</div>
    				<div class="form-group px-5">
    					<label for="">Recipient's Account Name</label>
    					<input type="text" name="{{ACCOUNT_NAME}}" class="form-control" value="<?= array_get($post, ACCOUNT_NAME, $accountName)?>" placeholder="Enter Account Name" required="required" />
    				</div>
    				<div class="form-group px-5">
    					<label for="">Recipient's Account Number</label>
    					<input type="text" name="{{ACCOUNT_NUMBER}}" class="form-control" value="<?= array_get($post, ACCOUNT_NUMBER, $accountNo)?>" placeholder="Enter Account No" required="required" />
    				</div>
    				<div class="form-group px-5">
    					<label for="">Bank</label>
    					<select name="{{BANK_NAME}}" id="{{BANK_NAME}}" class="form-control" required="required">
    						<option value="">select bank</option>
    						@foreach($banks as $bank)
    						<option value="{{$bank}}" <?= markSelected($bank, array_get($post, BANK_NAME, $bankName)) ?>>{{$bank}}</option>
    						@endforeach
    					</select>
    				</div>
    				<div class="form-group px-5">
    					<label for="">Password</label>
    					<input type="password" name="{{PASSWORD}}" class="form-control" value="<?= array_get($post, PASSWORD, $accountNo)?>" placeholder="Enter Password" required="required" />
    				</div>
    				<div class="form-group px-5 mt-3">
    					<button type="submit" class="form-control text-center py-2 btn submit-btn pointer d-block">Withdraw</button>
    				</div>
    				<br />
    			</form>
			</div>
		</div>
	</div>
<script type="text/javascript">

</script>
<style>
    #withdrawal{
/*         background-color: #33333399; */
    }

    @media (max-width:480px)  {
		
	}	
	
</style>
</div>	

@endsection