<?php

$title = isset($title) ? $title : SITE_TITLE;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{assets('favicon.ico')}}" >
    <title>{{$title}}</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<!--     <link href="{{assets('lib/bootstrap4/css/bootstrap.min.css')}}" rel="stylesheet"> -->
    <!-- Custom Fonts -->
    <link href="{{ assets('lib/font-awesome-4.6.3/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- jQuery -->
    <script type="text/javascript" src="{{ assets('lib/jquery.min.js')}}"></script>
<!--     <link href="{{assets('css/vali.css')}}" rel="stylesheet"> -->
    @yield('meta')
</head>
<body class="app">

	<div class="container">
	@yield('content')
	</div>
	
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
	<!-- 
	<script src="{{assets('lib/popper.min.js')}}"></script>
	<script type="text/javascript" src="{{assets('lib/bootstrap4/js/bootstrap.min.js')}}"></script>
	<script src="{{assets('js/vali.js')}}"></script>
	<script src="{{assets('lib/pace/pace.min.js')}}"></script>
	 -->
</body>
</html>
