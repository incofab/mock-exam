<?php

?>
<div id="sidebar" >
    <div id="side-menu">
    	<div id="user-details" class="py-2 px-2">
    		<div id="avatar" class="pull-left text-center">
    			<span>{{substr('Firstname', 0, 1)}}</span><span>{{substr('Lastname', 0, 1)}}</span>
    		</div>
    		<div id="user-name" class="pull-left pl-2">
    			<div><b>Incofab Ikenna</b></div>
    			<div>07036098561</div>
    		</div>
    		<div class="clearfix"></div>
    	</div>
    	@include('dummy._sidebar')
    </div>
</div>



