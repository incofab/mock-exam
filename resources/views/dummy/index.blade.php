<?php //die(json_encode($allCourseYearQuestions));?>
@extends('dummy.layout')

@section('content')

<style>
.question-main{
    font-weight: 500;
}
#question-list .question-col:nth-child(odd){
    border-right: 3px solid #333;
}
</style>

<div class="tab-content mt-4" id="questions-content" >
	<div class="row position-relative" id="question-list">
	@foreach($allCourseYearQuestions as $question)
	<div class="pt-3 col-md-6 question-col" >
		<div>
    		{{--
    		<div class="pl-5">
        		<div class="instruction my-1 font-weight-bold">
        			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil quis.
        		</div>
        		<div class="passage my-1">
        			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero blanditiis natus recusandae eius.
        		</div>
    		</div>
    		--}}
    		<div class="row mx-0">
    			<div class="col-auto">{{$question[QUESTION_NO]}}.</div>
    			<div class="col">
            		<div class="question-main">
                		<div class="question-text">
                			{{$question[QUESTION]}}
                		</div>
                		<?php 
                		$total = $question[OPTION_A].$question[OPTION_B].$question[OPTION_C].$question[OPTION_D].$question[OPTION_E];
                		$len = strlen($total);
                		$col = 'col';
                		if($len < (4+7)*4) $col = 'col';
                		else if($len < (7+7)*4) $col = 'col-6';
                		else $col = 'col-12';
                		
                		?>
                		<div class="options row">
                			<?php $optionText = $question[OPTION_A]; $optionLetter = 'A'; ?>
                			@include('dummy._option')
                			<?php $optionText = $question[OPTION_B]; $optionLetter = 'B'; ?>
                			@include('dummy._option')
                			<?php $optionText = $question[OPTION_C]; $optionLetter = 'C'; ?>
                			@include('dummy._option')
                			<?php $optionText = $question[OPTION_D]; $optionLetter = 'D'; ?>
                			@include('dummy._option')
                			@if(!empty($question[OPTION_E]))
                			<?php $optionText = $question[OPTION_E]; $optionLetter = 'E'; ?>
                			@include('dummy._option')
                			@endif
                		</div>
                	</div>
            	</div>
        	</div>
    	</div>
	</div>
	@endforeach
	</div>
    	
    	
    	

</div>

<script type="text/javascript">
$('nav .nav-tabs').on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
    
});
</script>

@endsection