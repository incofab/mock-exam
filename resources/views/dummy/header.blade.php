<?php 
?>
<style>
.app-header{
    color: #efefef;
    display: block;
    padding: 0;
    font-size: 20px;
}
.app-header .name{
    font-family: 'Niconne';
    padding: 0 15px;
    font-size: 26px;
    font-weight: 400;
}
.app-header *{
    line-height: 50px;
}
</style>
<!-- Navbar-->
<header class="app-header">
	<div class="row">
		<div class="col-6">
			<div class="name">Firstname Lastname</div>
		</div>
		<div class="col-6" >
			<div align="right" class="pr-2">
				<i class="fa fa-history"></i>
				<span>Time Remaining</span>
				<span id="rem-time">00:00:00</span>
			</div>
		</div>
	</div>
	
</header>