<?php
$title = "Admin - All Exam Centes | " . SITE_TITLE;
$page = 'manage';
$subCat = 'admin';

?>
@extends('admin.layout')

@section('dashboard_content')

<div class="app-title">
	<div>
		<h1>
			<i class="fa fa-dashboard"></i> Exam Centers
		</h1>
		<p>List of all Exam Centers</p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> <a href="{{getAddr('admin_dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item">Exam Centers</li>
	</ul>
</div>
	
<div class="tile">
    <div class="tile-header clearfix mb-3">
    	<a href="{{getAddr('admin_view_add_center')}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> New</a>
    </div>
    <div class="tile-body">
    	<table class="table table-hover table-bordered" id="data-table" >
    		<thead>
    			<tr>
    				<th>Code</th>
    				<th>Username</th>
    				<th>Name</th>
    				<th>Email</th>
    				<th>Phone</th>
    				<th><i class="fa fa-bars"></i></th>
    			</tr>
    		</thead>
			@foreach($allRecords as $record)
				<tr title="Address: {{$record[ADDRESS]}}" >
					<td>{{$record[CENTER_CODE]}}</td>
					<td>{{$record[USERNAME]}}</td>
					<td>{{$record[CENTER_NAME]}}</td>
					<td>{{$record[EMAIL]}}</td>
					<td>{{$record[PHONE_NO]}}</td>
					<td>
						<i class="fa fa-bars"
						   tabindex="0"
						   role="button" 
                           data-html="true" 
                           data-toggle="popover" 
                           title="Options" 
                           data-placement="bottom"
                           data-content="<div>
                            <div><small><i class='fa fa-pencil'></i> <a href='{{getAddr('admin_edit_center', [$record[TABLE_ID]])}}' class='btn btn-link'>Edit</a></small></div>
                            @if($record[STATUS] == STATUS_SUSPENDED)
                                <div><small><i class='fa fa-circle-o'></i> <a href='{{getAddr('admin_center_unsuspend', [$record[TABLE_ID]])}}' class='btn btn-link'>Unsuspend</a></small></div>
                            @else
                                <div><small><i class='fa fa-circle-o'></i> <a href='{{getAddr('admin_center_suspend', [$record[TABLE_ID]])}}' class='btn btn-link'>Suspend</a></small></div>
                            @endif
                            <div><small><i class='fa fa-trash'></i> <a href='{{getAddr('admin_delete_center', [$record[TABLE_ID]])}}' class='btn btn-link text-danger'>Delete</a></small></div>
                            "></i>
					</td>
				</tr>
			@endforeach
		</table>
	</div>
	<div class="tile-footer">
		@include('common.paginate')
	</div>
</div>

<!-- Data table plugin-->
<script type="text/javascript" src="{{assets('lib/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{assets('lib/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
$('#data-table').DataTable({
    "iDisplayLength": 50
});
$(function () {
//   $('[data-toggle="popover"]').popover();
  var popOverSettings = {
		    selector: '[data-toggle="popover"]', //Sepcify the selector here
//	 	    content: function () {
//	 	        return $('#popover-content').html();
//	 	    }
	}
	
	$('#data-table').popover(popOverSettings);
})
</script>

@stop
