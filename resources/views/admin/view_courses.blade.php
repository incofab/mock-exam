<?php
$donFlashMessages = true;
$title = "Admin - All Courses | " . SITE_TITLE;
$page = 'manage';
$subCat = 'admin';

?>
@extends('admin.layout')

@section('dashboard_content')

	<div >
		<ol class="breadcrumb">
			<li><a href="{{getAddr('admin_dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active ml-2"><i class="fa fa-users"></i> Install/Uninstall Courses</li>
		</ol>
		<h4 class="">Admin Available Courses</h4>
	</div>
	@include('common.form_message')
	<div class="clearfix mb-2"><a href="{{getAddr('ccd_register_course')}}" class="btn btn-primary pull-right" title="Register a new course">
		<i class="fa fa-plus"> Add</i></a>
	</div>
	<div>
		<table class="table table-striped">
			<tr>
				<th>Course Title</th>
				<th>Course Code</th>
				<th>Description</th>
				<th>State</th>
			</tr>
			@foreach($courses as $course)
				<tr>
					<td>{{$course[COURSE_TITLE]}}</td>
					<td>{{$course[COURSE_CODE]}}</td>
					<td>{{$course[DESCRIPTION]}}</td>
					<td>
						<a href="{{getAddr('admin_export_content', [$course[TABLE_ID], '?next='.getAddr('')])}}" 
							onclick="return confirm('Download {{$course[COURSE_CODE]}} now? \nThis is take a few minutes and should not be interrupted')"
							class="btn btn-sm btn-warning mr-2"> <i class="fa fa-download"></i> Download </a>
    					@if($courseInstaller->isCourseInstalled($course[TABLE_ID]))
    						<a href="{{getAddr('admin_uninstall_course', [$course[TABLE_ID], '?next='.getAddr('')])}}" 
    							class="btn btn-sm btn-danger"> <i class="fa fa-trash"></i> Uninstall </a>
    						<a href="{{getAddr('admin_install_courses', [$course[TABLE_ID], '?next='.getAddr('')])}}" 
    							class="btn btn-sm btn-primary"> <i class="fa fa-plus"></i> Upload More </a>
						@else
    						<a href="{{getAddr('admin_install_courses', [$course[TABLE_ID], '?next='.getAddr('')])}}" 
    							class="btn btn-sm btn-success"> <i class="fa fa-upload"></i> Install </a>
    					@endif
					</td>
				</tr>
			@endforeach
		</table>
	</div>
	

@stop
