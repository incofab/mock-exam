<style>
.app-sidebar {
    padding-top: 50px;
}
.app-sidebar__user{
    margin-bottom: 0;
    padding-top: 20px;
    padding-bottom: 20px;
    background-color: rgba(51, 51, 51, 0.4);
}
.app-sidebar__user_box{
    background-image: url("{{assets('img/images/material-bg.jpg')}}");
    background-size: cover;
}
.treeview-item {
    padding: 15px 5px 15px 20px;
}
</style>
<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
	<div class="app-sidebar__user_box">
    	<div class="app-sidebar__user">
    		<img class="app-sidebar__user-avatar" style="width: 48px; height: 48px; background-color: #afb7c4;"
    			src="{{assets('img/default.png')}}"
    			alt="User Image">
    		<div>
    			<p class="app-sidebar__user-name text-truncate">{{$data[USERNAME]}}</p>
    			<p class="app-sidebar__user-designation text-truncate">Administrative Staff</p>
    		</div>
    	</div>
	</div>
	<ul class="app-menu">
		<li><a class="app-menu__item active" href="{{getAddr('admin_dashboard')}}"><i
				class="app-menu__icon fa fa-dashboard"></i><span
				class="app-menu__label">Dashboard</span></a>
		</li>
		<li class="treeview"><a class="app-menu__item" href="#"
			data-toggle="treeview"><i class="app-menu__icon fa fa-edit"></i><span
				class="app-menu__label">Exam Centers</span><i
				class="treeview-indicator fa fa-angle-right"></i></a>
			<ul class="treeview-menu">
				<li><a class="treeview-item" href="{{getAddr('admin_view_all_centers')}}"><i
						class="icon fa fa-hall"></i> View Centers</a>
				</li>
				<li><a class="treeview-item" href="{{getAddr('admin_view_add_center')}}"><i
						class="icon fa fa-plus"></i> Add Center</a>
				</li>
			</ul>
		</li>
		<?php /*
		<li class="treeview"><a class="app-menu__item" href="#"
			data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i><span
				class="app-menu__label">Students</span><i
				class="treeview-indicator fa fa-angle-right"></i></a>
			<ul class="treeview-menu">
				<li><a class="treeview-item" href="bootstrap-components.html"><i
						class="icon fa fa-graduation-cap"></i> View Students</a>
				</li>
				<li><a class="treeview-item"
					href="https://fontawesome.com/v4.7.0/icons/" target="_blank"
					rel="noopener"><i class="icon fa fa-plus"></i> Add Student</a>
				</li>
			</ul>
		</li>
		*/?>
		<?php if($data[LEVEL] == \App\Models\Admin::ACCESS_LEVEL_FULL_ACCESS): ?>
		<li><a class="treeview-item" href="{{getAddr('admin_all')}}"><i
				class="icon fa fa-users"></i> Admin Users</a>
		</li>
		<?php endif; ?>
		<li><a class="treeview-item" href="{{getAddr('admin_exam_contents')}}"><i
				class="icon fa fa-eye"></i> Exam Content/Body</a>
		</li>
		<li><a class="app-menu__item" href="{{getAddr('admin_install_courses')}}"><i
				class="app-menu__icon fa fa-pie-chart"></i><span
				class="app-menu__label">Install/Uninstall Courses</span></a>
		</li>
		<li><a class="app-menu__item" href="{{getAddr('ccd_home')}}"><i
				class="app-menu__icon fa fa-pie-chart"></i><span
				class="app-menu__label">Content Developer</span></a>
		</li>
			<?php /*
		<li class="treeview"><a class="app-menu__item" href="#"
			data-toggle="treeview"><i class="app-menu__icon fa fa-book"></i><span
				class="app-menu__label">Content Developer</span><i
				class="treeview-indicator fa fa-angle-right"></i></a>
			<ul class="treeview-menu">
				<li><a class="treeview-item" href="{{getAddr('ccd_all_courses')}}"><i
						class="icon fa fa-hall"></i> Subjects</a>
				</li>
				<?php $uploadMultiContentObj = new \App\Core\UploadMultipleContent(); ?>
				@if(!($uploadMultiContentObj->isBonusContentInstalled()))
				<li><a class="treeview-item" href="{{getAddr('ccd_installed_bonus_content')}}"
					onclick="return confirm('Note: This will take a while and should not be interrupted. \n\nContinue?')" ><i
						class="icon fa fa-hall"></i> Install Bonus Content</a>
				</li>
				@endif
				@if(!($uploadMultiContentObj->isBonusContent2Installed()))
				<li><a class="treeview-item" href="{{getAddr('ccd_installed_bonus_content', ['2'])}}"
					onclick="return confirm('Note: This will take a while and should not be interrupted. \n\nContinue?')" ><i
						class="icon fa fa-hall"></i> Install Bonus Content 2</a>
				</li>
				@endif
			</ul>
		</li>
			*/?>
	</ul>
</aside>