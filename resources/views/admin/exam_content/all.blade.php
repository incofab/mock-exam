<?php
$title = "Admin - All Exam Content | " . SITE_TITLE;
$page = 'manage';
$subCat = 'admin';

?>
@extends('admin.layout')

@section('dashboard_content')

	<div class="app-title">
    	<div >
    		<ol class="breadcrumb">
    			<li><a href="{{getAddr('admin_dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    			<li class="active ml-2"><i class="fa fa-users"></i> Exam Content</li>
    		</ol>
    		<h4 class="">Available Exam Content/Body</h4>
    	</div>
	</div>
	<div class="row justify-content-center">
    	<div class="tile">
    		<a href="<?= getAddr('admin_exam_content_create') ?>" class="btn btn-success pull-right mb-2" >
    				<i class="fa fa-plus"></i> <span>Register New</span>					
    		 </a>
    		<h2 class="tile-title">Available Exam Content</h2>
    		<table class="table table-responsive table-striped table-bordered">
        			<tr>
        				<th>Institution</th>
        				<th>Exam Body/Name</th>
        				<th>Exam fullname</th>
        				<th>Courses</th>
        				<th></th>
        			</tr>
        			@foreach($allRecords as $record)
        				<tr>
        					<td>{{$record[INSTITUTION]}}</td>
        					<td>{{$record[EXAM_NAME]}}</td>
        					<td>{{$record[FULLNAME]}}</td>
        					<td>
        						<a href="{{getAddr('ccd_all_courses', $record[TABLE_ID])}}" 
        							class="btn-link"> View Courses</a>
        					</td>
        					<td>
        						<a href="{{getAddr('admin_exam_content_edit', $record[TABLE_ID])}}" 
        							class="btn btn-sm btn-danger"> <i class="fa fa-edit"></i> Edit</a>
        						<a href="{{getAddr('admin_exam_content_delete', $record[TABLE_ID])}}" 
        							onclick="return confirm('Delete? Be careful because this will delete all courses and sessions under it')"
        							class="btn btn-sm btn-danger"> <i class="fa fa-trash"></i> Delete</a>
        						@if($outputJson->isOutputJsonAvailable($record[EXAM_NAME]))
        						<a href="{{getAddr('admin_output_json_delete', [$record[TABLE_ID], '?next='.getAddr('')])}}" 
        							onclick="return confirm('Delete already compiled json?')"
        							class="btn btn-sm btn-warning mr-2"> <i class="fa fa-download"></i> Delete JSON </a>
        						@else
        						<a href="{{getAddr('admin_output_json', [$record[TABLE_ID], '?next='.getAddr('')])}}" 
        							onclick="return confirm('Compile and output json?')"
        							class="btn btn-sm btn-warning mr-2"> <i class="fa fa-download"></i> Compile to JSON </a>
        						@endif
        					</td>
        				</tr>
        			@endforeach
        		</table>
    	</div>	
	</div>
	

@stop
