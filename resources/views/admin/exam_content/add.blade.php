<?php
$title = "Admin - Add Exam Content | " . SITE_TITLE;

$errors = isset($errors) ? $errors : [];
$post = isset($post) ? $post : [];
$valErrors = \Session::getFlash('val_errors', []);

?>

@extends('admin.layout')

@section('dashboard_content')

<div id="place-order">
	<div class="app-title">
    	<div >
    		<ol class="breadcrumb">
    			<li><a href="{{getAddr('admin_dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    			<li class="active ml-2"><i class="fa fa-users"></i> Exam Content</li>
    		</ol>
    		<h4 class="">Register Exam Content</h4>
    	</div>
 	</div>
</div>

<div class="row justify-content-center">
	<div class="col-sm-10 col-md-8">
    	<div class="tile">
    		<h2 class="tile-title">Register Exam Content</h2>
        	<form method="POST" action="" name="register">
        		@include('common.form_message')
        		<div class="form-group">
        			<label for="">Institution [Optional]</label>
        			<input type="text" id="" name="<?= INSTITUTION ?>" value="<?= getValue($post, INSTITUTION)  ?>" 
        				placeholder="Institution" class="form-control">
        		</div>
        		<div class="form-group">
        			<label for="">Exam Body/Name <i>(In short)</i></label>
        			<input type="text" id="" name="<?= EXAM_NAME ?>" value="<?= getValue($post, EXAM_NAME)  ?>" 
        				placeholder="Exam Body/Name" class="form-control">
        		</div>
        		<div class="form-group">
        			<label for="">Exam Body/Name <i>(In full)</i> [Optional]</label>
        			<input type="text" id="" name="<?= FULLNAME ?>" value="<?= getValue($post, FULLNAME)  ?>" 
        				placeholder="Exam Body/Name in full" class="form-control">
        		</div>
        		<div class="form-group">
        			<input type="hidden" name="<?= TABLE_ID ?>" value="<?= $tableID ?? '' ?>" />
        			<input type="submit" value="submit" style="width: 60%; margin: auto;" 
        				class="btn btn-primary btn-block" /><br /><br />
        		</div>
        	</form>
        </div>
	</div>
</div>

@endsection