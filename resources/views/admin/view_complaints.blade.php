<?php
$title = "Admin - View Complaints | " . SITE_TITLE;
$page = 'complaints';
$subCat = 'complaints';
$username = isset($username) ? $username : null;
?>
@extends('admin.layout')

@section('dashboard_content')

	<div>
		<ol class="breadcrumb">
			<li><a href="{{getAddr('admin_dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			@if($username)
			<li><a href="{{getAddr('admin_view_all_users')}}"><i class="fa fa-users"></i> Users</a></li>				
			<li><a href="{{getAddr('admin_view_user_profile')}}"><i class="fa fa-users"></i> Profile</a></li>
			@endif
			<li class="active"><i class="fa fa-user"></i> Complaints</li>
		</ol>
	</div>
	
	<div >
		<h3>{{$title}}</h3>
		@foreach($allRecords as $record)
			<fieldset class="{{($record[IS_RESOLVED] ? 'is_resolved' : '')}}">
			<legend>{{$record[CATEGORY]}}</legend>
			<div class="complaint_row" >
				<div class="row">
					<div class="col-sm-4"><b>{{$record[USERNAME]}} ({{$record[PHONE_NO]}})</b></div>
					<div class="col-sm-4 text-right">{{$record[CREATED_AT]}}</div>
					<div class="col-sm-4 text-right">
						@if(!$record[IS_RESOLVED])
						<a href="{{getAddr('admin_resolved_complaint', [$record[TABLE_ID], $username])}}" 
							onclick="return confirm('Are you sure?')" 
							><small>Mark as Resolved</small>
						</a>
						@endif
					</div>
				</div>
				@if($record[IMAGE_1])
				<div class="img-container"><img src="{{assets('img/complaints/' . $record[IMAGE_1])}}" alt="" class="img-responsive" /></div>
				@endif
				<div class="text" style="padding: 10px 5px; line-height: 1.67em;">
					{{$record[COMPLAINT]}}
				</div>
				@if($record[IMAGE_2])
				<div class="img-container"><img src="{{assets('img/complaints/' . $record[IMAGE_2])}}" alt="" class="img-responsive" /></div>
				@endif
				<div>
					<a href="{{getAddr('admin_send_sms', $record[PHONE_NO])}}" class="btn btn-primary btn-sm" 
						><i class='fa fa-edit'></i> Reply by SMS</a>
					<a href="{{getAddr('admin_send_notification', $record[USERNAME])}}" class="btn btn-primary  btn-sm"
						><i class='fa fa-edit'></i> Reply by Notification</a>
					<a href="{{getAddr('admin_delete_complaint', [$record[TABLE_ID], $username])}}" 
						class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"
							 ><i class='fa fa-trash'></i>  Delete</a>
				</div>
			</div>
			</fieldset>
		@endforeach
	</div>
<style>
	.is_resolved{ background-color: #f6f6f6; }
	.img-container{ width: 80%; margin: auto; }
</style>
@stop