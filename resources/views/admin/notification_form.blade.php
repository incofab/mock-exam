<?php
$title = "Admin - Nofication Form | " . SITE_TITLE;
$page = 'manage';

$subCat = 'notification';
?>
@extends('admin.layout')

@section('dashboard_content')

	<div class="row">
		<ol class="breadcrumb">
			<li><a href="{{getAddr('admin_dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active"><i class="fa fa-users"></i> Send Notification</li>
		</ol>
	</div>
	
	<h3>Send Notification</h3>
          
	<form action="" method="post">
	   
	   <div class="form-group" >
			<label for="">Username / Phone number</label>
			<input type="text" name="<?= USERNAME ?>" value="<?= getValue($post, USERNAME, $username) ?>" 
				 required class="form-control">
		</div>
	   
	   <div class="form-group" >
			<label for="">Display Title</label>
			<input type="text" name="<?= TITLE ?>" value="<?= getValue($post, TITLE) ?>" 
				placeholder="A very short title" required class="form-control" maxlength="100">
		</div>
		
	   <div class="form-group" >
			<label for="">Message</label>
			<textarea name="{{MESSAGE}}" class="form-control" cols="30" rows="5">{{getValue($post, MESSAGE)}}</textarea>
		</div>
		
		<input type="hidden" name="send_notification" value="true" />
		<input type="hidden" name="<?= CSRF_TOKEN ?>" value="<?= \Session::getCsrfValue() ?>" />
		<input type="submit"  name="send" class="btn btn-success" value="Send">
	</form>    
	
	

@stop
