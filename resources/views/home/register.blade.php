<?php 
$title = 'Registration | ' . SITE_TITLE;
?>

@extends('home.layout')

@section('content')
	
	@include('common.register_form')

@endsection