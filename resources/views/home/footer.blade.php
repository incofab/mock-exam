<!-- ***** Footer Area Start ***** -->
<footer class="footer-social-icon text-center bg-white section_padding_70 clearfix mb-0 py-3" id="footer">
    <!-- footer logo -->
    <div class="footer-text">
        <h2 id="footer-header">{{SITE_TITLE}}</h2>
    </div>
    <!-- social icon-->
    <div class="footer-social-icon">
        <a href="{{FACEBOOK_PAGE}}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="{{TWITTER_PAGE}}"><i class="active fa fa-twitter" aria-hidden="true"></i></a>
        <a href="{{INSTAGRAM_PAGE}}"> <i class="fa fa-instagram" aria-hidden="true"></i></a>
        <a href="{{GOOGLE_PLUS_PAGE}}"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
    </div>
    <div class="footer-menu">
        <nav>
            <ul>
                <li><a href="#">About</a></li>
                <li><a href="#">Terms &amp; Conditions</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </nav>
    </div>
    <!-- Foooter Text-->
    <div class="copyright-text">
        <!-- ***** Removing this text is now allowed! This template is licensed under CC BY 3.0 ***** -->
        <p>Copyright &copy;{{date('Y')}} <a href="{{getAddr('home')}}" >{{SITE_TITLE}}</a></p>
    </div>
</footer>
<!-- ***** Footer Area Start ***** -->

<style>
#footer{
    position: static !important;
    width: 100% !important;
/*     border-top: 1px solid #fb397d; */
    -webkit-box-shadow:0px 0px 10px 3px #8d8989 inset;
    -moz-box-shadow:0px 0px 10px 3px #8d8989 inset;
    box-shadow:0px 0px 10px 3px #8d8989 inset;
    overflow: hidden;
}
#footer #footer-header{
    font-size: 3rem;
}
</style>