<?php 
$title ='Welcome | '.SITE_TITLE;
?>
@extends('vali_layout')

@section('body')

<div id="home">
<header>
	<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
		<a class="navbar-brand" href="#">{{SITE_TITLE}}</a>
	</nav>
</header>
<br /><br />
<br />
<div class="container-fluid w-75">
	@include('common.message')
	<div class="row">
		<div class="col-6">
			<a href="{{getAddr('student_exam_login')}}" class="slab shadow-lg rounded mx-auto border border-light" >
				<i class="fa fa-graduation-cap fa-3x"></i>
				<span>Take Exam</span>
			</a>
		</div>
		<div class="col-6">
			<a href="{{getAddr('exam_demo')}}" class="slab shadow-lg rounded mx-auto border border-light" >
				<i class="fa fa-desktop fa-3x"></i>
				<span>Exam Demo</span>
			</a>
		</div>
		<br /><br />
		<br /><br />
		<br /><br />
		<div class="col-6">
			<a href="{{getAddr('home_view_exam_result')}}" class="slab shadow-lg rounded mx-auto border border-light" >
				<i class="fa fa-certificate fa-3x"></i>
				<span>View Result</span>
			</a>
		</div>
		<div class="col-6">
			<a href="{{getAddr('center_login')}}" class="slab shadow-lg rounded mx-auto border border-light" >
				<i class="fa fa-sign-in fa-3x"></i>
				<span>Center Login</span>
			</a>
		</div>
	</div>
</div>

<style>
body{
    background: #fbffff;
}
#home .navbar{
    background: #009688 !important;
    padding: 15px;
}
#home .slab:focus, #home .slab:active,
#home .slab:hover{
    text-decoration: none;
}
#home .slab{
    text-decoration: none;
    display: block;
    padding: 20px 15px;
    margin-top: 60px;
    height: 150px;
    width: 180px;
    background: white;
}
#home .slab i{
    display: block;
    text-align: center;
    margin-top: 10px;
}
#home .slab span{
    display: block;
    text-align: center;
    margin-top: 10px;
    margin-top: 20px;
    font-size: 1.4rem;
}
</style>
</div>
@endsection

