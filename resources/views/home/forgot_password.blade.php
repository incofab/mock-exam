<?php
$title = "Forgot Password | " . SITE_TITLE;
$post = isset($post) ? $post : [];

$login = true;
?>
@extends('home.layout')

@section('content')

<style>
    #login-cover{
        background-image: url("{{assets('img/images/abstract.jpg')}}");
    }
    #login{
        background-color: rgba(21, 20, 20, 0.82);
        border-radius: 5px;
        padding: 15px;
        border: 1px solid #383a3a;
    }
    #login .color-primary{
        color: #f90;
        font-weight: normal;
    }
    #login label, #login h3, #login p{
        color: #efefef;
    }
</style>
<div id="login-cover">
	<br />
	<div class="row" style="margin: 0;" >
	<div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3" id="login">
		@if(isset($forgotPassword))
		<div class="" >
			<h3 class="text-center">Forgot Password</h3>
			<form autocomplete="off" method="POST" action="" name="login">
				<br />
				<div class="form-group" >
					<label for="">Phone number</label>
					<input type="text" name="forgot_password" value="<?= getValue($post, 'forgot_password')  ?>" 
						placeholder="Enter Phone number" required class="form-control">
				</div>
				<input type="hidden" name="<?= CSRF_TOKEN ?>" value="<?= \Session::getCsrfValue() ?>" />
				<input type="submit"  name="login" style="" 
					class="btn btn-primary btn-block pull-right" value="Send" >
			</form>
			<br /><br />
			<p class="text-center">Don't have an account yet? <strong> 
				<a href="<?= getAddr('register_user')?>" class="">Create account</a> </strong> 
			</p>
			<br />
		</div>
		@elseif(isset($resetPassword))
		<div class="" >
			<h3 class="text-center" >Reset Password</h3> 
			<form autocomplete="off" method="POST" action="" name="login">
				<br />
				<div class="form-group" >
					<label for="">New Password</label>
					<input type="Password" name="<?= PASSWORD?>" placeholder="Your desired password" 
						required="required" class="form-control" />
				</div>
				<div class="form-group" >
					<label for="">Confirm Password</label>
					<input type="Password" name="<?= PASSWORD_CONFIRMATION ?>" placeholder="Confirm password" 
						required="required" class="form-control" />
				</div>
				<div class="form-group" >
					<label for="">Reset Code</label>
					<input type="text" name="reset_code"  
						placeholder="Enter the reset code sent to your phone" required class="form-control">
				</div>
				<input type="hidden" name="reset_password" value="true" />
				<input type="hidden" name="<?= CSRF_TOKEN ?>" value="<?= \Session::getCsrfValue() ?>" />
				<input type="submit"  style="" 
					class="btn btn-primary btn-block pull-right" value="Reset" >
			</form>
			<br /><br />
			<p class="text-center">Don't have an account yet? <strong> 
				<a href="<?= getAddr('register_user')?>" class="">Create account</a> </strong> 
			</p>
			<br />
		</div>
		@endif
	</div>
	</div>
	<br /><br />
</div>

@stop