<style>
.single-feature img.img-icon{
    width: 30px;
    height: auto;
    float: left;
    margin-right: 10px;
}    
</style>
<!-- ***** Awesome Features Start ***** -->
<section class="awesome-feature-area bg-white section_padding_0_50 clearfix" id="how-it-works" >
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Heading Text -->
                <div class="section-heading text-center">
                    <h2>How it works</h2>
                    <div class="line-shape"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Single Feature Start -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-feature">
<!--                     <i class="ti-user" aria-hidden="true"></i> -->
                    <img src="{{assets('img/icons/deposit.png')}}" alt="Deposit" class="img-icon" />
                    <h5>How To Deposit</h5>
                    <p>
                    	Convert your airtime to cash by either entering the recharge pin 
                    	or by transferring the amount you wish to convert to the 
                    	SHARE AND SELL Phone number  provided in the  deposit form.
                    	<b class="site-title">{{SITE_TITLE}}</b> provide 2 ways of making deposits:
                    	<p>Using <a href="{{getAddr('home_quick_cash')}}">Quick Cash</a></p>
                    	<p>Creating Cheetahpay account</p>
                    </p>
                </div>
            </div>
            <!-- Single Feature Start -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-feature">
<!--                     <i class="ti-pulse" aria-hidden="true"></i> -->
                    <img src="{{assets('img/icons/fast-food.png')}}" alt="Quick Cash" class="img-icon" />
                    <h5>Quick Cash</h5>
                    <p>
                    	Quick cash lets you convert your airtime to cash without need for 
                    	creating account with <b class="site-title">{{SITE_TITLE}}</b>.
                    	All you have to do is click this <a href="{{getAddr('home_quick_cash')}}">link</a>,
                    	supply the airtime and bank details, your money will be processed 
                    	and delivered to your bank account as soon as possible. <br />
                    	<b>Note:</b> Charge rate for quick cash method is different from the normal method. 
                    </p>
                </div>
            </div>
            <!-- Single Feature Start -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-feature">
<!--                     <i class="ti-dashboard" aria-hidden="true"></i> -->
                    <img src="{{assets('img/icons/dashboard.png')}}" alt="Dashboard" class="img-icon" />
                    <h5>Deposit from Dashboard</h5>
                    <p>
                    	With a <b class="sit-title">{{SITE_TITLE}}</b> user account, you 
                    	can recharge multiple airtime cards, receive payments and donations 
                    	from people. All transactions will be recorded and you can grow 
                    	balance. This system attracts cheaper charge rate than the "Quick cash"
                    	method and relatively more convinient.
                    </p>
                </div>
            </div>
            <?php /* 
            <!-- Single Feature Start -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-feature">
                    <i class="ti-headphone" aria-hidden="true"></i>
                    <h5>24/7 Online Support</h5>
                    <p>
                    	We are always availabe to respond to your enquiries and answer your 
                    	questions. Contact us via SMS, Phone call or Whatsapp at 
                    	<b><a href="https://api.whatsapp.com/send?phone={{WHATSAPP_NO }}">{{WHATSAPP_NO}}</a></b>, 
                    	OR email us at <a href="mailto:{{SITE_EMAIL}}" >{{SITE_EMAIL}}</a>.
                    </p>
                </div>
            </div>
            */ ?>
        </div>

    </div>
<style>
#features{
    position: static;
}
</style>
</section>
<!-- ***** Awesome Features End ***** -->
