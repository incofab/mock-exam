<?php
$messageTitle = isset($messageTitle) ? $messageTitle : SITE_TITLE;
$title = $messageTitle;
$messageBody = isset($messageBody) ? $messageBody : SITE_TITLE;
?>

@extends('vali_layout') 

@section('body')

<section class="material-half-bg">
	<div class="cover"></div>
</section>
<section class="login-content">
	<div class="logo">
		<h1>{{SITE_TITLE}}</h1>
	</div>
	<div class="login-box">
		<form class="login-form" action="" method="post">
			<h3 class="login-head">
				{{$messageTitle}}
			</h3>
        	<br /><br />
			<div class="form-group h5">
				{{$messageBody}}
			</div>
			<br /><br />
			<a href="{{getAddr('home')}}" class="btn btn-primary btn-block btn-lg">
				<i class="fa fa-home fa-lg fa-fw"></i> Home
			</a>
		</form>
	</div>
</section>
<!-- The javascript plugin to display page loading on top-->
<script type="text/javascript">

</script>

@endsection
