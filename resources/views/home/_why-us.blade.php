<?php

?>
<style>
.single-icon{
    width: 70px;
    margin-left: auto;
    margin-right: auto;
}
</style>

<!-- ***** Special Area Start ***** -->
<section class="special-area bg-white pt-4" id="features">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Section Heading Area -->
                <div class="section-heading text-center">
                    <h2>Features</h2>
                    <div class="line-shape"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- Single Special Area -->
            <div class="col-12 col-md-4">
                <div class="single-special text-center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="single-icon">
<!--                         <i class="ti-mobile" aria-hidden="true"></i> -->
                        <img src="{{assets('img/icons/time-fast.png')}}" alt="Fast and Easy" class="img-fluid" />
                    </div>
                    <h4>Fast and Easy</h4>
                    <p>
                    	This platform is very easy to use, Deposits and withdrawals are
                    	processed in the shortest possible time. No unnecessary delays.
                    </p>
                </div>
            </div>
            <!-- Single Special Area -->
            <div class="col-12 col-md-4">
                <div class="single-special text-center wow fadeInUp" data-wow-delay="0.4s">
                    <div class="single-icon">
                    	<img src="{{assets('img/icons/code.png')}}" alt="API Integration" class="img-fluid" />
<!--                         <i class="ti-ruler-pencil" aria-hidden="true"></i> -->
                    </div>
                    <h4>API Integration</h4>
                    <p>
                    	Websites and E-commerce stores can seamlessly integrate this platform
                    	into their website using our freely available APIs.
                    	Wordpress users can also install our plugin.
                    </p>
                </div>
            </div>
            <!-- Single Special Area -->
            <div class="col-12 col-md-4">
                <div class="single-special text-center wow fadeInUp" data-wow-delay="0.6s">
                    <div class="single-icon">
<!--                         <i class="ti-settings" aria-hidden="true"></i> -->
                        <img src="{{assets('img/icons/deposit.png')}}" alt="Convenient" class="img-fluid" />
                    </div>
                    <h4>Convenience</h4>
                    <p>
                    	You must <b>NOT</b> create an account with us to convert your airtime.
                    	Simply use the <a href="{{getAddr('home_quick_cash')}}">Quick Cash</a>
                    	option and have your money delivered directly to you bank account.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <?php /*
    <!-- Special Description Area -->
    <div class="special_description_area mt-150">
        <div class="special_description_content">
            <div class="app-download-area">
                <div class="app-download-btn wow fadeInUp" data-wow-delay="0.2s">
                    <!-- Google Store Btn -->
                    <a href="#">
                        <i class="fa fa-android"></i>
                        <p class="mb-0"><span>available on</span> Google Store</p>
                    </a>
                </div>
                <div class="app-download-btn wow fadeInDown" data-wow-delay="0.4s">
                    <!-- Apple Store Btn -->
                    <a href="#">
                        <i class="fa fa-apple"></i>
                        <p class="mb-0"><span>available on</span> Apple Store</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    */ ?>
</section>
<!-- ***** Special Area End ***** -->
