<?php

?>
<section class="bg-white pt-5">
	<div class="section-heading text-center">
        <h2 class="m-0 p-0">What We Do</h2>
        <div class="line-shape"></div>
    </div>
    <div class="row mx-0">
    	<div class="col-sm-6">
    		<div class="airtime-cash wow fadeInDown" data-wow-delay="0.5s">
                <img src="{{assets('img/images/cheetah-pay-img.jpeg')}}" alt="" class="img-fluid w-100">
            </div>
    	</div>
    	<div class="col-sm-6">
    		<article>
            	<p class="text">
            		{{SITE_TITLE}} helps you to quickly convert your airtime to physical Cash, 
            		buy and sell products online using airtime of any network of your choice, 
            		make and receive airtime donations, etc... 
            	</p>
    		</article>
    	</div>
    </div>
</section>
