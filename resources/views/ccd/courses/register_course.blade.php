<?php


?>

@extends('ccd.layout')

@section('content')

	 <div class="templatemo-content-widget white-bg">
	 
		 <header class="text-center">
			<h2>Register Course</h2><hr />
		 </header>

		<?= (empty($errorMsg) ? '' : "<p class=\"report\">$errorMsg</p><hr />" ) ?>
		<form action="" method="post" >
			<?php /*
			<?= (empty($errors[EXAM_CONTENT_ID]) ? '' : "<p class=\"report\">{$errors[EXAM_CONTENT_ID][0]}</p>" ) ?>
			<div class="form-group">
				<label for="" >Exam Body/Content</label><br />
				<select name="{{EXAM_CONTENT_ID}}" id="" required="required" class="form-control">
					<option value="">Select Exam Body/Content</option>
					@foreach($examContents as $examContent)
					<option value="{{$examContent[TABLE_ID]}}" title="{{$examContent[FULLNAME]}}"
					<?= markSelected(getValue($post, EXAM_CONTENT_ID), $examContent[TABLE_ID]) ?> >{{$examContent[EXAM_NAME]}}</option>
					@endforeach
				</select>
			</div>
			*/?>
			<?= (empty($errors[COURSE_CODE]) ? '' : "<p class=\"report\">{$errors[COURSE_CODE][0]}</p>" ) ?>
			<div class="form-group">
			
				<label for="" >Course Code</label><br />
				<input type="text" name="<?= COURSE_CODE ?>" value="<?= getValue($post, COURSE_CODE) ?>"  class="form-control" />
			
			</div>
			
			<?= (empty($errors[COURSE_TITLE]) ? '' : "<p class=\"report\">{$errors[COURSE_TITLE][0]}</p>" ) ?>
			<div class="form-group">
			
				<label for="" >Course Fullname</label><br />
				<input type="text" name="<?= COURSE_TITLE ?>" value="<?= getValue($post, COURSE_TITLE) ?>" class="form-control"/>
			
			</div>
			
			<?= (empty($errors[DESCRIPTION]) ? '' : "<p class=\"report\">{$errors[DESCRIPTION][0]}</p>" ) ?>
			<div class="form-group">
			
				<label for="" >Description</label><br />
				<textarea name="<?= DESCRIPTION ?>" id="" cols="60" rows="6" class="useEditor form-control" 
					><?= getValue($post, DESCRIPTION)?></textarea>
			
			</div>
						
			<br />
			<div class="form-group">
			
				@if(isset($edit))
					<input type="hidden" name="update_course" value="true" />
					<input type="hidden" name="<?= TABLE_ID ?>" value="<?= $table_id ?>" />
				@else
					<input type="hidden" name="register_course" value="true" />
				@endif
				
				<input type="submit" value="submit" class="templatemo-blue-button width-20" /><br /><br />
				
			</div>
					
		</form>
	</div>
			
			
@stop