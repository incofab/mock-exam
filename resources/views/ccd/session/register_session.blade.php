<?php


?>

@extends('ccd.layout')

@section('content')
	<div class="templatemo-content-widget white-bg">
		 <header class="text-center"> 
			<h2>Register Session</h2><hr />
		 </header>
		<?= (empty($errorMsg) ? '' : "<p class=\"report\">$errorMsg</p><hr />" ) ?>
		<form action="" method="post" enctype="multipart/form-data" >
			
			<?= (empty($errors[SESSION]) ? '' : "<p class=\"report\">{$errors[SESSION][0]}</p>" ) ?>
			<div class="form-group">
			
				<label for="" >Course Year</label><br />
				<input type="text" name="<?= SESSION ?>" value="<?= getValue($post, SESSION) ?>" class="form-control" />
			
			</div>
			
			<?= (empty($errors[CATEGORY]) ? '' : "<p class=\"report\">{$errors[CATEGORY][0]}</p>" ) ?>
			<div class="form-group">
			
				<label for="" >Category [optional]</label><br />
				<input type="text" name="<?= CATEGORY ?>" value="<?= getValue($post, CATEGORY) ?>" class="form-control" />
			
			</div>  
			
			<?= (empty($errors[GENERAL_INSTRUCTIONS]) ? '' : "<p class=\"report\">{$errors[GENERAL_INSTRUCTIONS][0]}</p>" ) ?>
			<div class="form-group">
			
				<label for="" >General Instructions</label><br />
				<textarea name="<?= GENERAL_INSTRUCTIONS ?>" id="" cols="60" rows="6" class="wysiwyg form-control"
					><?= getValue($post, GENERAL_INSTRUCTIONS)?></textarea>
			
			</div>
			<br />
			<?= (empty($errors[FILE_PATH]) ? '' : "<p class=\"report\">{$errors[FILE_PATH][0]}</p>" ) ?>
			<div class="form-group">
				@if(empty($post[FILE_PATH]))
				<label for="" >Content File</label><br />
				@else
				<label for="" >Content Available (<?= pathinfo($post[FILE_PATH], PATHINFO_FILENAME) ?>)</label><br />
				@endif
				<input type="file" name="<?= FILE_PATH ?>" value="<?= getValue($post, FILE_PATH) ?>" class="form-control" />
			</div>
			<br />
			<fieldset>
				<legend>Per Question Instructions [if any]</legend>
			
			<a href="javascript:addInstruction()" class="templatemo-blue-button width-20" >Add Per Question Instruction</a> <br /><br />
				
			<div id="per_question_instruction">
					
			@foreach($allInstructions as $instruction)
				<div class="form-group" ><a href="#" id="remove" >Remove </a>	
						
			  		<label for="" >Per Question Instruction</label><br />
			   		<textarea name="<?= ALL_INSTRUCTION . '[' . INSTRUCTION . '][]' ?>" id="" cols="60" rows="6"
			    		 ><?= $instruction[INSTRUCTION] ?></textarea>
			   		<br /><br />
			   		<div>
				   		<label for="" >From:</label> 
				   		<input type="number" name="<?= ALL_INSTRUCTION . '[' . FROM_ . '][]' ?>" size="5" value="<?= $instruction[FROM_] ?>" />
				   		<label for="" >To:</label>
				   		<input type="number" name="<?= ALL_INSTRUCTION . '[' . TO_ . '][]' ?>" size="5" value="<?= $instruction[TO_] ?>" />
				   		<input type="hidden" name="<?= ALL_INSTRUCTION . '[' . TABLE_ID . '][]'?>" value="<?= $instruction[TABLE_ID ]?>" />
			   		</div>			   
			   
	   		   </div>
	   		 @endforeach
					
			</div>
			
			</fieldset>
			 
			 <hr />
			 
			 
			<fieldset>
				<legend>Passage [if any]</legend>
			
			<a href="javascript:addPassage()" class="templatemo-blue-button width-20" >Add Passage</a> <br /><br />
				
			<div id="passages">
						
				@foreach($allPassages as $passage)
					<div class="form-group" ><a href="#" id="remove" >Remove </a>	
							
				  		<label for="" >Passage</label><br />
				   		<textarea name="<?= ALL_PASSAGES . '[' . PASSAGE . '][]' ?>" id="" cols="60" rows="6"
				    		 ><?= $passage[PASSAGE] ?></textarea>
				   		<br /><br />
				   		<div>
					   		<label for="" >From:</label> 
					   		<input type="number" name="<?= ALL_PASSAGES . '[' . FROM_ . '][]' ?>" size="5" value="<?= $passage[FROM_] ?>" />
					   		<label for="" >To:</label>
					   		<input type="number" name="<?= ALL_PASSAGES . '[' . TO_ . '][]' ?>" size="5" value="<?= $passage[TO_] ?>" />
					   		<input type="hidden" name="<?= ALL_PASSAGES . '[' . TABLE_ID . '][]'?>" value="<?= $passage[TABLE_ID ]?>" />
				   		</div>			   
				   
		   		   </div>
		   		 @endforeach
	   		 
			</div>
			
			</fieldset>
			
			<hr />
			<br /> 
			<div class="form-group"> 
			
				@if(isset($edit))
					<input type="hidden" name="update_regd_course_year" value="true" />
					<input type="hidden" name="<?= TABLE_ID ?>" value="<?= $year_id ?>" />
				@else
					<input type="hidden" name="register_course_year" value="true" />
				@endif
				
				<input type="hidden" name="<?= COURSE_ID ?>" value="<?= $courseId?>" />
				<input type="submit" value="submit" class="templatemo-blue-button width-20" /><br /><br />
				
			</div>
					
		<script type="text/javascript" src="<?= ROOT_FOLDER . 'public/js/lib/tinymce/tinymce.min.js' ?>"></script>
		<script type="text/javascript"> tinymce.init({selector:'textarea.wysiwyg'});</script>
		</form>
		
		
		  		   
   		   

		<script type="text/javascript">
			
			var INSTRUCTION = "<?= INSTRUCTION ?>";
			var PASSAGE = "<?= PASSAGE ?>";
			var ALL_PASSAGES = "<?= ALL_PASSAGES ?>";
			var ALL_INSTRUCTION = "<?= ALL_INSTRUCTION ?>";
			var FROM_ = "<?= FROM_ ?>";
			var TO_ = "<?= TO_ ?>";
			
			function addInstruction() {
				
				var source   = $('script#mus_instruction').html();
				var template = Handlebars.compile(source);
				
				var musData = {
						TO_ : TO_,
						FROM_ : FROM_,
						INSTRUCTION : INSTRUCTION,
						ALL_INSTRUCTION: ALL_INSTRUCTION,
				};
				
				$('#per_question_instruction').append(template(musData));
		   		   
			}

			$(document).on('click', '#remove', function() {
				$(this).parent().remove();
				return false;
			});

			function addPassage() {
				
				var source   = $('script#mus_passage').html();
				var template = Handlebars.compile(source);
				
				var musData = {
						TO_ : TO_,
						FROM_ : FROM_,
						PASSAGE : PASSAGE,
						ALL_PASSAGES: ALL_PASSAGES
				};
				
				$('#passages').append(template(musData));
						   		   
			}


			
		
		</script>
		
	</div>
	
		<script type="x-tmpl-mustache" class="mustache" id="mus_instruction">
		
			<div class="form-group" ><a href="#" id="remove" >Remove </a>	
		  		<label for="" >Per Question Instruction</label><br />
		   		<textarea name="@{{ALL_INSTRUCTION}}[@{{INSTRUCTION}}][]" id="" cols="60" rows="6"></textarea>
		   		<br /><br />
		   		
		   		<div>
			   		<label for="" >From:</label> 
			   		<input type="number" name="@{{ALL_INSTRUCTION}}[@{{FROM_}}][]" size="5" value="" />
			   		<label for="" >To:</label>
			   		<input type="number" name="@{{ALL_INSTRUCTION}}[@{{TO_}}][]" size="5" value="" />
		   		</div>			   
   		   </div>
   		</script>
   		   
   		   <!-- Mus ends -->
		<script type="x-tmpl-mustache" class="mustache" id="mus_passage">
			<div class="form-group" ><a href="#" id="remove" >Remove </a>	
		  		<label for="" >Passage</label><br />
		   		<textarea name="@{{ALL_PASSAGES}}[@{{PASSAGE}}][]" id="" cols="60" rows="6"></textarea>
		   		<br /><br />
		   		
		   		<div>
			   		<label for="" >From:</label> 
			   		<input type="number" name="@{{ALL_PASSAGES}}[@{{FROM_}}][]" size="5" value="" />
			   		<label for="" >To:</label>
			   		<input type="number" name="@{{ALL_PASSAGES}}[@{{TO_}}][]" size="5" value="" />
		   		</div>			   
   		   </div>
		</script> 		
			
@stop