<?php


?>

@extends('ccd.layout')

@section('content')
<?php
/*
	<script type="text/javascript" async="async" 
		src="<?= ROOT_FOLDER . 'public/js/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML'?>"></script>
*/
?>
	
 <div class="templatemo-content-widget white-bg ">
	 <header class="text-center">
		<h2>Preview {{ $courseCode }} Question of {{ $year }}</h2><hr />
	 </header>
	@include('ccd.common.preview_single_question')
</div>
<script type="text/javascript">
var imgBaseDir = '<?= ADDR.\App\Parser\GenericParse::IMG_OUTPUT_PUBLIC_PATH; ?>';
$(function(){
	$('.question-container').find('img').each(function(i, ele) {
		var $img = $(ele);
    	var src = $img.attr('src');
    	var imgPath = imgBaseDir+'{{$courseId}}/{{$year}}/'+src;
    	
//     	$img.attr('src', imgPath);
    	$img.attr('src', getImageAddr('{{$courseId}}', '{{$year_id}}', src, '{{$year}}'));
	});
});

</script>
			
@stop