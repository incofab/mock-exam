<?php

$uploadURL = getAddr('ccd_upload_question_images', [$courseId, $year_id]);
$delayTinyMCE = true;
// dDie($uploadURL);
?>

@extends('ccd.layout')

@section('content')
<style>
    #num-div{
        position: fixed; top: 0; right: 0; 
        background-color: #333333aa;
        color: #efefef;
        font-weight: bold;
        font-size: 1.5em;
        padding: 5px 15px;
    }
</style>
	<div id="num-div">No: <span id="num">{{$num}}</span></div>
	<div class="templatemo-content-widget white-bg ">
		<div class="row">
			<div class="col-sm-3">
                <a href="<?= getAddr('ccd_all_session_questions', [$courseId, $year_id]) ?>" 
                 	class="templatemo-blue-button width-20" title="Go back to question listings" >
                		<i class="fa fa-arrow-left"></i> <span>Back</span>					
                </a>
			</div>
			<div class="col-sm-9 clearfix">
				<div class="pull-right">
				</div>
			</div>
		</div>
        <br />
        <br />
        <header class="text-center">
        	<h2>Enter Question</h2><hr />
        </header>
		<?= (empty($errorMsg) ? '' : "<p class=\"report\">$errorMsg</p><hr />" ) ?>
		<form action="" method="post" name="record-question" id="record-question-form">
			<?= (empty($errors[QUESTION_NO]) ? '' : "<p class=\"report\">{$errors[QUESTION_NO][0]}</p>" ) ?>
			<div class="row">
				<div class="col-sm-6">
        			<div class="form-group">
        				<label for="" >No: </label>
        				<input type="number" name="<?= QUESTION_NO ?>" value="<?= $num ?>" id="question-no" 
        					onchange="questionNumberChanged(this)" required="required" class="form-control w-25"/>
        			</div>
				</div>
			</div>
			<?= (empty($errors[QUESTION]) ? '' : "<p class=\"report\">{$errors[QUESTION][0]}</p>" ) ?>
			<div class="form-group">
				<label for="" >Question</label><br />
				<textarea name="<?= QUESTION ?>" id="" cols="60" rows="6" class="useEditor" 
					><?= getValue($post, QUESTION)?></textarea>
			</div>
			<br/>
			<div class="form-group row">
				<div class="col-md-6">
					<?= (empty($errors[OPTION_A]) ? '' : "<p class=\"report\">{$errors[OPTION_A][0]}</p>" ) ?>
					<label for="" >A</label><br />
					<textarea name="<?= OPTION_A ?>" cols="30" rows="3" class="useEditor"
						><?= getValue($post, OPTION_A)?></textarea>
				</div>
				<div class="col-md-6">
					<?= (empty($errors[OPTION_B]) ? '' : "<p class=\"report\">{$errors[OPTION_B][0]}</p>" ) ?>
					<label for="" >B</label><br />
					<textarea name="<?= OPTION_B ?>" cols="30" rows="3"  class="useEditor"
						><?= getValue($post, OPTION_B)?></textarea>
				</div>
			</div> 
			<br/>
			<div class="form-group row">
				<div class="col-md-6">
					<?= (empty($errors[OPTION_C]) ? '' : "<p class=\"report\">{$errors[OPTION_C][0]}</p>" ) ?>
					<label for="" >C</label><br />
					<textarea name="<?= OPTION_C ?>" cols="30" rows="3" class="useEditor"
						><?= getValue($post, OPTION_C)?></textarea>
				</div>
				<div class="col-md-6">
					<?= (empty($errors[OPTION_D]) ? '' : "<p class=\"report\">{$errors[OPTION_D][0]}</p>" ) ?>
					<label for="" >D</label><br />
					<textarea name="<?= OPTION_D ?>" cols="30" rows="3" class="useEditor" ><?= getValue($post, OPTION_D)?></textarea>
				</div>
			</div> 
			<br/>
			<div class="form-group row">
				<div class="col-md-6">
					<?= (empty($errors[OPTION_E]) ? '' : "<p class=\"report\">{$errors[OPTION_E][0]}</p>" ) ?>
					<label for="" >E</label><br />
					<textarea name="<?= OPTION_E ?>" cols="30" rows="3" class="useEditor"><?= getValue($post, OPTION_E)?></textarea>
				</div>
			</div> 
			<br/>
			<?= (empty($errors[ANSWER]) ? '' : "<p class=\"report\">{$errors[ANSWER][0]}</p>" ) ?>
			<div class="form-group">
				<label for="" >Select Answer: &emsp;</label>
				<select name="<?= ANSWER ?>" id="" required="required" class="form-control w-25">
					<option value="">select Answer</option>
					<option  <?= (getValue($post, ANSWER) == 'A') ? 'selected="selected"' : '' ?> >A</option>
					<option  <?= (getValue($post, ANSWER) == 'B') ? 'selected="selected"' : '' ?> >B</option>
					<option  <?= (getValue($post, ANSWER) == 'C') ? 'selected="selected"' : '' ?> >C</option>
					<option  <?= (getValue($post, ANSWER) == 'D') ? 'selected="selected"' : '' ?> >D</option>
					<option  <?= (getValue($post, ANSWER) == 'E') ? 'selected="selected"' : '' ?> >E</option>
				</select>
			
			</div>
			 <br/>
			<?= (empty($errors[ANSWER_META]) ? '' : "<p class=\"report\">{$errors[ANSWER_META][0]}</p>" ) ?>
			<div class="form-group">
				<label for="" >Explanation of the answer [optional]</label><br />
				<textarea name="<?= ANSWER_META ?>" id="" cols="60" rows="6" class="useEditor"><?= getValue($post, ANSWER_META)?></textarea>
			</div> 
			<br /> 
			<div class="form-group">
				<input type="hidden" name="<?= COURSE_SESSION_ID ?>" value="<?= $year_id ?>" />
				<input type="hidden" name="<?= COURSE_ID ?>" value="<?= $courseId ?>" />
				@if(!empty($next) && ($next != getAddr('')))
				<input type="hidden" name="next" value="<?= $next ?>" />
				@endif
				@if(isset($edit))
					<div class="clear-fix">
    					<input type="hidden" name="update_question" value="true" />
    					<input type="hidden" name="<?= TABLE_ID ?>" value="<?= $table_id ?>"/>
    					<input type="submit" name="submit_update" value="Update" class="templatemo-blue-button width-20 float-left"  />
    					<input type="submit" name="save_and_goto_next_question" value="Save & Goto Next" 
    						class="templatemo-blue-button width-20 float-right"  />
					</div>
				@else
					<input type="hidden" name="add_new_question" value="true" />
					<input type="submit" value="Submit" class="templatemo-blue-button width-20" />
				@endif
				<br /><br />
			</div>
		</form>
	</div>
	@include('ccd.common.tinymce')
@if(!isset($edit))
<script type="text/javascript">
	var courseSessionId = {{$year_id}};
	var currentQuestionNo = {{$num}};
	var addQuestionAPI = "{{getAddr('ccd_new_session_question_api')}}";
	initTinymce();

	function questionNumberChanged(obj) {
		$('#num').text($(obj).val());
	}

</script>
<script src="{{assets('js/add-question.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pace-js@latest/pace-theme-default.min.css">
@else
<script type="text/javascript">

	$(function(){
		$('#record-question-form').find('.useEditor').each(function(i, ele) {
			var $ele = $(ele);
			var varStr = $ele.val();
			
			// console.log('Str', varStr);
			var $parsedHtml = $($.parseHTML(`<div>${varStr}</div>`));
			// console.log('Val', $parsedHtml.html());

			$parsedHtml.find('img').each(function(j, e) {
				var $img = $(e);
		    	var src = $img.attr('src');
		    	
		    	$img.attr('src', getImageAddr('{{$courseId}}', '{{$year_id}}', src, '{{$year}}'));
			});
	    	$ele.val($parsedHtml.html());
// 	    	console.log($val);
		});
		initTinymce();
	});
	
</script>
@endif
			
@stop