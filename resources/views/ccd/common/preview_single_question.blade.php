<?php

$instruction = \App\Controllers\CCD\BaseCCD::getInstruction($allInstructions, $questionObj[QUESTION_NO]);

$passage = \App\Controllers\CCD\BaseCCD::getPassage($allPassages, $questionObj[QUESTION_NO]);

$topic = $questionObj['topic'];

?>

 <div class="templatemo-content-widget white-bg question-container mt-2 card shadow rounded-0" >
 
 	@if($instruction)
 	<div>
		<label for="" ><b>Instruction: </b></label> 
		<b>Questions {{$instruction[FROM_]}} - {{$instruction[TO_]}}</b>
		<div class="text">{{ $instruction[INSTRUCTION] }}</div>
	</div>
	<br />
	@endif
	
 	@if($passage)
 	<div>
		<label for="" ><b>Passage: </b></label>
		<b>Questions {{$passage[FROM_]}} - {{$passage[TO_]}}</b>
		<div class="text">{{ $passage[PASSAGE] }}</div>
	</div>
	<br />
	@endif
	
	<div>
		<div class="clearfix">
			<div class="pull-left">No: {{ $questionObj[QUESTION_NO] }} </div>
			@if(!empty($topic[TITLE]))
			<div class="pull-right"><strong>Topic: {{$topic[TITLE]}}</strong></div>
			@endif
		</div>
		<div class="question-text text mt-2">{{ $questionObj[QUESTION] }}</div>
	</div>
 	<br />
	<div class="row">
		<div class="col-md-6">
			<label for="" >(A) </label>
			<div class="text">{{ $questionObj[OPTION_A] }}</div>
		</div>
		<div class="col-md-6">
			<label for="" >(B) </label>
			<div class="text">{{ $questionObj[OPTION_B] }}</div>
		</div>
	</div> 
	<div class="row">
		<div class="col-md-6">
			<label for="" >(C) </label>
			<div class="text">{{ $questionObj[OPTION_C] }}</div>
		</div>
		<div class="col-md-6">
			<label for="" >(D) </label>
			<div class="text">{{ $questionObj[OPTION_D] }}</div>
		</div>
	</div> 
	<div class="row">
		<div class="col-md-6">
			<label for="" >(E) </label>
			<div class="text">{{ $questionObj[OPTION_E] }}</div>
		</div>
	</div>
	<br />
	<div>
		<div style="margin-bottom: 10px;">
			<label for="">Answer: </label> <span>{{ $questionObj[ANSWER] }}</span> 
		</div>
		<div id="answer-explanation">
			<label for=""><u>Explanation:</u></label> <span>{{ $questionObj[ANSWER_META] }}</span>
		</div>
	</div> 
	<div style="margin-top: 20px;">
		<a href="<?= getAddr('ccd_edit_session_question', [$courseId, $year_id, $questionObj[TABLE_ID]])."?next=$next" ?>"
			class="templatemo-blue-button width-20" ><span>Edit</span></a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="<?= getAddr('ccd_delete_session_question', [$courseId, $year_id, $questionObj[TABLE_ID]]) ?>"
			class="templatemo-blue-button width-20" onclick="return confirm('Are you sure?')"
			 ><span>Delete</span></a>	
	</div>
</div>

