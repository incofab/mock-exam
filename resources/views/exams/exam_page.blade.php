<?php
$title = 'Student Mock Exam | ' . SITE_TITLE;

?>
@extends('exams.layout')

@section('dashboard_content')


<style>
#questions-content{
    line-height: 1.8rem;
    font-size: 1.15rem;
    font-family: 'ABeeZee', sans-serif;
    padding-bottom: 30px;
}
#questions-content .question-no{
    font-size: 1.05rem;
}
#questions-content .option{
    padding: 5px 0;
}
#questions-content .option-letter{
    font-weight: 500;
    padding-right: 4px;
}
#questions-content .option-text{
    padding-left: 4px;
    vertical-align: baseline;
}
#questions-content .option-text *{
    display: inline !important;
    vertical-align: middle;
}
#questions-content .instruction{
    font-weight: bold;
    margin: 10px 0;
}
#questions-content .passage{
    margin: 10px 0;
    padding: 7px 0;
    border-top: 1px solid #e0e0e0;
    border-bottom: 1px solid #e0e0e0;
}
#questions-content .passage:empty,
#questions-content .instruction:empty
{
    display: none;
}
#questions-content .passage:blank, 
#questions-content .passage:-moz-only-whitespace, 
#questions-content .instruction:blank, 
#questions-content .instruction:-moz-only-whitespace
{
    display: none;
}
#questions-content .question-numbers-tab{
    margin-top: 5px;
    padding-top: 10px;
    border-top: 1px solid #e0e0e0;
}
#questions-content .question-numbers-tab > li{
    float: left;
    width: 50px;
    height: 50px;
    border: 1px solid #2f2f2f;
    border-radius: 3px;
    margin: 0 4px 4px 0;
    line-height: 45px;
}
#questions-content .question-numbers-tab > li.current{
    font-weight: bold;
    border-width: 2px;
}
#questions-content .question-numbers-tab > li.attempted.current{
    color: inherit;
    font-weight: bold;
    background-color: transparent;
}
#questions-content .question-numbers-tab > li.attempted{
    background-color: #009688;
    color: #efefef;
    font-weight: 500;
    border-color: #007569;
    border-width: 2px;
}
#exam-layout .question-nav{
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    background: #09524b;
}
#exam-layout #next-question,
#exam-layout #previous-question{
    border: 1px solid #0d1214;
    border-radius: 0;
}
#exam-layout #exam-tabs{
    position: fixed;
    top: 50px;
    z-index: 999;
    background: #eee;
    left: 0;
    right: 0;
    padding-left: 15px;
    padding-right: 15px;
}
</style>

<div id="exam-layout" >

    <nav id="exam-tabs" class="clearfix">
    	<div class="nav nav-tabs pull-left" id="nav-tab" role="tablist" >
		{{-- Include Exam Tabs --}}
		</div>
		<div class="pull-right py-2">
			<b >Exam No: <span id="exam-no">0000000000</span></b>
		</div>
    </nav>
    <div class="tab-content mt-4" id="questions-content" >
    	{{-- Include Question Container --}}
    </div>
    
    <div class="question-nav text-center clearfix">
		<button class="btn btn-primary pull-left" id="previous-question">&laquo; Previous</button>
		<button class="btn btn-primary pull-right" id="next-question">Next &raquo;</button>
		<button class="btn btn-primary mx-auto px-3" id="stop-exam"
			data-toggle="tooltip" data-placement="top" title="Submit and end this exam. Cannot be resumed" >
			<i class="fa fa-paper-plane"></i> Submit
		</button>
	</div>
</div>
@include('common.calculator')
@include('exams._modal')

@stop


@section('scripts')

<div id="handlebars-templates">
	<script type="x-tmpl-mustache" id="mus-exam-tab" >@include('exams.mus._tab')</script>
	<script type="x-tmpl-mustache" id="mus-exam-question-container" >@include('exams.mus._question_container')</script>
	<script type="x-tmpl-mustache" id="mus-exam-question" >@include('exams.mus._question')</script>
	<script type="x-tmpl-mustache" id="mus-exam-grid-question-numbers" >@include('exams.mus._grid_question_numbers')</script>
</div>

<script type="text/javascript">
	var ADDR = '<?= ADDR ?>';
	var DEV = '<?= DEV ?>';
	var isDemo = <?= json_encode(empty($is_demo) ? false : true); ?>;
	var examData = <?= json_encode($exam_data) ?>;
	var studentData = <?= json_encode($student_data) ?>;
</script>

<script data-main="{{assets('ts/exams.js')}}" src="{{assets('js/lib/require.js')}}"></script>

@include('common.mathjax')

@stop





