<?php 
?>
<style>
.app-header{
    color: #efefef;
    display: block;
    padding: 0;
    font-size: 20px;
}
.app-header #name{
    font-family: 'Niconne';
    padding: 0 15px;
    font-size: 26px;
    font-weight: 400;
}
.app-header *{
    line-height: 50px;
}
</style>
<!-- Navbar-->
<header class="app-header">
	<div class="row">
		<div class="col-7 text-truncate">
			<div id="name">Firstname Lastname</div>
		</div>
		<div class="col-5" >
			<div align="right" class="pr-3 clearfix">
				<div class="pull-right text-truncate">
    				<i class="fa fa-calculator toggleCalculator pointer"></i>
    				<span id="timer" class="ml-3">00:00:00</span>
					<i class="fa fa-pause pointer ml-3 d-none" id="pause-exam" data-toggle="tooltip" data-placement="left" title="Pause this exam. You can resume later" ></i>
				</div>
			</div>
		</div>
	</div>
</header>