<?php
$title = "Register Exam | " . SITE_TITLE;
$donFlashMessages = true;
$errors = isset($errors) ? $errors : $sessionModel->getFlash('error');
$post = isset($post) ? $post : [];
$valErrors = $sessionModel->getFlash('val_errors', []);
if($valErrors) $errors = null;
?>

@extends('centers.layout')

@section('dashboard_content')

<div class="app-title">
	<div>
		<h1>
			<i class="fa fa-dashboard"></i> Exams
		</h1>
		<p>Register Exam for {{$student[LASTNAME]}} {{$student[FIRSTNAME]}}</p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i> <a href="{{getAddr('admin_dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item"><a href="{{getAddr('center_view_all_exams')}}">Exams</a></li>
		<li class="breadcrumb-item">Register</li>
	</ul>
</div>
<div>
	<div class="tile">
		<h3 class="tile-title">{{ isset($edit) ? 'Edit' : 'Register' }} Exam</h3>
		<div class="">
			<dl class="row">
				<dt class="col-sm-3">Name</dt>
				<dd class="col-sm-9">{{$student[LASTNAME]}} {{$student[FIRSTNAME]}}</dd>
				<dt class="col-sm-3">Center No</dt>
				<dd class="col-sm-9">{{$student[CENTER_CODE]}}</dd>
				<dt class="col-sm-3">Student ID</dt>
				<dd class="col-sm-9">{{$student[STUDENT_ID]}}</dd>
			</dl>
		</div>
    	@if($valErrors)
    	<div class="alert alert-danger">
    		@foreach($valErrors as $vError)
    			<p><i class="fa fa-star" style="color: #cc4141;"></i> {{implode('<br />', $vError)}}</p>
    		@endforeach
    	</div>
    	@endif
    	@if($errors)
    	<div class="alert alert-danger">
   			<div><i class="fa fa-star" style="color: #cc4141;"></i> {{$errors}}</div>
    	</div>
    	@endif
		<form action="" method="post">
    		<div class="tile-body">
				<label class="control-label"><b>Duration</b></label> 
				<div class="row w-75 pl-3">
<!-- 					<div class="col-md-10 col-lg-9"> -->
						<div class="form-group col-3 px-1">
							<label class="control-label">Hours</label> 
							<input class="form-control" type="text" placeholder="Hours" name="hours" 
								value="<?= getValue($post, 'hours', 3)  ?>" >
						</div>
						<div class="form-group col-1 text-center px-1">
							<label class="control-label">&nbsp;</label> 
							<div class="form-control">:</div>
						</div>
						<div class="form-group col-3 px-1">
							<label class="control-label">Mins</label> 
							<input class="form-control" type="text" placeholder="Minutes" name="minutes" 
								value="<?= getValue($post, 'minutes', 0)  ?>" >
						</div>
						<div class="form-group col-1 text-center px-1">
							<label class="control-label">&nbsp;</label> 
							<div class="form-control">:</div>
						</div>
						<div class="form-group col-3 px-1">
							<label class="control-label">Seconds</label> 
							<input class="form-control" type="text" placeholder="Seconds" name="seconds" 
								value="<?= getValue($post, 'seconds', 0)  ?>" >
						</div>
<!-- 					</div> -->
				</div>
				<?php /*
				<div class="form-group">
					<label class="control-label">Mock</label>
					<select name="{{SESSION_ID}}" id="" required="required" class="form-control" >
						<option value="">select mock</option>
						<?php $i = 0; ?>
						@foreach($sessions as $acadSession)
						<?php $i++; ?> 
						<option value="{{$acadSession[TABLE_ID]}}" 
						  <?= markSelected(array_get($post, SESSION_ID), $acadSession[TABLE_ID]) ?>>Mock {{$i}}</option>
						@endforeach
					</select>
				</div>
				*/ ?>
				<div class="form-group w-75" >
					<label class="control-label">Subjects</label>
					<select name="{{SESSION_ID}}[]" id="select-subjects" required="required" 
						class="form-control" multiple="multiple" >
						@foreach($subjects as $subject)
						<?php 
						  $sessions = \App\Models\Sessions::where(COURSE_CODE, '=', $subject[COURSE_CODE])
						      ->orderBy(TABLE_ID, 'DESC')->take(3)->get();
						  $i = 0;
						?>
    						@foreach($sessions as $acadSession)
    						<?php $i++; ?> 
    						<option value="{{$acadSession[TABLE_ID]}}" >{{$subject[COURSE_TITLE]}} (Mock - {{$i}})</option>
    						@endforeach
						@endforeach
					</select>
				</div>
				
				@if(isset($edit))
				<div>
					<input type="hidden" name="{{TABLE_ID}}" value="$tableId" />
				</div>
				@endif
    		</div>
    		<div class="tile-footer">
				<input type="hidden" name="{{CENTER_CODE}}" value="{{$data[CENTER_CODE]}}" />
				<input type="hidden" name="{{STUDENT_ID}}" value="{{$student[STUDENT_ID]}}" />
    			<button class="btn btn-primary" type="submit">
    				<i class="fa fa-fw fa-lg fa-check-circle"></i>Register
    			</button>
    			&nbsp;&nbsp;&nbsp;
    			<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
    		</div>
		</form>
	</div>
</div>
<script type="text/javascript" src="{{assets('lib/select2.min.js')}}"></script>
<script type="text/javascript">
$('#select-subjects').select2();
</script>

@endsection