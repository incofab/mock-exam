
<div class="tab-pane fade show @{{compare tabIndex 0 'active'}}" id="nav-@{{exam_subject_id}}" role="tabpanel" >
    <div class="question-main">
    	@include('exams.mus._question')
    </div>
    <ul class="question-numbers-tab list-unstyled clearfix text-center">
    	@include('exams.mus._grid_question_numbers')
	</ul>
</div>

