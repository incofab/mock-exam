

<div class="question-main">
	<div class="tile text-center p-1 mb-3">
		<div class="tile-title question-no mb-0">Question @{{serialNo current_question.index}} of @{{questionsLength}}</div>
	</div>
	<div class="instruction">@{{getInstruction tabIndex}}</div>
	<div class="passage">@{{getPassage tabIndex}}</div>
	<div class="question-text">{@{{current_question.question}}}</div>
	<div class="options">
		<div class="animated-radio-button option">
			<label class="pointer selection"> 
				<span class="option-letter">A)</span> 
				<input type="radio" name="option" data-selection="A" 
					@{{isOptionSelected tabIndex current_question.question_id 'A'}} > 
				<span class="label-text"><span class="option-text">{@{{current_question.option_a}}}</span></span>
			</label>
		</div>
		<div class="animated-radio-button option">
			<label class="pointer selection"> 
				<span class="option-letter">B)</span> 
				<input type="radio" name="option" data-selection="B"
					@{{isOptionSelected tabIndex current_question.question_id 'B'}} > 
				<span class="label-text"><span class="option-text">{@{{current_question.option_b}}}</span></span>
			</label>
		</div>
		<div class="animated-radio-button option">
			<label class="pointer selection"> 
				<span class="option-letter">C)</span> 
				<input type="radio" name="option" data-selection="C"
					@{{isOptionSelected tabIndex current_question.question_id 'C'}} > 
				<span class="label-text"><span class="option-text">{@{{current_question.option_c}}}</span></span>
			</label>
		</div>
		@{{#if current_question.option_d}}
		<div class="animated-radio-button option">
			<label class="pointer selection"> 
				<span class="option-letter">D)</span> 
				<input type="radio" name="option" data-selection="D"
					@{{isOptionSelected tabIndex current_question.question_id 'D'}} > 
				<span class="label-text"><span class="option-text">{@{{current_question.option_d}}}</span></span>
			</label>
		</div>
		@{{/if}}
		@{{#if current_question.option_e}}
		<div class="animated-radio-button option">
			<label class="pointer selection"> 
				<span class="option-letter">E)</span> 
				<input type="radio" name="option" data-selection="E"
					@{{isOptionSelected tabIndex current_question.question_id 'E'}} > 
				<span class="label-text"><span class="option-text">{@{{current_question.option_e}}}</span></span>
			</label>
		</div>
		@{{/if}}
	</div>
</div>
    
    