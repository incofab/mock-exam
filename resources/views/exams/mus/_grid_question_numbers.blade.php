
@{{#each questions}}
<li data-question_no="@{{question_no}}" data-question_id="@{{question_id}}"
	data-question_index="@{{@index}}" 
	class="pointer @{{isAttemptedQuestion ../tabIndex question_id}} @{{compare ../current_question.question_id question_id 'current' ''}}" 
	>@{{serialNo @index}}</li>
@{{/each}}