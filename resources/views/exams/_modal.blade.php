<?php
?>
<!-- Modal -->
<div class="modal fade" id="examModal" tabindex="-1" role="dialog"
	aria-labelledby="examModalLabel" aria-hidden="true" data-modal_for="">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="examModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">...</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary negative-btn" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary positive-btn">Ok</button>
			</div>
		</div>
	</div>
</div>