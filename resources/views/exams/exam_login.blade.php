<?php
$title = "Login Student for Exam | " . SITE_TITLE;
$donFlashMessages = true;
$errors = isset($errors) ? $errors : $sessionModel->getFlash('error');
$post = isset($post) ? $post : [];
$valErrors = $sessionModel->getFlash('val_errors', []);
if($valErrors) $errors = null;
?>

@extends('vali_layout') 

@section('body')

<section class="material-half-bg">
	<div class="cover"></div>
</section>
<section class="login-content">
	<div class="logo">
		<h1>{{SITE_TITLE}}</h1>
	</div>
	<div class="login-box">
		<form class="login-form" action="" method="post">
			<h3 class="login-head">
                <small>
                    <small>
                    	<i class="fa fa-lg fa-fw fa-graduation-cap"></i> &nbsp;&nbsp;
                    </small>
                    {{!empty($is_demo)? 'Demo Page' : 'Exam Page'}} - {{date('Y')}} 
                </small>
			</h3>
			@if($valErrors)
        	<div class="alert alert-danger">
        		@foreach($valErrors as $vError)
        			<p><i class="fa fa-star" style="color: #cc4141;"></i> {{implode('<br />', $vError)}}</p>
        		@endforeach
        	</div>
        	@endif
        	@if($errors)
        	<div class="alert alert-danger">
       			<div><i class="fa fa-star" style="color: #cc4141;"></i> {{$errors}}</div>
        	</div>
        	@endif
        	<?php /*
			<div class="form-group">
				<label class="control-label">Student ID</label> <input
					class="form-control" type="text" placeholder="Student ID" autofocus
					name="{{STUDENT_ID}}">
			</div>
        	*/?>
        	<br /><br />
			<div class="form-group">
				<label class="control-label">Exam Number</label> <input
					class="form-control" type="text" placeholder="Exam No"
					name="{{EXAM_NO}}">
            	@if(!empty($is_demo))
            	<div class="text-info"><small>Demo Exam No: <strong>00000000</strong></small></div>
            	@endif
			</div>
			<br />
			<div class="form-group btn-container">
				<button class="btn btn-primary btn-block">
					<i class="fa fa-sign-in fa-lg fa-fw"></i>Start Exam
				</button>
			</div>
		</form>
	</div>
</section>
<!-- The javascript plugin to display page loading on top-->
<script type="text/javascript">

</script>

@endsection
