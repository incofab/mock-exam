<?php
$title = isset($title) ? $title : SITE_TITLE;
?>

@extends('vali_layout')

@section('body')
	
@include('exams.header')

<div class="container-fluid" style="margin-top: 95px;" id="exam-base-container">
@yield('dashboard_content')
</div>
<script type="text/javascript">
var appHeaderHeight = 50;
$(function() {
	setContainerMargin();		
});
$(window).on('resize', function(e) {
	setContainerMargin();		
});
function setContainerMargin() {
	var navHeader = $('#exam-layout #exam-tabs').outerHeight(true);
	$('#exam-base-container').css('marginTop', appHeaderHeight + navHeader);		
}
</script>

	
@endsection


