<?php
$title = "Exam Result | " . SITE_TITLE;
$donFlashMessages = true;
$errors = isset($errors) ? $errors : $sessionModel->getFlash('error');
$post = isset($post) ? $post : [];
$valErrors = $sessionModel->getFlash('val_errors', []);
if($valErrors) $errors = null;
?>

@extends('vali_layout') 

@section('body')

<section class="material-half-bg">
	<div class="cover"></div>
</section>
<section class="login-content">
	<div class="logo">
		<h1>{{SITE_TITLE}}</h1>
	</div>
	<div class="login-box">
		<form class="login-form" action="" method="get">
			<h3 class="login-head">
				<i class="fa fa-lg fa-fw fa-certificate"></i> 
				View My Result
			</h3>
			@if($valErrors)
        	<div class="alert alert-danger">
        		@foreach($valErrors as $vError)
        			<p><i class="fa fa-star" style="color: #cc4141;"></i> {{implode('<br />', $vError)}}</p>
        		@endforeach
        	</div>
        	@endif
        	@if($errors)
        	<div class="alert alert-danger">
       			<div><i class="fa fa-star" style="color: #cc4141;"></i> {{$errors}}</div>
        	</div>
        	@endif
        	<br />
			<div class="form-group">
				<label class="control-label">Exam Number</label> 
				<input class="form-control" type="text" placeholder="Exam No" name="{{EXAM_NO}}">
			</div>
			<?php /*
			<div class="form-group">
				<label class="control-label">Student ID</label> 
				<input class="form-control" type="text" placeholder="Student ID" name="{{STUDENT_ID}}">
			</div>
			*/?>
			<br />
			<div class="form-group btn-container">
				<button class="btn btn-primary btn-block">
					<i class="fa fa-sign-in fa-lg fa-fw"></i>View Result
				</button>
			</div>
		</form>
	</div>
</section>
<!-- The javascript plugin to display page loading on top-->
<script type="text/javascript">

</script>

@endsection
