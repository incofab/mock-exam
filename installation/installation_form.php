<?php
if (!defined('ROOT_FOLDER')){
    die('Wrong route');
}
require_once APP_DIR.'../installation/installation.php';

$obj = new Installation();

if(!empty($_POST))
{
    $db_name  = 'ecenter';//$_POST['dbname'];
    $username = 'root';//$_POST['dbusername'];
    $password = '';//$_POST['dbpassword'];
    $title = $_POST['title'];
    if (empty($title)) $title = 'Examiner';
    
    $obj->runInstallation($db_name, $username, $password, $title);
    
    exit();
    return;
}
$pk = $obj->generateCode();
$xx = $obj->hashValue($pk);
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= ROOT_FOLDER.'favicon.ico' ?>">
    <title>Fresh Installation</title>
    <!-- Bootstrap core CSS -->
    <link href="<?= ROOT_FOLDER.'public/lib/bootstrap4/css/bootstrap.min.css'?>" rel="stylesheet">
	<script type="text/javascript" src="<?= ROOT_FOLDER.'public/lib/jquery.min.js' ?>"></script>
  </head>
  <body>
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand <?= $xx ?>" href="#">Fresh Installation</a>
      </nav>
    </header>
	<br /><br />
	<br /><br />
    <div class="container-fluid">
    	<div class="row">
    		<div class="offset-md-2 col-md-8 offset-sm-3 col-sm-6">
				<div class="card">
					<div class="card-header"><div class="h3"><b>Examiner</b></div></div>
					<div class="card-body">
						<p class="card-text">Enter the required credentials to initialize this application</p>
						<form action="" method="post">
							<div class="form-group">
								<label for="title">Business Name</label> 
								<input type="text" class="form-control" name="title" id="title" placeholder="Business Name">
							</div>
							<?php /*
							<div class="form-group">
								<label for="dbname">Database Name</label> 
								<input type="text" class="form-control" id="dbname" name="dbname" value="cbt" required="required" placeholder="Database Name"> 
								<small class="form-text text-muted">Allow defaults if you are not sure</small>
							</div>
							<div class="form-group">
								<label for="dbusername">Database Username</label> 
								<input type="text" class="form-control" name="dbusername" value="root" required="required" id="dbusername" placeholder="Database Username">
								<small class="form-text text-muted">Allow defaults if you are not sure</small>
							</div>
							<div class="form-group">
								<label for="dbpassword">Database Password</label> 
								<input type="text" class="form-control" name="dbpassword" id="dbpassword" placeholder="Database Password">
								<small class="form-text text-muted">Leave black if you didnt create any password</small>
							</div>
							*/ ?>
							<div class="form-group">
								<label for="activation_key">Product Key: <b><?= $pk ?></b></label> 
								<input type="text" class="form-control" name="activation_key" id="activation_key" placeholder="Enter Activation Key">
								<input type="hidden" name="product" value="<?= $pk ?>" />
							</div>
							<button type="submit" class="btn btn-primary" onclick="return confirm('are you sure?')">Run Installations Now</button>
						</form>
					</div>
					<div class="card-footer text-muted">
						<small><i>powered by <a href="mailto:incofabikenna@gmail.com">Inco Technologies</a></i></small>
					</div>
				</div>
			</div>
    	</div>
    </div>
	<script type="text/javascript" src="<?= ROOT_FOLDER.'public/lib/bootstrap4/js/bootstrap.bundle.min.js' ?>"></script>
  </body>
</html>
