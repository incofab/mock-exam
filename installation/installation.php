<?php
use App\Core\CodeGenerator;

class Installation{

    private $migrationInit = ROOT_FOLDER.'init';

//     private $sysID;
    
    function __construct() 
    {
//         $this->sysID = new \App\Core\SystemIdentificator();
    }
    
    function runInstallation($db_name, $username, $password, $title) 
    {
        ini_set('max_execution_time', 480);  
        
        if(empty($_REQUEST['activation_key']) || empty($_REQUEST['product']))
        {
            die('<h2>Invalid Activation Key</h2>');
        }
        
        $activation = trim($_REQUEST['activation_key']);
        
        $product = trim($_REQUEST['product']);

//         $plan = trim($_REQUEST['plan']);

//         $pin = trim($_REQUEST['pin']);
        
        $activation = str_replace(' ', '', $activation);
        
        if($activation !== 'MASTER' && $this->hashValue($product) !== $activation)
        {
            die('<h2>Invalid Activation Key</h2>');
        }
        
//         $subscriptionKey = $this->getSubscriptionKey($this->sysID->getId(), $plan, $pin);

        try {
            $dbh = new PDO("mysql:host=localhost", $username, $password);
            
            $dbh->exec("CREATE DATABASE IF NOT EXISTS $db_name DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci")
                or die(print_r($dbh->errorInfo(), true));
                    
            $this->createInstallationFile($db_name, $username, $password, $title /*, $subscriptionKey */);
            
            $this->createHtaccessFile();
//             die("Installed, Now heading to {$this->migrationInit}");
            header("Location: {$this->migrationInit}");
            
        } catch (PDOException $e) {
            die("DB ERROR: ". $e->getMessage());
        }
    }
    
    private function createInstallationFile($db_name, $username, $password, $title /*, $subscriptionKey */) 
    {
        $str = "<?php ".PHP_EOL.PHP_EOL."/* Installation constants */".PHP_EOL
            .PHP_EOL."define('DB_HOSTNAME', 'localhost');"
            .PHP_EOL."define('DB_NAME', '$db_name');"
            .PHP_EOL."define('DB_USERNAME', '$username');"
            .PHP_EOL."define('DB_PASSWORD', '$password');"
//             .PHP_EOL."define('SUBSCRIPTION_KEY', '$subscriptionKey');"
            .PHP_EOL."define('ROOT_FOLDER', '".ROOT_FOLDER."');"
            .PHP_EOL."define('SITE_TITLE', '$title');"
            .PHP_EOL.PHP_EOL."define('DEV', false);"
            .PHP_EOL.PHP_EOL."define('ADDR', ROOT_FOLDER);"
            .PHP_EOL.PHP_EOL."define('USE_MINIFIED', false);"
            ;
        
        $filename = APP_DIR.'../config/inst.php';
        
        file_put_contents($filename, $str);
    }
    
    private function createHtaccessFile() {
        $tab = '    ';
        $str = "<IfModule mod_rewrite.c>"            
            
            .PHP_EOL.PHP_EOL.$tab."<IfModule mod_negotiation.c>"
            .PHP_EOL.$tab."Options -MultiViews"
            .PHP_EOL.$tab."</IfModule>"
            .PHP_EOL
            .PHP_EOL.$tab."RewriteEngine On"
            .PHP_EOL
            .PHP_EOL.$tab."# Redirect Authorization headers"
            .PHP_EOL.$tab."SetEnvIf Authorization \"(.*)\" HTTP_AUTHORIZATION=$1"
            .PHP_EOL
            .PHP_EOL.$tab."RewriteBase ".ROOT_FOLDER.' '
            .PHP_EOL.$tab."# Removes  trailing slashes"
            .PHP_EOL.$tab."RewriteRule ^(.*)/$ $1 [L,R=301]"
            .PHP_EOL
            .PHP_EOL.$tab."# Redirect Trailing Slashes If Not A Folder..."
            .PHP_EOL.$tab."RewriteCond %{REQUEST_FILENAME} !-d"
            .PHP_EOL.$tab."RewriteRule ^(.*)/$ /$1 [L,R=301]"
            .PHP_EOL
            .PHP_EOL.$tab."# Handle Front Controller..."
            .PHP_EOL.$tab."RewriteCond %{REQUEST_FILENAME} !-d"
            .PHP_EOL.$tab."RewriteCond %{REQUEST_FILENAME} !-f"
            .PHP_EOL.$tab."RewriteRule ^ index.php [L]"
            
            .PHP_EOL.PHP_EOL."</IfModule>"
            ;
        
        $filename = APP_DIR.'../.htaccess';
        
        file_put_contents($filename, $str);
    }
    
    function generateCode() 
    {
        require_once APP_DIR.'Core/CodeGenerator.php';
        
        $codeGen = new CodeGenerator();
        
        $code = $codeGen->generateCodes(12);
    
        return $code;
    }
    
    function hashValue($code) 
    {
        if(strlen($code) != 12) return null;
        
        $arr = [
            substr($code, 0, 3),
            substr($code, 3, 3),
            substr($code, 6, 3),
            substr($code, 9, 3),
        ];
        
        $newCode = $arr[2].$arr[0].$arr[3].$arr[1];
        
        $newCode = 'EX-'.strrev($newCode);
        
        return $newCode;
    }
    
}




